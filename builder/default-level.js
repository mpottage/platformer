import {getJSON, dataDir, listingFile} from '../src/levels/data.js';
import {loadSavedObject} from '../src/levels/parse.js';

document.addEventListener("DOMContentLoaded", function() {
    var match = window.location.hash.match(/l=(.*)/);
    var hash = (match && match[1]) || "";
    getJSON(listingFile).then(function(listing) {;
        if(!listing)
            document.body.classList.add("fail");
        else {
            var split = false;
            for(let entry of listing) {
                if(entry["id"]==hash) {
                    console.log("split");
                    split = true;
                    getJSON(dataDir+"/"+entry["source"]).then(function(res) {
                        if(!res)
                            document.body.classList.add("fail");
                        else {
                            try {
                                //Check that we have a valid level
                                loadSavedObject(res);
                                sessionStorage["builderLevel"] = JSON.stringify(res);
                                sessionStorage["builder/filename"] = "";
                                window.location.replace("./");
                            }
                            catch(e) {
                                console.log(e);
                                document.body.classList.add("fail");
                            }
                        }
                    });
                    break;
                }
            }
            if(!split) {
                document.body.classList.add("fail");
            }
        }
    });
});
