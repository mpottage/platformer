//Copyright Matthew Pottage 2016-2019
//Specialised version of SingleGame for use in the level builder.
import {BuilderDraw, builder_registerAll,
    resizeGameContainer} from './builder.js'
import {getCleanLevel} from '../src/builder/level.js';
import {FreeCamera as BuilderCamera} from '../src/builder/free-camera.js';
import {StartPositions} from '../src/core/level.js';
import {Box} from '../src/core/utility.js';
import {SingleGame} from '../single/single.js'
import {resizeCanvas} from '../src/core/resize-canvas.js';
import {Environment as BuilderEnvironment} from '../src/builder/mods/runtime.js';
import {getFlagsAndControls, getPortals} from '../src/builder/connections.js';
"use strict"
class BuilderGame extends SingleGame {
    constructor() {
        super();
        this.environment = new BuilderEnvironment();
        this.builderDraw = new BuilderDraw();
        this._freeze = false;
    }
    //Override default game running state. Prevents running being reset normally
    //when the game is.
    get forceFrozen() {
        return this._freeze;
    }
    set forceFrozen(val) {
        this._freeze = val;
        this.running = !this._freeze;
    }
    updateConnections() {
        this.builderDraw.setFlagDetails(getFlagsAndControls(this.level.objs));
        this.builderDraw.setPipeDetails(getPortals(this.level.objs));
    }
    reset() {
        super.reset();
        this.running = this.running && !this._freeze; //Restore frozen state.
        //Teleport immediately to start due to start position (if necessary)
        //(accomodate for when the game is frozen).
        this.level.objs.forEach(function(o) {
            if(o instanceof StartPositions)
                o.update(0, this.collisions, this.manager, this.caches);
        }, this);
        this.manager.players.forEach(p => p.teleporter.update(p, this.collisions));
        this.cameraBox = new Box(-Infinity, -Infinity, 0, 0);
    }
    //Switch objects on when the free camera gets near them
    shutdownTest(obj) {
        return super.shutdownTest(obj)
            && (!(this.camera instanceof BuilderCamera)
                || !Box.isIntersection(obj.box, this.cameraBox));
    }
    draw(interval) {
        this.context.save();
        super.draw(interval);
        if(this.builderDraw.willDraw(this.environment))
            this.builderDraw.draw(this, this.context, this.environment);
        this.context.restore();
    }
    update(interval) {
        if(this.level.suspendingOptimization) {
            let w = this.canvas.width/this.scale/this.camera.scale/this.level.scale;
            let h = this.canvas.height/this.scale/this.camera.scale/this.level.scale;
            //Used so that when moving the view around using "Free Camera" all
            //the objects seen will not be suspended (multiplier for values
            //chosen based on experimentation).
            this.cameraBox = new Box(-this.camera.offsets.x-w*.25,
                -this.camera.offsets.y-h*.5, 1.5*w, 2*h);
        }
        super.update(interval);
    }
    //Keep to the level being edited.
    getNextLevel(prev) {
        return this.allLevels["builder"];
    }
    loadLevels() {
        this.allLevels["builder"] = getCleanLevel();
        return Promise.resolve();
    }
    event_registerAll() {
        super.event_registerAll();
        builder_registerAll(this);
    }
    event_registerResize() {
        window.addEventListener("resize", resizeGameContainer.bind(null, this));
        window.addEventListener("load", resizeGameContainer.bind(null, this));
    }
}

//Start the game.
var globalGame = new BuilderGame();
document.addEventListener("DOMContentLoaded", function() { globalGame.start(); });
