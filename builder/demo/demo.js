//Copyright Matthew Pottage 2018
"use strict"
//Setup a more elaborate level than the default loaded in the level builder.
window.addEventListener("DOMContentLoaded", function() {
    sessionStorage["builderLevel"] = "new Level(9905,1200,[ new JumpThroughPlatform(new Box(-1,-5000,1,15600),\"#000\"), new Platform(new Box(9905,-5000,1,15600),\"#000\"), new Platform(new Box(0,1280,9905,1),\"transparent\"), new StartPositions([{\"x\":10,\"y\":10}]), new Platform(new Box(520,735,615,465),\"#b69a52\"), new Platform(new Box(1125,695,1845,505),\"#b69a52\"), new Platform(new Box(2965,760,3630,440),\"#b69a52\"), new Platform(new Box(4035,660,1315,110),\"#b69a52\"), new Platform(new Box(6575,725,3330,475),\"#b69a52\"), new Platform(new Box(7835,660,1405,75),\"#b69a52\"), new Platform(new Box(0,805,525,395),\"#b69a52\"), new WinRegion(new Box(9855,0,50,900)), new Platform(new Box(1640,580,435,15),\"#b69a52\"), new Platform(new Box(2245,495,350,15),\"#0017bd\"), new Platform(new Box(3190,495,350,15),\"#b69a52\"), new WoodenBox(3410,430,new NothingPickup(3440,447.5)), new Platform(new Box(5535,570,325,15),\"#b69a52\"), new Platform(new Box(5155,455,285,15),\"#b69a52\"), new Platform(new Box(6065,490,285,15),\"#b69a52\"), new Platform(new Box(6645,595,140,15),\"#000000\"), new Platform(new Box(6915,245,270,15),\"#b69a52\"), new TenCoin(7026,146), ],1,true);";
    sessionStorage["builder/filename"] = "";
    window.location = "../";
});
