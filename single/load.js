//Copyright Matthew Pottage 2015-2018. Run a single player game.
import {SingleGame, formatTime} from './single.js'
import {recoverLevelName} from '../src/levels/random.js';
import {getJSON, dataDir} from '../src/levels/data.js';
class SinglePlay extends SingleGame {
    constructor() {
        super();
        this.hashLevelId = ""; //Has the level to play been set by the URL hash?
        this.editLevelLink = null;
        this.lockLevelButton = null
        this.lockLevelButtonEnd = null
    }
    start() {
        this.hashLevelId = getLevelFromHash();
        //Lock Level button events are in the game class as they depend on
        //code related to this.hashLevel.
        this.lockLevelButton = document.querySelector("#lockLevel");
        this.lockLevelButtonEnd = document.querySelector("#lockLevelEnd");
        [this.lockLevelButton, this.lockLevelButtonEnd].forEach(
            btn => btn.addEventListener("click", (function(e) {
                e.target.blur();
                this.toggleLevelLocked();
        }).bind(this)));
        this.editLevelLink = document.querySelector("#editLevel");
        return getJSON(dataDir+"/listing-name-mapping.json").then((function(res) {
            if(res)
                this._upgradeLevelNames(res);
        }).bind(this)).then(() => super.start().then((function() {
            this._updateLockLevelButton();
        }).bind(this)));
    }
    reset() {
        super.reset();
        this.editLevelLink.href = this.editLevelLink.href.split("#")[0]+"#l="+
            recoverLevelName(this.allLevels, this.level);
    }
    _updateLockLevelButton() {
        var lockedText = "Unlock Level";
        var unlockedText = "Lock Level";
        if(this.allLevels[this.hashLevelId]) {
            this.lockLevelButton.title = lockedText;
            this.lockLevelButton.innerHTML = "<span>"+lockedText+"</span>";
            this.lockLevelButtonEnd.innerHTML = this.lockLevelButton.innerHTML;
            document.body.classList.add("levelLocked");
        }
        else {
            this.lockLevelButton.title = unlockedText;
            this.lockLevelButton.innerHTML = "<span>"+unlockedText+"</span>";
            this.lockLevelButtonEnd.innerHTML = this.lockLevelButton.innerHTML;
            document.body.classList.remove("levelLocked");
        }
    }
    toggleLevelLocked() {
        if(this.allLevels[this.hashLevelId])
            window.location.hash = "";
        else
            window.location.hash = "l="+recoverLevelName(this.allLevels, game.level);
    }
    _upgradeLevelNames(mapping) {
        if(localStorage["single/level-name-version"]!=mapping["version"]) {
            localStorage["single/level-name-version"] = mapping["version"];
            for(let map of mapping["swap"]) {
                let timingName = "single/level/"+map["old"];
                let newTiming = "single/level/"+map["new"];
                if(localStorage[timingName] && !localStorage[newTiming])
                    localStorage[newTiming] = localStorage[timingName];
                let coinsName = "single/level/"+map["old"]+"/coins";
                let newCoins = "single/level/"+map["new"]+"/coins";
                if(localStorage[coinsName] && !localStorage[newCoins])
                    localStorage[newCoins] = localStorage[coinsName];
            }
        }
    }
    _bestCompletionTimes(levelName) {
        //Would be better if the name stated that it stored the time
        var name = "single/level/"+levelName;
        var best = this.timeElapsed;
        if(localStorage[name])
            best = Math.min(best, parseFloat(localStorage[name]));
        localStorage[name] = best;
        var bestCont = document.getElementById("bestTime");
        if(bestCont) {
            bestCont.innerHTML = formatTime(best);
            if(best==this.timeElapsed)
                document.body.classList.add("best-time");
            else
                document.body.classList.remove("best-time");
        }
    }
    _mostCoins(levelName) {
        var name = "single/level/"+levelName+"/coins";
        var most = this.player.coins;
        if(localStorage[name])
            most = Math.max(most, parseInt(localStorage[name]));
        localStorage[name] = most;
        var mostCont = document.getElementById("mostCoins");
        if(mostCont) {
            mostCont.innerHTML = most;
            if(most==this.player.coins)
                document.body.classList.add("most-coins");
            else
                document.body.classList.remove("most-coins");
        }
    }
    applyWinning() {
        super.applyWinning();
        var levelName = recoverLevelName(this.allLevels, game.level);
        if(this.player.passedLevel) {
            this._bestCompletionTimes(levelName);
        }
        this._mostCoins(levelName);
    }
    event_registerAll() {
        super.event_registerAll();
        this.event_registerHashChange();
    }
    //Set level from hash.
    event_registerHashChange() {
        window.addEventListener("hashchange", (function() {
            this.hashLevelId = getLevelFromHash();
            if(this.allLevels[this.hashLevelId]) {
                if(this.allLevels[this.hashLevelId]!==this.level)
                    this.reset();
                this._updateLockLevelButton();
            }
            else {
                this._updateLockLevelButton();
            }
        }).bind(this));
    }
    event_registerKeyboard() {
        super.event_registerKeyboard();
        window.addEventListener("keydown", (function(e) {
            if(!e.ctrlKey && !e.metaKey && !e.altKey
                && e.key=="l") {
                this.toggleLevelLocked();
                e.preventDefault();
            }
        }).bind(this));
    }
    //Get the next level to play.
    getNextLevel(prevLevel) {
        if(this.allLevels[this.hashLevelId]) //Forced to play this level.
            return this.allLevels[this.hashLevelId];
        else
            return super.getNextLevel(prevLevel);
    }
}

function getLevelFromHash() {
    //Pick different level if selected via hash, returns true if one is set.
    //Format of hash is "#l=[level name]"
    var match = window.location.hash.match(/l=(.*)/);
    if(match)
        return match[1];
    return "";
}

var game = new SinglePlay();
document.addEventListener("DOMContentLoaded", function() {
    game.start();
    var earlyRestart = document.querySelector("#earlyRestart");
    earlyRestart.addEventListener("click", function(e) {
        e.target.blur();
        game.reset()
    });
});
