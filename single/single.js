//Copyright Matthew Pottage 2015-2019.
//Creates and manages a singleplayer platformer (see single-load.js to run it).
import {loadLevels} from '../src/levels/data.js';
import {getRandomLevel} from '../src/levels/random.js';
import {ObjectManager, TextureManager, RenderCacheManager, Environment} from '../src/core/runtime.js';
import {CollisionsEngine} from '../src/core/collisions/engine.js';
import {Camera} from '../src/core/camera.js';
import {Player} from '../src/core/player/player.js';
import {neededTextures, screenHeight} from '../src/core/utility.js';
import {resizeCanvas} from '../src/core/resize-canvas.js';
import {playerWidth, playerHeight} from '../src/single/constants.js';
export class SingleGame {
    constructor() {
        this.textures = new TextureManager("../textures");
        this.caches = new RenderCacheManager();
        this.manager = new ObjectManager();
        this.collisions = new CollisionsEngine();
        this.environment = new Environment();
        this.allLevels = {};
        this.level = null;
        this.camera = new Camera(this.level);
        this.player = null;
        this.context = null;
        this.canvas = null;
        this.scale = 1; //Scaling applied to make the canvas fit the screen.
        this.running = true; //Has the game not ended (e.g. display win/lose message).
        this.prevTimestamp = 0; //Used to find difference in time between frames.
        this.progress = null; //Progress bar (% run through level).
        this.restartButton = null;
        //Used to load visual textures
        this.loadingBar = null;
        this.loadingMoreTextures = false;
        //Health/Coins onscreen containers for the player.
        this.healthContainer = null;
        this.coinsContainer = null;
        this.visibilityLoss = false;
        //Timers
        this.timer = null;
        this.timeElapsed = 0;
        //Messages
        this.messageContainer = null;
        //Distance from player for objects to be shutdown in this.collisions.
        this.minMovementLimitX = 4000;
        this.movementLimitX = this.minMovementLimitX;
        this.minMovementLimitY = screenHeight*2;
        this.movementLimitY = this.minMovementLimitY;
    }
    //Assumes there are textures to load. Don't call if there aren't any as will
    //get a loading bar flicker.
    _loadRemainingTextures(finishedCallback=function(){}) {
        this.loadingMoreTextures = true;
        document.body.classList.remove("got-resources");
        document.body.classList.add("loading-textures");
        this.textures.load((function(loaded, total) {
            //Cause loading bar to progress.
            this.loadingBar.value = (loaded/total).toString();
        }).bind(this)).then((function() {
            document.body.classList.remove("loading-textures");
            //Show the game again.
            document.body.classList.add("got-resources");
            this.loadingMoreTextures = false;
            finishedCallback();
            window.requestAnimationFrame(this.main.bind(this));
        }).bind(this));
    }
    //Resets game, without page reload.
    reset() {
        //Reset any level-lost/level-won/messages states.
        document.body.classList.remove("level-lost");
        document.body.classList.remove("level-won");
        document.body.classList.remove("show-message");
        this.running = true;
        this.timeElapsed = 0;
        this._displayTimeElapsed();
        //New player
        this.player = new Player(playerWidth, playerHeight, "#00b0ff");
        //Set player to the start of the level, avoiding level edging.
        this.player.box.y = 10;
        this.player.box.x = 10;
        //Reset all levels.
        for(var key in this.allLevels)
            this.allLevels[key].reset();
        this.level = this.getNextLevel(this.level);
        this.textures.removeAll();
        this.caches.purgeMissingTextures(this.textures);
        this.manager.clear();
        this.collisions.clear();
        this.environment.clear();
        this.movementLimitX = this.minMovementLimitX;
        this.movementLimitY = this.minMovementLimitY;
        if(this.level.suspendingOptimization)
            this.collisions.setShutdown(this.shutdownTest.bind(this));
        this.level.register(this.collisions, this.manager, this.caches, this.environment);
        this.player.register(this.collisions, this.manager, this.caches);
        this.collisions.autoProtect();
        this.camera.changeLevel(this.level);
        this.environment.apply(this.level);
        //Get any level unique textures
        this.level.addTextures(this.textures);
        //Switch out of game mode to get the extra textures
        //(this will stop the main loop and restart it when ready)
        if(this.textures.anyPending())
            this._loadRemainingTextures(this._cacheRefresh.bind(this));
        //Finish normally
        else
            this._cacheRefresh();
    }
    _cacheRefresh() {
        this.caches.refresh(this.scale*this.level.scale, this.textures);
    }
    shutdownTest(obj) {
        var toBox = this.player.box;
        var dx = Math.abs(obj.box.x+obj.box.width/2-toBox.x-toBox.width/2);
        var dy = Math.abs(obj.box.y+obj.box.height/2-toBox.y-toBox.height/2);
        return dx>this.movementLimitX || dy>this.movementLimitY;
    }
    setupEndLevel() {
        this.restartButton.disabled = false; //Button is disabled when not in use.
        this.restartButton.focus();
        document.getElementById("endCoins").innerHTML = this.player.coins;
        document.getElementById("endHealth").innerHTML = this.player.health.value;
    }
    _displayTimeElapsed() {
        this.timer.innerHTML = formatTime(this.timeElapsed);
    }
    _setCompletionTimes() {
        this.timeElapsed = Math.round(this.timeElapsed*100)/100;
        var currentCont = document.getElementById("currentTime");
        if(currentCont)
            currentCont.innerHTML = formatTime(this.timeElapsed);
    }
    applyWinning() {
        //Handle winning/losing.
        if(this.player.disabled) { //Player died, display lost message.
            document.body.classList.add("level-lost");
            this.setupEndLevel();
            this._setCompletionTimes();
            this.running = false;
        }
        else if(this.player.passedLevel) { //Player passed, display win message.
            document.body.classList.add("level-won");
            this.setupEndLevel();
            this._setCompletionTimes();
            this.running = false;
        }
    }
    draw(interval) {
        this.context.setTransform(1, 0, 0, 1, 0, 0);
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        //Move camera
        this.camera.update(this.player, this.canvas.width/this.scale,
                this.canvas.height/this.scale, interval);
        //Draw level and player (including when the game has paused/finished).
        this.context.scale(this.scale, this.scale); //Scale the game to fit the canvas.
        this.camera.apply(this.context);
        this.manager.draw(this.context, this.caches, this.environment);
    }
    _showMessage() {
        if(this.player.message.length>0) {
            this.messageContainer.innerHTML =
                //Disable any HTML special characters or tags
                this.player.message.replace(/&/g, "&amp;")
                                   .replace(/</g, "&lt;")
                                   .replace(/>/g, "&gt;");
            document.body.classList.add("show-message");
            this.player.message = "";
        }
        else {
            document.body.classList.remove("show-message");
        }
    }
    update(interval) {
        this.timeElapsed += interval;
        this.applyWinning();
        //Move player, collect coins, lose health, etc.
        this.manager.update(interval, this.collisions, this.caches, this.environment);
        this.collisions.update();
        this._displayTimeElapsed();
        this._showMessage();
        this.healthContainer.innerHTML = this.player.health.value.toString();
        this.coinsContainer.innerHTML = this.player.coins.toString();
    }
    //Main loop
    main(timestamp) {
        //Got to wait for the textures to be ready
        if(this.loadingMoreTextures)
            return;
        //Focus was lost, ignore the change in time since last running main().
        //  This is equivalent to stopping the game clock when focus is lost.
        if(this.visibilityLoss) {
            this.prevTimestamp = timestamp;
            this.visibilityLoss = false;
        }
        var interval = (timestamp-this.prevTimestamp)/1000;
        if(this.level.suspendingOptimization) {
            this.movementLimitX = Math.max(this.minMovementLimitX,
                Math.ceil(this.canvas.width/this.scale/this.level.scale*2));
            this.movementLimitY = Math.max(this.minMovementLimitY, screenHeight*2/this.level.scale);
        }
        if(this.running)
            this.update(interval);
        this.caches.interimRefresh(this.scale*this.level.scale, this.textures);
        this.draw(interval);
        this.progress.value = this.player.box.x/this.level.width;
        this.prevTimestamp = timestamp; //Record timestamp.
        window.requestAnimationFrame(this.main.bind(this));
    }
    //Starts the game (call after/when the HTML page has loaded).
    start() {
        this.canvas = document.getElementById("platformer");
        this.context = this.canvas.getContext("2d");
        this.progress = document.querySelector("#game-container progress");
        this.restartButton = document.getElementById("restart");
        this.healthContainer = document.getElementById("health");
        this.coinsContainer = document.getElementById("coins");
        this.timer = document.getElementById("timer");
        this.messageContainer = document.getElementById("message");
        this.restartButton.addEventListener("click", (function(e) {
            this.reset(); //Restart the game.
            e.target.disabled = true;
            e.target.blur();
        }).bind(this), false);
        //Sets initial timestamp.
        window.requestAnimationFrame((function(stamp) {
            this.prevTimestamp = stamp;
        }).bind(this));
        //Load image textures.
        this.textures.add(neededTextures);
        this.loadingBar = document.getElementById("loading-progress");
        document.body.classList.add("loading-textures");
        return this.textures.load((function(loaded, total) {
            //Cause loading bar to progress.
            this.loadingBar.value = (loaded/total).toString();
        }).bind(this)).then((function() {
            this.loadingBar.value = "0";
            document.body.classList.remove("loading-textures");
            document.body.classList.add("loading-levels");
            return this.loadLevels();
        }).bind(this)).then((function() {
            document.body.classList.remove("loading-levels");
            document.body.classList.add("got-resources");
            //Finish starting the game.
            this.textures.protect();
            this.scale = resizeCanvas(this.canvas);
            //Get current time, after everything is loaded
            this.prevTimestamp = performance.now();
            //Start clean game.
            this.reset();
            window.requestAnimationFrame(this.main.bind(this));
            //Register controls (now applicable).
            this.event_registerAll();
        }).bind(this));
    }
    //Register all controls that require the game to be set up first (note
    //event_ prefix).
    event_registerVisibility() {
        //Note that focus is lost.
        //  Also stop any player actions (e.g. left/right/jump).
        document.addEventListener("visibilitychange", (function() {
            if(document.hidden) {
                this.player.action.left = false;
                this.player.action.right = false;
                this.player.action.jump = false;
            }
            else
                this.visibilityLoss = true; //Avoid time jump.
        }).bind(this));
    }
    event_registerResize() {
        window.addEventListener("resize", (function() {
            this.scale = resizeCanvas(this.canvas);
            //After the textures are loaded the caches will be refreshed anyway
            if(!this.loadingMoreTextures)
                this.caches.refresh(this.scale*this.level.scale, this.textures);
        }).bind(this));
    }
    event_registerTouch() {
        var self = this;
        //Touch controls
        if("ontouchstart" in document) {
            document.body.classList.add("touch");
            var right = document.getElementById("right");
            var left = document.getElementById("left");
            var jump = document.getElementById("jump");
            var throwStone = document.getElementById("throwStone");
            var useSpecial = document.getElementById("useSpecial");
            right.addEventListener("touchstart", function(e) {
                e.preventDefault();
                self.player.action.right = true;
            }, false);
            right.addEventListener("touchend", function(e) {
                e.preventDefault();
                self.player.action.right = false;
            }, false);
            left.addEventListener("touchstart", function(e) {
                e.preventDefault();
                self.player.action.left = true;
            }, false);
            left.addEventListener("touchend", function(e) {
                e.preventDefault();
                self.player.action.left = false;
            }, false);
            jump.addEventListener("touchstart", function(e) {
                e.preventDefault();
                self.player.action.jump = true;
            }, false);
            jump.addEventListener("touchend", function(e) {
                e.preventDefault();
                self.player.action.jump = false;
            }, false);
            throwStone.addEventListener("touchstart", function(e) {
                e.preventDefault();
                self.player.action.throwStone = true;
            }, false);
            throwStone.addEventListener("touchend", function(e) {
                e.preventDefault();
                self.player.action.throwStone = false;
            }, false);
            useSpecial.addEventListener("touchstart", function(e) {
                e.preventDefault();
                self.player.action.useSpecial = true;
            }, false);
            useSpecial.addEventListener("touchend", function(e) {
                e.preventDefault();
                self.player.action.useSpecial = false;
            }, false);
        }
    }
    event_registerKeyboard() {
        var self = this;
        //Keyboard controls
        window.addEventListener("keydown", function(e) {
            if(!e.ctrlKey && !e.metaKey && !e.altKey) {
                var key = e.key || e.keyIdentifier;
                var keyUsed = true;
                if(key=="Left" || key=="ArrowLeft")
                    self.player.action.left = true;
                else if(key=="Right" || key=="ArrowRight")
                    self.player.action.right = true;
                else if(key=="Up" || key=="ArrowUp")
                    self.player.action.jump = true;
                else if(key=="=" || key=="U+00BB" || key=="U+003D")
                    self.reset();
                else if(key=="Space" || key==" " || key=="U+0020" || key=="Shift")
                    self.player.action.throwStone = true;
                else if(key=="/" || key=="U+00BF" || key=="s" || key=="U+0053")
                    self.player.action.useSpecial = true;
                else
                    keyUsed = false;
                if(keyUsed && self.running)
                    e.preventDefault();
            }
        }, false);
        window.addEventListener("keyup", function(e) {
            if(!e.ctrlKey && !e.metaKey && !e.altKey) {
                var key = e.key || e.keyIdentifier;
                if(key=="Left" || key=="ArrowLeft")
                    self.player.action.left = false;
                else if(key=="Right" || key=="ArrowRight")
                    self.player.action.right = false;
                else if(key=="Up" || key=="ArrowUp")
                    self.player.action.jump = false;
                else if(key=="Space" || key==" " || key=="U+0020" || key=="Shift")
                    self.player.action.throwStone = false;
                else if(key=="/" || key=="U+00BF" || key=="s" || key=="U+0053")
                    self.player.action.useSpecial = false;
            }
        }, false);
    }
    //Register all DOM event handlers.
    event_registerAll() {
        this.event_registerResize();
        this.event_registerTouch();
        this.event_registerKeyboard();
        this.event_registerVisibility();
    }
    //Get the next level to play.
    getNextLevel(prevLevel) {
        return getRandomLevel(this.allLevels, prevLevel);
    }
    loadLevels() {
        return loadLevels((function(loaded, total) {
            this.loadingBar.value = (loaded/total).toString();
        }).bind(this)).then((function(res) {
            this.allLevels = res;
        }).bind(this));
    }
}

export function formatTime(time) {
    var str = (Math.round(time*100)/100).toString();
    var dotI = str.lastIndexOf('.');
    if(dotI==-1)
        str += ".00";
    else if((str.length-dotI-1)<2)
        str += "0";
    return str;
}
