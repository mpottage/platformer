//Copyright Matthew Pottage 2016.
"use strict"
//Controls setting the colour theme for HTML pages.
//The current theme is saved in localStorage["theme"].
//The pages are coloured by CSS, which uses the theme class which is added to
// <body> (e.g. for "blue" the class "blue" is added to <body>).
//A click listener is added to any DOM objects with class "cycle-themes"
// to make clicking on it repeated cycle through setting all color themes.
document.addEventListener("DOMContentLoaded", function() {
    "use strict";
    var themes = ["blue", "grey", "pink", "classic", "dark"]; //Available themes.
    var classList = document.body.classList;
    function setTheme(color) {
        themes.forEach(t => classList.remove(t));
        classList.add(color);
        localStorage["theme"] = color;
    }
    function getDefault() {
        if(window.matchMedia("(prefers-color-scheme: dark)").matches)
            return "dark";
        else
            return "blue";
    }
    setTheme(localStorage["theme"] || getDefault());
    //Changed theme if changed on any other page.
    window.addEventListener("storage", function(e) {
        if(e.key=="theme")
            setTheme(e.newValue);
    });
    //Optional elements to use to cycle through color themes.
    var cycles = document.querySelectorAll(".cycle-themes");
    for(var i=0; i<cycles.length; ++i) {
        cycles[i].addEventListener("click", function(e) {
            e.preventDefault();
            var nextIndex = (themes.indexOf(localStorage["theme"])+1)%themes.length;
            setTheme(themes[nextIndex]);
        });
    }
    //Handle history forward/back and the theme having changed.
    window.addEventListener("pageshow", function() {
        setTheme(localStorage["theme"]);
    });
});
