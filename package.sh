#!/bin/sh

#Creates a ZIP containing all necessary files for Platform 2, with suitable
# preprocessing for deploying on a webserver (e.g. JavaScript minified).
# The result is placed in ./tmp/package.zip.

closure='java -jar ./tools/closure-compiler.jar -O ADVANCED --use_types_for_optimization false --language_out=ECMASCRIPT_2017 --jscomp_off checkVars'
csso='./tools/downloads/node_modules/csso/bin/csso'
core_js=$(find ./src/core -name \*.js)
single_js=$(find ./src/single -name \*.js)
builder_js=$(find ./src/builder -name \*.js)
#Clean up after any previous package by removing ./tmp directory.
rm -rf tmp
#Add textures
mkdir -p tmp/builder/demo tmp/multiplayer tmp/single
zip -r tmp/package textures graphics single/*.html single/graphics \
    builder/graphics builder/textures externals/*.min.js \
    builder/*.html builder/demo/*.html multiplayer/graphics \
    multiplayer/*.html *.html levels/data
#Minify JavaScript.
$closure $core_js $single_js single/single.js single/load.js \
    src/levels/*.js >tmp/single/load.js
$closure $core_js $single_js single/single.js builder/builder.js \
    $builder_js src/levels/*.js builder/game.js >tmp/builder/game.js
$closure builder/demo/demo.js >tmp/builder/demo/demo.js
$closure $core_js src/levels/*.js builder/default-level.js \
    >tmp/builder/default-level.js
$closure $core_js multiplayer/multiplayer.js multiplayer/load.js \
    src/levels/*.js >tmp/multiplayer/load.js
$closure $core_js multiplayer/multiplayer.js src/levels/*.js \
    multiplayer/custom.js >tmp/multiplayer/custom.js
$closure theme.js >tmp/theme.js
$closure multiplayer/index.js >tmp/multiplayer/index.js
#Minify CSS
find ./ -name "*.css" ! -path "./tmp/*" ! -path "./extra/*" \
    -exec $csso '{}' --output 'tmp/{}' \;
#Add to package.
cd tmp
zip package single/load.js builder/game.js builder/demo/demo.js \
    builder/default-level.js multiplayer/*.js theme.js *.css \
    single/*.css builder/*.css multiplayer/*.css
cd ../
