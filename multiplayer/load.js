import {MultiplayerGame} from './multiplayer.js'
var game = new MultiplayerGame();
document.addEventListener("DOMContentLoaded", function() {
    game.start();
});
