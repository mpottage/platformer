document.addEventListener("DOMContentLoaded", function() {
    //Get requested number of players (1-4) and save it so that multiplayer.js
    //can read it later.
    var count = document.getElementById("playerCount");
    var storage = "multiplayer/playerCount";
    count.value = localStorage[storage] = localStorage[storage] || 3;
    count.addEventListener("change", function() {
        var newVal = Math.round(parseFloat(count.value));
        //Valid value (1-4)
        if(!isNaN(newVal) && newVal>=1 && newVal<=4)
            localStorage[storage] = newVal;
        count.value = localStorage[storage];
    });
});
