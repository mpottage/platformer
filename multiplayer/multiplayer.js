//Copyright Matthew Pottage 2015-2020.
//Multiplayer version of Platform 2.
// Player 1 uses the arrow keys, player 2 uses WASD, 3 uses IJKL, and 4 uses
// TFGH to control their player.
import {loadLevels} from '../src/levels/data.js';
import {getRandomLevel, getRandomTagged} from '../src/levels/random.js';
import {ObjectManager, TextureManager, RenderCacheManager, Environment} from '../src/core/runtime.js';
import {CollisionsEngine} from '../src/core/collisions/engine.js';
import {Camera} from '../src/core/camera.js';
import {Player} from '../src/core/player/player.js';
import {neededTextures, screenHeight} from '../src/core/utility.js';
import {resizeCanvas} from '../src/core/resize-canvas.js';
import {Box} from '../src/core/utility.js';

//Describes the split-screen view for MultiplayerGame.draw to use
// Each entry is the layout from when there is i+1 players.
var canvasLayout = [
    [new Box(0, 0, 1, 1)],
    [new Box(0, 0, 1, .5), new Box(0, .5, 1, .5)],
    [new Box(.5, 0, .5, .5), new Box(0, 0, .5, .5),
       new Box(0, .5, 1, .5)],
    [new Box(.5, 0, .5, .5),new Box(0, 0, .5, .5),
        new Box(0, .5, .5, .5),new Box(.5, .5, .5, .5),]];
export class MultiplayerGame {
    constructor() {
        this.players = [];
        this.wins = []; //Stores wins for each player.
        this.expectedPlayerNo = 4;
        this.manager = new ObjectManager();
        this.collisions = new CollisionsEngine();
        this.textures = new TextureManager("../textures");
        this.caches = new RenderCacheManager();
        this.environment = new Environment();
        this.allLevels = {};
        this.level = null;
        this.cameras = [];
        this.context = null;
        this.canvas = null;
        this.prevTimestamp = 0; //Used to find difference in time between frames.
        this.scale = 1; //Scaling applied to make the canvas fit the screen.
        this.running = true; //Has the game not ended (e.g. display win/lose message).
        this.tag = "";
        //Used to load visual textures
        this.loadingBar = null;
        this.loadingMoreTextures = false;
        //Store references to Coin/Health page containers.
        this.coinsElems = []; //DOM ids are "player-[1-4]-coins"
        this.healthElems = []; //Ids "player-[1-4]-health".
        this.visibilityLoss = false;
        //Messages
        this.messageContainer = null;
        this.minMovementLimitX = 3000;
        this.minMovementLimitY = 2*screenHeight;
        this.movementLimitX = 0;
        this.movementLimitY = 0;
    }
    //Assumes there are textures to load. Don't call if there aren't any as will
    //get a loading bar flicker.
    _loadRemainingTextures(finishedCallback=function(){}) {
        this.loadingMoreTextures = true;
        document.body.classList.remove("got-resources");
        document.body.classList.add("loading-textures");
        this.textures.load((function(loaded, total) {
            //Cause loading bar to progress.
            this.loadingBar.value = (loaded/total).toString();
        }).bind(this)).then((function() {
            document.body.classList.remove("loading-textures");
            //Show the game again.
            document.body.classList.add("got-resources");
            this.loadingMoreTextures = false;
            finishedCallback();
            window.requestAnimationFrame(this.main.bind(this));
        }).bind(this));
    }
    //Resets game, without page reload.
    reset() {
        this.running = true;
        //Reset all levels.
        for(var key in this.allLevels)
            this.allLevels[key].reset();
        this.level = this.getNextLevel(this.level);
        this.textures.removeAll();
        this.caches.purgeMissingTextures(this.textures);
        this.collisions.clear();
        this.manager.clear();
        this.environment.clear();
        this.level.register(this.collisions, this.manager, this.caches);
        this.cameras = [];
        var oldCoins = this.players.map(function(p) { return p.coins; });
        //New players
        this.players = [
            new Player(11, 90, "#00b0ff"),
            new Player(11, 90, "#66ff00"),
            new Player(11, 90, "#ffcc00"),
            new Player(11, 90, "#2b4d4d"),
        ];
        this.players = this.players.slice(0, this.expectedPlayerNo);
        this.players.forEach(function(p, i) {
            //Carry coins across levels
            if(oldCoins[i]>0)
                p.increaseCoins(oldCoins[i]);
            //Set players to the start of the level, avoiding level edging.
            p.box.y = 10;
            p.box.x = 10 + i*20;
            p.register(this.collisions, this.manager);
            this.cameras.push(new Camera(this.level));
        }, this);
        this.movementLimitX = this.minMovementLimitX;
        this.movementLimitY = this.minMovementLimitY;
        if(this.level.suspendingOptimization)
            this.collisions.setShutdown(this.shutdownTest.bind(this));
        this.collisions.autoProtect();
        while(this.wins.length<this.players.length)
            this.wins.push(0);
        this.environment.apply(this.level);
        //Get any level unique textures
        this.level.addTextures(this.textures);
        if(this.textures.anyPending())
            this._loadRemainingTextures(this._cacheRefresh.bind(this));
        //Finish normally
        else
            this._cacheRefresh();
    }
    _cacheRefresh() {
        //The height of each section of canvasLayout for a given number of
        //players is the same.
        this.caches.refresh(
                this.scale*this.level.scale*canvasLayout[this.players.length-1][0].height,
                this.textures);
    }
    shutdownTest(obj) {
        var dx = Math.min.apply(null, this.players.map(
            p => Math.abs(obj.box.x+obj.box.width/2-p.box.x-p.box.width/2)
        ));
        var dy = Math.min.apply(null, this.players.map(
            p => Math.abs(obj.box.y+obj.box.height/2-p.box.y-p.box.height/2)
        ));
        return dx>this.movementLimitX || dy>this.movementLimitY;
    }
    getNextLevel(prevLevel) {
        if(this.tag.length>0)
            return getRandomTagged(this.allLevels, prevLevel, this.tag);
        else
            return getRandomLevel(this.allLevels, prevLevel);
    }
    loadLevels() {
        return loadLevels((function(loaded, total) {
            this.loadingBar.value = (loaded/total).toString();
        }).bind(this)).then((function(res) {
            this.allLevels = res;
        }).bind(this));
    }
    addWinOn(player) {
        var i = this.players.indexOf(player)+1;
        ++this.wins[i-1];
        document.getElementById("player-"+i+"-wins").innerHTML = this.wins[i-1];
        for(var j=1; j<=this.players.length; ++j)
            document.body.classList.remove("player-"+j+"-won-last");
        document.body.classList.add("player-"+i+"-won-last");
        this.reset();
    }
    applyWinning() {
        var alivePlayers = this.players.filter(function(p) { return p.health.value>0; });
        if(alivePlayers.length==0)
            this.reset();
        else if(alivePlayers.length==1 && this.players.length>1) {
            this.addWinOn(alivePlayers[0]);
        }
        else {
            for(var i=0; i<alivePlayers.length; ++i)
                if(alivePlayers[i].passedLevel) {
                    this.addWinOn(alivePlayers[i]);
                    break;
                }
        }
    }
    _showMessages() {
        var withMessage = this.players.find(p => p.message.length!=0);
        if(withMessage) {
            this.messageContainer.innerHTML = withMessage.message;
            document.body.classList.add("show-message");
            withMessage.message = "";
        }
        else {
            document.body.classList.remove("show-message");
        }
    }
    update(interval) {
        this.applyWinning();
        //Move player, collect coins, lose health, etc.
        this.manager.update(interval, this.collisions, this.caches, this.environment);
        this.collisions.update(); //Move all objects, resolving collisions.
        this.players.forEach(function(p, i) {
            this.coinsElems[i].innerHTML = p.coins.toString();
            this.healthElems[i].innerHTML = p.health.value.toString();
        }, this);
        this._showMessages();
    }
    draw(interval) {
        //Draws a 4-screen view, one player's view in each quadrant.
        var layout = canvasLayout[this.players.length-1];
        for(var i=0; i<this.players.length; ++i) {
            this.context.save();
            //Restrict player to one corner of the canvas.
            this.context.beginPath();
            this.context.rect(layout[i].x*this.canvas.width,
                    layout[i].y*this.canvas.height,
                    layout[i].width*this.canvas.width,
                    layout[i].height*this.canvas.height);
            this.context.translate(layout[i].x*this.canvas.width,
                    layout[i].y*this.canvas.height);
            this.context.clip();
            var viewScale = this.scale*layout[i].height;
            this.context.scale(viewScale, viewScale); //Scale the game to fit the canvas.
            var screenWidth = this.canvas.width*layout[i].width/viewScale;
            var screenHeight = this.canvas.height*layout[i].height/viewScale;
            //Normal draw:
            this.cameras[i].update(this.players[i], screenWidth, screenHeight, interval);
            this.cameras[i].apply(this.context);
            this.manager.draw(this.context, this.caches, this.environment);
            this.context.restore();
            this.context.beginPath();
            this.context.rect(layout[i].x*this.canvas.width,
                    layout[i].y*this.canvas.height,
                    layout[i].width*this.canvas.width,
                    layout[i].height*this.canvas.height);
            this.context.stroke();
        }
    }
    //Main loop
    main(timestamp) {
        //Got to wait for the textures to be ready
        if(this.loadingMoreTextures)
            return;
        //Focus was lost, ignore the change in time since last running main().
        //  This is equivalent to stopping the game clock when focus is lost.
        if(this.visibilityLoss) {
            this.prevTimestamp = timestamp;
            this.visibilityLoss = false;
        }
        var interval = (timestamp-this.prevTimestamp)/1000;
        this.canvas.width = this.canvas.width; //Clears canvas.
        if(this.level.suspendingOptimization) {
            this.movementLimitX = Math.max(3000,
                Math.ceil(this.canvas.width/this.scale/this.level.scale * 
                    Math.max.apply(null,
                       canvasLayout[this.players.length-1].map(b => b.width))));
            this.movementLimitY = Math.max(this.minMovementLimitY, screenHeight*2/this.level.scale);
        }
        if(this.running)
            this.update(interval);
        this.caches.interimRefresh(this.scale*this.level.scale, this.textures);
        this.draw(interval);
        this.prevTimestamp = timestamp; //Record timestamp.
        window.requestAnimationFrame(this.main.bind(this));
    }
    start() {
        this.canvas = document.getElementById("platformer");
        this.context = this.canvas.getContext("2d");
        for(var i=1; i<=4; ++i) {
            this.coinsElems.push(document.getElementById("player-"+i+"-coins"));
            this.healthElems.push(document.getElementById("player-"+i+"-health"));
        }
        this.messageContainer = document.getElementById("message");
        //Sets initial timestamp.
        window.requestAnimationFrame((function(stamp) {
            this.prevTimestamp = stamp;
        }).bind(this));
        //Load image textures.
        this.textures.add(neededTextures);
        this.loadingBar = document.getElementById("loading-progress");
        document.body.classList.add("loading-textures");
        this.textures.load((function(loaded, total) {
            //Cause loading bar to progress.
            this.loadingBar.value = (loaded/total).toString();
        }).bind(this))
        .then((function() {
            this.loadingBar.value = "0";
            document.body.classList.remove("loading-textures");
            document.body.classList.add("loading-levels");
            return this.loadLevels();
        }).bind(this))
        .then((function() {
            document.body.classList.remove("loading-levels");
            document.body.classList.add("got-resources");
            this.tag = getTagFromHash(this.allLevels);
            //Finish starting the game.
            this.textures.protect();
            this.resizeCanvas();
            //Get current time, after everything is loaded
            this.prevTimestamp = performance.now();
            //Register controls (now applicable).
            this.event_registerAll();
            //Start clean game.
            this.reset();
            window.requestAnimationFrame(this.main.bind(this));
        }).bind(this));
    }
    event_registerVisibility() {
        //Note that focus is lost.
        //  Also stop any player actions (e.g. left/right/jump).
        document.addEventListener("visibilitychange", (function() {
            if(document.hidden) {
                this.players.forEach(function(player) {
                    player.action.left = false;
                    player.action.right = false;
                    player.action.jump = false;
                });
            }
            else
                this.visibilityLoss = true; //Avoid time jump.
        }).bind(this));
    }
    event_registerCount() {
        var pc = localStorage["multiplayer/playerCount"] || 3;
        this.expectedPlayerNo = parseInt(pc, 10);
        document.body.classList.add("only-"+this.expectedPlayerNo+"-player");
        window.addEventListener("storage", (function(e) {
            if(e.key=="multiplayer/playerCount") {
                var newVal = parseInt(e.newValue, 10);
                this.expectedPlayerNo = newVal;
                for(var i=1; i<=4; ++i)
                    document.body.classList.remove("only-"+i+"-player");
                document.body.classList.add("only-"+newVal+"-player");
                this.reset();
            }
        }).bind(this));
    }
    resizeCanvas() {
        this.canvas.style.height = Math.max(0,
                window.innerHeight-this.canvas.getBoundingClientRect().top)+"px";
        this.scale = resizeCanvas(this.canvas);
    }
    event_registerResize() {
        window.addEventListener("resize", (function() {
            this.resizeCanvas();
            this.caches.refresh(this.scale*this.level.scale, this.textures);
        }).bind(this));
    }
    event_registerKeyboard() {
        var self = this;
        //Keyboard controls
        window.addEventListener("keydown", function(e) {
            if(!e.ctrlKey && !e.metaKey && !e.altKey) {
                var key = e.key || e.keyIdentifier;
                var lower = key.toLowerCase();
                var actionTaken = true;
                if(key=="Left" || key=="ArrowLeft")
                    self.players[0].action.left = true;
                else if(key=="Right" || key=="ArrowRight")
                    self.players[0].action.right = true;
                else if(key=="Up" || key=="ArrowUp")
                    self.players[0].action.jump = true;
                else if(key=="Shift")
                    self.players[0].action.throwStone = true;
                else if(key=="/")
                    self.players[0].action.useSpecial = true;
                else if(lower=="a" || key=="U+0041")
                    self.players[1].action.left = true;
                else if(lower=="d" || key=="U+0044")
                    self.players[1].action.right = true;
                else if(lower=="w" || key=="U+0057")
                    self.players[1].action.jump = true;
                else if(lower=="\\" || key=="|" || key=="U+005C" || key=="U+00DC")
                    self.players[1].action.throwStone = true;
                else if(lower=="z")
                    self.players[1].action.useSpecial = true;
                else if(lower=="j" || key=="U+004A")
                    self.players[2].action.left = true;
                else if(lower=="l" || key=="U+004C")
                    self.players[2].action.right = true;
                else if(lower=="i" || key=="U+0049")
                    self.players[2].action.jump = true;
                else if(lower=="n" || key=="U+004E")
                    self.players[2].action.throwStone = true;
                else if(lower=="b")
                    self.players[2].action.useSpecial = true;
                else if(lower=="t" || key=="U+0054")
                    self.players[3].action.jump = true;
                else if(lower=="f" || key=="U+0046")
                    self.players[3].action.left = true;
                else if(lower=="h" || key=="U+0048")
                    self.players[3].action.right = true;
                else if(lower=="c" || key=="U+0043")
                    self.players[3].action.throwStone = true;
                else if(lower=="x")
                    self.players[3].action.useSpecial = true;
                else if(key=="=" || key=="U+00BB")
                    self.reset();
                else
                    actionTaken = false;
                if(actionTaken)
                    e.preventDefault();
            }
        }, false);
        window.addEventListener("keyup", function(e) {
            if(!e.ctrlKey && !e.metaKey && !e.altKey) {
                var key = e.key || e.keyIdentifier;
                var lower = key.toLowerCase();
                if(key=="Left" || key=="ArrowLeft")
                    self.players[0].action.left = false;
                else if(key=="Right" || key=="ArrowRight")
                    self.players[0].action.right = false;
                else if(key=="Up" || key=="ArrowUp")
                    self.players[0].action.jump = false;
                else if(lower=="a" || key=="U+0041")
                    self.players[1].action.left = false;
                else if(lower=="d" || key=="U+0044")
                    self.players[1].action.right = false;
                else if(lower=="w" || key=="U+0057")
                    self.players[1].action.jump = false;
                else if(lower=="j" || key=="U+004A")
                    self.players[2].action.left = false;
                else if(lower=="l" || key=="U+004C")
                    self.players[2].action.right = false;
                else if(lower=="i" || key=="U+0049")
                    self.players[2].action.jump = false;
                else if(lower=="t" || key=="U+0054")
                    self.players[3].action.jump = false;
                else if(lower=="f" || key=="U+0046")
                    self.players[3].action.left = false;
                else if(lower=="h" || key=="U+0048")
                    self.players[3].action.right = false;
            }
        }, false);
    }
    event_registerHashChange() {
        window.addEventListener("hashchange", (function() {
            var prevTag = this.tag;
            this.tag = getTagFromHash(this.allLevels);
            if(prevTag!=this.tag)
                this.reset();
        }).bind(this));
    }
    event_registerAll() {
        this.event_registerVisibility();
        this.event_registerResize();
        this.event_registerCount();
        this.event_registerKeyboard();
        this.event_registerHashChange();
    }
}

function getTagFromHash(allLevels) {
    var match = window.location.hash.match(/t=(.*)/);
    if(match && getRandomTagged(allLevels, null, match[1]))
        return match[1];
    else
        return "";
}
