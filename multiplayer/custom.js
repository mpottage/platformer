//Copyright Matthew Pottage 2015-2018.
//Lets the user set up a multiplayer game with a custom selection of levels.
import {loadSavedObject, loadTextLevel, oldToJSONFormat0} from '../src/levels/parse.js';
import {MultiplayerGame} from './multiplayer.js';

class CustomGame extends MultiplayerGame {
    loadLevels() {
        return Promise.resolve(); //Levels set directly
    }
}
var playerWidth = 11;
var playerHeight = 90;
var game = new CustomGame();
window.addEventListener("load", function() {
    var openLevel = document.getElementById("openLevel");
    var openFile = document.createElement("input");
    openFile.type = "file";
    openFile.multiple = true;
    openFile.accept = ".js,.json,.txt";
    openFile.style.display = "none";
    document.body.appendChild(openFile);
    openFile.addEventListener("change", function() {
        openLevel.blur();
        document.body.classList.add("level-loading");
        var left = openFile.files.length;
        for(var i=0; i<openFile.files.length; ++i) {
            var f = openFile.files[i];
            var reader = new FileReader();
            reader.addEventListener("load", (function(name, e) {
                var newLevel = loadTextLevel(e.target.result);
                if(newLevel) {
                    game.allLevels[name] = newLevel;
                    --left;
                    if(left<=0) {
                        document.body.classList.remove("level-loading");
                        document.body.classList.add("levels-loaded");
                        game.start();
                        document.body.classList.add("levels-loaded");
                    }
                }
                else {
                    window.alert("Unable to load level from \""+name+"\".");
                    document.body.classList.remove("level-loading");
                    document.body.classList.add("loading-aborted");
                }
            }).bind(null, f.name));
            reader.addEventListener("error", (function(name, e) {
                window.alert("Unable to load \""+name+"\".");
                document.body.classList.remove("level-loading");
                document.body.classList.add("loading-aborted");
            }).bind(null, f.name));
            reader.readAsText(f);
        }
    });
    openLevel.addEventListener("click", function(e) {
        openFile.click();
    });
});
