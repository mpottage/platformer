#!/bin/sh
#Downloads tools needed to run package.sh
rm -rf tools
mkdir -p tools/downloads
cd tools/downloads
#Bug in newer package csso-cli prevents processing single/single.css
npm install csso@2.3.2
wget https://dl.google.com/closure-compiler/compiler-latest.zip
unzip compiler-latest.zip
cd ../
mv downloads/closure-compiler*.jar closure-compiler.jar
