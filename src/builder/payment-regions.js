//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {getAllObjs, getRootParent} from './filtering.js';
import {ReplaceObject} from './add-delete-actions.js';

export function getPRMergeAction(game, obj) {
    var objs = getAllObjs(game.manager, game.level, obj.box, o => o instanceof core.PaymentRegion);
    if(objs.length>0) { //Assumed that any other regions have already been merged
        var b1 = objs[0].box;
        var b2 = obj.box;
        var x = Math.min(b1.x, b2.x);
        var y = Math.min(b1.y, b2.y);
        var newBox = new core.Box(x, y, Math.max(b1.x+b1.width, b2.x+b2.width)-x,
            Math.max(b1.y+b1.height, b2.y+b2.height)-y);
        var replacement = new mods.PaymentRegion(objs[0].name, newBox, objs[0].requires+obj.requires);
        var original = getRootParent(objs[0], game.level.objs) || objs[0];
        if(original instanceof core.ApplyFlag)
            replacement = new mods.ApplyFlag(original.flag, replacement);
        return new ReplaceObject(original, replacement);
    }
    return null;
}
