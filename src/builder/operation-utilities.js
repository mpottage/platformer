//Copyright Matthew Pottage 2019
import {Operation} from './operation.js';

//Suppresses release callback for "delay" times for "next".
export class DelayRelease extends Operation {
    constructor(next, delay) {
        super(next);
        this.delay = delay;
    }
    abort(state, game) { this.next.abort(state, game); }
    release() {
        --this.delay;
        if(this.delay<=0)
            return this.next;
        else
            return this;
    }
}
