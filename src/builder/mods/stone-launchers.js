//Copyright Matthew Pottage 2019.
import * as core from '../../core/import-level-objects.js';

export class StoneLauncher extends core.StoneLauncher {
    getParams(state) {
        return [
            {name: "Launch Delay", type: "number", min: 0.1, max: 20, step: 0.1, value: this.launchDelay},
            {name: "Power", type: "number", min: 0.5, max: 2, step: 0.1, value: Math.max(Math.abs(this.launchVec.x), Math.abs(this.launchVec.y))}
        ];
    }
    setParam(name, value) {
        if(name=="Launch Delay")
            this.launchDelay = value;
        else if(name=="Power") {
            if(this.launchVec.x>0)
                this.launchVec.x = value;
            else if(this.launchVec.x<0)
                this.launchVec.x = -value;
            if(this.launchVec.y>0)
                this.launchVec.y = value;
            else if(this.launchVec.y<0)
                this.launchVec.y = -value;
        }
        else
            throw new TypeError("Unknown parameter.");
    }
}

export class TargettingStoneLauncher extends core.TargettingStoneLauncher {
    getParams(state) {
        return [
            {name: "Launch Delay", type: "number", min: 0.1, max: 20, step: 0.1, value: this.launchDelay},
            {name: "Power", type: "number", min: 0.5, max: 2, step: 0.1, value: this.effectivePower}
        ];
    }
    setParam(name, value) {
        if(name=="Launch Delay")
            this.launchDelay = value;
        else if(name=="Power") {
            if(this.originalLV.x>0)
                this.originalLV.x = value;
            else if(this.originalLV.x<0)
                this.originalLV.x = -value;
            if(this.originalLV.y>0)
                this.originalLV.y = value;
            else if(this.originalLV.y<0)
                this.originalLV.y = -value;
            this.effectivePower = value;
        }
        else
            throw new TypeError("Unknown parameter.");
    }
}
