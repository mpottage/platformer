//Copyright Matthew Pottage 2019.
import * as core from '../../core/import-level-objects.js';
import {getGridSize} from '../utility.js';

export class SpikeRow extends core.SpikeRow {
    getParams(state) {
        return [
            {name: "Spike Width", type: "number", min: 5, max: 100,
                step: getGridSize(state.snapToGrid), value: this.targetWidth}
        ];
    }
    setParam(name, value) {
        if(name=="Spike Width") {
            this.targetWidth = value;
            this._setSpikeWidth(this.targetWidth);
        }
        else
            throw new TypeError("Unknown parameter");
    }
}

export class SpikeColumn extends core.SpikeColumn {
    getParams(state) {
        return [
            {name: "Spike Height", type: "number", min: 5, max: 100,
                step: getGridSize(state.snapToGrid), value: this.targetHeight}
        ];
    }
    setParam(name, value) {
        if(name=="Spike Height") {
            this.targetHeight = value;
            this._setSpikeHeight(this.targetHeight);
        }
        else
            throw new TypeError("Unknown parameter");
    }
}
