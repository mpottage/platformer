//Copyright Matthew Pottage 2019.
import * as core from '../../core/import-level-objects.js';
import {activatedRegionBox} from '../visuals.js';

export class SnowEmitter extends core.SnowEmitter {
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgba(0,44,255,.5)");
    }
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    getParams(state) {
        return [
            {name: "Advanced: Lifetime", type: "number", "min": 2, "max": 20,
                step: 0.1, value: this.cacheTime}
        ];
    }
    setParam(name, value) {
        if(name=="Advanced: Lifetime") {
            this.cacheTime = value;
            this._setupPulses();
            this.pool.apply(o => o.disable());
        }
        else
            throw new TypeError("Unknown parameter");
    }
}

export class RainEmitter extends core.RainEmitter {
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgba(153,25,255,.6)");
    }
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    getParams(state) {
        return [
            //max: 6 chosen as a rain emitter has 3.2 times as many particles as
            //a snow emitter, per second of cacheTime and (20, max for snow
            //emitter)/3.2 is approx 6
            {name: "Advanced: Lifetime", type: "number", "min": 1, "max": 6,
                step: 0.1, value: this.cacheTime}
        ];
    }
    setParam(name, value) {
        if(name=="Advanced: Lifetime") {
            this.cacheTime = value;
            this._setupPulses();
            this.pool.apply(o => o.disable());
        }
        else
            throw new TypeError("Unknown parameter");
    }
}
