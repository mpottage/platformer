//Copyright Matthew Pottage 2019.
import * as runtime from '../../core/runtime.js';

export class Environment extends runtime.Environment {
    setBuilderDetails(val) {
        this._vars["builder details"] = val;
    }
    setBuilderLogic(val) {
        this._vars["builder logic"] = val;
    }
    clear() {
        if(this._vars) {
            var save = {details: this._vars["builder details"],
                logic: this._vars["builder logic"]}
            super.clear();
            this._vars["builder details"] = save.details;
            this._vars["builder logic"] = save.logic;
        }
        else {
            super.clear();
            this._vars["builder details"] = false;
            this._vars["builder logic"] = false;
        }
    }
}
