//Copyright Matthew pottage 2019.
import * as core from '../../core/import-level-objects.js';
import {outlineDisabled, drawLRPath, drawUDPath} from '../visuals.js';

export class FadingPlatform extends core.FadingPlatform {
    draw(context, caches, environment) {
        super.draw(context, caches, environment);
        if(environment.get("builder details") && !this.activated)
            outlineDisabled(context, this);
    }
}

export class CollapsingPlatform extends core.CollapsingPlatform {
    getParams(state) {
        return [
            {name: "Collapse Delay", type: "number", "min": 0, "max": 20, step: 0.1, value: this.delay}
        ];
    }
    setParam(name, value) {
        if(name=="Collapse Delay")
            this.delay = value;
        else
            throw new TypeError("Unknown parameter");
    }
}

export class Destructible extends core.Destructible {
    draw(context, caches, environment) {
        super.draw(context, caches, environment);
        if(environment.get("builder details") && this.disabled)
            outlineDisabled(context, this);
    }
    get select() {
        return true;
    }
}

export class LRPlatform extends core.LRPlatform {
    draw(context, caches, environment) {
        drawLRPath(context, environment, this);
        super.draw(context, caches, environment);
    }
    getParams(state) {
        return [{name: "Acceleration", type: "number", min: 5, max: 1500, step: 5, value: this.xAccel}];
    }
    setParam(name, value) {
        if(name=="Acceleration")
            this.xAccel = value;
    }
}

export class UDPlatform extends core.UDPlatform {
    draw(context, caches, environment) {
        drawUDPath(context, environment, this);
        super.draw(context, caches, environment);
    }
    getParams(state) {
        return [{name: "Acceleration", type: "number", min: 5, max: 1500, step: 5, value: this.yAccel}];
    }
    setParam(name, value) {
        if(name=="Acceleration")
            this.yAccel = value;
        else
            throw new TypeError("Unknown parameter");
    }
}
