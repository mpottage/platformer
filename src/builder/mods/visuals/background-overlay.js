//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';

export class VisualOnlyBackground extends core.VisualOnlyBackground {
    constructor(box, color) {
        super(box, color);
        this.builderGroup = null;
    }
    reset() {
        super.reset();
        this.builderGroup = null;
    }
}
export class VisualOnlyOverlay extends core.VisualOnlyOverlay {
    constructor(box, color) {
        super(box, color);
        this.builderGroup = null;
    }
    reset() {
        super.reset();
        this.builderGroup = null;
    }
}
