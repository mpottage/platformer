//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';

export class Darkness extends core.Darkness {
    reset() {
        super.reset();
        this.builderShow = true;
    }
    overlayDraw(context, caches) {
        if(this.builderShow)
            super.overlayDraw(context, caches);
    }
    get select() {
        return this.builderShow;
    }
}
