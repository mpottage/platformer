import * as core from '../../core/import-level-objects.js';
import {showLaserDir} from '../visuals.js';
import {getGridSize} from '../utility.js';

function getLaserParams(state, laser) {
    var followParams = (laser.follow.getParams && laser.follow.getParams(state)) || [];
    followParams.forEach(p => p.name = "Source "+p.name);
    return [{name: "Range", type: "number", min: 5, max: 25000,
        value: laser.rangeLimit, step: getGridSize(state.snapToGrid)}
    ].concat(followParams);
}
function setLaserParam(laser, name, value) {
    if(name=="Range") {
        laser.rangeLimit = value;
        laser.prevAdjPos.x = laser.box.x;
        laser.prevAdjPos.y = laser.box.y;
    }
    else if(laser.follow.setParam)
        laser.follow.setParam(name.replace(/^Source /,""), value);
    else
        throw new TypeError("Unknown parameter");
}
export class CuttingLaser extends core.CuttingLaser {
    draw(context, caches, environment) {
        if(!showLaserDir(context, environment, this))
            super.draw(context, caches, environment);
    }
    getLinkCenter() {
        return this.follow.box.center;
    }
    get select() {
        return false;
    }
    getParams(state) {
        return getLaserParams(state, this);
    }
    setParam(name, value) {
        setLaserParam(this, name, value);
    }
}

export class TriggerLaser extends core.TriggerLaser {
    draw(context, caches, environment) {
        if(!showLaserDir(context, environment, this))
            super.draw(context, caches, environment);
    }
    getLinkCenter() {
        return this.follow.box.center;
    }
    get select() {
        return false;
    }
    getParams(state) {
        return getLaserParams(state, this);
    }
    setParam(name, value) {
        setLaserParam(this, name, value);
    }
}
