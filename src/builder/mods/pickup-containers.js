//Copyright Matthew Pottage 2019.
import * as core from '../../core/import-level-objects.js';
import {CollisionsEngine} from '../../core/collisions/engine.js';
import {ObjectManager} from '../../core/runtime.js';
export class WoodenBox extends core.WoodenBox {
    constructor(x, y, obj) {
        super(x, y, obj);
        this.suppressCE = new CollisionsEngine();
        this.suppressM = new ObjectManager();
    }
    register(ce, m, caches) {
        super.register(ce, m, caches);
        //Just get this.obj to properly register any render caches.
        this.obj.register(this.suppressCE, this.suppressM, caches);
    }
    accepting(obj) {
        return differentPickupTo(obj, this.obj);
    }
    draw(context, caches, environment) {
        super.draw(context, caches, environment);
        pickupBoxDraw(context, caches, environment, this);
    }
}
export class Present extends core.Present {
    constructor(x, y, obj) {
        super(x, y, obj);
        this.suppressCE = new CollisionsEngine();
        this.suppressM = new ObjectManager();
    }
    register(ce, m, caches) {
        super.register(ce, m, caches);
        this.obj.register(this.suppressCE, this.suppressM, caches);
    }
    accepting(obj) {
        return differentPickupTo(obj, this.obj);
    }
    draw(context, caches, environment) {
        super.draw(context, caches, environment);
        pickupBoxDraw(context, caches, environment, this);
    }
}

//Visual for a level builder for a pickup spawner. Shows a preview of the
//pickup(s) that could be spawned, and is faded if the pickup has been spawned.
export class PickupSpawner extends core.PickupSpawner {
    constructor(objs, box) {
        super(objs, box);
        this.suppressCE = new CollisionsEngine();
        this.suppressM = new ObjectManager();
    }
    reset() {
        super.reset();
        this.objs.forEach(function(o) { o.reset() });
    }
    accepting(obj) {
        for(var i=0; i<this.objs.length; ++i)
            if(differentPickupTo(obj, this.objs[i]))
                return true;
        return false;
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        manager.addOverlay(this);
        manager.addDraw(this);
        this.objs.forEach(function(o) {
            o.register(this.suppressCE, this.suppressM, caches);
        }, this);
    }
    _maxCOHeight() {
        var max = this.objs[0].box.height;
        for(var i=1; i<this.objs.length; ++i)
            if(this.objs[i].box.height>max)
                max = this.objs[i].box.height;
        return max;
    }
    _maxCOWidth() {
        var max = this.objs[0].box.width;
        for(var i=1; i<this.objs.length; ++i)
            if(this.objs[i].box.width>max)
                max = this.objs[i].box.width;
        return max;
    }
    _overlay3Or4ContainedObjs(context, caches) {
        //Position objects for rendering.
        var midYDiff = Math.min(this.box.height/2-2, this._maxCOHeight())/2;
        var midXDiff = Math.min(this.box.width/2-2, this._maxCOWidth())/2;
        var midBelowY = this.box.y+this.box.height/2+2;
        var o0 = this.objs[0];
        o0.box.x = this.box.x+this.box.width/2-2-Math.max(midXDiff+o0.box.width/2, o0.box.width);
        o0.box.y = midBelowY+Math.max(0, (midYDiff-o0.box.height/2));
        var o1 = this.objs[1];
        o1.box.x = this.box.x+this.box.width/2+2+Math.max(0, midXDiff-o1.box.width/2);
        o1.box.y = midBelowY+Math.max(0, (midYDiff-o1.box.height/2));
        var midAboveY = this.box.y+this.box.height/2-2;
        var o2 = this.objs[2];
        o2.box.y = midAboveY-Math.max(o2.box.height, midYDiff+o2.box.height/2);
        if(this.objs.length==3) {
            o2.box.x = this.box.x+this.box.width/2-o2.box.width/2;
        }
        else if(this.objs.length==4) {
            o2.box.x = this.box.x+this.box.width/2-2-Math.max(midXDiff+o2.box.width/2, o2.box.width);
            var o3 = this.objs[3];
            o3.box.x = this.box.x+this.box.width/2+2+Math.max(0, midXDiff-o3.box.width/2);
            o3.box.y = midAboveY-Math.max(o3.box.height, midYDiff+o3.box.height/2);
        }
    }
    _overlayContainedObjs(context, caches, environment) {
        if(this.objs.length==1) {
            var o = this.objs[0];
            o.box.x = this.box.x+this.box.width/2-o.box.width/2;
            o.box.y = this.box.y+this.box.height/2-o.box.height/2;
        }
        else if(this.objs.length==2) {
            var midXDiff = Math.min(this.box.width/2-2, this._maxCOWidth())/2;
            var o0 = this.objs[0];
            o0.box.x = this.box.x+this.box.width/2-2-Math.max(midXDiff+o0.box.width/2, o0.box.width);
            o0.box.y = this.box.y+this.box.height/2-o0.box.height/2;
            var o1 = this.objs[1];
            o1.box.x = this.box.x+this.box.width/2+2+Math.max(0, midXDiff-o1.box.width/2);
            o1.box.y = this.box.y+this.box.height/2-o1.box.height/2;
        }
        else if(this.objs.length<=4)
            this._overlay3Or4ContainedObjs(context, caches);
        else
            throw new Error("Unsupported number of items in BuilderPickupSpawner: "+this.objs.length);
        //Render
        this.objs.forEach(function(o) {
            if(o instanceof core.Pickup) {
                var prevD = o.disabled;
                o.disabled = false;
                o.draw(context, caches, environment);
                o.disabled = prevD;
            }
            else {//if o instanceof core.BouncingPickup
                var prevD = o.pickup.disabled;
                o.pickup.disabled = false;
                o.pickup.draw(context, caches, environment);
                o.pickup.disabled = prevD;
            }
        });
    }
    generalDraw(context, caches, environment) {
        //Draw objects contained within it.
        context.save();
        context.beginPath();
        context.rect(this.box.x, this.box.y, this.box.width, this.box.height);
        context.clip();
        var prevSpawnedPos = {x: 0, y: 0};
        if(this.spawned) {
            context.globalAlpha *= .4;
            prevSpawnedPos.x = this.spawned.box.x;
            prevSpawnedPos.y = this.spawned.box.y;
        }
        else
            context.globalAlpha *= .8;
        this._overlayContainedObjs(context, caches, environment);
        if(this.spawned) {
            this.spawned.box.x = prevSpawnedPos.x;
            this.spawned.box.y = prevSpawnedPos.y;
        }
        context.restore();
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details")) {
            if(!this.spawned) {
                context.fillStyle = "rgba(255,255,255,.4)";
                this.box.fill(context);
                this.generalDraw(context, caches, environment);
            }
            context.lineWidth = 4;
            context.strokeStyle = "#555";
            context.strokeRect(this.box.x+2, this.box.y+2,
                    this.box.width-4, this.box.height-4);
        }
    }
    draw(context, caches, environment) {
        if(environment.get("builder details")) {
            if(this.spawned) {
                context.fillStyle = "rgba(255,255,255,.2)";
                this.box.fill(context);
                this.generalDraw(context, caches, environment);
            }
        }
    }
}

//Check if two pickups are of different types.
function differentPickupTo(obj, to) {
    return (obj instanceof core.Pickup && obj.constructor!=to.constructor)
        || ((obj instanceof core.BouncingPickup) &&
                (obj.constructor!=to.constructor ||
                 obj.pickup.constructor!=to.pickup.constructor));
}
//Visual to preview pickup spawned by a pickup box
function pickupBoxDraw(context, caches, environment, obj) {
    if(!obj.disabled && environment.get("builder details")) {
        obj.obj.box.x = obj.box.x+obj.box.width/2-obj.obj.box.width/2;
        obj.obj.box.y = obj.box.y+obj.box.height/2-obj.obj.box.height/2;
        //Draw dashed-line box outline surrounding the object.
        context.beginPath();
        context.strokeStyle = "rgba(255,255,255,.8)";
        context.lineWidth = 3;
        context.setLineDash([10, 5]);
        context.strokeRect(obj.obj.box.x-1.5, obj.obj.box.y-1.5,
                obj.obj.box.width+3, obj.obj.box.height+3);
        context.setLineDash([]);
        //Draw faded contained object in the middle of the outline.
        var prevGA = context.globalAlpha;
        context.globalAlpha *= .9;
        if(obj.obj instanceof core.Pickup)
            obj.obj.draw(context, caches, environment);
        else//if obj.obj instanceof core.BouncingPickup
            obj.obj.pickup.draw(context, caches, environment);
        context.globalAlpha = prevGA;
    }
}
