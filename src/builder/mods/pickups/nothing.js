//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';
import {neededTextures} from '../../../core/utility.js';
import {StretchImageCache} from '../../../core/render-caches.js';

neededTextures.push("NothingPickup.svg");
export class NothingPickup extends core.NothingPickup {
    register(collisions, manager, caches) {
        manager.addDraw(this);
        if(!caches.get("NothingPickup.svg"))
            caches.set("NothingPickup.svg",
                    new StretchImageCache("NothingPickup.svg", this.box.width,
                        this.box.height));
    }
    draw(context, caches, environment) {
        if(environment.get("builder details"))
            caches.get("NothingPickup.svg").draw(context, this.box);
    }
}
