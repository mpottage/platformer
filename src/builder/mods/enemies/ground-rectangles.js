//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';
import {startPosSquare} from '../../visuals.js';

export class BrownRectangle extends core.BrownRectangle {
    draw(context, caches, environment) {
        startPosSquare(context, environment, this);
        super.draw(context, caches, environment);
    }
}

export class OrangeRectangle extends core.OrangeRectangle {
    draw(context, caches, environment) {
        startPosSquare(context, environment, this);
        super.draw(context, caches, environment);
    }
}
