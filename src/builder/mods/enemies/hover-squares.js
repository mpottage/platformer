//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';
import {startPosSquare} from '../../visuals.js';

export class OrangeSquare extends core.OrangeSquare {
    draw(context, caches, environment) {
        startPosSquare(context, environment, this);
        super.draw(context, caches, environment);
    }
}
export class BlackSquare extends core.BlackSquare {
    draw(context, caches, environment) {
        startPosSquare(context, environment, this);
        super.draw(context, caches, environment);
    }
}
export class GhostSquare extends core.GhostSquare {
    draw(context, caches, environment) {
        startPosSquare(context, environment, this);
        super.draw(context, caches, environment);
    }
}
export class PurpleSquare extends core.PurpleSquare {
    draw(context, caches, environment) {
        startPosSquare(context, environment, this);
        super.draw(context, caches, environment);
    }
}
