//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';
import {drawLRPath} from '../../visuals.js';
import {getGridSize} from '../../utility.js';

function getBTParams(state, bt) {
    return [{name: "Start Offset", type: "number", min: 0, max: bt.endPos.x-bt.startPos.x,
        value: bt.alternateStartX-bt.startPos.x, step: getGridSize(state.snapToGrid)}];
}
function setBTParam(state, bt, name, value) {
    if(name=="Start Offset") {
        bt.alternateStartX = value+bt.startPos.x;
        bt.triangle.translate(bt.alternateStartX-bt.box.x, 0);
    }
    else
        throw new TypeError("Unknown parameter");
}

export class BadTriangle extends core.BadTriangle {
    draw(context, caches, environment) {
        drawLRPath(context, environment, this);
        super.draw(context, caches);
    }
    getParams(state) {
        return getBTParams(state, this);
    }
    setParam(name, value) {
        setBTParam(this, name, value);
    }
}
export class OrangeTriangle extends core.OrangeTriangle {
    draw(context, caches, environment) {
        drawLRPath(context, environment, this);
        super.draw(context, caches);
    }
    getParams(state) {
        return getBTParams(state, this);
    }
    setParam(name, value) {
        setBTParam(this, name, value);
    }
}
export class YellowTriangle extends core.YellowTriangle {
    draw(context, caches, environment) {
        drawLRPath(context, environment, this);
        super.draw(context, caches);
    }
    getParams(state) {
        return getBTParams(state, this);
    }
    setParam(name, value) {
        setBTParam(this, name, value);
    }
}
export class GreyTriangle extends core.GreyTriangle {
    draw(context, caches, environment) {
        drawLRPath(context, environment, this);
        super.draw(context, caches);
    }
    getParams(state) {
        return getBTParams(state, this);
    }
    setParam(name, value) {
        setBTParam(this, name, value);
    }
}
export class PurpleTriangle extends core.PurpleTriangle {
    draw(context, caches, environment) {
        drawLRPath(context, environment, this);
        super.draw(context, caches);
    }
    getParams(state) {
        return getBTParams(state, this);
    }
    setParam(name, value) {
        setBTParam(this, name, value);
    }
}
export class PinkTriangle extends core.PinkTriangle {
    draw(context, caches, environment) {
        drawLRPath(context, environment, this);
        super.draw(context, caches);
    }
    getParams(state) {
        return getBTParams(state, this);
    }
    setParam(name, value) {
        setBTParam(this, name, value);
    }
}
export class BlueTriangle extends core.BlueTriangle {
    draw(context, caches, environment) {
        //Avoid route indicator going up/down with the triangle.
        var currY = this.box.y;
        this.box.y = this.startPos.y-this.box.height;
        drawLRPath(context, environment, this);
        this.box.y = currY;
        super.draw(context, caches);
    }
    getParams(state) {
        return getBTParams(state, this);
    }
    setParam(name, value) {
        setBTParam(this, name, value);
    }
}
export class TurquoiseTriangle extends core.TurquoiseTriangle {
    draw(context, caches, environment) {
        drawLRPath(context, environment, this, environment);
        super.draw(context, caches);
    }
    getParams(state) {
        return getBTParams(state, this);
    }
    setParam(name, value) {
        setBTParam(this, name, value);
    }
}
