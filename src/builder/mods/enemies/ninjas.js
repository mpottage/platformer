//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';
import {ninjaStartPos} from '../../visuals.js';

export class Ninja extends core.Ninja {
    draw(context, caches, environment) {
        ninjaStartPos(context, environment, this);
        super.draw(context, caches, environment);
    }
}
export class TurquoiseNinja extends core.TurquoiseNinja {
    draw(context, caches, environment) {
        ninjaStartPos(context, environment, this);
        super.draw(context, caches, environment);
    }
}
export class GreenNinja extends core.GreenNinja {
    draw(context, caches, environment) {
        ninjaStartPos(context, environment, this);
        super.draw(context, caches, environment);
    }
}
