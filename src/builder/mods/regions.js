//Copyright Matthew Pottage 2019.
import * as core from '../../core/import-level-objects.js';
import {activatedRegionBox, drawOutlineBox} from '../visuals.js';
import {getAngle} from '../../core/utility.js';

//A visible WinRegion for the level builder.
export class WinRegion extends core.WinRegion{
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgba(0, 180, 234, 0.5)");
    }
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
}
//A visible DeathRegion for the level builder.
export class DeathRegion extends core.DeathRegion {
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgb(230, 20, 20)");
    }
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
}

export class HitRegion extends core.HitRegion {
    constructor(name, box) {
        super(name, box);
        this.linkColor = "rgb(255, 198, 0)";
    }
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            drawOutlineBox(context, this.box, "rgba(255, 198, 0, 0.6)", "#333");
    }
}

export class ToStartRegion extends core.ToStartRegion {
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgba(140, 140, 140, 0.5)");
    }
}

export class SuppressWeapons extends core.SuppressWeapons {
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgba(255, 172, 114, 0.5)");
    }
}

export class ClearSpecials extends core.ClearSpecials {
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgba(255, 0, 114, 0.5)");
    }
}

export class MessageRegion extends core.MessageRegion {
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgba(200, 162, 200, 0.8)");
    }
    getParams(state) {
        return [
            {name: "Text", type: "string", value: this.message}
        ];
    }
    setParam(name, value) {
        if(name=="Text")
            this.message = value;
        else
            throw new TypeError("Unknown parameter");
    }
}

export const airCurrentLength = 200;
//Visual for air currents in the level builder.
//  > Direction is shown with an arrow.
//  > Power is indicated by the intensity of the green fill and the number of
//  heads on the direction arrow.
export class AirCurrent extends core.AirCurrent {
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details")) {
            var arrowColor = "#01640F";
            if(!this.activated) //Faded arrow.
                arrowColor = "#54995E";
            //Different opacity for each power.
            var opacity = 0.5+0.3*Math.log2(this.power||0.5);
            activatedRegionBox(context, this, "rgba(37, 255, 69, "+opacity+")");
            //Indicate the direction of the current.
            var c = this.box.center;
            context.beginPath();
            context.moveTo(c.x, c.y);
            var length = Math.min(airCurrentLength, Math.min(this.box.width/2,
                        this.box.height/2));
            var endPoint = {x: c.x+this.vec.x*length, y: c.y+this.vec.y*length};
            context.lineTo(endPoint.x, endPoint.y);
            context.lineWidth = 4;
            context.strokeStyle = arrowColor;
            context.stroke();
            //Semi-circle at the end of direction line.
            context.beginPath();
            var angle = getAngle(this.vec.x || 0, this.vec.y || 0); //Avoid -0.
            context.arc(endPoint.x, endPoint.y, Math.min(10, length),
                    angle-Math.PI/2, angle+Math.PI/2);
            context.fillStyle = arrowColor;
            context.fill();
        }
    }
}

//Visual for a PaymentRegion in the level builder.
//  > Has a separate coin visual inside it for every coin that it charges.
export class PaymentRegion extends core.PaymentRegion {
    constructor(name, box, coins) {
        super(name, box, coins);
        this.linkColor = "rgb(237, 254, 1)";
    }
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
    _drawRequiredCoins(context) {
        var padding = 5;
        var coinSpace = 2*padding+2*core.Coin.radius; //Coin and 5px padding.
        //Get at least one coin in
        var scale = Math.min(1, this.box.height/coinSpace, this.box.width/coinSpace);
        var maxRows = Math.floor(this.box.height/(coinSpace*scale));
        var maxCols = Math.floor(this.box.width/(coinSpace*scale));
        var fit = maxRows*maxCols; //How many coins we can fit in
        //Scale so that all the coins fit (adding 1 row and column each time)
        while(fit<this.requires) {
            scale *= Math.min(maxRows/(maxRows+1), maxCols/(maxCols+1));
            maxRows = Math.floor(this.box.height/(coinSpace*scale));
            maxCols = Math.floor(this.box.width/(coinSpace*scale));
            fit = maxRows*maxCols;
        }
        //Get a better shape for the coins to be laid out in (approximately
        //square).
        var idealRows = Math.floor(Math.sqrt(this.requires));
        var idealCols = Math.ceil(Math.sqrt(this.requires));
        while(idealRows*idealCols<this.requires) {
            ++idealRows;
            if(idealRows*idealCols<this.requires)
                ++idealCols;
        }
        if(idealCols>maxCols) {
            idealCols = maxCols;
            var extra = this.requires-idealCols*idealRows;
            idealRows += Math.ceil(extra/idealCols);
        }
        if(idealRows>maxRows) {
            idealRows = maxRows;
            var extra = this.requires-idealCols*idealRows;
            idealCols += Math.ceil(extra/idealRows);
        }
        //Place coins in (grid centered).
        var center = this.box.center;
        var dx = (-coinSpace*idealCols/2+padding+core.Coin.radius)*scale+center.x;
        var dy = (-coinSpace*idealRows/2+padding+core.Coin.radius)*scale+center.y;
        context.fillStyle = core.Coin.color;
        context.beginPath();
        var row = 0;
        var col = 0;
        for(var i=0; i<this.requires; ++i) {
            context.arc(col*scale*coinSpace+dx, row*scale*coinSpace+dy,
                core.Coin.radius*scale, 0, 2*Math.PI);
            context.closePath();
            ++col;
            if(col>=idealCols) {
                ++row;
                col = 0;
            }
        }
        context.fill();
    }
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details")) {
            activatedRegionBox(context, this, "rgba(237, 254, 1, 0.6)");
            this._drawRequiredCoins(context);
        }
    }
    getParams(state) {
        return [
            {name: "Payment", type: "number", min: 1, max: 200, step: 1, value: this.requires}
        ];
    }
    setParam(name, value) {
        if(name=="Payment")
            this.requires = value;
        else
            throw new TypeError("Unknown parameter");
    }
}

export class PortalRegion extends core.PortalRegion {
    backgroundDraw(context, caches, environment) {
        if(environment.get("builder details"))
            activatedRegionBox(context, this, "rgba(136,65,46,.5)");
    }
    register(collisions, manager) {
        super.register(collisions, manager);
        manager.addBackground(this);
    }
}

