//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';
import {drawApplyNode, drawMakeNode, logicInputRadius} from '../../visuals.js';

export class MakeAndApplyFlag extends core.MakeAndApplyFlag {
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        manager.addDraw(this);
    }
    draw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic")) {
            var c = this.obj.box.center;
            drawApplyNode(context, c);
            drawMakeNode(context, c);
        }
    }
    getLinkInPos() {
        var p = this.obj.box.center;
        p.x -= logicInputRadius;
        return p;
    }
    getLinkOutPos() {
        var p = this.obj.box.center;
        p.x += logicInputRadius;
        return p;
    }
    getParams(state) {
        return (this.obj.getParams && this.obj.getParams(state)) || [];
    }
    setParam(name, value) {
        if(this.obj.setParam)
            this.obj.setParam(name, value);
        else
            throw new TypeError("Unknown parameter");
    }
}

export class MakeFlagFrom extends core.MakeFlagFrom {
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        manager.addDraw(this);
    }
    draw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawMakeNode(context, this.obj.box.center);
    }
    getLinkOutPos() {
        var p = this.obj.box.center;
        p.x += logicInputRadius;
        return p;
    }
    getParams(state) {
        return (this.obj.getParams && this.obj.getParams(state)) || [];
    }
    setParam(name, value) {
        if(this.obj.setParam)
            this.obj.setParam(name, value);
        else
            throw new TypeError("Unknown parameter");
    }
}

export class ApplyFlag extends core.ApplyFlag {
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        manager.addDraw(this);
    }
    draw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawApplyNode(context, this._center());
    }
    _center() {
        if(this.obj.getLinkCenter)
            return this.obj.getLinkCenter();
        else
            return this.obj.box.center;
    }
    getLinkInPos() {
        var p = this._center();
        p.x -= logicInputRadius;
        return p;
    }
    getParams(state) {
        return (this.obj.getParams && this.obj.getParams(state)) || [];
    }
    setParam(name, value) {
        if(this.obj.setParam)
            this.obj.setParam(name, value);
        else
            throw new TypeError("Unknown parameter");
    }
}
