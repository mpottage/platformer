//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';
import {logicInputRadius, drawLogicItem, rootLogicNode} from '../../visuals.js';

export class OrFlags extends core.OrFlags {
    constructor(name, flags, builderX, builderY) {
        super(name, flags);
        this.box = new core.Box(builderX || 0, builderY || 280,
                30+logicInputRadius, 30);
        this.linkColor = "#00a99c";
    }
    register(ce, m) {
        super.register(ce, m);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawLogicItem(context, this.box, this.linkColor, logicInputRadius);
    }
    getLinkInPos() {
        return {x: this.box.x+2, y: this.box.y+this.box.height/2};
    }
    getLinkOutPos() {
        return {x: this.box.x+this.box.width, y: this.box.y+this.box.height/2};
    }
    get select() {
        return showLogic;
    }
}

export class AndFlags extends core.AndFlags {
    constructor(name, flags, builderX, builderY) {
        super(name, flags);
        this.box = new core.Box(builderX || 0, builderY || 280,
                30+logicInputRadius, 30);
        this.linkColor = "#1f2888";
    }
    register(ce, m) {
        super.register(ce, m);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawLogicItem(context, this.box, this.linkColor, logicInputRadius);
    }
    getLinkInPos() {
        return {x: this.box.x+2, y: this.box.y+this.box.height/2};
    }
    getLinkOutPos() {
        return {x: this.box.x+this.box.width, y: this.box.y+this.box.height/2};
    }
    get select() {
        return showLogic;
    }
}

export class HoldAnd extends core.HoldAnd {
    constructor(name, flags, builderX, builderY) {
        super(name, flags);
        this.box = new core.Box(builderX || 0, builderY || 280,
                30+logicInputRadius, 30);
        this.linkColor = "#1f7788";
    }
    register(ce, m) {
        super.register(ce, m);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawLogicItem(context, this.box, this.linkColor, logicInputRadius);
    }
    getLinkInPos() {
        return {x: this.box.x+2, y: this.box.y+this.box.height/2};
    }
    getLinkOutPos() {
        return {x: this.box.x+this.box.width, y: this.box.y+this.box.height/2};
    }
    get select() {
        return showLogic;
    }
}

export class RelayFlag extends core.RelayFlag {
    constructor(name, from, builderX, builderY) {
        super(name, from);
        this.box = new core.Box(builderX || 0, builderY || 280,
                20+logicInputRadius, 20);
        this.linkColor = "black";
    }
    register(ce, m) {
        super.register(ce, m);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawLogicItem(context, this.box, this.linkColor, logicInputRadius);
    }
    getLinkInPos() {
        return {x: this.box.x+2, y: this.box.y+this.box.height/2};
    }
    getLinkOutPos() {
        return {x: this.box.x+this.box.width, y: this.box.y+this.box.height/2};
    }
    get select(){
        return showLogic;
    }
}

export class NegateFlag extends core.NegateFlag {
    constructor(name, toNegate, builderX, builderY) {
        super(name, toNegate);
        this.box = new core.Box(builderX || 0, builderY || 280,
                20+logicInputRadius, 20);
        this.linkColor = "red";
    }
    register(ce, m) {
        super.register(ce, m);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawLogicItem(context, this.box, this.linkColor, logicInputRadius);
    }
    getLinkInPos() {
        return {x: this.box.x+2, y: this.box.y+this.box.height/2};
    }
    getLinkOutPos() {
        return {x: this.box.x+this.box.width, y: this.box.y+this.box.height/2};
    }
    get select(){
        return showLogic;
    }
}

export class FlagBeenTrue extends core.FlagBeenTrue {
    constructor(name, tracking, builderX, builderY) {
        super(name, tracking);
        this.box = new core.Box(builderX || 0, builderY || 280,
                20+logicInputRadius, 20);
        this.linkColor = "#4f8100";
    }
    register(ce, m) {
        super.register(ce, m);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawLogicItem(context, this.box, this.linkColor, logicInputRadius);
    }
    getLinkInPos() {
        return {x: this.box.x+2, y: this.box.y+this.box.height/2};
    }
    getLinkOutPos() {
        return {x: this.box.x+this.box.width, y: this.box.y+this.box.height/2};
    }
    get select(){
        return showLogic;
    }
}

export class Timeout extends core.Timeout {
    constructor(output, input, dur, x, y) {
        super(output, input, dur);
        this.box = new core.Box(x, y, 30, 20);
        if(dur<1)
            this.linkColor = "#FFCA00";
        else if(dur<5)
            this.linkColor = "#FFA500";
        else if(dur<10)
            this.linkColor = "#CF8600";
        else
            this.linkColor = "#AB6F00";
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        manager.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawLogicItem(context, this.box, this.linkColor, logicInputRadius);
    }
    getLinkInPos() {
        return {x: this.box.x+2, y: this.box.y+this.box.height/2};
    }
    getLinkOutPos() {
        return {x: this.box.x+this.box.width, y: this.box.y+this.box.height/2};
    }
    get select(){
        return showLogic;
    }
}

export class AlwaysTrue extends core.AlwaysTrue {
    constructor(name, x, y) {
        super(name);
        this.linkColor = "#00a7ff";
        this.box = new core.Box(x||0, y||0, 30, 30);
    }
    register(ce, m, rcm) {
        super.register(ce, m, rcm);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            rootLogicNode(context, this.box.center, this.box.width/2, this.linkColor);
    }
    get select(){
        return showLogic;
    }
}
export class RandomlyTrue extends core.RandomlyTrue {
    constructor(name, numer, denom, x, y) {
        super(name, numer, denom);
        this.linkColor = "#aaa";
        if(denom==3)
            this.linkColor = "#888";
        else if(denom==5)
            this.linkColor = "#666";
        this.box = new core.Box(x||0, y||0, 30, 30);
    }
    register(ce, m, rcm) {
        super.register(ce, m, rcm);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            rootLogicNode(context, this.box.center, this.box.width/2, this.linkColor);
    }
    get select(){
        return showLogic;
    }
}

export class ToggleFlag extends core.ToggleFlag {
    constructor(name, toNegate, builderX, builderY) {
        super(name, toNegate);
        this.box = new core.Box(builderX || 0, builderY || 280,
                20+logicInputRadius, 20);
        this.linkColor = "#ff00db";
    }
    register(ce, m) {
        super.register(ce, m);
        m.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details") && environment.get("builder logic"))
            drawLogicItem(context, this.box, this.linkColor, logicInputRadius);
    }
    getLinkInPos() {
        return {x: this.box.x+2, y: this.box.y+this.box.height/2};
    }
    getLinkOutPos() {
        return {x: this.box.x+this.box.width, y: this.box.y+this.box.height/2};
    }
    get select(){
        return showLogic;
    }
}
