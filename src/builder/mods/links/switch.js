//Copyright Matthew Pottage 2019.
import * as core from '../../../core/import-level-objects.js';

export class Switch extends core.Switch {
    getParams(state) {
        return [
            {name: "Default On", type: "checkbox", value: this.defaultOn}
        ];
    }
    setParam(name, value) {
        if(name=="Default On") {
            this.defaultOn = value;
            this._setDefaultState();
        }
        else
            throw new TypeError("Unknown parameter");
    }
}
