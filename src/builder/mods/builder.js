//Copyright Matthew Pottage 2019.
import * as core from '../../core/import-level-objects.js';
export class Group {
    constructor(objs) {
        this.objs = objs;
    }
    register(collisions, manager, caches) {
        this.objs.forEach(o => o.register(collisions, manager, caches));
    }
    reset() {
        this.objs.forEach(o => {
            o.reset()
            if(o instanceof core.VisualOnlyBackground
                || o instanceof core.VisualOnlyOverlay)
                o.builderGroup = this;
        });
    }
}
