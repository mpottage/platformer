//Copyright Matthew Pottage 2019.
import {getGridSize} from '../utility.js';
import * as core from '../../core/import-level-objects.js';

export class Spring extends core.Spring {
    getParams(state) {
        return [
            {name: "Power", type: "number", min: 1, max: 10, step: 1, value: this.springVal/100},
            {name: "Extension", type: "number", min: 5, max: 50,
                step: getGridSize(state.snapToGrid), value: this.springDiff}
        ];
    }
    setParam(name, value) {
        if(name=="Power")
            this.springVal = value*100;
        else if(name=="Extension") {
            this.springDiff = value;
            this._setConstraints();
        }
        else
            throw new TypeError("Unknown parameter.");
    }
}
