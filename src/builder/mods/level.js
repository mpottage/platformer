//Copyright Matthew Pottage 2019.
import * as core from '../../core/import-level-objects.js';
import {playerWidth, playerHeight} from '../../single/constants.js'

var positionColors = ["#00b0ff", "#66ff00", "#ffcc00", "#2b4d4d"];
//Gets the colour of start position for player i (returns "purple" if the player
//doesn't exist)
function getPosColor(i) {
    return positionColors[i] || "purple";
}
//Visual to preview a start position
export class Position {
    constructor(box, color) {
        this.box = box;
        this.color = color;
    }
    register(collisions, manager) {
        manager.addOverlay(this);
    }
    overlayDraw(context, caches, environment) {
        if(environment.get("builder details")) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= 0.5;
            context.fillStyle = this.color;
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
    }
}
//Creates visuals (and selectable objects to modify) player start positions.
export class StartPositions extends core.StartPositions {
    constructor(positions) {
        super(positions);
        this.visualPos = this.positions.map(function(p, i) {
            return new Position(new core.Box(p.x, p.y, playerWidth,
                        playerHeight), getPosColor(i))
        });
    }
    syncPositions(manager) {
        this.positions = [];
        this.visualPos.forEach(function(p, i) {
            this.positions.push({x: p.box.x, y: p.box.y});
            p.color = getPosColor(i);
        }, this);
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.visualPos.forEach(function(vp) {
            vp.register(collisions, manager, caches);
        });
    }
}
