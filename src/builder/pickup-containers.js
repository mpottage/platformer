//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js';
import {registerAll} from './utility.js';
import {Action} from './action.js';
import {getTopObj, getRootParent} from './filtering.js';

export function getAccepting(snapToGrid, game, x, y, testObj) {
    var obj = getTopObj(snapToGrid, game.manager, game.level, x, y,
            //Account for testObj and (if BouncingPickup) any contained pickup.
            obj => (obj!=testObj &&
                (!(testObj instanceof core.BouncingPickup) || testObj.pickup!=obj)));
    if((obj instanceof core.PickupBox || obj instanceof core.PickupSpawner)
            && obj.accepting(testObj))
        return obj;
    else
        return null;
}

//Get the action (if any given the position [x, y]) to put obj inside a pickup
//container
export function getAddAction(snapToGrid, game, x, y, obj) {
    var cont = getAccepting(snapToGrid, game, x, y, obj);
    if(cont instanceof core.PickupBox)
        return new AddPickupBoxObject(cont, obj);
    else if(cont instanceof core.PickupSpawner)
        return new AddPSpawnerObject(cont, obj);
    else
        return null;
}

export function getSpawnerToBox(snapToGrid, game, x, y) {
    var wooden = getTopObj(snapToGrid, game.manager, game.level, x, y);
    if(wooden instanceof core.PickupBox
            && x>=(wooden.box.x+10) && y>=(wooden.box.y+10)
            && x<=(wooden.box.x+wooden.box.width-10)
            && y<=(wooden.box.y+wooden.box.height-10))
        return wooden ;
    else
        return null;
}

//Put an object inside a pickup box.
class AddPickupBoxObject extends Action {
    constructor(pickupBox, newObj) {
        super();
        this.pickupBox = pickupBox;
        this.prevObj = null;
        this.newObj = newObj;
    }
    apply(game) {
        this.prevObj = this.pickupBox.obj;
        this.pickupBox.obj = this.newObj;
        registerAll(game);
    }
    revoke(game) {
        if(this.prevObj) {
            this.pickupBox.obj = this.prevObj;
            registerAll(game);
        }
    }
}

//Add an object to a pickup spawner.
class AddPSpawnerObject extends Action {
    constructor(spawner, newObj) {
        super();
        this.spawner = spawner;
        this.newObj = newObj;
        this.prevList = null;
    }
    _resetByParent(game) {
        var o = getRootParent(this.spawner, game.level.objs);
        if(o)
            o.reset();
        else
            this.spawner.reset();
    }
    apply(game) {
        var s = this.spawner;
        this.prevList = s.objs;
        s.objs = s.objs.slice(1, s.objs.length);
        s.objs.push(this.newObj);
        this._resetByParent(game);
        registerAll(game);
    }
    revoke(game) {
        if(this.prevList) {
            this.spawner.objs = this.prevList;
            this._resetByParent(game);
            registerAll(game);
        }
    }
}
