//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {round5} from './utility.js';
import {getNonBarrierStart, getVisibleChildren} from './filtering.js';

//Given a list of visible objects contained in a level, center them around a
//point (gameX, gameY), keeping their relative positions, and their boxes inside
//the level.
export function centerObjsAt(state, level, children, gameX, gameY) {
    //Center new objects at the click position.
    var box = getMinimumBox(children);
    var center = box.center;
    var offset = {x: Math.round(gameX-center.x), y: Math.round(gameY-center.y)};
    if(state.snapToGrid) {
        offset.x = round5(offset.x);
        offset.y = round5(offset.y);
    }
    //Ensure that the objects are not off any side of the level.
    if(box.x+offset.x<=0)
        offset.x = -box.x;
    else if(box.x+box.width+offset.x>=level.width)
        offset.x = level.width-box.x-box.width;
    if(box.y+offset.y<=0)
        offset.y = -box.y;
    else if(box.y+box.height+offset.y>=level.height)
        offset.y = level.height-box.y-box.height;
    //Apply offset.
    children.forEach(function(o) {
        translateObj(o, offset.x, offset.y);
    });
}

//Get the smallest box surrounding all the items supplied.
export function getMinimumBox(objs, start=0) {
    var minX = Infinity;
    var maxX = 0;
    var minY = Infinity;
    var maxY = 0;
    for(var i=start; i<objs.length; ++i) {
        var obj = objs[i];
        if(!obj.box &&
                (obj instanceof core.ApplyFlag || obj instanceof core.MakeFlagFrom
                 || obj instanceof core.MakeAndApplyFlag))
            obj = obj.obj;
        if(obj instanceof core.Laser)
            obj = obj.follow;
        //Special case with groups
        if(obj instanceof mods.Group) {
            let box = getMinimumBox(obj.objs);
            minX = Math.min(minX, box.x);
            maxX = Math.max(maxX, box.x+box.width);
            minY = Math.min(minY, box.y);
            maxY = Math.max(maxY, box.y+box.height);
        }
        else if(obj.box) {
            //Test all possible extents of an object. See translateObj.
            if(!obj.startPos || obj.endPos) { //Ignore free moving enemies
                minX = Math.min(minX, obj.box.x);
                maxX = Math.max(maxX, obj.box.x+obj.box.width);
                if(!(obj instanceof core.CollapsingPlatform) || !obj.collapsed) {
                    minY = Math.min(minY, obj.box.y);
                    maxY = Math.max(maxY, obj.box.y+obj.box.height);
                }
            }
            if(obj.startPos) {
                minX = Math.min(minX, obj.startPos.x);
                maxX = Math.max(maxX, obj.startPos.x+obj.box.width);
                minY = Math.min(minY, obj.startPos.y);
                if(!(obj instanceof core.BadTriangle))
                    maxY = Math.max(maxY, obj.startPos.y+obj.box.height);
            }
            if(obj.endPos) {
                minX = Math.min(minX, obj.endPos.x);
                maxX = Math.max(maxX, obj.endPos.x+obj.box.width);
                minY = Math.min(minY, obj.endPos.y);
                if(!(obj instanceof core.BadTriangle))
                    maxY = Math.max(maxY, obj.endPos.y+obj.box.height);
            }
            if(obj.startX!==undefined) {
                minX = Math.min(minX, obj.startX);
                maxX = Math.max(maxX, obj.startX+obj.box.width);
            }
            if(obj.startY!==undefined) {
                minY = Math.min(minY, obj.startY);
                maxY = Math.max(maxY, obj.startY+obj.box.height);
            }
            if(obj.frames)
                obj.frames.forEach(function(f) {
                    minX = Math.min(minX, f.box.x);
                    maxX = Math.max(maxX, f.box.x+f.box.width);
                    minY = Math.min(minY, f.box.y);
                    maxY = Math.max(maxY, f.box.y+f.box.height);
                });
        }
    }
    return new core.Box(minX, minY, maxX-minX, maxY-minY);
}

//Returns the y-coordinate of the edge below (x, y), at most dist distance below
//(x,y). Returns y if no edge is found.
export function getEdgeYBelow(x, y, collisions, dist, levelHeight) {
    if(dist<=0)
        throw new Error("Bad edge check, dist="+dist);
    var res = collisions.rayTrace(x, y, x, y+dist);
    var closest = res[0];
    for(var i=1; i<res.length; ++i)
        if(!(closest instanceof core.Platform) ||
                ((res[i] instanceof core.Platform) && res[i].box.y-y<closest.box.y-y))
            closest = res[i];
    if(closest instanceof core.Platform)
        return closest.box.y;
    else
        return Math.min(y+dist, levelHeight);
}

//Moves obj by vector (dx, dy)
//  > Adjusts start/end positions of any movement parameters when needed..
export function translateObj(obj, dx, dy) {
    obj.box.x += dx;
    obj.box.y += dy;
    //Adjust starting position (if any)
    if(obj.startPos) {
        obj.startPos.x += dx;
        obj.startPos.y += dy;
        if(obj.teleporter)
            obj.teleporter.setDefault(obj.startPos.x, obj.startPos.y);
    }
    //Adjust movement end position (if any)
    if(obj.endPos) {
        obj.endPos.x += dx;
        obj.endPos.y += dy;
    }
    //Adjust movement parameters for LR and UD routes
    if(obj.alternateStartX!==undefined)
        obj.alternateStartX += dx;
    if(obj.alternateStartY!==undefined)
        obj.alternateStartY += dy;
    //Adjust constraints for objects that move themselves to default positions
    //(ToggleButton, Spring, ...)
    if(obj.startX!==undefined)
        obj.startX += dx;
    if(obj.startY!==undefined)
        obj.startY += dy;
    if(obj.frames) {
        obj.frames.forEach(function(f) {
            translateObj(f, dx, dy);
        });
    }
    //Move triangle (BadTriangle, OrangeTriangle, ...)
    if(obj.triangle)
        obj.triangle.translate(dx, dy);
    //Move spawned objects that depend on the parents position
    //  (WoodenBox, PickupSpawner, ...)
    if(obj.spawned && !obj.spawned.motion)
        translateObj(obj.spawned, dx, dy);
    //Move scanner so that it reflects the object's new position
    if(obj.scanner)
        translateObj(obj.scanner, dx, dy);
    //Move child object relative to the parent (CuttingLaser, ...)
    if(obj.follow)
        translateObj(obj.follow, dx, dy);
}

//Move every object (excluding default barriers) in a level
export function translateAll(level, manager, offsetX, offsetY) {
    var i = getNonBarrierStart(level);
    for(; i<level.objs.length; ++i) {
        if(level.objs[i] instanceof core.ApplyFlag
                || level.objs[i] instanceof core.MakeFlagFrom
                || level.objs[i] instanceof core.MakeAndApplyFlag)
            translateObj(level.objs[i].obj, offsetX, offsetY);
        else if(level.objs[i] instanceof mods.StartPositions) {
            level.objs[i].visualPos.forEach(function(p) {
                translateObj(p, offsetX, offsetY)
            });
            level.objs[i].syncPositions(manager);
        }
        else if(level.objs[i].box)
            translateObj(level.objs[i], offsetX, offsetY);
        else if(level.objs[i] instanceof mods.Group)
            getVisibleChildren([level.objs[i]]).forEach(o => translateObj(o, offsetX, offsetY));
    }
}
