//Copyright Matthew Pottage 2019-2020.
import * as core from '../core/import-level-objects.js'
import {clipLevelX, clipLevelY} from './utility.js';

export function resetObjs(game) {
    var prev = {x: game.player.box.x, y: game.player.box.y,
        health: game.player.health.value};
    game.reset();
    //Move player back to previous position (if not obstructed).
    game.player.teleporter.toAuto(prev.x, prev.y);
    [game.player.box.x, game.player.box.y] = [prev.x, prev.y];
    if(isNaN(prev.health) || Math.abs(prev.health)==Infinity)
        game.player.health.increase(Infinity);
}

export function revealAllCovers(level, gameRunning) {
    if(gameRunning) {
        level.objs.forEach(function(obj) {
            if(obj instanceof core.Cover)
                obj.fading = true;
            else if(obj instanceof core.Darkness
                || obj instanceof core.LighteningEffect)
                obj.builderShow = false;
            else if(obj instanceof core.ApplyFlag
                || obj instanceof core.MakeFlagFrom
                || obj instanceof core.MakeAndApplyFlag) {
                if(obj.obj instanceof core.Cover)
                    obj.obj.fading = true;
                else if(obj.obj instanceof core.Darkness
                    || obj.obj instanceof core.LighteningEffect)
                    obj.obj.builderShow = false;
            }
        });
    }
    else {
        level.objs.forEach(function(obj) {
            if(obj instanceof core.Cover) {
                obj.disabled = true;
                obj.opacity = 0;
            }
            else if(obj instanceof core.Darkness
                || obj instanceof core.LighteningEffect)
                obj.builderShow = false;
            else if(obj instanceof core.ApplyFlag
                || obj instanceof core.MakeFlagFrom
                || obj instanceof core.MakeAndApplyFlag) {
                if(obj.obj instanceof core.Cover) {
                    obj.obj.disabled = true;
                    obj.obj.opacity = 0;
                }
                else if(obj.obj instanceof core.Darkness
                    || obj.obj instanceof core.LighteningEffect)
                    obj.obj.builderShow = false;
            }
        });
    }
}

export function movePlayer(game, gameX, gameY) {
    game.player.box.x = clipLevelX(game.level, gameX - game.player.box.width/2);
    game.player.box.y = clipLevelY(game.level, gameY) - game.player.box.height/2;

    //Ensure that player doesn't intersect right level barrier
    //(left is handled by clipLevelX).
    if(game.player.box.x+game.player.box.width >= game.level.width)
        game.player.box.x = game.level.width-game.player.box.width;

    //Prevent teleporter changing position.
    game.player.teleporter.cancel();

    game.collisions.protect(game.player);
}
