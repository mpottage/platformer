//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js';
import * as mods from './mods/import-level-objects.js';
import {Operation} from './operation.js';
import {round5, getMinResize, getScale} from './utility.js';
import {getNonBarrierStart} from './filtering.js';
import {ActionGroup} from './action.js';
import {DeleteObject, AddObject} from './add-delete-actions.js';
import {highlightBox} from './visuals.js';

export class CutoutOperation extends Operation {
    constructor() {
        super();
        this.corner1 = null;
        this.corner2 = null;
    }
    press(state, game, gX, gY) {
        this.corner1 = this.corner2 = {x: gX, y: gY};
        if(state.snapToGrid) {
            this.corner1.x = round5(this.corner1.x);
            this.corner1.y = round5(this.corner1.y);
        }
        return this;
    }
    move(state, game, gX, gY) {
        this.corner2 = {x: gX, y: gY};
        if(state.snapToGrid) {
            this.corner2.x = round5(this.corner2.x);
            this.corner2.y = round5(this.corner2.y);
        }
        return this;
    }
    release(state, game) {
        var box = new core.Box(Math.min(this.corner1.x, this.corner2.x),
            Math.min(this.corner1.y, this.corner2.y),
            Math.abs(this.corner1.x-this.corner2.x),
            Math.abs(this.corner1.y-this.corner2.y));
        //Get all objects which can be cut (most platforms which don't generate
        //flags).
        var roots = game.level.objs.slice(getNonBarrierStart(game.level)).filter(function(obj) {
            if(obj instanceof core.ApplyFlag)
                obj = obj.obj;
            return (obj.constructor===core.Platform
                || obj instanceof core.JumpThroughPlatform
                || obj instanceof core.DownThroughPlatform
                || obj instanceof core.RightThroughPlatform
                || obj instanceof core.LeftThroughPlatform
                || obj instanceof core.CollapsingPlatform
                || obj instanceof core.IcePlatform || obj instanceof core.Destructible
                || obj instanceof core.HealthLoss || obj instanceof core.LRPlatform
                || obj instanceof core.UDPlatform || obj instanceof core.FadingPlatform)
                    && core.Box.isIntersection(obj.box, box);
            });
        //Strip away ApplyFlag.
        var platforms = roots.map(function(obj) {
            if(obj instanceof core.ApplyFlag)
                return obj.obj;
            else
                return obj;
        });
        var minSize = getMinResize(state.snapToGrid);
        var newBoxes = platforms.map(function(obj) {
            //Prevent overlapping moving platforms to avoid collisions errors.
            if(obj instanceof core.LRPlatform || obj instanceof core.UDPlatform)
                return splitBox(obj.box, box, 0);
            else
                return splitBox(obj.box, box, minSize/2);
        });
        var actions = [];
        for(var i=0; i<platforms.length; ++i) {
            //Remove old object.
            actions.push(new DeleteObject(roots[i]));
            newBoxes[i].forEach(function(b) {
                if(b.width>=minSize && b.height>=minSize) {
                    var newObj = clonePlatformWBox(platforms[i], b);
                    //Apply flags where necessary.
                    if(roots[i] instanceof core.ApplyFlag)
                        newObj = new core.ApplyFlag(roots[i].flag, newObj);
                    actions.push(new AddObject(newObj));
                }
            });
        }
        var aGroup = new ActionGroup(actions);
        aGroup.apply(game);
        state.actionHistory.push(aGroup);
    }
    draw(state, game, context) {
        highlightBox(context,
            new core.Box(Math.min(this.corner1.x, this.corner2.x),
                Math.min(this.corner1.y, this.corner2.y),
                Math.abs(this.corner1.x-this.corner2.x),
                Math.abs(this.corner1.y-this.corner2.y)),
            "red", getScale(game));
    }
}

//Returns the equivalent of platform at its default position, but with its
//default box changed to box.
//  Any movement paths are offset LR/UD so they are in sync when moving.
function clonePlatformWBox(platform, box) {
    if(platform.constructor===core.Platform)
        return new core.Platform(box, platform.color);
    else if(platform.constructor===core.JumpThroughPlatform)
        return new core.JumpThroughPlatform(box, platform.color);
    else if(platform.constructor===core.DownThroughPlatform)
        return new core.DownThroughPlatform(box, platform.color);
    else if(platform.constructor===core.RightThroughPlatform)
        return new core.RightThroughPlatform(box, platform.color);
    else if(platform.constructor===core.LeftThroughPlatform)
        return new core.LeftThroughPlatform(box, platform.color);
    else if(platform.constructor===core.IcePlatform)
        return new core.IcePlatform(box, platform.color);
    else if(platform.constructor===core.ThinIcePlatform)
        return new core.ThinIcePlatform(box, platform.color);
    else if(platform.constructor===mods.CollapsingPlatform)
        return new mods.CollapsingPlatform(box, platform.color);
    else if(platform.constructor instanceof core.Destructible)
        return new core.Destructible(box, platform.color);
    else if(platform.constructor===core.HealthLoss)
        return new core.HealthLoss(box, platform.color);
    else if(platform instanceof core.LRPlatform) {
        var dx = box.x-platform.box.x;
        box.x = platform.alternateStartX+dx;
        return new mods.LRPlatform(box,
            platform.startPos.x+dx,
            platform.endPos.x+dx,
            platform.color, platform.xAccel);
    }
    else if(platform instanceof core.UDPlatform) {
        var dy = box.y-platform.box.y;
        box.y = platform.alternateStartY+dy;
        return new mods.UDPlatform(box,
            platform.startPos.y+dy,
            platform.endPos.y+dy, platform.color,
            platform.yAccel);
    }
    else if(platform instanceof core.FadingPlatform)
        return new mods.FadingPlatform(box,
            platform.color);
    else
        throw new TypeError("Unexpected type of platform.");
}

function restrictBox(box, restrictor) {
    var oldEndX = box.x+box.width;
    var oldEndY = box.y+box.height;
    box.x = Math.max(box.x, restrictor.x);
    box.y = Math.max(box.y, restrictor.y);
    box.width = Math.min(oldEndX, restrictor.x+restrictor.width)-box.x;
    box.height = Math.min(oldEndY, restrictor.y+restrictor.height)-box.y;
    return box;
}
function splitBox(box, delBox, padding) {
    if(!core.Box.isIntersection(delBox, box))
        return box;
    var insideBox = restrictBox(new core.Box(delBox.x, delBox.y, delBox.width, delBox.height), box);
    var boxes = [];
    if(insideBox.x>box.x)
        boxes.push(new core.Box(box.x, box.y, insideBox.x-box.x, box.height));
    if(insideBox.y>box.y)
        boxes.push(new core.Box(insideBox.x-padding, box.y,
            insideBox.width+2*padding, insideBox.y-box.y));
    var endInsideX = insideBox.x+insideBox.width;
    var endX = box.x+box.width;
    if(endInsideX<endX)
        boxes.push(new core.Box(endInsideX, box.y, endX-endInsideX, box.height));
    var endInsideY = insideBox.y+insideBox.height;
    var endY = box.y+box.height;
    if(endInsideY<endY)
        boxes.push(new core.Box(insideBox.x-padding, endInsideY,
            insideBox.width+2*padding, endY-endInsideY));
    boxes.forEach(b => restrictBox(b, box));
    return boxes;
}
