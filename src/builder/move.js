//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js';
import * as pickupContainers from './pickup-containers.js';
import * as mods from './mods/import-level-objects.js';
import {clipLevelX, clipLevelY, getScale, round5} from './utility.js';
import {getTopObj, getRootParent, getNonBarrierStart, getVisibleChildren} from './filtering.js';
import {highlightBox, selectHighlight, hoverHighlight} from './visuals.js';
import {Action, ActionGroup} from './action.js';
import {Operation} from './operation.js';
import {DeleteObject} from './add-delete-actions.js';
import {translateObj, translateAll} from './positioning-utilities.js';

//Move an object. Supports any object with a box.  Either pass the object to
//move into the constructor or call press after creating the operation.
export class MoveOperation extends Operation {
    //obj: Any object with a box.
    //altOrigin: Position mouse/touch to move the object started from (if you
    //passed obj to the constructor).
    constructor(next, obj, altOrigin) {
        super(next);
        if(obj && obj.box) {
            this.obj = obj;
            this.origin = altOrigin;
        }
        else {
            this.obj = null;
            this.origin = {x: 0, y: 0};
        }
        this.offset = {x: 0, y: 0};
    }
    //Get object that should be moved, if obj isn't the right one
    static wrapNested(obj, game) {
        //Select the BouncingPickup wrapper around a Pickup if obj is a Pickup
        //and nested inside a BouncingPickup, to move start position correctly.
        if(obj instanceof core.Pickup) {
            var root = getRootParent(obj, game.level.objs);
            if(root instanceof core.BouncingPickup)
                return root;
            else if(root instanceof core.MakeFlagFrom
                    && root.obj instanceof core.BouncingPickup)
                return root.obj;
        }
        return obj;
    }
    //Set object to move (if it hasn't been set yet) based on click position.
    press(state, game, gameX, gameY) {
        if(this.obj)
            return this;
        var gameX = clipLevelX(game.level, gameX);
        var gameY = clipLevelY(game.level, gameY);
        var obj = getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY);
        var root = getRootParent(obj, game.level.objs);
        if(root instanceof mods.Group) {
            return new MoveMultiple(this.next, getVisibleChildren(root.objs), {x: gameX, y: gameY});
        }
        else {
            //Get object to move.
            this.obj = MoveOperation.wrapNested(obj, game);
            //Start moving.
            if(this.obj) {
                this.origin = {x: gameX, y: gameY}; //Position to offset moving.
                return this;
            }
            else { //Not found an object, end operation.
                return this.next;
            }
        }
    }
    release(state, game) {
        var action = null;
        if(this.offset.x!=0 || this.offset.y!=0) {
            //End operation, adding history entry.
            action = new MoveAction(this.obj, this.offset.x, this.offset.y);
        }
        //Moving a Pickup or BouncingPickup into a PickupBox or PickupSpawner.
        if((this.obj instanceof core.Pickup) || (this.obj instanceof core.BouncingPickup)) {
            var extraAction = pickupContainers.getAddAction(state.snapToGrid,
                game, this.origin.x+this.offset.x, this.origin.y+this.offset.y,
                this.obj);
            if(extraAction) {
                if(this.obj instanceof core.Pickup)
                    action = new ActionGroup([
                            new AbsMovePickup(this.obj, this.offset.x, this.offset.y),
                            new DeleteObject(this.obj), extraAction]);
                else //Need to adjust this.obj.startPos correctly.
                    action = new ActionGroup([
                            new AbsMoveBouncing(this.obj, this.offset.x, this.offset.y),
                            new DeleteObject(this.obj), extraAction]);
                action.apply(game);
            }
        }
        else if(action && this.obj instanceof mods.Position) {
            var parent = getRootParent(this.obj, game.level.objs);
            if(parent instanceof mods.StartPositions) {
                action = new MoveStartPosition(parent, action);
                parent.syncPositions(game.manager);
            }
        }
        if(action)
            state.actionHistory.push(action);
        return this.next;
    }
    move(state, game, gameX, gameY) {
        //Moving the object based on the pointer's offset from the position it
        //started dragging the object from.
        gameX = clipLevelX(game.level, gameX);
        gameY = clipLevelY(game.level, gameY);
        var offsetX = gameX-this.origin.x;
        var offsetY = gameY-this.origin.y;
        if(state.snapToGrid) {
            offsetX = round5(offsetX);
            offsetY = round5(offsetY);
        }
        translateObj(this.obj, offsetX-this.offset.x,
                offsetY-this.offset.y);
        this.offset.x = offsetX;
        this.offset.y = offsetY;
        return this;
    }
    getHover(state, game) {
        //See release(state, game).
        if((this.obj instanceof core.Pickup) || (this.obj instanceof core.BouncingPickup))
            return pickupContainers.getAccepting(state.snapToGrid, game,
                this.origin.x+this.offset.x, this.origin.y+this.offset.y,
                this.obj);
        else
            return null;
    }
    draw(state, game, context) {
        highlightBox(context, this.obj.box, selectHighlight, getScale(game));
        var hover = this.getHover(state, game);
        if(hover)
            highlightBox(context, hover.box, hoverHighlight, getScale(game));
    }
}

export class MoveMultiple extends Operation {
    constructor(next, objs, altOrigin) {
        super(next);
        this.objs = objs;
        this.origin = altOrigin || {x: 0, y: 0};
        this.offset = {x: 0, y: 0};
    }
    move(state, game, gX, gY) {
        var newOffsetX = gX-this.origin.x;
        var newOffsetY = gY-this.origin.y;
        if(state.snapToGrid) {
            newOffsetX = round5(newOffsetX);
            newOffsetY = round5(newOffsetY);
        }
        this.objs.forEach(function(o) {
            translateObj(o, newOffsetX-this.offset.x, newOffsetY-this.offset.y);
        }, this);
        this.offset.x = newOffsetX;
        this.offset.y = newOffsetY;
        return this;
    }
    draw(state, game, context) {
        this.objs.forEach(function(o) {
            highlightBox(context, o.box, selectHighlight, getScale(game));
        });
    }
    release(state, game) {
        if(this.offset.x!=0 || this.offset.y!=0) {
            state.actionHistory.push(new ActionGroup(this.objs.map(function(o) {
                return new MoveAction(o, this.offset.x,  this.offset.y);
            }, this)));
        }
        return this.next;
    }
}

//Moved an object.
class MoveAction extends Action {
    // obj: Object moved.
    // offsetX/Y: Offset moved.
    constructor(obj, offsetX, offsetY) {
        super();
        this.obj = obj;
        this.offset = {x: offsetX, y: offsetY};
    }
    apply(game) {
        translateObj(this.obj, this.offset.x, this.offset.y);
    }
    revoke(game) {
        translateObj(this.obj, -this.offset.x, -this.offset.y);
    }
}

class MoveStartPosition extends Action {
    //obj: instanceof BuilderStartPositions.
    //moveAction: Action moving the start position.
    constructor(obj, moveAction) {
        super();
        this.obj = obj;
        this.action = moveAction;
    }
    apply(game) {
        this.obj.syncPositions(game.manager);
        this.action.apply(game);
    }
    revoke(game) {
        this.action.revoke(game);
        this.obj.syncPositions(game.manager);
    }
}

//Move Pickup absolutely rather than (above) relative to previous position.
//  Used when moving a pickup into a pickup container
class AbsMovePickup extends Action {
    constructor(obj, appliedOffsetX, appliedOffsetY) {
        if(!(obj instanceof core.Pickup))
            throw new TypeError("Pickup required.");
        super();
        this.obj = obj;
        this.newPos = {x: this.obj.box.x, y: this.obj.box.y};
        this.oldPos = {x: this.obj.box.x-appliedOffsetX,
            y: this.obj.box.y-appliedOffsetY};
    }
    apply(game) {
        this.obj.box.x = this.newPos.x;
        this.obj.box.y = this.newPos.y;
        this.obj.reset();
    }
    revoke(game) {
        this.obj.box.x = this.oldPos.x;
        this.obj.box.y = this.oldPos.y;
        this.obj.reset();
    }
}

//Variation on AbsMovePickup for a bouncing pickup.
class AbsMoveBouncing extends Action {
    constructor(obj, appliedOffsetX, appliedOffsetY) {
        if(!(obj instanceof core.BouncingPickup))
            throw new TypeError("BouncingPickup required.");
        super();
        this.obj = obj;
        this.newStart = {x: this.obj.startPos.x, y: this.obj.startPos.y};
        this.oldStart = {x: this.obj.startPos.x-appliedOffsetX,
            y: this.obj.startPos.y-appliedOffsetY};
    }
    apply(game) {
        this.obj.startPos.x = this.newStart.x;
        this.obj.startPos.y = this.newStart.y;
        this.obj.reset();
    }
    revoke(game) {
        this.obj.startPos.x = this.oldStart.x;
        this.obj.startPos.y = this.oldStart.y;
        this.obj.reset();
    }
}

//Move every object in a level (excluding default barriers)
export class MoveAllAction extends Action {
    constructor(level, offsetX, offsetY) {
        super();
        this.offset = {x: offsetX, y: offsetY};
        this.level = level;
    }
    apply(game) {
        translateAll(this.level, game.manager, this.offset.x, this.offset.y);
        translateObj(game.player, this.offset.x, this.offset.y);
    }
    revoke(game) {
        translateAll(this.level, game.manager, -this.offset.x, -this.offset.y);
        translateObj(game.player, -this.offset.x, -this.offset.y);
    }
}
