//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {Operation} from './operation.js';
import {registerAll, getUniqueId, getScale} from './utility.js';
import {Action, ActionGroup} from './action.js';
import {ReplaceObject} from './add-delete-actions.js';
import {getTopObj, getRootParent} from './filtering.js';
import {getFlagsAndControls, getFlagNames} from './connections.js';
import {highlightBox, hoverHighlight, selectHighlight} from './visuals.js';

//Link mode. Create a logic link between two objects or delete all incoming
//links on an object. Requires "press" to be called on creation.
export class AdjustLinks extends Operation {
    constructor(next) {
        super(next);
        this.outputObj = null;
        //Action to replace an object to get a flag output (not always used).
        this.replaceForFlag = null;
        this.pos = {x: 0, y: 0};
    }
    //Gets the object which the link operation would start at, given a click at
    //(gameX, gameY) in game.
    // This includes all objects with health (inc NPCs and wooden boxes).
    static getStart(state, game, gameX, gameY) {
        return getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY,
                function(obj, container) {
                    var o = container || obj;
                    return hasFlagOutput(game, o) || (o instanceof core.ApplyFlag)
                        || (o instanceof core.MakeAndApplyFlag) || canMakeFlag(o);
                })
    }
    press(state, game, gameX, gameY) {
        //Objects which have a flag output.
        var obj = AdjustLinks.getStart(state, game, gameX, gameY);
        //Might have an object nested in a control, so get the control.
        obj = getRootParent(obj, game.level.objs) || obj;
        if(obj) {
            //Set inital link end position
            this.pos.x = gameX;
            this.pos.y = gameY;
            //Normal, got output flag, so look for object to link to when the
            //mouse is released.
            if(obj.name) {
                this.outputObj = obj;
                return this;
            }
            //Need to convert an object to have an output flag.
            else if(canMakeFlag(obj)) {
                this.outputObj = new mods.MakeFlagFrom(getUniqueId("Obj", getFlagNames(game)), obj);
                this.replaceForFlag = new ReplaceObject(obj, this.outputObj);
                this.replaceForFlag.apply(game);
                return this;
            }
            //No object immediate with an output flag or which can have one
            //attached to the root found, look for an object which already has a
            //flag applied to it (just ApplyFlag).
            else if(obj instanceof core.ApplyFlag) {
                if(hasFlagOutput(game, obj.obj)) { //We can link directly from this object.
                    this.outputObj = obj.obj;
                    return this;
                }
                else if(canMakeFlag(obj.obj)) { //Object needs conversion before linking.
                    this.outputObj = new mods.MakeAndApplyFlag(getUniqueId("Obj", getFlagNames(game)),
                            obj.flag, obj.obj);
                    this.replaceForFlag = new ReplaceObject(obj, this.outputObj);
                    this.replaceForFlag.apply(game);
                    return this;
                }
                else { //Can't link from, so clear flags.
                    var action = new ClearInputFlags(obj);
                    action.apply(game);
                    state.actionHistory.push(action);
                    return this.next;
                }
            }
        }
        return this.next;
    }
    move(state, game, gameX, gameY) {
        //Remember last position of point (used to decide object to link to).
        this.pos.x = gameX;
        this.pos.y = gameY;
        return this;
    }
    //Get object that the operation if it would end at (posX, posY) would
    //link to (or clear the links on if necessary).
    static getEnd(snapToGrid, game, posX, posY) {
        return getTopObj(snapToGrid, game.manager, game.level, posX, posY,
                function(obj, cont) {
                    if(cont instanceof core.MakeFlagFrom)
                        return obj.activated!==undefined;
                    else {
                        var o = cont || obj;
                        return o.activated!==undefined || o.watchedChanged || (obj instanceof core.RelayFlag);
                    }
            });
    }
    release(state, game) {
        var flagsDetails = getFlagsAndControls(game.level.objs);
        var target = AdjustLinks.getEnd(state.snapToGrid, game, this.pos.x, this.pos.y);
        if(target) { //Link ended on an object
            //Accessed nested object that isn't handling the flag
            if(!target.watchedChanged)
                target = getRootParent(target, game.level.objs) || target;
            var action = null;
            //Link to a different object.
            if(target!==this.outputObj
                    && (!(target instanceof core.ApplyFlag) || target.obj!==this.outputObj)) {
                if(target.activated!==undefined) {
                    //Need to wrap with ApplyFlag for flags to affect it or
                    //change flag on ApplyFlag already wrapping it.
                    action = new ReplaceObject(target,
                            new mods.ApplyFlag(this.outputObj.name, target));
                }
                //Need to support both an input and output flag.
                else if(target instanceof core.MakeFlagFrom) {
                    var replacement =
                                new mods.MakeAndApplyFlag(target.name,
                                        this.outputObj.name, target.obj);
                    action = new ActionGroup([
                            new ReplaceObject(target, replacement),
                            new AddLogicLink(replacement, this.outputObj.name)
                    ]);
                }
                //Normal add link, but check that this.outputObj doesn't already
                //link to target.
                else if(target.watchedChanged
                        && flagsDetails[this.outputObj.name].users.indexOf(target)==-1) {
                    action = new AddLogicLink(target, this.outputObj.name);
                }
                if(action && foundCycle(flagsDetails, target, this.outputObj)) {
                    var root = getRootParent(this.outputObj, game.level.objs)
                        || this.outputObj; //May be in an ApplyFlag.
                    action = new ActionGroup([action, new ClearInputFlags(root)]);
                }
            }
            //Clicked on the same object, disconnect any inputs.
            else if((target instanceof core.ApplyFlag) || (target instanceof core.MakeAndApplyFlag)
                    || (target.watchedChanged && flagsDetails[target.name].linkedTo)) {
                action = new ClearInputFlags(target);
            }
        }
        if(action) {
            action.apply(game);
            if(this.replaceForFlag) //Record replacement.
                action = new ActionGroup([this.replaceForFlag, action]);
            state.actionHistory.push(action);
        }
        else if(this.replaceForFlag) {
            this.replaceForFlag.revoke(game);
        }
        return this.next;
    }
    //Gets the object for highlighting and BuilderDrawLinks to draw the link
    //from.
    getSelected() {
        if(this.outputObj instanceof core.MakeFlagFrom
                || this.outputObj instanceof core.MakeAndApplyFlag)
            return this.outputObj.obj;
        else if(this.outputObj instanceof core.TriggerLaser)
            return this.outputObj.follow;
        else
            return this.outputObj;
    }
    draw(state, game, context) {
        var selected = this.getSelected();
        if(selected)
            highlightBox(context, selected.box, selectHighlight,
                getScale(game));
        //See release().
        var hover = AdjustLinks.getEnd(state.snapToGrid, game, this.pos.x, this.pos.y);
        if(hover)
            highlightBox(context, hover.box, hoverHighlight, getScale(game));
        //Show line emitting from outputObj, like a standard one from DrawLinks,
        //but not connected to a second object.
        var obj = this.outputObj;
        if(obj instanceof core.ApplyFlag || obj instanceof core.MakeFlagFrom
            || obj instanceof core.MakeAndApplyFlag)
            obj = obj.obj;
        var from;
        if(this.outputObj.getLinkOutPos)
            from = this.outputObj.getLinkOutPos();
        else if(obj.getLinkCenter)
            from = obj.getLinkCenter();
        else
            from = obj.box.center;
        if(from) {
            context.beginPath();
            context.strokeStyle = obj.linkColor || obj.color || "black";
            context.lineWidth = 2;
            context.moveTo(from.x, from.y);
            context.lineTo(this.pos.x, this.pos.y);
            context.stroke();
        }
    }
}

export function hasFlagOutput(game, obj) {
    var flagsDetails = getFlagsAndControls(game.level.objs);
    return obj.name && obj.name in flagsDetails && flagsDetails[obj.name].owner==obj;
}
//Stops any supplied users from following a flag.
export class CleanUsers extends Action {
    //flagName: Flag to stop its users using it.
    //users: Users of the flag.
    constructor(flagName, users) {
        super();
        this.flag = flagName;
        this.users = users;
        this.extraActions = null; //Extra actions used to clean up.
    }
    apply(game) {
        var appliedActions = [];
        this.users.forEach(function(obj) {
            if(obj instanceof core.ApplyFlag)
                appliedActions.push(new ReplaceObject(obj, obj.obj));
            else if(obj instanceof core.MakeAndApplyFlag)
                appliedActions.push(new ReplaceObject(obj, new mods.MakeFlagFrom(obj.name, obj.obj)));
            else if(obj instanceof core.RelayFlag || obj instanceof core.NegateFlag
                    || obj instanceof core.FlagBeenTrue || obj instanceof core.Timeout
                    || obj instanceof core.ToggleFlag)
                obj.tracking = "";
            else if(obj instanceof core.OrFlags || obj instanceof core.AndFlags
                    || obj instanceof core.HoldAnd) {
                var i = obj.flagNames.indexOf(this.flag);
                if(i!=-1)
                    obj.flagNames.splice(i, 1);
            }
            else
                throw new TypeError("AddLogicLink: Unsupported object");
        }, this);
        this.extraActions = new ActionGroup(appliedActions);
        this.extraActions.apply(game);
        registerAll(game); //update flags details.
    }
    revoke(game) {
        if(this.extraActions) {
            this.extraActions.revoke(game);
            //Only need to add flag back to "orFlags".
            this.users.forEach(function(obj) {
                if(obj instanceof core.RelayFlag || obj instanceof core.NegateFlag
                    || obj instanceof core.FlagBeenTrue || obj instanceof core.Timeout
                    || obj instanceof core.ToggleFlag)
                    obj.tracking = this.flag;
                else if((obj instanceof core.OrFlags || obj instanceof core.AndFlags
                        || obj instanceof core.HoldAnd)
                        && obj.flagNames.indexOf(this.flag)==-1)
                    obj.flagNames.push(this.flag);
            }, this);
            registerAll(game);
        }
    }
}

//Replaces any MakeFlagFrom and MakeAndApplyFlag objects with their nested
//objects if the flag made by the parent object is no longer in use.  This
//prevents unnecessary clutter in the level code.
export class CleanFlagReplacements extends Action {
    constructor() {
        super();
        this.done = null;
    }
    apply(game) {
        var actions = [];
        var flagsDetails = getFlagsAndControls(game.level.objs);
        for(var f in flagsDetails) {
            if(flagsDetails[f].owner instanceof core.MakeFlagFrom
                    && flagsDetails[f].users.length==0) {
                var obj = flagsDetails[f].owner;
                actions.push(new ReplaceObject(obj, obj.obj));
            }
            else if(flagsDetails[f].owner instanceof core.MakeAndApplyFlag
                    && flagsDetails[f].users.length==0) {
                var obj = flagsDetails[f].owner;
                actions.push(new ReplaceObject(obj, new mods.ApplyFlag(obj.flag, obj.obj)));
            }
        }
        this.done = new ActionGroup(actions);
        this.done.apply(game);
    }
    revoke(game) {
        if(this.done)
            this.done.revoke(game);
    }
}

//Can a flag be created from obj, where obj doesn't already have a flag output.
function canMakeFlag(obj) {
    return (obj.getOutputVal)
        || (obj instanceof core.Pickup) || (obj instanceof core.BouncingPickup)
        || (obj.health!==undefined) || (obj instanceof core.Destructible)
        || (obj instanceof core.GreySquare);

}

//Make an object (which watches a flag) watch a new flag.
//  Handles swapping an old flag for a new one if the object only watches one
//  flag, or adding an additional flag to follow when applied to something like AndFlags.
//
//  Should there be any unused flags created by MakeFlagFrom or MakeAndApplyFlag
//  as a result of this action it will remove the wrappers from the objects they
//  were created on.
class AddLogicLink extends Action {
    constructor(obj, flagName) {
        super();
        if(!(obj.watchedChanged))
            throw new TypeError("Require an object that watches a flag.");
        this.obj = obj;
        this.flag = flagName;
        //Not always used (only when adding a link mandates replacing another).
        this.prevFlag = "";
        this.cleanLinks = null; //See CleanFlagReplacements.
    }
    apply(game) {
        if((this.obj instanceof core.ApplyFlag) || (this.obj instanceof core.MakeAndApplyFlag)) {
            this.prevFlag = this.obj.flag;
            this.obj.flag = this.flag;
            this.cleanLinks = new CleanFlagReplacements();
        }
        else if(this.obj instanceof core.RelayFlag
                || this.obj instanceof core.NegateFlag
                || this.obj instanceof core.FlagBeenTrue
                || this.obj instanceof core.Timeout
                || this.obj instanceof core.ToggleFlag) {
            this.prevFlag = this.obj.tracking;
            this.obj.tracking = this.flag;
            this.cleanLinks = new CleanFlagReplacements();
        }
        else if(this.obj instanceof core.OrFlags
                || this.obj instanceof core.AndFlags
                || this.obj instanceof core.HoldAnd) {
            if(this.obj.flagNames.indexOf(this.flag)==-1)
                this.obj.flagNames.push(this.flag);
        }
        else
            throw new TypeError("AddLogicLink: Unsupported object");
        this.obj.reset();
        registerAll(game);
        if(this.cleanLinks) //Possibly orphaned an output.
            this.cleanLinks.apply(game);
    }
    revoke(game) {
        if(this.cleanLinks)
            this.cleanLinks.revoke(game);
        if((this.obj instanceof core.ApplyFlag) || (this.obj instanceof core.MakeAndApplyFlag))
            this.obj.flag = this.prevFlag;
        else if(this.obj instanceof core.RelayFlag
                || this.obj instanceof core.NegateFlag
                || this.obj instanceof core.FlagBeenTrue
                || this.obj instanceof core.Timeout
                || this.obj instanceof core.ToggleFlag)
            this.obj.tracking = this.prevFlag;
        else if(this.obj instanceof core.OrFlags
                || this.obj instanceof core.AndFlags
                || this.obj instanceof core.HoldAnd) {
            var i = this.obj.flagNames.indexOf(this.flag);
            if(i!=-1)
                this.obj.flagNames.splice(i, 1);
        }
        else
            throw new TypeError("AddLogicLink: Unsupported object");
        this.obj.reset();
        registerAll(game);
    }
}

//Stops an object from watching any flags
class ClearInputFlags extends Action {
    constructor(obj) {
        super();
        if(!obj.watchedChanged)
            throw new TypeError("Require an object with flag inputs.");
        this.obj = obj;
        this.prevFlags = [];
        this.extraAction = null;
        this.cleanup = new CleanFlagReplacements();
    }
    apply(game) {
        if(this.obj instanceof core.ApplyFlag) {
            this.extraAction = new ReplaceObject(this.obj, this.obj.obj);
            this.extraAction.apply(game);
        }
        else if(this.obj instanceof core.MakeAndApplyFlag) {
            this.extraAction = new ReplaceObject(this.obj, new mods.MakeFlagFrom(this.obj.name, this.obj.obj));
            this.extraAction.apply(game);
        }
        else if(this.obj instanceof core.RelayFlag
                || this.obj instanceof core.NegateFlag
                || this.obj instanceof core.FlagBeenTrue
                || this.obj instanceof core.Timeout
                || this.obj instanceof core.ToggleFlag) {
            this.prevFlags.push(this.obj.tracking);
            this.obj.tracking = "";
        }
        else if(this.obj instanceof core.OrFlags || this.obj instanceof core.AndFlags
                || this.obj instanceof core.HoldAnd) {
            this.prevFlags = this.obj.flagNames;
            this.obj.flagNames = [];
        }
        else
            throw new Error("Unrecognised flag object.");
        if(!this.extraAction)
            registerAll(game);
        this.cleanup.apply(game);
    }
    revoke(game) {
        this.cleanup.revoke(game);
        if(this.extraAction)
            this.extraAction.revoke(game);
        else if((this.obj instanceof core.RelayFlag || this.obj instanceof core.NegateFlag
                    || this.obj instanceof core.FlagBeenTrue || this.obj instanceof core.Timeout
                    || this.obj instanceof core.ToggleFlag)
                && this.prevFlags.length!=0)
            this.obj.tracking = this.prevFlags[0];
        else if(this.obj instanceof core.OrFlags || this.obj instanceof core.AndFlags
                    || this.obj instanceof core.HoldAnd)
            this.obj.flagNames = this.prevFlags;
        else
            throw new Error("Unrecognised flag object.");
        if(!this.extraAction)
            registerAll(game);
    }
}

//Checks if connecting "target" to "from" would create a cycle by looking
//for connections from "from" to "target".
function foundCycle(flagsDetails, from, target) {
    if(from==target)
        return true;
    else if(from instanceof core.ApplyFlag //Nested output flags.
            && from.obj.name!==undefined)
        //Permit TriggerLasers cycles (e.g. switch off if been triggered).
        return !(from.obj instanceof core.TriggerLaser)
            && foundCycle(flagsDetails, from.obj, target);
    //Permit timeout cycles (e.g. alternating actions).
    else if(from instanceof core.Timeout
        || target instanceof core.Timeout)
        return false;
    else if(from.name!==undefined){
        var users = flagsDetails[from.name].users;
        for(var i=0; i<users.length; ++i) {
            if(foundCycle(flagsDetails, users[i], target))
                return true;
        }
    }
    else
        return false;
}
