//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js';
import {AdjustLinks} from './adjust-links.js';

export class DrawLinks {
    constructor(links) {
        this.links = links;
        this.flags = Object.keys(this.links);
    }
    draw(manager, context, caches) {
        context.lineCap = "round";
        this.flags.forEach(function(flag) {
            var details = this.links[flag];
            //Used to hide outgoing links if the object is inside a mods.Group
            if(details.showDetails) {
                var flagObj = details.owner;
                var obj = flagObj;
                if(flagObj instanceof core.MakeFlagFrom
                        || flagObj instanceof core.MakeAndApplyFlag)
                    obj = obj.obj;
                if(obj.box) {
                    context.beginPath();
                    context.strokeStyle = obj.linkColor || obj.color || "black";
                    if(manager.getFlag(details.owner.name))
                        context.lineWidth = 4;
                    else
                        context.lineWidth = 2;
                    var box = obj.box;
                    var linkOut;
                    if(flagObj.getLinkOutPos)
                        linkOut = flagObj.getLinkOutPos();
                    else if(obj.getLinkCenter)
                        linkOut = obj.getLinkCenter();
                    else
                        linkOut = obj.box.center;
                    details.users.forEach(function(obj) {
                        context.moveTo(linkOut.x, linkOut.y);
                        var inObj = obj;
                        if(obj instanceof core.ApplyFlag || obj instanceof core.MakeAndApplyFlag)
                            obj = obj.obj;
                        var pos = null;
                        if(inObj.getLinkInPos)
                            pos = inObj.getLinkInPos();
                        else if(obj.box)
                            pos = obj.box.center;
                        if(pos)
                            context.lineTo(pos.x, pos.y);
                    }, this);
                    context.stroke();
                }
            }
        }, this);
    }
}

//Render connections between portals
export class DrawPortals {
    constructor(portals) {
        this.portals = portals;
        this.pipes = Object.keys(this.portals);
    }
    draw(context, caches) {
        context.lineCap = "round";
        context.strokeStyle = "black";
        context.lineWidth = 4;
        this.pipes.forEach(function(pipe) {
            var details = this.portals[pipe];
            if(details.showDetails
                && details.owner && details.user) {
                context.beginPath();
                var c1 = details.owner.box.center;
                context.moveTo(c1.x, c1.y);
                var c2 = details.user.box.center;
                context.lineTo(c2.x, c2.y);
                context.stroke();
            }
        }, this);
    }
}
