import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {ActionGroup} from './action.js';
import {AddObject, DeleteObject} from './add-delete-actions.js';
import {CleanUsers, CleanFlagReplacements} from './adjust-links.js';
import {getFlagsAndControls, getPortals} from './connections.js';
import {getVisibleChildren} from './filtering.js';
import {SelectOperation} from './select.js';

export function groupObjs(state, game, operation) {
    //Reversed so the objects get rendered in the right order after grouping
    var objs = operation.getModObjs(game).reverse();
    if(objs.length>1) {
        var actions = [];
        var flatObjs = [];
        for(let o of objs) {
            actions.push(new DeleteObject(o));
            if(o instanceof mods.Group)
                flatObjs = flatObjs.concat(o.objs);
            else
                flatObjs.push(o);
        }
        if(flatObjs.length>0) {
            //Remove any outgoing and incoming links and pipes that go outside
            //the group
            tidyUpGroupFlags(flatObjs, game, actions);
            tidyUpGroupPortals(flatObjs, game, actions);
            var group = new mods.Group(flatObjs);
            actions.push(new AddObject(group));
            var finalAction = new ActionGroup(actions);
            finalAction.apply(game);
            state.actionHistory.push(finalAction);
            return new SelectOperation(null, getVisibleChildren([group]));
        }
    }
    return operation;
}

export function ungroupObjs(state, game, operation) {
    var objs = operation.getModObjs(game);
    if(objs.length>0) {
        var actions = [];
        var affected = [];
        for(let groups of objs) {
            if(groups instanceof mods.Group) {
                actions.push(new DeleteObject(groups));
                for(let o of groups.objs) {
                    actions.push(new AddObject(o));
                    affected.push(o);
                }
            }
        }
        if(actions.length>0) {
            var finalAction = new ActionGroup(actions);
            finalAction.apply(game);
            state.actionHistory.push(finalAction);
            //Reversed so if the selected objects are cloned it's done in the
            //right order
            return new SelectOperation(null, getVisibleChildren(affected.reverse()));
        }
    }
    return operation;
}

//Modifies flatObjs and actions
function tidyUpGroupFlags(flatObjs, game, actions) {
    //Handle outgoing and incoming links
    var groupFlags = getFlagsAndControls(flatObjs);
    var allFlags = getFlagsAndControls(game.level.objs);
    for(var f in groupFlags) {
        //Prevent outgoing links
        if(groupFlags[f].owner) {
            var unwantedUsers = [];
            for(let user of allFlags[f].users) {
                if(groupFlags[f].users.indexOf(user)==-1)
                    unwantedUsers.push(user);
            }
            if(unwantedUsers.length>0) {
                actions.push(new CleanUsers(f, unwantedUsers));
                if(unwantedUsers.length==allFlags[f].users.length) {
                    var owner = allFlags[f].owner;
                    if(owner instanceof core.MakeFlagFrom)
                        flatObjs[flatObjs.indexOf(owner)] = owner.obj;
                    else if(owner instanceof core.MakeAndApplyFlag)
                        flatObjs[flatObj.indexOf(owner)] =
                            new ApplyFlag(owner.flag, owner.obj);
                }
            }
        }
        //Prevent incoming links
        else {
            var unwantedUsers = [];
            for(let user of groupFlags[f].users) {
                if(user instanceof core.ApplyFlag)
                    flatObjs[flatObjs.indexOf(user)] = user.obj;
                else if(user instanceof core.MakeAndApplyFlag)
                    flatObjs[flatObjs.indexOf(user)] =
                        new mods.MakeFlag(obj.name, obj.obj);
                else if(!(user instanceof core.RelayFlag))
                    unwantedUsers.push(user);
            }
            if(unwantedUsers.length>0) {
                actions.push(new CleanUsers(f, unwantedUsers));
                actions.push(new CleanFlagReplacements());
            }
        }
    }
}

//Modifies flatObjs and actions
function tidyUpGroupPortals(flatObjs, game, actions) {
    var groupPipes = getPortals(flatObjs);
    var allPipes = getPortals(game.level.objs);
    for(var p in groupPipes) {
        //Prevent outgoing portals
        if(groupPipes[p].owner
            && !groupPipes[p].user
            && allPipes[p].user)
            actions.push(new ClearPortalInput(allPipes[p].user, p));
        else if(!groupPipes[p].owner)
            actions.push(new ClearPortalInput(groupPipes[p].user, p));
    }
}
