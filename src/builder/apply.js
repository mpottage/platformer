//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {getTopObj} from './filtering.js';

export function canApply(obj) {
    return (obj instanceof core.CollapsingPlatform)
        || obj.health || (obj instanceof core.GreySquare)
        || (obj instanceof core.Switch) || (obj instanceof core.ToggleButton)
        || (obj instanceof core.Cover) || (obj instanceof core.Darkness)
        || (obj instanceof core.LighteningEffect) || (obj instanceof core.Pickup)
        || (obj instanceof core.BouncingPickup)
        || ((obj instanceof core.PickupSpawner) && obj.spawned)
        || (obj instanceof core.FlagBeenTrue) || (obj instanceof core.Destructible)
        || (obj instanceof core.PaymentRegion);
}
export function applyObjectAt(state, game, gameX, gameY) {
    var obj = getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY, canApply);
    if(obj) {
        if(obj instanceof core.CollapsingPlatform)
            obj.collapsed = true;
        if(obj.health)
            obj.health.kill();
        else if(obj instanceof core.Switch) {
            if(obj.toggled)
                obj.box.x = obj.startX;
            else
                obj.box.x = obj.startX+obj.slideWidth-obj.box.width;
            obj.toggled = !obj.toggled;
        }
        else if(obj instanceof core.ToggleButton)
            obj.toggled = !obj.toggled;
        else if(obj instanceof core.Cover) {
            obj.disabled = true;
            obj.opacity = 0;
        }
        else if(obj instanceof core.Darkness
            || obj instanceof core.LighteningEffect)
            obj.builderShow = false;
        else if(obj instanceof core.Pickup
            || obj instanceof core.Destructible
            || obj instanceof core.GreySquare)
            obj.disabled = true;
        else if(obj instanceof core.BouncingPickup)
            obj.pickup.disabled = true;
        else if(obj instanceof core.PickupSpawner && obj.spawned) {
            if(obj.spawned instanceof core.Pickup)
                obj.spawned.disabled = true;
            else
                obj.spawned.pickup.disabled = true;
        }
        else if(obj instanceof core.FlagBeenTrue) {
            obj.prevValue = !obj.prevValue;
            game.manager.setFlag(obj.name, obj.prevValue);
        }
        else if(obj instanceof core.PaymentRegion)
            obj.paid = true;
    }
}
