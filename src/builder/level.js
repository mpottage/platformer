//Copyright Matthew Pottage 2019.
import * as mods from './mods/import-level-objects.js';
import * as core from '../core/import-level-objects.js';
import {Action, ActionGroup} from './action.js';
import {getNonBarrierStart} from './filtering.js';
import {FreeCamera} from './free-camera.js';
import {getMinimumBox} from './positioning-utilities.js';
import {registerAll, getUniqueId, round5, ceil5} from './utility.js';
import {getFlagsAndControls, getFlagNames} from './connections.js';
import {AddObject, ReplaceObject} from './add-delete-actions.js';
import {MoveAllAction} from './move.js';

export class ReplaceLevel extends Action{
    constructor(prev, replacement) {
        super();
        this.prevLevel = prev;
        this.newLevel = replacement;
    }
    apply(game) {
        replaceLevel(game, this.newLevel);
    }
    revoke(game) {
        replaceLevel(game, this.prevLevel);
    }
}

//Modify level so that it has a (now standard) barrier covering the top of the
//rectangle created by the left/right barriers (so a player cannot get outside
//the expected area), if it doesn't already have the barrier
export function upgradeBoundaries(level) {
    //Anything less than 3 and the boundaries are non-standard, so need to be
    //checked for upgrading
    if(getNonBarrierStart(level)<3 && level.objs.length>=2
        && level.objs[0].box && level.objs[0].box.x==-1
        && level.objs[1].box && level.objs[1].box.x>=level.width
        && (!level.objs[2] || !level.objs[2].box
                || level.objs[2].box.x!=0
                || level.objs[2].box.y!=level.objs[0].box.y
                || level.objs[2].box.width!=level.width))
        level.objs.splice(2, 0, new core.Platform(new core.Box(0, level.objs[0].box.y,
            level.width, 1)));
    return level;
}

//May do compatibility updates to newLevel, although they shouldn't affect
//functionality or behaviour.
export function replaceLevel(game, newLevel) {
    game.level = newLevel;
    game.allLevels["builder"] = game.level;
    game.reset();
    game.updateConnections();
    if(game.camera instanceof FreeCamera && game.camera.callback)
        game.camera.callback();
}

export function getCleanLevel() {
    return new core.Level(20000, 600, [
            new core.Platform(new core.Box(-1, -5000, 1, 15000)),
            new core.Platform(new core.Box(20000, -5000, 1, 15000)),
            new core.Platform(new core.Box(0, -5000, 20000, 1)),
            new core.Platform(new core.Box(0, 680, 20000, 1), "transparent"),
            new mods.StartPositions([{x: 10, y: 10}]),
            new core.Platform(new core.Box(0, 590, 200, 10), "#5f5f5f")], 1);
}

export class ChangeLevelWidth extends Action {
    constructor(level, prevWidth, newWidth) {
        super();
        this.level = level;
        this.prev = prevWidth;
        this.succ = newWidth;
    }
    static _apply(level, prev, succ) {
        var nbs = getNonBarrierStart(level);
        if(nbs>2) {
            level.objs[1].box.x = succ;
            level.objs[2].box.width = succ;
            if(nbs>3)
                level.objs[3].box.width = succ;
        }
        level.width = succ;
    }
    apply(game) {
        ChangeLevelWidth._apply(this.level, this.prev, this.succ);
        if(game.camera instanceof FreeCamera && game.camera.callback)
            game.camera.callback();
    }
    revoke(game) {
        ChangeLevelWidth._apply(this.level, this.succ, this.prev);
        if(game.camera instanceof FreeCamera && game.camera.callback)
            game.camera.callback();
    }
}

export class ChangeLevelHeight extends Action {
    constructor(level, prevHeight, newHeight) {
        super();
        this.level = level;
        this.prev = prevHeight;
        this.succ = newHeight;
    }
    static _apply(level, prev, succ) {
        var nbs = getNonBarrierStart(level);
        if(nbs>2) {
            //Check for default barriers (as in getCleanLevel).
            for(var i=0; i<2; ++i)
                level.objs[i].box.height += (succ-prev);
            if(nbs>3)
                level.objs[3].box.y += (succ-prev);
        }
        level.height = succ;
    }
    apply(game) {
        ChangeLevelHeight._apply(this.level, this.prev, this.succ);
        if(game.camera instanceof FreeCamera && game.camera.callback)
            game.camera.callback();
    }
    revoke(game) {
        ChangeLevelHeight._apply(this.level, this.succ, this.prev);
        if(game.camera instanceof FreeCamera && game.camera.callback)
            game.camera.callback();
    }
}

export class ChangeLevelScale extends Action {
    constructor(level, prevScale, newScale) {
        super();
        this.level = level;
        this.prev = prevScale;
        this.succ = newScale;
    }
    apply(game) {
        this.level.scale = this.succ;
        registerAll(game);
    }
    revoke(game) {
        this.level.scale = this.prev;
        registerAll(game);
    }
}

export class SetLevelSuspendingOptimization extends Action {
    constructor(level, newVal, oldVal) {
        super();
        this.level = level;
        this.newVal = newVal;
        this.oldVal = oldVal;
    }
    apply(game) {
        this.level.suspendingOptimization = this.newVal;
        registerAll(game);
    }
    revoke(game) {
        this.level.suspendingOptimization = this.oldVal;
        registerAll(game);
    }
}

export class ChangeLevelEnvSetup extends Action {
    //Setting oldVal or newVal to undefined will cause the setting to be removed
    //from the level, rather than an undefined value used
    constructor(level, name, oldVal, newVal) {
        super();
        this.level = level;
        this.name = name;
        this.oldVal = oldVal;
        this.newVal = newVal;
    }
    _setVal(game, next) {
        if(next===undefined)
            delete this.level.environmentSetup[this.name];
        else
            this.level.environmentSetup[this.name] = next;
        registerAll(game);
    }
    apply(game) {
        this._setVal(game, this.newVal);
    }
    revoke(game) {
        this._setVal(game, this.oldVal);
    }
}

//Get the smallest box surrounding all the items in the level.
export function getMinimumLevelBox(level) {
    return getMinimumBox(level.objs, getNonBarrierStart(level));
}

export function shrinkToFit(state, game) {
    var lvl = game.level;
    var min = preprocessShrinkBox(lvl);
    //Check that something is being done and it is shrinking the level size.
    if(isShrinking(min, lvl)) {
        var action = new ActionGroup([new ChangeLevelWidth(lvl, lvl.width, min.width),
                new ChangeLevelHeight(lvl, lvl.height, min.height),
                new MoveAllAction(lvl, -min.x, -min.y)]);
        action.apply(game);
        state.actionHistory.push(action);
    }
}
export function expandToFit(state, game) {
    var lvl = game.level;
    var min = preprocessExpandBox(lvl);
    if(isExpanding(min, lvl)) {
        var action = new ActionGroup([new ChangeLevelWidth(lvl, lvl.width, min.width),
                new ChangeLevelHeight(lvl, lvl.height, min.height),
                new MoveAllAction(lvl, -min.x, -min.y)]);
        action.apply(game);
        state.actionHistory.push(action);
    }
}
export function convertCovers(state, game) {
    var lvl = game.level;
    var actions = [];
    var flagsNames = getFlagNames(game);
    for(var i=0; i<lvl.objs.length; ++i)
        if(lvl.objs[i] instanceof core.Cover) {
            var name = getUniqueId("HR", flagsNames);
            actions.push(new ReplaceObject(lvl.objs[i],
                        new mods.ApplyFlag(name, lvl.objs[i])));
            var b = lvl.objs[i].box;
            actions.push(new AddObject(new mods.HitRegion(name,
                            new core.Box(b.x,b.y,b.width,b.height))));
            flagsNames.push(name);
        }
    if(actions.length>0) {
        var a = new ActionGroup(actions);
        a.apply(game);
        state.actionHistory.push(a);
    }
}
export function addFloor(state, game, height) {
    if(height<10 || height>600)
        height = 10;
    var action = new AddObject(new core.Platform(
                new core.Box(0, game.level.height-height, game.level.width, height),
                state.color));
    action.apply(game);
    state.actionHistory.push(action);
}
export function addBackground(state, game) {
    var action = new AddObject(new mods.VisualOnlyBackground(
        new core.Box(0, 0, game.level.width, game.level.height),
        state.color));
    action.apply(game);
    state.actionHistory.push(action);
}
export function suppressWeapons(state, game) {
    var action = new AddObject(new mods.SuppressWeapons(
        new core.Box(0, 0, game.level.width, game.level.height)));
    action.apply(game);
    state.actionHistory.push(action);
}
export function shortenFlagsIrrevocable(state, game) {
    //Create new flags for any objects that make them. Irrevocable.
    var flags = getFlagsAndControls(game.level.objs);
    var newNames = [];
    for(var f in flags) {
        var owner = flags[f].owner;
        if(owner && owner.name.search("-")!==-1) {
            var oldPrefix = f.split('/')[0];
            var newPrefix = "";
            if(oldPrefix==="MakeFlagFrom" || oldPrefix=="MakeApply" || oldPrefix=="ApplyFlag")
                newPrefix = "Obj";
            else
                newPrefix = Array.from(oldPrefix).filter(o => o.search(/[A-Z]/)===0).join("")
            owner.name = getUniqueId(newPrefix, newNames);
            newNames.push(owner.name);
            flags[f].users.forEach(function(o) {
                if(o instanceof core.ApplyFlag
                    || o instanceof core.MakeAndApplyFlag)
                    o.flag = owner.name;
                else if(o instanceof core.RelayFlag
                        || o instanceof core.NegateFlag
                        || o instanceof core.FlagBeenTrue
                        || o instanceof core.Timeout
                        || o instanceof core.ToggleFlag)
                    o.tracking = owner.name;
                else if(o instanceof core.OrFlags
                        || o instanceof core.AndFlags
                        || o instanceof core.HoldAnd)
                    o.flagNames[o.flagNames.indexOf(f)] = owner.name;
            });
        }
    }
    state.actionHistory.clear();
    game.updateConnections();
}

export const minLevelWidth = 1000;
export const minLevelHeight = 600;
export function preprocessShrinkBox(lvl) {
    var min = getMinimumLevelBox(lvl);
    min.height = Math.max(min.height+min.y, minLevelHeight);
    min.height = Math.min(lvl.height, ceil5(min.height));
    min.width = Math.max(min.width+min.x, minLevelWidth);
    min.width = Math.min(lvl.width, ceil5(min.width));
    min.x = 0;
    min.y = 0;
    return min;
}
export function isShrinking(min, lvl) {
    return (min.x!=0 || min.y!=0 || min.width!=lvl.width
                || min.height!=lvl.height) && min.width<=lvl.width
                && min.height<=lvl.height;
}

export function preprocessExpandBox(lvl) {
    var min = getMinimumLevelBox(lvl);
    min.width = Math.max(lvl.width, ceil5(Math.max(0, min.x)+min.width));
    min.height = Math.max(lvl.height, ceil5(Math.max(0, min.y)+min.height));
    min.x = Math.min(min.x, 0);
    min.y = Math.min(min.y, 0);
    return min;
}
export function isExpanding(min, lvl) {
    return (min.x!=0 || min.y!=0 || min.width!=lvl.width || min.height!=lvl.height)
        && (min.width>=lvl.width && min.height>=lvl.height);
}
export function anyUnlinkedCovers(lvl) {
    for(var i=0; i<lvl.objs.length; ++i)
        if(lvl.objs[i] instanceof core.Cover)
            return true;
    return false;
}
export function anyLongFlags(flagNames) {
    return flagNames.some(o => o.search("-")!==-1);
}
