//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {absValue} from '../core/utility.js';
import {getTopObj, getVisibleChildren, getRootParent, getAllObjs, getNonBarrierStart} from './filtering.js';
import {getScale, resizeBox, getDefaultState} from './utility.js';
import {centerObjsAt, getMinimumBox} from './positioning-utilities.js';
import {canResize, HistoryResizeOperation} from './resize.js';
import {HistoryLRMovingOperation, HistoryUDMovingOperation, canSetLRRoute, canSetUDRoute} from './route-setting.js';
import {Operation} from './operation.js';
import {MoveOperation, MoveMultiple} from './move.js';
import {groupDelete} from './delete-utilities.js';
import {loadTextObjects} from './parse-objects.js';
import {textObjectSave} from './save-objects.js';
import {renameFlagsAndPipes, getFlagNames, getPipeNames} from './connections.js';
import {ActionGroup} from './action.js';
import {ColorObject} from './color-utilities.js';
import {getImageName, SwapImage, isImageObject, imageUserCount, DeleteTexture, getImagesUsing} from './images.js';
import {highlightBox, selectHighlight, hoverHighlight, handleRadius,
    previewHandleHighlight, drawOneHandle} from './visuals.js';
import {AddMultipleObjects, DeleteObject} from './add-delete-actions.js';
import {getLRRouteHandle, getUDRouteHandle} from './route-setting.js';

//Operation to select and then manipulate one or more visible game objects.
//Functions include cloning, bulk color, bulk image, bulk delete, bulk movement
//or resizing a single object.
export class SelectOperation extends Operation {
    //selected: Visible (with box, unwrapped, or child) objects to manipulate.
    constructor(next, selected=[]) {
        super(next);
        this.selected = selected;
        this.origin = {x: 0, y: 0};
        this.box = new core.Box(0, 0, 0, 0);
        this.pos = {x: Infinity, y: Infinity};
        this.notSetup = selected.length==0;
        this.stillSelecting = this.notSetup;
    }
    //Given an object, if it is part of a group, then return all the visible
    //objects in the group, otherwise return a list of just the object.
    //  Used to make selecting one object in a group get the group selected.
    //obj: Object to process.
    //level: Level containing the object.
    _expandAnyGroup(level, obj) {
        var root = getRootParent(obj, level.objs);
        if(root instanceof mods.Group)
            return getVisibleChildren(root.objs);
        else
            return [obj];
    }
    press(state, game, gameX, gameY) {
        //If no items to be selected were passed into the constructor.
        if(this.notSetup) {
            //Remember starting position when/if dragging rectangle to select
            //objects.
            this.box.x = this.origin.x = gameX;
            this.box.y = this.origin.y = gameY;
            var top = getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY,
                    selectWithOp.bind(null, state));
            if(top)
                this.selected = this._expandAnyGroup(game.level, top);
            this.notSetup = false;
            return this;
        }
        //In select mode, resize or add/remove objects from selection.
        else if(state.mode=="s") {
            //Resize one object with drag handles.
            var handleDetails = null; //Prevents recomputing corner details.
            if(this.selected.length==1 && canResize(this.selected[0])
                //Save click details.
                && (handleDetails=clickedCornerHandle(gameX, gameY, this.selected[0].box,
                    game.camera.scale))) {
                return new HistoryResizeOperation(this, this.selected[0],
                    handleDetails.opposite, handleDetails.clicked);
            }
            //Adjust route of one left/right moving object using a drag handle.
            else if(this.selected.length==1 && canSetLRRoute(this.selected[0])
                && hasClickedLRHandle(gameX, gameY, this.selected[0], game.camera.scale)) {
                return new HistoryLRMovingOperation(this, this.selected[0]);
            }
            //Adjust route of one up/down moving object using a drag handle.
            else if(this.selected.length==1 && canSetUDRoute(this.selected[0])
                && hasClickedUDHandle(gameX, gameY, this.selected[0], game.camera.scale)) {
                return new HistoryUDMovingOperation(this, this.selected[0]);
            }
            //Not clicked on any drag handles, so attempt to select another
            //object (or clear the selection).
            else {
                var top = getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY,
                        selectWithOp.bind(null, state));
                if(top) {
                    if(this.selected.indexOf(top)==-1)
                        this.selected = this.selected.concat(this._expandAnyGroup(game.level, top));
                    else {
                        var expandedGroup = this._expandAnyGroup(game.level, top);
                        this.selected = this.selected.filter(o => expandedGroup.indexOf(o)==-1);
                    }
                    return this;
                }
                else
                    //Clicked on blank space, so creating a new selection.
                    //Selection could be empty or contain objects selected by a
                    //dragging a rectangle.
                    return (new SelectOperation(this.next)).press(state, game, gameX,
                        gameY)
            }
        }
        //Special behaviour in different modes to modify multiple objects at
        //once.
        else if(state.mode=="m")
            return this._moveMode(state, game, gameX, gameY, gameX, gameY);
        else if(state.mode=="d" && this.selected.length>0)
            return this._deleteMode(state, game, gameX, gameY);
        else if(state.mode=="i")
            return this._insertMode(state, game, gameX, gameY);
        else if(state.mode=="c")
            return this._colorMode(state, game, gameX, gameY);
        else if(state.mode=="b")
            return this._imageMode(state, game, gameX, gameY);
        else
            return this.next;
    }
    _moveMode(state, game, gameX, gameY) {
        var op = null;
        //Check that the click was made on the selection.
        var hit = getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY,
                o => this.selected.indexOf(o)!=-1);
        if(hit) {
            //When moving just one object permit moving a pickup into a pickup
            //spawner, which requires the "move one object" operation.
            if(this.selected.length==1)
                op = new MoveOperation(this, MoveOperation.wrapNested(hit, game),
                        {x: gameX, y: gameY});
            //Only movement, no special behaviour.
            else
                op = new MoveMultiple(this,
                        (this.selected.map(o => MoveOperation.wrapNested(o, game))),
                        {x: gameX, y: gameY});
            return op;
        }
        else
            return this.next;
    }
    _deleteMode(state, game, gameX, gameY) {
        //Check that the click was made on the selection.
        var hit = getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY,
                o => this.selected.indexOf(o)!=-1);
        if(hit)
            groupDelete(state, game, this.getModObjs(game));
        return this.next
    }
    //Cloning selected objects.
    _insertMode(state, game, gameX,  gameY) {
        //Duplicates the objects by generating save code and reloading it into
        //the level builder (necessary to convert to text to deep copy any
        //arrays).
        var cloned = loadTextObjects(textObjectSave(this.getModObjs(game)));
        var children = getVisibleChildren(cloned);
        //Center new objects where a click was made
        var action = new AddMultipleObjects(cloned.reverse());
        centerObjsAt(state, game.level, children, gameX, gameY);
        renameFlagsAndPipes(getFlagNames(game), getPipeNames(game), cloned);
        action.apply(game);
        state.actionHistory.push(action);
        //Change selection to the new objects.
        //As they are the same (except for position) as the original objects
        //cloning them gives the same results as cloning the original objects.
        this.selected = children;
        return this;
    }
    _colorMode(state, game, gameX, gameY) {
        //Check that the click was made on the selection.
        var hit = getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY,
                obj => this.selected.indexOf(obj)!=-1);
        if(hit) {
            //Only generate an action if there are any objects that could be
            //colored (consistent with colorObjAt).
            var possibleObjs = this.selected.filter(o => o.color!==undefined);
            if(possibleObjs.length>0) {
                var action = new ActionGroup(
                    possibleObjs.map(o => new ColorObject(o, o.color, state.color)));
                action.apply(game);
                state.actionHistory.push(action);
            }
            return this;
        }
        else
            return this.next
    }
    _imageMode(state, game, gameX, gameY) {
        var hit = getTopObj(state.snapToGrid, game.manager, game.level, gameX, gameY,
                obj => this.selected.indexOf(obj)!=-1);
        if(hit) {
            var actingOn = this.selected.filter(isImageObject);
            if(actingOn.length>0) {
                //Swap the images, deleting any textures no longer in use, and
                //adding the image to the textures if necessary
                var details = getImageName(game, state.currentImage);
                var actions = actingOn.map(o => new SwapImage(o, o.textureName, details.name));
                if(details.action) {
                    var imagesInUse = [];
                    for(var obj of actingOn) {
                        if(imagesInUse.indexOf(obj.textureName)==-1
                            && obj.textureName!=details.name)
                            imagesInUse.push(obj.textureName);
                    }
                    for(var img of imagesInUse) {
                        if(getImagesUsing(actingOn, img).length==imageUserCount(game, img))
                            actions.push(new DeleteTexture(game.level, img));
                    }
                    actions.push(details.action);
                }
                var action = new ActionGroup(actions);
                action.apply(game);
                state.actionHistory.push(action);
            }
            return this;
        }
        else
            return this.next
    }
    _applyBoxSelect(state, game) {
        //Get any objects which are inside the box
        var newSelected = getAllObjs(game.manager, game.level,
                this.box, selectWithOp.bind(null, state));
        this.selected = [];
        newSelected.forEach(function(o) {
            if(this.selected.indexOf(o)==-1)
                this.selected = this.selected.concat(this._expandAnyGroup(game.level, o));
        }, this);
    }
    move(state, game, gameX, gameY) {
        //If dragging a rectangle to create a selection
        if(this.stillSelecting) {
            //Update the selection box to the new extents
            resizeBox(this.origin, gameX, gameY, this.box, state.snapToGrid);
            if(!state.lazySelect)
                this._applyBoxSelect(state, game);
        }
        //Used when drawing hints about what the operation is doing.
        this.pos = {x: gameX, y: gameY};
        return this;
    }
    draw(state, game, context) {
        this.selected.forEach(function(o) {
            highlightBox(context, o.box, selectHighlight, getScale(game));
        });
        if(this.stillSelecting && (this.box.width>0 || this.box.height>0))
            highlightBox(context, this.box, "red", getScale(game));
        if(state.mode=="s") {
            //Highlight a hovered object if it could be added to current the
            //selection.
            var hover = getTopObj(state.snapToGrid, game.manager, game.level, this.pos.x,
                    this.pos.y, selectWithOp.bind(null, state));
            if(hover && this.selected.indexOf(hover)==-1)
                highlightBox(context, hover.box, hoverHighlight,
                    getScale(game));
            //Show drag handles (if any)
            if(this.selected.length==1) {
                //Resizing drag handles
                if(canResize(this.selected[0])) {
                    context.beginPath();
                    var radius = handleRadius/getScale(game);
                    getVisibleHandles(this.selected[0].box, getScale(game))
                        .forEach(function(c) {
                            context.arc(c.x, c.y, radius, 0, Math.PI*2);
                            context.closePath();
                    });
                    context.fillStyle = previewHandleHighlight;
                    context.fill();
                }
                //Left/right route adjustment drag handles
                if(canSetLRRoute(this.selected[0]))
                    drawOneHandle(context, getScale(game),
                        getLRRouteHandle(this.selected[0]),
                        previewHandleHighlight);
                //Left/right route adjustment drag handles
                if(canSetUDRoute(this.selected[0]))
                    drawOneHandle(context, getScale(game),
                        getUDRouteHandle(this.selected[0]),
                        previewHandleHighlight);
            }
        }
    }
    release(state, game) {
        if(this.stillSelecting && state.lazySelect)
            this._applyBoxSelect(state, game);
        if(this.selected.length>0) {
            //End dragging a rectangle
            this.stillSelecting = false;
            return this;
        }
        //No objects to manipulate, so operation complete
        else
            return this.next;
    }
    autoscroll() {
        return this.stillSelecting;
    }
    getModObjs(game) {
        var res = [];
        for(var o of this.selected) {
            var toAdd = getRootParent(o, game.level.objs) || o;
            if(res.indexOf(toAdd)==-1)
                res.push(toAdd);
        }
        return res;
    }
}

//Can visible obj be (safely) manipulated by a SelectOperation
export function selectWithOp(state, obj) {
    return (!(obj instanceof core.VisualOnlyBackground)
            && !(obj instanceof core.ImageBackground)
            && !(obj instanceof mods.Position))
        || (state.selectBackground && ((obj instanceof core.VisualOnlyBackground)
                || (obj instanceof core.ImageBackground)));
}
export function selectAllPossible(level) {
    var visible = getVisibleChildren(level.objs.slice(getNonBarrierStart(level)));
    var selectable = visible.filter(o => selectWithOp(getDefaultState(), o));
    return new SelectOperation(null, selectable);
}
//Get drag handles to resize an object (assumes box is on a resizable object)
function getVisibleHandles(box, scale) {
    var corners = box.corners;
    if(box.width<2*handleRadius/scale || box.height<2*handleRadius/scale)
        corners = [corners[1], corners[2]];
    return corners;
}
//Get information about the handle selected by a cursor at (x, y).
//Returns {clicked: [handle], opposite: [handle]} if one, otherwise null.
function clickedCornerHandle(x, y, box, scale) {
    var corners = getVisibleHandles(box, scale);
    var clicked = corners.filter(c => absValue(c.x-x, c.y-y)<=handleRadius/scale)[0];
    if(clicked)
        return {clicked: clicked,
            opposite: corners.filter(c => c.x!=clicked.x && c.y!=clicked.y)[0]};
    else
        return null;
}
function hasClickedLRHandle(x, y, obj, scale) {
    var handle = getLRRouteHandle(obj);
    return absValue(handle.x-x, handle.y-y)<=handleRadius/scale;
}
function hasClickedUDHandle(x, y, obj, scale) {
    var handle = getUDRouteHandle(obj);
    return absValue(handle.x-x, handle.y-y)<=handleRadius/scale;
}
