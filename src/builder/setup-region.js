//Copyright Matthew Pottage 2019.
import {getAngle} from '../core/utility.js';
import {getScale} from './utility.js';
import {to3dp} from '../core/utility.js';
import {highlightBox, selectHighlight, hoverHighlight} from './visuals.js';
import {Operation} from './operation.js';
import {airCurrentLength} from './mods/regions.js';

export class DirectCurrent extends Operation {
    constructor(next, obj) {
        super(next);
        this.obj = obj;
        this.vecTo = {x: 0, y: 0};
        this.moved = false;
    }
    move(state, game, gX, gY) {
        this.moved = true;
        var origin = this.obj.box.center;
        var dx = gX-origin.x;
        var dy = gY-origin.y;
        var angle = Math.round(getAngle(dx, dy)/(Math.PI/8))*Math.PI/8;
        this.obj.vec = {x: to3dp(Math.cos(angle)), y: to3dp(Math.sin(angle))};
        var abs = Math.min(airCurrentLength, Math.sqrt(dx*dx+dy*dy));
        this.vecTo = {x: this.obj.vec.x*abs, y: this.obj.vec.y*abs};
        return this;
    }
    draw(state, game, context) {
        highlightBox(context, this.obj.box, selectHighlight, getScale(game));
        if(this.moved) {
            context.beginPath();
            context.lineWidth = 4;
            context.strokeStyle = hoverHighlight;
            var origin = this.obj.box.center;
            context.moveTo(origin.x, origin.y);
            context.lineTo(this.vecTo.x+origin.x, this.vecTo.y+origin.y);
            context.stroke();
        }

    }
}

export class SetMessageOperation extends Operation {
    constructor(next, queryDialog, obj) {
        super(next);
        this.obj = obj;
        this.finished = false;
        this.shown = false;
        this.queryDialog = queryDialog;
    }
    press() {
        if(this.finished)
            return this.next;
        else
            return this;
    }
    move() { return this.press(); }
    release() { return this.press(); }
    draw() {
        if(!this.shown) {
            this.queryDialog("Text: ", this.obj.message, (function(newVal) {
                this.obj.message = newVal;
                this.finished = true;
            }).bind(this));
            this.shown = true;
        }
    }
}
