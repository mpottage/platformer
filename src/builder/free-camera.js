//Copyright Matthew Pottage 2019.
import {Camera} from '../core/camera.js';
import {to3dp, screenHeight} from '../core/utility.js';

//Camera for the level builder. It keeps the game.camera frozen, only moving when
//requested.
export class FreeCamera extends Camera {
    //callback: Called whenever the camera moves.
    constructor(level, prevCam, callback) {
        super(level);
        if(prevCam) {
            this.offsets.x = prevCam.offsets.x;
            this.offsets.y = prevCam.offsets.y;
        }
        else
            this.offsets.x = this.offsets.y = 0;
        this.scroll = {x: 0, xAccel: 0, y: 0, yAccel: 0};
        this.stateX = "noScroll";
        this.stateY = "noScroll";
        this.stateParams  = {
            "noScroll": {maxSpeed: 0, accelAccel: 0, initial: 0},
            "fastScroll": {maxSpeed: 3000, accelAccel: 120, initial: 800},
            "slowScroll": {maxSpeed: 800, accelAccel: 60, initial: 200},
        }
        this.dirX = 1; //-1 (go right), 1 (go left).
        this.dirY = 1; //-1 (go right), 1 (go left).
        this.callback = callback;
    }
    update(p, w, h, interval) {
        this.adjustScale(w, h);
        //Apply scaling (before determining viewport shift).
        w /=  this.scale*this.level.scale;
        h /=  this.scale*this.level.scale;
        //The game.camera accelerates to a max speed (defined in stateParams), with the
        //  acceleration also increasing, at a specified rate (accelAccel).
        //This least to the acceleration being proportional to the time spend
        //  scrolling in a particular direction.
        //Move x
        this.oldScrollX = this.scroll.x;
        if(this.scroll.x==0) {
            this.scroll.x = this.dirX*this.stateParams[this.stateX].initial;
            this.scroll.xAccel = 0;
        }
        this.scroll.xAccel += this.stateParams[this.stateX].accelAccel*interval;
        this.scroll.x += this.dirX*this.scroll.xAccel*interval;
        //Cap scroll speed.
        if(Math.abs(this.scroll.x)>this.stateParams[this.stateX].maxSpeed)
            this.scroll.x = this.dirX*this.stateParams[this.stateX].maxSpeed;
        //Move camera (accomodating for zoom).
        this.scrollX((this.oldScrollX+this.scroll.x)*interval/2/this.scale/this.level.scale, w);
        //Move y
        this.oldScrollY = this.scroll.y;
        if(this.scroll.y==0) {
            this.scroll.y = this.dirY*this.stateParams[this.stateY].initial;
            this.scroll.yAccel = 0;
        }
        this.scroll.yAccel += this.stateParams[this.stateY].accelAccel*interval;
        this.scroll.y += this.dirY*this.scroll.yAccel*interval;
        //Cap scroll speed.
        if(Math.abs(this.scroll.y)>this.stateParams[this.stateY].maxSpeed)
            this.scroll.y = this.dirY*this.stateParams[this.stateY].maxSpeed;
        //Move camera (accomodating for zoom).
        this.scrollY((this.oldScrollY+this.scroll.y)*interval/2/this.scale/this.level.scale, h);
    }
    setFastScrollX(dir) {
        this.stateX = "fastScroll";
        this.dirX = dir;
    }
    setSlowScrollX(dir) {
        this.stateX = "slowScroll";
        this.dirX = dir;
    }
    noScrollX() {
        this.stateX = "noScroll";
    }
    setFastScrollY(dir) {
        this.stateY = "fastScroll";
        this.dirY = dir;
    }
    setSlowScrollY(dir) {
        this.stateY = "slowScroll";
        this.dirY = dir;
    }
    //Change the camera zoom
    //viewportWidth, viewportHeight: As supplied to camera.update
    //focus (optional): Point to keep at the same relative position on screen,
    //  defaults to center of current view.
    setZoom(value, viewportWidth, viewportHeight, focus) {
        viewportWidth /= this.level.scale;
        viewportHeight /= this.level.scale;
        if(!focus)
            focus = {x: -this.offsets.x+viewportWidth/this.scale/2,
                y: -this.offsets.y+viewportHeight/this.scale/2};
        var percentage = {x: to3dp((focus.x+this.offsets.x)/viewportWidth*this.scale),
            y: to3dp((focus.y+this.offsets.y)/viewportHeight*this.scale)};
        this.scale = value;
        var newOffsets = {x: -focus.x+viewportWidth/this.scale*percentage.x,
            y: -focus.y+viewportHeight/this.scale*percentage.y};
        this.scrollX(newOffsets.x-this.offsets.x);
        this.scrollY(newOffsets.y-this.offsets.y);
    }
    noScrollY() {
        this.stateY = "noScroll";
    }
    scrollX(dx, w) {
        var prevX = this.offsets.x;
        //Changes the camera's x offset by dx, then calls this.callback if
        // this.offsets.x is changed.
        //If moving dx causes the camera to move off one end of the level, the
        //  movement is shorted so this doesn't happen and both this.scroll.x and
        //  this.scroll.xAccel are set to 0.
        this.offsets.x += dx;
        if(this.offsets.x>=0 || this.level.width<w) {
            this.offsets.x = 0;
            this.scroll.x = this.scroll.xAccel = 0;
        }
        else if(-this.offsets.x>this.level.width-w) {
            this.offsets.x = -this.level.width+w;
            this.scroll.x = this.scroll.xAccel = 0;
        }
        if(prevX!=this.offsets.x && this.callback)
            this.callback();
    }
    scrollY(dy, h) {
        var prevY = this.offsets.y;
        //Like scrollX, but for y.
        this.offsets.y += dy;
        if(this.offsets.y>=0 || this.level.height<h) {
            this.offsets.y = 0;
            this.scroll.y = this.scroll.yAccel = 0;
        }
        else if(-this.offsets.y>this.level.height-h) {
            this.offsets.y = -this.level.height+h;
            this.scroll.y = this.scroll.yAccel = 0;
        }
        if(prevY!=this.offsets.y && this.callback)
            this.callback();
    }
    //Necessary, the normal game.camera doesn't scale.
    apply(context) {
        if(this.scale!=1) {
            context.scale(this.scale, this.scale);
            if(this.scale<1) {
                context.beginPath();
                context.rect(0, 0, this.level.width*this.level.scale,
                    Math.max(screenHeight, this.level.height*this.level.scale));
                context.clip();
            }
        }
        super.apply(context);
    }
    //Prevent excessive scaling so that the level is a subset of the viewport.
    adjustScale(w, h) {
        if(w/this.scale>this.level.width*this.level.scale
            && h/this.scale>this.level.height*this.level.scale)
            this.scale = Math.min(w/this.level.width/this.level.scale,
                h/this.level.height/this.level.scale);
    }
    changeLevel(level) {
        var oldLevel = this.level;
        super.changeLevel(level);
        if(level!==oldLevel)
            super.reset();
    }
    reset() {} //Do nothing.
}
