import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {getUniqueId, round5} from './utility.js';
import {lightenColor} from './color-utilities.js';
import {renameFlagsAndPipes} from './connections.js';
import {getVisibleChildren} from './filtering.js';
import {translateObj} from './positioning-utilities.js';
import {loadTextObjects} from './parse-objects.js';
import {playerWidth, playerHeight} from '../single/constants.js'

//Used to create level objects, usually for when inserting in insert mode.
//x, y: Coordinates to center the object at, or to set corner to start resizing
//from (depending on the object).
//flagsNames, pipeNames: Needed to create a component.
//
//For example createLevelObj(10, 10, "Platform", "blue", [], []) will create a
// normal platform with top-left corner at (10, 10) that is blue.
export function createLevelObj(x, y, strName, color, flagsNames, pipeNames) {
    if(strName=="Platform")
        return new core.Platform(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="JumpThroughPlatform")
        return new core.JumpThroughPlatform(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="DownThroughPlatform")
        return new core.DownThroughPlatform(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="RightThroughPlatform")
        return new core.RightThroughPlatform(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="LeftThroughPlatform")
        return new core.LeftThroughPlatform(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="IcePlatform")
        return new core.IcePlatform(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="ThinIcePlatform")
        return new core.ThinIcePlatform(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="Destructible")
        return new mods.Destructible(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="Water")
        return new core.Water(new core.Box(x, y, 10, 10));
    else if(strName=="MarshWater")
        return new core.MarshWater(new core.Box(x, y, 10, 10));
    else if(strName=="SpikeRow")
        return new mods.SpikeRow(new core.Box(x, y-10, 10, 10), defaultSpikeWidth, color);
    else if(strName=="SpikeColumn")
        return new mods.SpikeColumn(new core.Box(x-10, y, 10, 10), defaultSpikeWidth, color);
    else if(strName=="HealthLoss")
        return new core.HealthLoss(new core.Box(x, y-10, 10, 10), color);
    else if(strName=="WinRegion")
        return new mods.WinRegion(new core.Box(x, y, 10, 10));
    else if(strName=="DeathRegion")
        return new mods.DeathRegion(new core.Box(x, y, 10, 10));
    else if(strName=="ToStartRegion")
        return new mods.ToStartRegion(new core.Box(x, y, 10, 10));
    else if(strName=="SuppressWeapons")
        return new mods.SuppressWeapons(new core.Box(x, y, 10, 10));
    else if(strName=="ClearSpecials")
        return new mods.ClearSpecials(new core.Box(x, y, 10, 10));
    else if(strName=="MessageRegion")
        return new mods.MessageRegion(new core.Box(x, y, 10, 10), "...");
    else if(strName=="VStrongAirCurrent")
        return new mods.AirCurrent(new core.Box(x, y, 10, 10), {x: 0, y: 0}, 4);
    else if(strName=="StrongAirCurrent")
        return new mods.AirCurrent(new core.Box(x, y, 10, 10), {x: 0, y: 0}, 2);
    else if(strName=="MediumAirCurrent")
        return new mods.AirCurrent(new core.Box(x, y, 10, 10), {x: 0, y: 0}, 1);
    else if(strName=="WeakAirCurrent")
        return new mods.AirCurrent(new core.Box(x, y, 10, 10), {x: 0, y: 0}, .5);
    else if(strName=="CollapsingPlatform")
        return new mods.CollapsingPlatform(new core.Box(x, y, 10, 10), color || "#919191");
    else if(strName=="Coin")
        return new core.Coin(x-core.Coin.radius, y-core.Coin.radius);
    else if(strName=="BouncingCoin")
        return new core.BouncingPickup(new core.Coin(x-core.Coin.radius, y-core.Coin.radius), 0, 0);
    else if(strName=="TripleCoin")
        return new core.TripleCoin(x-core.TripleCoin.radius, y-core.TripleCoin.radius);
    else if(strName=="BouncingTripleCoin")
        return new core.BouncingPickup(new core.TripleCoin(x-core.TripleCoin.radius,
                    y-core.TripleCoin.radius), 0, 0);
    else if(strName=="TenCoin")
        return new core.TenCoin(x-29, y-29);
    else if(strName=="BouncingTenCoin")
        return new core.BouncingPickup(new core.TenCoin(x-29, y-29), 0, 0);
    else if(strName=="HealthPoint")
        return new core.HealthPoint(x-10, y-10);
    else if(strName=="BouncingHealthPoint")
        return new core.BouncingPickup(new core.HealthPoint(x-10, y-10), 0, 0);
    else if(strName=="Star")
        return new core.Star(x-35, y-35, color);
    else if(strName=="WoodenBox")
        return new mods.WoodenBox(x-45, y-30, new mods.NothingPickup(0, 0));
    else if(strName=="Present")
        return new mods.Present(x-45, y-30, new mods.NothingPickup(0, 0));
    else if(strName=="ExtraJump")
        return new core.ExtraJumpPickup(x-10, y-10);
    else if(strName=="FixedAltitude")
        return new core.FixedAltitudePickup(x-10, y-10);
    else if(strName=="XSpeedBoost")
        return new core.XSpeedBoostPickup(x-10, y-10);
    else if(strName=="RapidReload")
        return new core.RapidReloadPickup(x-10, y-10);
    else if(strName=="MissileSpecial")
        return new core.MissileSpecialPickup(x-10, y-10);
    else if(strName=="missile unlimited")
        return new core.MissileSpecialUnlimited(x-10, y-10);
    else if(strName=="TripleThrow")
        return new core.TripleThrowPickup(x-10, y-10);
    else if(strName=="triple throw unlimited")
        return new core.TripleThrowUnlimited(x-10, y-10);
    else if(strName=="OrangeBullet")
        return new core.OrangeBulletPickup(x-10, y-10);
    else if(strName=="orange bullet unlimited")
        return new core.OrangeBulletUnlimited(x-10, y-10);
    else if(strName=="PinkBullet")
        return new core.PinkBulletPickup(x-10, y-10);
    else if(strName=="pink bullet unlimited")
        return new core.PinkBulletUnlimited(x-10, y-10);
    else if(strName=="PurpleBullet")
        return new core.PurpleBulletPickup(x-10, y-10);
    else if(strName=="purple bullet unlimited")
        return new core.PurpleBulletUnlimited(x-10, y-10);
    else if(strName=="GreenSwarm")
        return new core.GreenSwarmPickup(x-10, y-10);
    else if(strName=="green swarm unlimited")
        return new core.GreenSwarmUnlimited(x-10, y-10);
    else if(strName=="BlueBullet")
        return new core.BlueBulletPickup(x-10, y-10);
    else if(strName=="blue bullet unlimited")
        return new core.BlueBulletUnlimited(x-10, y-10);
    else if(strName=="BrownBullet")
        return new core.BrownBulletPickup(x-10, y-10);
    else if(strName=="brown bullet unlimited")
        return new core.BrownBulletUnlimited(x-10, y-10);
    else if(strName=="YellowBullet")
        return new core.YellowBulletPickup(x-10, y-10);
    else if(strName=="yellow bullet unlimited")
        return new core.YellowBulletUnlimited(x-10, y-10);
    else if(strName=="NinjaBullet")
        return new core.NinjaBulletPickup(x-10, y-10);
    else if(strName=="ninja bullet unlimited")
        return new core.NinjaBulletUnlimited(x-10, y-10);
    else if(strName=="BadTriangle")
        return new mods.BadTriangle(x, y, x);
    else if(strName=="OrangeTriangle")
        return new mods.OrangeTriangle(x, y, x);
    else if(strName=="YellowTriangle")
        return new mods.YellowTriangle(x, y, x);
    else if(strName=="PurpleTriangle")
        return new mods.PurpleTriangle(x, y, x);
    else if(strName=="GreyTriangle")
        return new mods.GreyTriangle(x, y, x);
    else if(strName=="PinkTriangle")
        return new mods.PinkTriangle(x, y, x);
    else if(strName=="BlueTriangle")
        return new mods.BlueTriangle(x, y, x);
    else if(strName=="TurquoiseTriangle")
        return new mods.TurquoiseTriangle(x, y, x);
    else if(strName=="Cover")
        return new core.Cover(new core.Box(x, y, 10, 10), color);
    else if(strName=="Darkness")
        return new mods.Darkness(new core.Box(x, y, 10, 10), color);
    else if(strName=="LRPlatform")
        return new mods.LRPlatform(new core.Box(x, y, 10, 10), x, x, color);
    else if(strName=="UDPlatform")
        return new mods.UDPlatform(new core.Box(x, y, 10, 10), y, y, color);
    else if(strName=="StoneLauncher")
        return new mods.StoneLauncher(new core.Box(x-10, y-10, 20, 20), {"x": 1, "y": 0},
                color);
    else if(strName=="TargettingStoneLauncher")
        return new mods.TargettingStoneLauncher(new core.Box(x-10, y-10, 20, 20),
                {"x": 1, "y": 0}, color, 2);
    else if(strName=="LargeSpring")
        return new mods.Spring(new core.Box(x, y, 10, 10), color, 5, 40);
    else if(strName=="MediumSpring")
        return new mods.Spring(new core.Box(x, y, 10, 10), color, 7, 25);
    else if(strName=="SmallSpring")
        return new mods.Spring(new core.Box(x, y, 10, 10), color, 8, 15);
    else if(strName=="Button")
        return new core.Button(getUniqueId("B", flagsNames), x-25, y-5, color);
    else if(strName=="CeilingButton")
        return new core.CeilingButton(getUniqueId("CB", flagsNames), x-25, y-5, color);
    else if(strName=="WallButton")
        return new core.WallButton(getUniqueId("WB", flagsNames), x-5, y-25, color);
    else if(strName=="LeftWallButton")
        return new core.LeftWallButton(getUniqueId("LWB", flagsNames), x-5, y-25, color);
    else if(strName=="ToggleButton")
        return new core.ToggleButton(getUniqueId("TB", flagsNames), x-25, y-5, color);
    else if(strName=="HitRegion")
        return new mods.HitRegion(getUniqueId("HR", flagsNames),
                new core.Box(x, y, 10, 10));
    else if(strName=="Switch")
        return new mods.Switch(getUniqueId("S", flagsNames), x-10, y-10, color);
    else if(strName=="FadingPlatform")
        return new mods.FadingPlatform(new core.Box(x, y, 10, 10), color);
    else if(strName=="OrFlags")
        return new mods.OrFlags(getUniqueId("OF", flagsNames), [], x-20, y-15);
    else if(strName=="AndFlags")
        return new mods.AndFlags(getUniqueId("AF", flagsNames), [], x-20, y-15);
    else if(strName=="HoldAnd")
        return new mods.HoldAnd(getUniqueId("HA", flagsNames), [], x-20,
            y-15);
    else if(strName=="RelayFlag")
        return new mods.RelayFlag(getUniqueId("RF", flagsNames), "", x-15, y-10);
    else if(strName=="NegateFlag")
        return new mods.NegateFlag(getUniqueId("NF", flagsNames), "", x-15, y-10);
    else if(strName=="FlagBeenTrue")
        return new mods.FlagBeenTrue(getUniqueId("FBT", flagsNames), "", x-15, y-10);
    else if(strName=="Timeout.25")
        return new mods.Timeout(getUniqueId("T", flagsNames), "", .25, x-15, y-10);
    else if(strName=="Timeout01")
        return new mods.Timeout(getUniqueId("T", flagsNames), "", 1, x-15, y-10);
    else if(strName=="Timeout05")
        return new mods.Timeout(getUniqueId("T", flagsNames), "", 5, x-15, y-10);
    else if(strName=="Timeout10")
        return new mods.Timeout(getUniqueId("T", flagsNames), "", 10, x-15, y-10);
    else if(strName=="AlwaysTrue")
        return new mods.AlwaysTrue(getUniqueId("AT", flagsNames), x-15, y-15);
    else if(strName=="RandomlyTrue2")
        return new mods.RandomlyTrue(getUniqueId("AT", flagsNames), 1, 2, x-15, y-15);
    else if(strName=="RandomlyTrue3")
        return new mods.RandomlyTrue(getUniqueId("AT", flagsNames), 1, 3, x-15, y-15);
    else if(strName=="RandomlyTrue5")
        return new mods.RandomlyTrue(getUniqueId("AT", flagsNames), 1, 5, x-15, y-15);
    else if(strName=="ToggleFlag")
        return new mods.ToggleFlag(getUniqueId("NF", flagsNames), "", x-15, y-10);
    else if(strName=="VisualOnlyOverlay")
        return new mods.VisualOnlyOverlay(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="SignRight")
        return new core.SignRight(x-40, y-30);
    else if(strName=="SignLeft")
        return new core.SignLeft(x-40, y-30);
    else if(strName=="SignUp")
        return new core.SignUp(x-30, y-40);
    else if(strName=="SignDown")
        return new core.SignDown(x-30, y-40);
    else if(strName=="SignTarget")
        return new core.SignTarget(x-40, y-30);
    else if(strName=="SignWarning")
        return new core.SignWarning(x-40, y-30);
    else if(strName=="SignWriting")
        return new core.SignWriting(x-40, y-30);
    else if(strName=="NothingPickup")
        return new mods.NothingPickup(x-15, y-15);
    else if(strName=="PickupSpawner1")
        return new mods.PickupSpawner(
               [new mods.NothingPickup(0, 0)], new core.Box(x, y, 60, 60));
    else if(strName=="PickupSpawner2")
        return new mods.PickupSpawner(
               [new mods.NothingPickup(0, 0),
                new mods.NothingPickup(0, 0)],
                new core.Box(x, y, 60, 60));
    else if(strName=="PickupSpawner3")
        return new mods.PickupSpawner(
               [new mods.NothingPickup(0, 0),
                new mods.NothingPickup(0, 0),
                new mods.NothingPickup(0, 0)],
                new core.Box(x, y, 60, 60));
    else if(strName=="PickupSpawner4")
        return new mods.PickupSpawner(
               [new mods.NothingPickup(0, 0),
                new mods.NothingPickup(0, 0),
                new mods.NothingPickup(0, 0),
                new mods.NothingPickup(0, 0)],
                new core.Box(x, y, 60, 60));
    else if(strName=="VisualOnlyBackground")
        return new mods.VisualOnlyBackground(new core.Box(x, y, 10, 10), color || "#5f5f5f");
    else if(strName=="StartPosition") //Make it with player dimensions.
        return new mods.Position(new core.Box(x-round5(playerWidth/2),
                    y-playerHeight/2, playerWidth, playerHeight), "black");
    else if(strName=="Ninja")
        return new mods.Ninja(x-5, y-30, color);
    else if(strName=="TurquoiseNinja")
        return new mods.TurquoiseNinja(x-5, y-30, color);
    else if(strName=="GreenNinja")
        return new mods.GreenNinja(x-5, y-30, color);
    else if(strName=="GreenSquare")
        return new mods.GreenSquare(x-100, y-100, color);
    else if(strName=="GreySquare")
        return new mods.GreySquare(x-30, y-30, color);
    else if(strName=="RedRectangle")
        return new mods.RedRectangle(x-25, y-55, color);
    else if(strName=="BrownRectangle")
        return new mods.BrownRectangle(x-35, y-20, color);
    else if(strName=="OrangeRectangle")
        return new mods.OrangeRectangle(x-35, y-20, color);
    else if(strName=="YellowRectangle")
        return new mods.YellowRectangle(x-35, y-20, color);
    else if(strName=="OrangeSquare")
        return new mods.OrangeSquare(x-10, y-10, color);
    else if(strName=="BlackSquare")
        return new mods.BlackSquare(x-10, y-10, color);
    else if(strName=="GhostSquare")
        return new mods.GhostSquare(x-10, y-10, color);
    else if(strName=="PurpleSquare")
        return new mods.PurpleSquare(x-10, y-10, color);
    else if(strName=="FixedCuttingLaser")
        return new mods.CuttingLaser(new core.Platform(new core.Box(x, y, 10, 10), color), {x: 0, y: 0});
    else if(strName=="LRCuttingLaser")
        return new mods.CuttingLaser(new mods.LRPlatform(new core.Box(x, y, 10, 10), x, x, color, 100),
                {x: 0, y: 0});
    else if(strName=="UDCuttingLaser")
        return new mods.CuttingLaser(new mods.UDPlatform(new core.Box(x, y, 10, 10), y, y, color, 100),
                {x: 0, y: 0});
    else if(strName=="FixedTriggerLaser")
        return new mods.TriggerLaser(getUniqueId("TL", flagsNames),
                new core.Platform(new core.Box(x, y, 10, 10), color), {x: 0, y: 0});
    else if(strName=="LRTriggerLaser")
        return new mods.TriggerLaser(getUniqueId("TL", flagsNames),
                new mods.LRPlatform(new core.Box(x, y, 10, 10), x, x, color, 100),
                {x: 0, y: 0});
    else if(strName=="UDTriggerLaser")
        return new mods.TriggerLaser(getUniqueId("TL", flagsNames),
                new mods.UDPlatform(new core.Box(x, y, 10, 10), y, y, color, 100),
                {x: 0, y: 0});
    else if(strName=="SnowEmitter")
        return new mods.SnowEmitter(new core.Box(x, y, 10, 10));
    else if(strName=="RainEmitter")
        return new mods.RainEmitter(new core.Box(x, y, 10, 10));
    else if(strName=="PaymentRegion01")
        return new mods.PaymentRegion(getUniqueId("PR", flagsNames),
                new core.Box(x, y, 10, 10), 1);
    else if(strName=="PaymentRegion05")
        return new mods.PaymentRegion(getUniqueId("PR", flagsNames),
                new core.Box(x, y, 10, 10), 5);
    else if(strName=="PaymentRegion10")
        return new mods.PaymentRegion(getUniqueId("PR", flagsNames),
                new core.Box(x, y, 10, 10), 10);
    else if(strName=="PaymentRegion20")
        return new mods.PaymentRegion(getUniqueId("PR", flagsNames),
                new core.Box(x, y, 10, 10), 20);
    else if(strName=="PortalRegion")
        return new mods.PortalRegion(getUniqueId("P", pipeNames), "",
            new core.Box(x, y, 10, 10));
    else if(strName=="LighteningEffect")
        return new mods.LighteningEffect(new core.Box(x, y, 10, 10), color);
    else if(strName=="VerticalGateGrounded") {
        var res = new mods.Group([
            new core.JumpThroughPlatform(new core.Box(-20,105,45,15),"transparent"),
            new core.Button("B/0",-25,310,"transparent"),
            new core.CeilingButton("CB/0",-20,-95,"transparent"),
            new mods.ApplyFlag("OF/1",new mods.UDPlatform(new core.Box(-10,110,25,210),-95,110,lightenColor(color,2/5),250)),
            new core.Platform(new core.Box(15,-110,15,30),"transparent"),
            new core.Platform(new core.Box(-25,-110,55,20),"transparent"),
            new core.Platform(new core.Box(-25,-110,15,30),"transparent"),
            new core.Platform(new core.Box(-25,105,15,220),"transparent"),
            new core.Platform(new core.Box(15,105,15,220),"transparent"),
            new core.Platform(new core.Box(-25,320,55,20),"transparent"),
            new mods.NegateFlag("NF/1","RF/0",-15,180),
            new mods.NegateFlag("NF/4","CB/0",-15,115),
            new mods.NegateFlag("NF/5","B/0",-15,280),
            new mods.AndFlags("AF/2",["NF/1","NF/4"],-15,155),
            new mods.AndFlags("AF/3",["NF/5","RF/0"],-15,240),
            new mods.OrFlags("OF/1",["AF/2","AF/3"],-15,210),
            new mods.RelayFlag("RF/0","",-120,70),
            new mods.VisualOnlyOverlay(new core.Box(-25,-110,55,30),color),
            new mods.VisualOnlyOverlay(new core.Box(-25,105,55,235),color),
        ]);
        renameFlagsAndPipes(flagsNames, pipeNames, res.objs);
        getVisibleChildren([res]).forEach(o => translateObj(o, x, y));
        return res;
    }
    else if(strName=="HorizontalGateFromLeft") {
        var res = new mods.Group([
            new mods.ApplyFlag("OF/0",new mods.LRPlatform(new core.Box(-345,-10,230,25),-345,-115,lightenColor(color,2/5),500)),
            new core.LeftWallButton("LWB/0",-350,-20,"transparent"),
            new core.WallButton("WB/0",100,-20,"transparent"),
            new core.Platform(new core.Box(-365,-25,260,15),"transparent"),
            new core.Platform(new core.Box(-365,15,260,15),"transparent"),
            new core.Platform(new core.Box(-365,-20,15,50),"transparent"),
            new core.Platform(new core.Box(100,-25,30,15),"transparent"),
            new core.Platform(new core.Box(100,15,30,15),"transparent"),
            new core.Platform(new core.Box(115,-15,15,35),"transparent"),
            new mods.ApplyFlag("LWB/0",new mods.FadingPlatform(new core.Box(100,-15,10,35),"transparent")),
            new mods.ApplyFlag("LWB/0",new mods.FadingPlatform(new core.Box(-125,-20,20,45),"transparent")),
            new mods.RelayFlag("RF/1","",-95,-115),
            new mods.AndFlags("AF/0",["NF/0","NF/1"],-255,-10),
            new mods.NegateFlag("NF/0","WB/0",-205,-10),
            new mods.OrFlags("OF/0",["AF/0","AF/1"],-295,-10),
            new mods.NegateFlag("NF/1","RF/1",-280,-5),
            new mods.AndFlags("AF/1",["RF/1","NF/2"],-315,-10),
            new mods.NegateFlag("NF/2","LWB/0",-335,-10),
            new mods.VisualOnlyOverlay(new core.Box(-365,-25,260,55),color),
            new mods.VisualOnlyOverlay(new core.Box(100,-25,30,55),color),
        ]);
        renameFlagsAndPipes(flagsNames, pipeNames, res.objs);
        getVisibleChildren([res]).forEach(o => translateObj(o, x, y));
        return res;
    }
    else if(strName=="VerticalGateCeiling") {
        //Create component, using save generated by the level builder.
        var res = loadTextObjects(`{"type": "_buffer", "version": 2,
            "objs": [{"type": "down through platform", "box": {"type": "box", "x": -30, "y": -105, "w": 55, "h": 15}, "color": "transparent", "_group": 0}, {"type": "apply flag", "flag": "RF/0", "obj": {"type": "fading platform", "box": {"type": "box", "x": -20, "y": 95, "w": 35, "h": 15}, "color": "transparent"}, "_group": 0}, {"type": "normal platform", "box": {"type": "box", "x": -30, "y": -310, "w": 15, "h": 220}, "color": "transparent", "_group": 0}, {"type": "normal platform", "box": {"type": "box", "x": 10, "y": -310, "w": 15, "h": 220}, "color": "transparent", "_group": 0}, {"type": "normal platform", "box": {"type": "box", "x": -30, "y": -325, "w": 55, "h": 20}, "color": "transparent", "_group": 0}, {"type": "apply flag", "flag": "OF/0", "obj": {"type": "up-down platform", "box": {"type": "box", "x": -15, "y": -305, "w": 25, "h": 205}, "start": -305, "end": -95, "color": "`+lightenColor(color,2/5)+`", "accel": 250}, "_group": 0}, {"type": "floor button", "x": -25, "y": 95, "color": "transparent", "name": "B/2", "_group": 0}, {"type": "ceiling button", "x": -25, "y": -310, "color": "transparent", "name": "CB/2", "_group": 0}, {"type": "normal platform", "box": {"type": "box", "x": 10, "y": 95, "w": 15, "h": 30}, "color": "transparent", "_group": 0}, {"type": "normal platform", "box": {"type": "box", "x": -30, "y": 105, "w": 55, "h": 20}, "color": "transparent", "_group": 0}, {"type": "normal platform", "box": {"type": "box", "x": -30, "y": 95, "w": 15, "h": 30}, "color": "transparent", "_group": 0}, {"type": "overlay visual", "box": {"type": "box", "x": -30, "y": 95, "w": 55, "h": 30}, "color": "`+color+`", "_group": 0}, {"type": "relay flag", "name": "RF/0", "flag": "", "_x": -125, "_y": -35, "_group": 0}, {"type": "not flag", "name": "NF/0", "flag": "RF/0", "_x": -20, "_y": -240, "_group": 0}, {"type": "not flag", "name": "NF/1", "flag": "B/2", "_x": -20, "_y": -305, "_group": 0}, {"type": "not flag", "name": "NF/2", "flag": "CB/2", "_x": -20, "_y": -140, "_group": 0}, {"type": "and flags", "name": "AF/0", "flags": ["NF/0", "NF/1"], "_x": -20, "_y": -265, "_group": 0}, {"type": "and flags", "name": "AF/1", "flags": ["NF/2", "RF/0"], "_x": -20, "_y": -180, "_group": 0}, {"type": "or flags", "name": "OF/0", "flags": ["AF/0", "AF/1"], "_x": -20, "_y": -210, "_group": 0}, {"type": "overlay visual", "box": {"type": "box", "x": -30, "y": -325, "w": 55, "h": 235}, "color": "`+color+`", "_group": 0}]
        }`)[0];
        renameFlagsAndPipes(flagsNames, pipeNames, res.objs);
        getVisibleChildren([res]).forEach(o => translateObj(o, x, y));
        return res;
    }
    else if(strName=="HorizontalGateFromRight") {
        //Create component, using save generated by the level builder.
        var res = loadTextObjects(`{"type":"_buffer","objs":[{"type":"left wall button","x":-115,"y":-20,"color":"transparent","name":"LWB/6","_group":0},{"type":"right wall button","x":335,"y":-20,"color":"transparent","name":"WB/6","_group":0},{"type":"normal platform","box":{"type":"box","x":105,"y":-25,"w":260,"h":15},"color":"transparent","_group":0},{"type":"normal platform","box":{"type":"box","x":105,"y":15,"w":260,"h":15},"color":"transparent","_group":0},{"type":"normal platform","box":{"type":"box","x":350,"y":-20,"w":15,"h":50},"color":"transparent","_group":0},{"type":"normal platform","box":{"type":"box","x":-130,"y":-25,"w":30,"h":15},"color":"transparent","_group":0},{"type":"normal platform","box":{"type":"box","x":-130,"y":15,"w":30,"h":15},"color":"transparent","_group":0},{"type":"normal platform","box":{"type":"box","x":-130,"y":-15,"w":15,"h":35},"color":"transparent","_group":0},{"type":"apply flag","flag":"WB/6","obj":{"type":"fading platform","box":{"type":"box","x":-110,"y":-15,"w":10,"h":35},"color":"transparent"},"_group":0},{"type":"apply flag","flag":"WB/6","obj":{"type":"fading platform","box":{"type":"box","x":105,"y":-20,"w":20,"h":45},"color":"transparent"},"_group":0},{"type":"apply flag","flag":"OF/6","obj":{"type":"left-right platform","box":{"type":"box","x":115,"y":-10,"w":230,"h":25},"start":-115,"end":115,"color":"`+lightenColor(color,2/5)+`","accel":500},"_group":0},{"type":"relay flag","name":"RF/6","flag":"","_x":60,"_y":-115,"_group":0},{"type":"and flags","name":"AF/12","flags":["NF/18","NF/19"],"_x":225,"_y":-20,"_group":0},{"type":"not flag","name":"NF/18","flag":"LWB/6","_x":280,"_y":-15,"_group":0},{"type":"or flags","name":"OF/6","flags":["AF/12","AF/13"],"_x":160,"_y":-10,"_group":0},{"type":"not flag","name":"NF/19","flag":"RF/6","_x":200,"_y":-5,"_group":0},{"type":"and flags","name":"AF/13","flags":["RF/6","NF/20"],"_x":130,"_y":-10,"_group":0},{"type":"not flag","name":"NF/20","flag":"WB/6","_x":115,"_y":-5,"_group":0},{"type":"overlay visual","box":{"type":"box","x":-130,"y":-25,"w":30,"h":55},"color":"`+color+`","_group":0},{"type":"overlay visual","box":{"type":"box","x":105,"y":-25,"w":260,"h":55},"color":"`+color+`","_group":0}],"version":2}`)[0];
        renameFlagsAndPipes(flagsNames, pipeNames, res.objs);
        getVisibleChildren([res]).forEach(o => translateObj(o, x, y));
        return res;
    }
    //Image set later to ensure it is added to the level correctly
    else if(strName=="ImageBackground")
        return new core.ImageBackground(new core.Box(x, y, 10, 10), "");
    else if(strName=="ImageOverlay")
        return new core.ImageOverlay(new core.Box(x, y, 10, 10), "");
    else
        throw new Error("Unknown game object: '"+strName+"'");
}

const defaultSpikeWidth = 10;
