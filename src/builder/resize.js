//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js';
import {Operation} from './operation.js';
import {Action} from './action.js';
import {registerAll, clipLevelX, clipLevelY, getScale, resizeBox, round5} from './utility.js';
import {highlightBox, handleRadius, selectHighlight, selectHandleHighlight} from './visuals.js';
import {isImageObject} from './images.js';

//Resize the box of an object (no history entry created).
//Supports all objects for which "canResize" returns true.
export class ResizeOperation extends Operation {
    //obj: Object to resize box on
    //startPos: Fixed corner the box is being resized around, and this will be a
    //corner on the box after it has been resized..
    constructor(next, obj, startPos) {
        super(next);
        this.obj = obj;
        this.startPos = startPos;
    }
    move(state, game, gX, gY) {
        resizeBox(this.startPos, clipLevelX(game.level, gX),
                clipLevelY(game.level, gY), this.obj.box,
                state.snapToGrid);
        updateInsertResize(game, this.startPos, this.obj);
        return this;
    }
    draw(state, game, context) {
        highlightBox(context, this.obj.box, selectHighlight, getScale(game));
        //Draw select handle for corner currently being dragged.
        var currentPos = this.obj.box.oppositeCorner(this.startPos);
        context.beginPath();
        context.arc(currentPos.x, currentPos.y, handleRadius/getScale(game),
            0, Math.PI*2);
        context.fillStyle = selectHandleHighlight;
        context.fill();
    }
}

//Like ResizeOperation, but creates a history entry for the new size.
export class HistoryResizeOperation extends ResizeOperation {
    //obj: Object to modify
    //root: Fixed corner which will exist on the newly resized box.
    //focus: Corner which is being repositioned (needed for history entry).
    constructor(next, obj, root, focus) {
        super(next, obj, root);
        this.cornerBefore = focus;
    }
    move(state, game, gX, gY) {
        resizeBox(this.startPos, clipLevelX(game.level, gX),
                clipLevelY(game.level, gY), this.obj.box,
                state.snapToGrid);
        updateResize(game, this.startPos, this.obj);
        return this;
    }
    release(state, game) {
        var cornerAfter = this.obj.box.oppositeCorner(this.startPos);
        state.actionHistory.push(new ResizeAction(this.obj, this.startPos, this.cornerBefore, cornerAfter));
        return super.release(state, game);
    }
}

//Resize an object.
class ResizeAction extends Action {
    //root: Corner which was unaffected by the resize.
    //cornerBefore: Position of opposite corner to root before resize.
    //cornerAfter: Position of opposite corner to root after resize.
    constructor(obj, root, cornerBefore, cornerAfter) {
        super();
        this.obj = obj;
        this.root = root;
        this.before = cornerBefore;
        this.after = cornerAfter;
    }
    apply(game) {
        resizeBox(this.root, this.after.x, this.after.y, this.obj.box, false, true);
        updateResize(game, this.root, this.obj);
    }
    revoke(game) {
        resizeBox(this.root, this.before.x, this.before.y, this.obj.box, false, true);
        updateResize(game, this.root, this.obj);
    }
}

//Can obj be resized with ResizeOperation?
export function canResize(obj) {
    return (obj instanceof core.Platform || obj instanceof core.SpikeRow
            || obj instanceof core.SpikeColumn || obj instanceof core.Region
            || obj instanceof core.Water || obj instanceof core.Cover
            || obj instanceof core.VisualOnlyOverlay
            || obj instanceof core.VisualOnlyBackground
            || obj instanceof core.HealthLoss || obj instanceof core.Darkness
            || obj instanceof core.LighteningEffect
            || obj instanceof core.SnowEmitter || obj instanceof core.RainEmitter
            || obj instanceof core.PickupSpawner
            || obj instanceof core.ImageOverlay
            || obj instanceof core.ImageBackground
            ) && !(obj instanceof core.Button) && !(obj instanceof core.Switch)
            && !(obj instanceof core.WallButton) && !(obj instanceof core.PickupBox);
}

//Update obj's state to accomodate new size correctly when inserting a new
//object (including changing spike directions).
//game: Game containing the object, and may need updating.
//game: Game containing the object.
//root: Corner from which resize started.
//obj: Object affected.
function updateInsertResize(game, root, obj) {
    //Set spike direction based on the direction the box is being dragged.
    //If resizing spikes, adjust width.
    if(obj instanceof core.SpikeRow) {
        obj.dir = obj.box.y<root.y?-1:1;
        obj._setSpikeWidth(obj.targetWidth);
    }
    else if(obj instanceof core.SpikeColumn) {
        obj.dir = obj.box.x<root.x?-1:1;
        obj._setSpikeHeight(obj.targetHeight);
    }
    //Set moving platforms (adjusting start/end positions as necessary).
    else if(obj instanceof core.LRPlatform) {
        obj.startPos.x = obj.alternateStartX
            = obj.endPos.x = obj.box.x;
        obj.startPos.y = obj.startPos.y = obj.box.y;
    }
    else if(obj instanceof core.UDPlatform) {
        obj.startPos.y = obj.alternateStartY
            = obj.endPos.y = obj.box.y;
        obj.startPos.x = obj.startPos.x = obj.box.x;
    }
    else
        updateResize(game, root, obj);
}
//Update obj's state to accomodate new size correctly when resizing (like
//adjusting start positions, spike widths/heights, etc.).
//game: Game containing the object, and may need updating.
//root: Corner from which resize started.
//obj: Object affected.
function updateResize(game, root, obj) {
    if(obj instanceof core.CollapsingPlatform)
        obj.startY = obj.box.y;
    //Update spike width/height to fill new height/width
    else if(obj instanceof core.SpikeRow)
        obj._setSpikeWidth(obj.targetWidth);
    else if(obj instanceof core.SpikeColumn)
        obj._setSpikeHeight(obj.targetHeight);
    //Prevent moving platforms going off route and not adjusting correctly.
    else if(obj instanceof core.LRPlatform) {
        if(obj.box.x<obj.startPos.x)
            obj.box.x = obj.startPos.x;
        obj.startPos.y = obj.endPos.y = obj.box.y;
    }
    else if(obj instanceof core.UDPlatform) {
        if(obj.box.y<obj.startPos.y)
            obj.box.y = obj.startPos.y;
        obj.startPos.x = obj.endPos.x = obj.box.x;
    }
    //Make spring constraints fit the new spring size.
    else if(obj instanceof core.Spring) {
        obj.frames.forEach(function(f) {
            game.collisions.remove(f);
        });
        obj._setConstraints();
        obj.frames.forEach(function(f) {
            game.collisions.add(f);
        });
        obj.startY = obj.box.y;
    }
    //Move spawned pickup with the the pickup spawner (excluding moving pickups,
    //as that can look odd).
    else if(obj instanceof core.PickupSpawner && obj.spawned
        && !obj.spawned.motion) {
        var ob = obj.box;
        var sb = obj.spawned.box;
        sb.x = ob.x+ob.width/2-sb.width/2;
        sb.y = ob.y+ob.height/2-sb.height/2;
    }
    //Have the right number of particles in an emitter for its size
    //(as there is more snow, for example, in a bigger SnowEmitter).
    else if(obj instanceof core.SnowEmitter
        || obj instanceof core.RainEmitter) {
        obj._setupPulses();
        obj.pool.apply(o => o.disable());
        registerAll(game);
    }
    //Ensure that the cache of the image is the correct size, and cause all
    //caches of this image to be refreshed to avoid having too many sizes
    //cached.
    else if(isImageObject(obj)) {
        game.caches.removePrefixed(obj.textureName);
        registerAll(game);
    }
}
