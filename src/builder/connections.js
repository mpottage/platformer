//Copyright Matthew Pottage 2019
import * as core from '../core/import-level-objects.js';
import * as mods from './mods/import-level-objects.js';
import {getUniqueId} from './utility.js';

export function getFlagNames(game) {
    return Object.keys(getFlagsAndControls(game.level.objs));
}
export function getPipeNames(game) {
    return Object.keys(getPortals(game.level.objs));
}

//Given objects in a level it returns a dictionary containg all flags in use and
//the object creating the flag and any users.
//Format: {"name": {owner: reference, users: [], linkedTo: boolean,
//          showDetails: boolean}}.
export function getFlagsAndControls(levelObjs) {
    var res = {};
    levelObjs.forEach(function(o) {
        if(o instanceof core.ApplyFlag || o instanceof core.MakeAndApplyFlag) {
            if(!res[o.flag])
                res[o.flag] = {owner: null, users: [o], showDetails: true};
            else
                res[o.flag].users.push(o);
        }
        if(o instanceof core.ApplyFlag)
            o = o.obj; //Now look at any flags made by held object.
        if((o instanceof core.Button) || (o instanceof core.Switch)
                || (o instanceof core.WallButton)
                || (o instanceof core.RelayFlag)
                || (o instanceof core.NegateFlag) || (o instanceof core.FlagBeenTrue)
                || (o instanceof core.Timeout) || (o instanceof core.OrFlags)
                || (o instanceof core.AndFlags) || (o instanceof core.HoldAnd)
                || (o instanceof core.HitRegion) || (o instanceof core.MakeFlagFrom)
                || (o instanceof core.AlwaysTrue) || (o instanceof core.MakeAndApplyFlag)
                || (o instanceof core.TriggerLaser)
                || (o instanceof core.PaymentRegion) || (o instanceof core.RandomlyTrue)
                || (o instanceof core.ToggleFlag)) {
            if(!res[o.name])
                res[o.name] = {owner: o, users: [], showDetails: true,
                    linkedTo: false};
            else
                res[o.name].owner = o;
        }
        if((o instanceof core.RelayFlag || o instanceof core.NegateFlag
            || o instanceof core.FlagBeenTrue || o instanceof core.Timeout
            || o instanceof core.ToggleFlag)
                && o.tracking!="") {
            res[o.name].linkedTo = true;
            if(!res[o.tracking])
                res[o.tracking] = {owner: null, users: [o], linkedTo: false,
                    showDetails: true};
            else
                res[o.tracking].users.push(o);
        }
        if(o instanceof core.OrFlags || o instanceof core.AndFlags
                    || o instanceof core.HoldAnd) {
            res[o.name].linkedTo = o.flagNames.length>0;
            o.flagNames.forEach(function(f) {
                if(!res[f])
                    res[f] = {owner: null, users: [o], linkedTo: false,
                        showDetails: true};
                else
                    res[f].users.push(o);
            });
        }
        if(o instanceof mods.Group) {
            var tmp = getFlagsAndControls(o.objs);
            for(var f in tmp) {
                if(!res[f]) {
                    res[f] = tmp[f];
                }
                //Already got an owner or details
                else {
                    if(tmp[f].owner)
                        res[f].owner = tmp[f].owner;
                    res[f].users = res[f].users.concat(tmp[f].users);
                }
                //Hide outgoing links if the owner of the flag is inside the
                //group
                res[f].showDetails = !tmp[f].owner;
            }
        }
    });
    return res;
}

//Given objects in a level it returns a dictionary of all pipes in use, and what
//they connect to.
//Format: {"pipe": {owner: reference, user: }}.
export function getPortals(levelObjs) {
    var res = {};
    levelObjs.forEach(function(o) {
        if(o instanceof core.ApplyFlag)
            o = o.obj;
        if(o instanceof mods.Group) {
            var tmp = getPortals(o.objs);
            for(var p in tmp) {
                if(!res[p]) {
                    res[p] = tmp[p];
                }
                //Already got an owner or details
                else {
                    if(tmp[p].owner)
                        res[p].owner = tmp[p].owner;
                    res[p].user = tmp[p].user;
                }
                //Hide outgoing pipes if the owner of the pipe is inside the
                //group
                res[p].showDetails = !tmp[p].owner;
            }
        }
        else if(o instanceof core.PortalRegion) {
            if(!res[o.output])
                res[o.output] = {owner: o, user: null, showDetails: true};
            else
                res[o.output].owner = o;
            if(!res[o.input])
                res[o.input] = {owner: null, user: o, showDetails: true};
            else
                res[o.input].user = o;
        }
    });
    return res;
}

//Create new flags/pipes for any objects that make them, retaining internal
//links to avoid the cloned setup mixing with the original setup.
export function renameFlagsAndPipes(flagsNames, pipeNames, cloned) {
    var flags = getFlagsAndControls(cloned);
    for(var f in flags) {
        var owner = flags[f].owner;
        if(owner) {
            owner.name = getUniqueId(f.split('/')[0], flagsNames);
            flagsNames.push(owner.name); //Cleaned up later by action.
            flags[f].users.forEach(function(o) {
                if(o instanceof core.ApplyFlag)
                    o.flag = owner.name;
                else if(o instanceof core.RelayFlag
                        || o instanceof core.NegateFlag
                        || o instanceof core.FlagBeenTrue
                        || o instanceof core.Timeout
                        || o instanceof core.ToggleFlag)
                    o.tracking = owner.name;
                else if(o instanceof core.OrFlags
                        || o instanceof core.AndFlags
                        || o instanceof core.HoldAnd)
                    o.flagNames[o.flagNames.indexOf(f)] = owner.name;
            });
        }
    }
    var pipes = getPortals(cloned);
    for(var p in pipes) {
        var details = pipes[p];
        if(details.owner) {
            details.owner.output = getUniqueId("P", pipeNames)
            pipeNames.push(details.owner.output);
            if(details.user)
                details.user.input = details.owner.output;
        }
        else
            details.user.input = "";
    }
}
