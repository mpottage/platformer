import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {getFlagNames, getPipeNames} from './connections.js';
import {DelayRelease} from './operation-utilities.js';
import {LRMovingOperation, UDMovingOperation} from './route-setting.js';
import {ActionGroup} from './action.js';
import {createLevelObj} from './create-object.js';
import {AddObject, ReplaceObject} from './add-delete-actions.js';
import {clipLevelX, clipLevelY} from './utility.js';
import {AimLauncher, DirectLaser} from './set-direction.js';
import {AddStartPosition} from './start-position-actions.js';
import {getUniqueId} from './utility.js';
import {ResizeOperation, canResize} from './resize.js';
import * as pickupContainers from './pickup-containers.js';
import {translateObj, getEdgeYBelow} from './positioning-utilities.js';
import {MoveMultiple} from './move.js';
import {getPRMergeAction} from './payment-regions.js';
import {isImageObject, getImageName} from './images.js';
import {getTopObj, getVisibleChildren, getRootParent} from './filtering.js';
import {DirectCurrent, SetMessageOperation} from './setup-region.js';

//Create a new object of a specified type and insert it into the level at
//position (gameX, gameY).
export function insertObjAt(state, game, queryDialog, gameX, gameY) {
    var insertPos = {x: clipLevelX(game.level, gameX),
        y: clipLevelY(game.level, gameY)};
    var newObj = createLevelObj(insertPos.x, insertPos.y, state.insertObject,
        state.color, getFlagNames(game), getPipeNames(game));
    var action = new AddObject(newObj);
    var operation = null;
    //Inserting BadTriangle has 2 stages. Setting the start position and setting
    //the end position. Same for StoneLauncher.
    if(newObj instanceof core.BadTriangle) {
        var point = {x: newObj.box.x, y: newObj.startPos.y};
        //Position the triangle on a nearby platform (at most its height away -
        //otherwise it wasn't intended to be on one).
        var diffY = getEdgeYBelow(point.x+1, point.y, game.collisions,
                newObj.box.height, game.level.height)-point.y;
        translateObj(newObj, 0, diffY);
        operation = new DelayRelease(
                new LRMovingOperation(null, newObj, insertPos), 1);
    }
    else if(newObj instanceof core.StoneLauncher)
        operation = new DelayRelease(new AimLauncher(null, newObj), 1);
    //Adding a new Pickup (or BouncingPickup) to a PickupBox or PickupSpawner.
    else if((newObj instanceof core.Pickup)
            || (newObj instanceof core.BouncingPickup)) {
        var newAction = pickupContainers.getAddAction(state.snapToGrid, game,
            insertPos.x, insertPos.y, newObj);
        if(newAction)
            action = newAction;
    }
    else if(newObj instanceof mods.Position) {
        var cont = game.level.objs.find(function(o) {
            return o instanceof mods.StartPositions;
        });
        if(cont)
            action = new AddStartPosition(cont, newObj);
        else
            action = new AddObject(new mods.StartPositions(
                            [{x: newObj.box.x, y: newObj.box.y}])
                    );
    }
    else if(newObj instanceof core.PickupSpawner) {
        var wooden = pickupContainers.getSpawnerToBox(state.snapToGrid, game,
            insertPos.x, insertPos.y);
        if(wooden) {
            newObj.box.x = wooden.box.x;
            newObj.box.y = wooden.box.y;
            newObj.box.width = wooden.box.width;
            newObj.box.height = wooden.box.height;
            var root = getRootParent(wooden, game.level.objs);
            if(root instanceof core.MakeFlagFrom)
                action = new AddObject(new mods.ApplyFlag(root.name, newObj));
            else if(!root) {
                var flagName = getUniqueId("Obj", getFlagNames(game));
                action = new ActionGroup([
                    new ReplaceObject(wooden,
                        new mods.MakeFlagFrom(flagName, wooden)),
                    new AddObject(new mods.ApplyFlag(flagName, newObj))
                ]);
            }
        }
        else
            operation = new ResizeOperation(null, newObj, insertPos);
    }
    else if(newObj instanceof core.Laser
            && newObj.follow instanceof core.Platform) {
        operation = new ResizeOperation(null, newObj.follow, insertPos);
        operation.next = new DirectLaser(null, newObj);
        if(newObj.follow instanceof core.LRPlatform)
            operation.next.next = new LRMovingOperation(null, newObj.follow);
        else if(newObj.follow instanceof core.UDPlatform)
            operation.next.next = new UDMovingOperation(null, newObj.follow);
    }
    else if(isFloorSnapping(newObj)) {
        var point = {x: newObj.box.x+newObj.box.width/2,
            y: newObj.box.y+newObj.box.height};
        var shiftLimit = 1+newObj.box.height;
        var diffY = getEdgeYBelow(point.x, point.y, game.collisions,
                shiftLimit, game.level.height)-point.y;
        if(diffY!==shiftLimit) //If the object doesn't need to move too far
            translateObj(newObj, 0, diffY);
    }
    else if(canResize(newObj)) {
        operation = new ResizeOperation(null, newObj, insertPos);
    }
    else if(newObj instanceof mods.Group) {
        operation = new MoveMultiple(null,
            getVisibleChildren(newObj.objs), {x: gameX, y: gameY});
    }
    if(newObj instanceof core.LRPlatform)
        operation.next = new LRMovingOperation(null, newObj);
    else if(newObj instanceof core.UDPlatform)
        operation.next = new UDMovingOperation(null, newObj);
    else if(newObj instanceof core.AirCurrent)
        operation.next = new DirectCurrent(null, newObj);
    else if(newObj instanceof core.PaymentRegion) {
        var newAction = getPRMergeAction(game, newObj);
        if(newAction) {
            action = newAction;
            operation = null;
        }
    }
    else if(newObj instanceof core.MessageRegion) {
        var sign = getTopObj(state.snapToGrid, game.manager, game.level,
            gameX, gameY);
        if(sign instanceof core.SignWriting) {
            newObj = new mods.MessageRegion(
                new core.Box(sign.box.x, sign.box.y, sign.box.width,
                    sign.box.height), "...");
            action = new AddObject(newObj);
            operation = new SetMessageOperation(null, queryDialog, newObj);
        }
        else {
            operation.next = new SetMessageOperation(null, queryDialog, newObj);
        }
    }
    else if(isImageObject(newObj)) {
        var res = getImageName(game, state.currentImage);
        if(res.action)
            action = new ActionGroup([action, res.action]);
        newObj.textureName = res.name;
    }
    //Implicit registerAll to ensure the new object doesn't overlay the player.
    action.apply(game);
    state.actionHistory.push(action);
    return operation;
}

function isFloorSnapping(obj) {
    return (obj instanceof core.Button
        && !(obj instanceof core.CeilingButton))
        || (obj instanceof core.PickupBox)
        || (obj instanceof core.SignRight) || (obj instanceof core.SignLeft)
        || (obj instanceof core.SignTarget) || (obj instanceof core.SignWarning)
        || (obj instanceof core.SignWriting);
}
