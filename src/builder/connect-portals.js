import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {Operation} from './operation.js';
import {Action, ActionGroup} from './action.js';
import {registerAll, getUniqueId, getScale} from './utility.js';
import {getPortals} from './connections.js';
import {getTopObj, getRootParent} from './filtering.js';
import {highlightBox, hoverHighlight, selectHighlight} from './visuals.js';

//Portal mode. Connects or disconnects two portals. Requires "press" to be
//called on creation.
export class ConnectPortals extends Operation {
    constructor(next) {
        super(next);
        this.outputObj = null;
        this.pos = {x: 0, y: 0};
    }
    //Get the portal (not inside a group) the operation would start or end at
    //at, if at position (gameX, gameY) in the game.
    static getStart(snapToGrid, game, gameX, gameY) {
        return getTopObj(snapToGrid, game.manager, game.level, gameX, gameY,
            obj => (obj instanceof core.PortalRegion
                && !(getRootParent(obj, game.level.objs) instanceof mods.Group)));
    }
    press(state, game, gameX, gameY) {
        //Get a portal to modify
        this.outputObj = ConnectPortals.getStart(state.snapToGrid, game, gameX, gameY);
        if(this.outputObj) {
            this.pos.x = gameX;
            this.pos.y = gameY;
            return this;
        }
        else
            return this.next;
    }
    move(state, game, gameX, gameY) {
        //Remember last position of point (used to decide portal to connect to).
        this.pos.x = gameX;
        this.pos.y = gameY;
        return this;
    }
    release(state, game) {
        var target = ConnectPortals.getStart(state.snapToGrid, game, this.pos.x, this.pos.y);
        var pipesDetails = getPortals(game.level.objs);
        if(target && target!=this.outputObj) {
            var action = new ConnectPortalsAction(this.outputObj, target,
                this.outputObj.input, target.input);
            var toClear = [pipesDetails[this.outputObj.output].user,
                pipesDetails[target.output].user];
            for(var user of toClear) {
                if(user)
                    action = new ActionGroup([action, new
                        ClearPortalInput(user, user.input)]);
            }
            action.apply(game);
            state.actionHistory.push(action);
        }
        return this.next;
    }
    draw(state, game, context) {
        highlightBox(context, this.outputObj.box, selectHighlight,
            getScale(game));
        var hover = ConnectPortals.getStart(state.snapToGrid, game, this.pos.x, this.pos.y);
        if(hover)
            highlightBox(context, hover.box, hoverHighlight, getScale(game));
        //Draw outgoing line from portal being dragged from
        context.beginPath();
        var from = this.outputObj.box.center;
        context.moveTo(from.x, from.y);
        context.lineTo(this.pos.x, this.pos.y);
        context.stroke();
    }
}

//Make it so that no portals teleport to portal
export class ClearPortalInput extends Action {
    constructor(portal, oldInput) {
        super();
        this.portal = portal;
        this.prev = oldInput;
    }
    apply(game) {
        this.portal.input = "";
        registerAll(game);
    }
    revoke(game) {
        this.portal.input = this.prev;
    }
}

//Make two portals teleport between each other.
class ConnectPortalsAction extends Action {
    constructor(portal1, portal2, prevP1Input, prevP2Input) {
        super();
        this.portal1 = portal1;
        this.portal2 = portal2;
        this.prevP1Input = prevP1Input;
        this.prevP2Input = prevP2Input;
    }
    apply(game) {
        this.portal1.input = this.portal2.output;
        this.portal2.input = this.portal1.output;
        registerAll(game);
    }
    revoke(game) {
        this.portal1.input = this.prevP1Input;
        this.portal2.input = this.prevP2Input;
    }
}
