//Copyright Matthew Pottage 2019.
import {getAngle} from '../core/utility.js';
import {getScale} from './utility.js';
import {highlightBox, selectHighlight} from './visuals.js';
import {Operation} from './operation.js';

//Gets the direction that is best approximated by the position (gameX, gameY)
//relative to box. Returns {x:1,y:0}, {x:-1,y:0}, {x:0,y:1} or {x:0,y:-1}
function getDirection(box, gameX, gameY) {
    var center = box.center;
    //Offset of user to the launcher's center
    var angle = getAngle(gameX-center.x, gameY-center.y);
    //Split area around the object into quadrants (divide by Math.PI/2, then
    //round giving 4 possible values), then use cos/sin to get x/y values.
    var scaled = Math.round(angle/(Math.PI/2))*Math.PI/2;
    return {"x": Math.round(Math.cos(scaled)),
            "y": Math.round(Math.sin(scaled))}
}

//Set the direction a new stone launcher is firing.
export class AimLauncher extends Operation {
    //launcher: StoneLauncher to direct
    constructor(next, launcher) {
        super(next);
        this.obj = launcher;
    }
    move(state, game, gameX, gameY) {
        this.obj.launchVec = getDirection(this.obj.box, gameX, gameY);
        if(this.obj.originalLV)
            this.obj.originalLV = this.obj.launchVec;
        return this;
    }
    draw(state, game, context) {
        highlightBox(context, this.obj.box, selectHighlight, getScale(game));
    }
}

//Set the direction the beam of a new laser goes.
export class DirectLaser extends Operation {
    //laser: Laser to direct
    constructor(next, laser) {
        super(next);
        this.obj = laser;
        this.updated = false;
    }
    move(state, game, gameX, gameY) {
        if(!this.updated) {
            this.obj._setDimensions();
            this.obj.reset();
            this.updated = true;
        }
        this.obj.dirVec = getDirection(this.obj.follow.box, gameX, gameY);
        return this;
    }
    draw(state, game, context) {
        highlightBox(context, this.obj.follow.box, selectHighlight,
            getScale(game));
    }
}
