//Copyright Matthew Pottage 2019
import * as core from '../core/import-level-objects.js';
import * as mods from './mods/import-level-objects.js';
import * as levels_parse from '../levels/parse.js'

export function loadTextObjects(text) {
    try {
        var parsed = {};
        //Previous formats never started with "{".
        if(!text.startsWith("{"))
            parsed = levels_parse.oldToJSONFormat0(text);
        else
            parsed = JSON.parse(text);
        if(parsed["type"]!="level" && parsed["type"]!="_buffer")
            throw new Error("Can only safely load text for levels or buffers", parsed);
        return loadSavedObject(levels_parse.updateFormat(parsed));
    }
    catch(e) {
        console.log(e);
        return null;
    }
}
//It is assumed that if the save is an old version, it will be of type "level"
//or "_buffer" with "version" set to some value, otherwise it is assumed to be
//in the newest format.
//Special case: Any kind of save of a mods.Group must have an array in the
//save as the parent of any objects in the group.
function loadSavedObject(json, recurse=loadSavedObject) {
    if(json instanceof Array) {
        var lastGroup = null;
        var lastGroupID = null;
        var res = [];
        var groupID = -1;
        var currGroup = [];
        for(var o of json) {
            if(o["_group"]!=groupID) {
                if(currGroup.length>0) {
                    res.push(new mods.Group(currGroup));
                    currGroup = [];
                }
            }
            if(o["_group"]!==undefined) {
                groupID = o["_group"];
                currGroup.push(recurse(o));
            }
            else
                res.push(recurse(o));
        }
        if(currGroup.length>0)
            res.push(new mods.Group(currGroup));
        return res;
    }
    else if(json["type"]=="_buffer")
        return recurse(json["objs"]);
    else if(json["type"]=="start positions")
        return new mods.StartPositions(json["positions"]);
    else if(json["type"]=="apply flag")
        return new mods.ApplyFlag(json["flag"], recurse(json["obj"]));
    else if(json["type"]=="make flag")
        return new mods.MakeFlagFrom(json["name"], recurse(json["obj"]));
    else if(json["type"]=="make and apply flag")
        return new mods.MakeAndApplyFlag(json["name"], json["flag"], recurse(json["obj"]));
    else if(json["type"]=="collapsing platform")
        return new mods.CollapsingPlatform(recurse(json["box"]), json["color"], json["delay"]);
    else if(json["type"]=="left-right platform")
        return new mods.LRPlatform(recurse(json["box"]), json["start"], json["end"], json["color"], json["accel"]);
    else if(json["type"]=="up-down platform")
        return new mods.UDPlatform(recurse(json["box"]), json["start"], json["end"], json["color"], json["accel"]);
    else if(json["type"]=="fading platform")
        return new mods.FadingPlatform(recurse(json["box"]), json["color"]);
    else if(json["type"]=="destructible")
        return new mods.Destructible(recurse(json["box"]), json["color"]);
    else if(json["type"]=="nothing pickup")
        return new mods.NothingPickup(json["x"], json["y"]);
    else if(json["type"]=="brown triangle")
        return new mods.BadTriangle(json["start"]["x"], json["start"]["y"], json["end x"], json["alt start x"]);
    else if(json["type"]=="orange triangle")
        return new mods.OrangeTriangle(json["start"]["x"], json["start"]["y"], json["end x"], json["alt start x"]);
    else if(json["type"]=="yellow triangle")
        return new mods.YellowTriangle(json["start"]["x"], json["start"]["y"], json["end x"], json["alt start x"]);
    else if(json["type"]=="purple triangle")
        return new mods.PurpleTriangle(json["start"]["x"], json["start"]["y"], json["end x"], json["alt start x"]);
    else if(json["type"]=="grey triangle")
        return new mods.GreyTriangle(json["start"]["x"], json["start"]["y"], json["end x"], json["alt start x"]);
    else if(json["type"]=="pink triangle")
        return new mods.PinkTriangle(json["start"]["x"], json["start"]["y"], json["end x"], json["alt start x"]);
    else if(json["type"]=="blue triangle")
        return new mods.BlueTriangle(json["start"]["x"], json["start"]["y"], json["end x"], json["alt start x"], json["wave offset"]);
    else if(json["type"]=="turquoise triangle")
        return new mods.TurquoiseTriangle(json["start"]["x"], json["start"]["y"], json["end x"], json["alt start x"]);
    else if(json["type"]=="normal ninja")
        return new mods.Ninja(json["x"], json["y"], json["color"]);
    else if(json["type"]=="turquoise ninja")
        return new mods.TurquoiseNinja(json["x"], json["y"], json["color"]);
    else if(json["type"]=="green ninja")
        return new mods.GreenNinja(json["x"], json["y"], json["color"]);
    else if(json["type"]=="green square")
        return new mods.GreenSquare(json["x"], json["y"]);
    else if(json["type"]=="grey square")
        return new mods.GreySquare(json["x"], json["y"]);
    else if(json["type"]=="red rectangle")
        return new mods.RedRectangle(json["x"], json["y"]);
    else if(json["type"]=="brown rectangle")
        return new mods.BrownRectangle(json["x"], json["y"]);
    else if(json["type"]=="orange rectangle")
        return new mods.OrangeRectangle(json["x"], json["y"]);
    else if(json["type"]=="yellow rectangle")
        return new mods.YellowRectangle(json["x"], json["y"]);
    else if(json["type"]=="orange square")
        return new mods.OrangeSquare(json["x"], json["y"]);
    else if(json["type"]=="black square")
        return new mods.BlackSquare(json["x"], json["y"]);
    else if(json["type"]=="purple square")
        return new mods.PurpleSquare(json["x"], json["y"]);
    else if(json["type"]=="ghost square")
        return new mods.GhostSquare(json["x"], json["y"]);
    else if(json["type"]=="spike row")
        return new mods.SpikeRow(recurse(json["box"]), json["target width"], json["color"], json["direction"]);
    else if(json["type"]=="spike column")
        return new mods.SpikeColumn(recurse(json["box"]), json["target height"], json["color"], json["direction"]);
    else if(json["type"]=="stone launcher")
        return new mods.StoneLauncher(recurse(json["box"]), json["vector"], json["color"], json["delay"]);
    else if(json["type"]=="targetting stone launcher")
        return new mods.TargettingStoneLauncher(recurse(json["box"]), json["vector"], json["color"], json["delay"]);
    else if(json["type"]=="cutting laser")
        return new mods.CuttingLaser(recurse(json["follow"]), json["vector"], json["range"]);
    else if(json["type"]=="trigger laser")
        return new mods.TriggerLaser(json["name"], recurse(json["follow"]), json["vector"], json["range"]);
    else if(json["type"]=="wooden box")
        return new mods.WoodenBox(json["x"], json["y"], recurse(json["pickup"]));
    else if(json["type"]=="present")
        return new mods.Present(json["x"], json["y"], recurse(json["pickup"]));
    else if(json["type"]=="spring")
        return new mods.Spring(recurse(json["box"]), json["color"], json["power"], json["extension"]);
    else if(json["type"]=="air current")
        return new mods.AirCurrent(recurse(json["box"]), json["vector"], json["power"]);
    else if(json["type"]=="snow")
        return new mods.SnowEmitter(recurse(json["box"]), json["lifetime"]);
    else if(json["type"]=="rain")
        return new mods.RainEmitter(recurse(json["box"]), json["lifetime"]);
    else if(json["type"]=="win region")
        return new mods.WinRegion(recurse(json["box"]));
    else if(json["type"]=="death region")
        return new mods.DeathRegion(recurse(json["box"]));
    else if(json["type"]=="hit region")
        return new mods.HitRegion(json["name"], recurse(json["box"]));
    else if(json["type"]=="to start region")
        return new mods.ToStartRegion(recurse(json["box"]));
    else if(json["type"]=="suppress weapons")
        return new mods.SuppressWeapons(recurse(json["box"]));
    else if(json["type"]=="clear specials")
        return new mods.ClearSpecials(recurse(json["box"]));
    else if(json["type"]=="message region")
        return new mods.MessageRegion(recurse(json["box"]), json["message"]);
    else if(json["type"]=="portal region")
        return new mods.PortalRegion(json["output"], json["input"], recurse(json["box"]));
    else if(json["type"]=="darkness")
        return new mods.Darkness(recurse(json["box"]), json["color"]);
    else if(json["type"]=="lightening")
        return new mods.LighteningEffect(recurse(json["box"]), json["color"]);
    else if(json["type"]=="background visual")
        return new mods.VisualOnlyBackground(recurse(json["box"]), json["color"]);
    else if(json["type"]=="overlay visual")
        return new mods.VisualOnlyOverlay(recurse(json["box"]), json["color"]);
    else if(json["type"]=="switch")
        return new mods.Switch(json["name"], json["x"], json["y"], json["color"], json["on"]);
    else if(json["type"]=="relay flag")
        return new mods.RelayFlag(json["name"], json["flag"], json["_x"], json["_y"]);
    else if(json["type"]=="not flag")
        return new mods.NegateFlag(json["name"], json["flag"], json["_x"], json["_y"]);
    else if(json["type"]=="or flags")
        return new mods.OrFlags(json["name"], json["flags"], json["_x"], json["_y"]);
    else if(json["type"]=="and flags")
        return new mods.AndFlags(json["name"], json["flags"], json["_x"], json["_y"]);
    else if(json["type"]=="toggle flag")
        return new mods.ToggleFlag(json["name"], json["flag"], json["_x"], json["_y"]);
    else if(json["type"]=="flag been true")
        return new mods.FlagBeenTrue(json["name"], json["flag"], json["_x"], json["_y"]);
    else if(json["type"]=="hold and flags")
        return new mods.HoldAnd(json["name"], json["flags"], json["_x"], json["_y"]);
    else if(json["type"]=="always true")
        return new mods.AlwaysTrue(json["name"], json["_x"], json["_y"]);
    else if(json["type"]=="timeout flag")
        return new mods.Timeout(json["name"], json["flag"], json["duration"], json["_x"], json["_y"]);
    else if(json["type"]=="randomly true")
        return new mods.RandomlyTrue(json["name"], json["numerator"], json["denominator"], json["_x"], json["_y"]);
    else if(json["type"]=="pickup spawner")
        return new mods.PickupSpawner(recurse(json["pickups"]), recurse(json["box"]));
    else if(json["type"]=="payment region")
        return new mods.PaymentRegion(json["name"], recurse(json["box"]), json["requires"]);
    else
        return levels_parse.loadSavedObject(json, loadSavedObject);
}
