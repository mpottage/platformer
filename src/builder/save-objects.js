//Copyright Matthew Pottage 2019
import * as core from '../core/import-level-objects.js';
import * as mods from './mods/import-level-objects.js';

export function textObjectSave(obj) {
    if(!(obj instanceof Array) && !(obj instanceof core.Level))
        throw new Error("Can only safely convert a level or array to text");
    return JSON.stringify(applyVersionToSave(getObjectSave(obj)));
}

//Special case, a mods.Group MUST be inside an array to save properly.
function getObjectSave(obj, recurse=getObjectSave) {
    if(obj instanceof core.Box) {
        return {"type": "box", "x": obj.x, "y": obj.y, "w": obj.width, "h": obj.height};
    }
    else if(obj instanceof Array) {
        let res = [];
        let groupCount = 0;
        //Flatten any groups
        for(let o of obj) {
            if(o instanceof mods.Group) {
                let tmp = o.objs.map(o => recurse(o, recurse));
                for(let t of tmp)
                    t["_group"] = groupCount;
                res = res.concat(tmp);
                ++groupCount;
            }
            else
                res.push(recurse(o));
        }
        return res;
    }
    else if(obj instanceof core.Level) {
        return {"type": "level", "w": obj.width, "h": obj.height,
            "objs": getObjectSave(obj.objs), "scale": obj.scale,
            "suspending": obj.suspendingOptimization,
            "textures": obj.uniqueTextures,
            "environment setup": obj.environmentSetup
        };
    }
    else if(obj instanceof core.ThinIcePlatform) {
        var res = {"type": "thin ice", "box": recurse(obj.box),
            "color": obj.color};
        res["box"]["y"] = obj.startY;
        return res;
    }
    if(obj instanceof core.Destructible)
        return {"type": "destructible", "box": recurse(obj.box), "color": obj.color};
    else if(obj instanceof core.CollapsingPlatform) {
        var res = {"type": "collapsing platform", "box": recurse(obj.box),
            "color": obj.color, "delay": obj.delay};
        res["box"]["y"] = obj.startY;
        return res;
    }
    else if(obj instanceof core.LRPlatform) {
        let res = {"type": "left-right platform", "box": recurse(obj.box),
            "start": obj.startPos.x, "end": obj.endPos.x, "color": obj.color,
            "accel": obj.xAccel};
        res["box"]["x"] = obj.alternateStartX;
        return res;
    }
    else if(obj instanceof core.UDPlatform) {
        let res = {"type": "up-down platform", "box": recurse(obj.box),
            "start": obj.startPos.y, "end": obj.endPos.y, "color": obj.color,
            "accel": obj.yAccel};
        res["box"]["y"] = obj.alternateStartY;
        return res;
    }
    else if(obj instanceof core.TargettingStoneLauncher)
        return {"type": "targetting stone launcher", "box": recurse(obj.box),
            "vector": {"x": obj.originalLV.x, "y": obj.originalLV.y},
            "color": obj.color, "delay": obj.launchDelay};
    else if(obj instanceof core.StoneLauncher)
        return {"type": "stone launcher", "box": recurse(obj.box),
            "vector": {"x": obj.launchVec.x, "y": obj.launchVec.y},
            "color": obj.color, "delay": obj.launchDelay};
    else if(obj instanceof core.Spring) {
        let res = {"type": "spring", "box": recurse(obj.box), "color": obj.color,
            "power": obj.springVal/100, "extension": obj.springDiff};
        res["box"]["y"] = obj.startY;
        return res;
    }
    else if(obj instanceof core.WoodenBox)
        return {"type": "wooden box", "x": obj.box.x, "y": obj.box.y,
            "pickup": recurse(obj.obj)};
    else if(obj instanceof core.Present)
        return {"type": "present", "x": obj.box.x, "y": obj.box.y,
            "pickup": recurse(obj.obj)};
    else if(obj instanceof core.ToggleButton)
        return {"type": "toggle button", "x": obj.box.x, "y": obj.startY,
            "color": obj.color, "name": obj.name};
    else if(obj instanceof core.CeilingButton)
        return {"type": "ceiling button", "x": obj.box.x, "y": obj.startY,
            "color": obj.color, "name": obj.name};
    else if(obj instanceof core.Switch)
        return {"type": "switch", "x": obj.startX, "y": obj.box.y,
            "color": obj.color, "name": obj.name, "on": obj.defaultOn};
    else if(obj instanceof core.Button)
        return {"type": "floor button", "x": obj.box.x, "y": obj.startY,
            "color": obj.color, "name": obj.name};
    else if(obj instanceof core.LeftWallButton)
        return {"type": "left wall button", "x": obj.startX, "y": obj.box.y,
            "color": obj.color, "name": obj.name};
    else if(obj instanceof core.WallButton)
        return {"type": "right wall button", "x": obj.startX, "y": obj.box.y,
            "color": obj.color, "name": obj.name};
    else if(obj instanceof core.FadingPlatform)
        return {"type": "fading platform", "box": recurse(obj.box),
            "color": obj.color}
    else if(obj instanceof core.IcePlatform)
        return {"type": "ice", "box": recurse(obj.box), "color": obj.color}
    else if(obj instanceof core.HealthLoss)
        return {"type": "health loss", "box": recurse(obj.box),
            "color": obj.color}
    else if(obj instanceof core.JumpThroughPlatform)
        return {"type": "jump through platform", "box": recurse(obj.box),
            "color": obj.color}
    else if(obj instanceof core.DownThroughPlatform)
        return {"type": "down through platform", "box": recurse(obj.box),
            "color": obj.color}
    else if(obj instanceof core.RightThroughPlatform)
        return {"type": "right through platform", "box": recurse(obj.box),
            "color": obj.color}
    else if(obj instanceof core.LeftThroughPlatform)
        return {"type": "left through platform", "box": recurse(obj.box),
            "color": obj.color}
    else if(obj instanceof core.Platform)
        return {"type": "normal platform", "box": recurse(obj.box), "color": obj.color}
    else if(obj instanceof core.MarshWater)
        return {"type": "marsh water", "box": recurse(obj.box)}
    else if(obj instanceof core.Water)
        return {"type": "normal water", "box": recurse(obj.box)}
    else if(obj instanceof core.SpikeRow)
        return {"type": "spike row", "box": recurse(obj.box),
            "target width": obj.targetWidth, "direction": obj.dir,
            "color": obj.color};
    else if(obj instanceof core.SpikeColumn)
        return {"type": "spike column", "box": recurse(obj.box),
            "target height": obj.targetHeight, "direction": obj.dir,
            "color": obj.color};
    else if(obj instanceof core.WinRegion)
        return {"type": "win region", "box": recurse(obj.box)};
    else if(obj instanceof core.DeathRegion)
        return {"type": "death region", "box": recurse(obj.box)};
    else if(obj instanceof core.ToStartRegion)
        return {"type": "to start region", "box": recurse(obj.box)};
    else if(obj instanceof core.SuppressWeapons)
        return {"type": "suppress weapons", "box": recurse(obj.box)};
    else if(obj instanceof core.ClearSpecials)
        return {"type": "clear specials", "box": recurse(obj.box)};
    else if(obj instanceof core.MessageRegion)
        return {"type": "message region", "box": recurse(obj.box),
            "message": obj.message};
    else if(obj instanceof core.AirCurrent)
        return {"type": "air current", "box": recurse(obj.box),
            "vector": obj.vec, "power": obj.power};
    else if(obj instanceof core.TripleCoin)
        return {"type": "triple coin", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.TenCoin)
        return {"type": "ten coin", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.Coin)
        return {"type": "coin", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.HealthPoint)
        return {"type": "health point", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.Star)
        return {"type": "star", "x": obj.box.x, "y": obj.box.y, "color": obj.color};
    else if(obj instanceof core.ExtraJumpPickup)
        return {"type": "extra jump", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.FixedAltitudePickup)
        return {"type": "fixed altitude", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.XSpeedBoostPickup)
        return {"type": "speed boost", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.RapidReloadPickup)
        return {"type": "rapid reload", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.MissileSpecialPickup)
        return {"type": "missile", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.MissileSpecialUnlimited)
        return {"type": "missile unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.TripleThrowPickup)
        return {"type": "triple throw", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.TripleThrowUnlimited)
        return {"type": "triple throw unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.OrangeBulletPickup)
        return {"type": "orange bullet", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.OrangeBulletUnlimited)
        return {"type": "orange bullet unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.PinkBulletPickup)
        return {"type": "pink bullet", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.PinkBulletUnlimited)
        return {"type": "pink bullet unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.PurpleBulletPickup)
        return {"type": "purple bullet", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.PurpleBulletUnlimited)
        return {"type": "purple bullet unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.GreenSwarmPickup)
        return {"type": "green swarm", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.GreenSwarmUnlimited)
        return {"type": "green swarm unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.BlueBulletPickup)
        return {"type": "blue bullet", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.BlueBulletUnlimited)
        return {"type": "blue bullet unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.BrownBulletPickup)
        return {"type": "brown bullet", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.BrownBulletUnlimited)
        return {"type": "brown bullet unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.YellowBulletPickup)
        return {"type": "yellow bullet", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.YellowBulletUnlimited)
        return {"type": "yellow bullet unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.NinjaBulletPickup)
        return {"type": "ninja bullet", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.NinjaBulletUnlimited)
        return {"type": "ninja bullet unlimited", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.GreyTriangle)
        return {"type": "grey triangle",
            "start": {"x": obj.startPos.x, "y": obj.startPos.y},
            "end x": obj.endPos.x, "alt start x": obj.alternateStartX};
    else if(obj instanceof core.BlueTriangle)
        return {"type": "blue triangle",
            "start": {"x": obj.startPos.x, "y": obj.startPos.y},
            "end x": obj.endPos.x, "alt start x": obj.alternateStartX,
            "wave offset": obj.startOffset};
    else if(obj instanceof core.TurquoiseTriangle)
        return {"type": "turquoise triangle",
            "start": {"x": obj.startPos.x, "y": obj.startPos.y},
            "end x": obj.endPos.x, "alt start x": obj.alternateStartX};
    else if(obj instanceof core.PinkTriangle)
        return {"type": "pink triangle",
            "start": {"x": obj.startPos.x, "y": obj.startPos.y},
            "end x": obj.endPos.x, "alt start x": obj.alternateStartX};
    else if(obj instanceof core.PurpleTriangle)
        return {"type": "purple triangle",
            "start": {"x": obj.startPos.x, "y": obj.startPos.y},
            "end x": obj.endPos.x, "alt start x": obj.alternateStartX};
    else if(obj instanceof core.OrangeTriangle)
        return {"type": "orange triangle",
            "start": {"x": obj.startPos.x, "y": obj.startPos.y},
            "end x": obj.endPos.x, "alt start x": obj.alternateStartX};
    else if(obj instanceof core.YellowTriangle)
        return {"type": "yellow triangle",
            "start": {"x": obj.startPos.x, "y": obj.startPos.y},
            "end x": obj.endPos.x, "alt start x": obj.alternateStartX};
    else if(obj instanceof core.BadTriangle)
        return {"type": "brown triangle",
            "start": {"x": obj.startPos.x, "y": obj.startPos.y},
            "end x": obj.endPos.x, "alt start x": obj.alternateStartX};
    else if(obj instanceof core.Cover)
        return {"type": "cover", "box": recurse(obj.box), "color": obj.color};
    else if(obj instanceof core.Darkness)
        return {"type": "darkness", "box": recurse(obj.box),
            "color": obj.color};
    else if(obj instanceof core.HitRegion)
        return {"type": "hit region", "name": obj.name,
            "box": recurse(obj.box)};
    else if(obj instanceof core.OrFlags)
        return {"type": "or flags", "name": obj.name, "flags": obj.flagNames,
            "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.AndFlags)
        return {"type": "and flags", "name": obj.name, "flags": obj.flagNames,
            "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.HoldAnd)
        return {"type": "hold and flags", "name": obj.name,
            "flags": obj.flagNames, "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.ApplyFlag)
        return {"type": "apply flag", "flag": obj.flag,
            "obj": recurse(obj.obj)};
    else if(obj instanceof core.RelayFlag)
        return {"type": "relay flag", "name": obj.name,
            "flag": obj.tracking, "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.NegateFlag)
        return {"type": "not flag", "name": obj.name,
            "flag": obj.tracking, "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.FlagBeenTrue)
        return {"type": "flag been true", "name": obj.name,
            "flag": obj.tracking, "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.Timeout)
        return {"type": "timeout flag", "name": obj.name,
            "duration": obj.duration, "flag": obj.tracking,
            "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.AlwaysTrue)
        return {"type": "always true", "name": obj.name,
            "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.RandomlyTrue)
        return {"type": "randomly true", "name": obj.name,
            "numerator": obj.numer, "denominator": obj.denom,
            "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.ToggleFlag)
        return {"type": "toggle flag", "name": obj.name,
            "flag": obj.tracking, "_x": obj.box.x, "_y": obj.box.y};
    else if(obj instanceof core.VisualOnlyOverlay)
        return {"type": "overlay visual", "box": recurse(obj.box),
            "color": obj.color};
    else if(obj instanceof core.VisualOnlyBackground)
        return {"type": "background visual", "box": recurse(obj.box),
            "color": obj.color};
    else if(obj instanceof core.ImageOverlay)
        return {"type": "image overlay", "box": recurse(obj.box),
            "texture name": obj.textureName};
    else if(obj instanceof core.ImageBackground)
        return {"type": "image background", "box": recurse(obj.box),
            "texture name": obj.textureName};
    else if(obj instanceof core.MakeFlagFrom)
        return {"type": "make flag", "name": obj.name, "obj": recurse(obj.obj)};
    else if(obj instanceof core.MakeAndApplyFlag)
        return {"type": "make and apply flag", "name": obj.name,
            "flag": obj.flag, "obj": recurse(obj.obj)};
    else if(obj instanceof core.SignRight)
        return {"type": "sign right", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.SignLeft)
        return {"type": "sign left", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.SignUp)
        return {"type": "sign up", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.SignDown)
        return {"type": "sign down", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.SignTarget)
        return {"type": "sign target", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.SignWarning)
        return {"type": "sign warning", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.SignWriting)
        return {"type": "sign writing", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.NothingPickup)
        return {"type": "nothing pickup", "x": obj.box.x, "y": obj.box.y};
    else if(obj instanceof core.PickupSpawner)
        return {"type": "pickup spawner", "pickups": recurse(obj.objs),
            "box": recurse(obj.box)};
    else if(obj instanceof core.StartPositions)
        return {"type": "start positions", "positions": obj.positions};
    else if(obj instanceof core.GreenNinja)
        return {"type": "green ninja", "x": obj.startPos.x, "y": obj.startPos.y,
            "color": obj.color};
    else if(obj instanceof core.TurquoiseNinja)
        return {"type": "turquoise ninja",
            "x": obj.startPos.x, "y": obj.startPos.y, "color": obj.color};
    else if(obj instanceof core.Ninja)
        return {"type": "normal ninja",
            "x": obj.startPos.x, "y": obj.startPos.y, "color": obj.color};
    else if(obj instanceof core.GreenSquare)
        return {"type": "green square",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.GreySquare)
        return {"type": "grey square",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.RedRectangle)
        return {"type": "red rectangle",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.OrangeRectangle)
        return {"type": "orange rectangle",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.BrownRectangle)
        return {"type": "brown rectangle",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.YellowRectangle)
        return {"type": "yellow rectangle",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.OrangeSquare)
        return {"type": "orange square",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.BlackSquare)
        return {"type": "black square",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.GhostSquare)
        return {"type": "ghost square",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.PurpleSquare)
        return {"type": "purple square",
            "x": obj.startPos.x, "y": obj.startPos.y};
    else if(obj instanceof core.BouncingPickup) {
        let res = {"type": "bouncing pickup", "pickup": recurse(obj.pickup),
            "velocity": {"x": obj.initialV.x, "y": obj.initialV.y}};
        res["pickup"]["x"] = obj.startPos.x;
        res["pickup"]["y"] = obj.startPos.y;
        return res;
    }
    else if(obj instanceof core.TriggerLaser)
        return {"type": "trigger laser", "name": obj.name,
            "follow": recurse(obj.follow),
            "vector": {"x": obj.dirVec.x, "y": obj.dirVec.y},
            "range": obj.rangeLimit};
    else if(obj instanceof core.CuttingLaser)
        return {"type": "cutting laser", "follow": recurse(obj.follow),
            "vector": {"x": obj.dirVec.x, "y": obj.dirVec.y},
            "range": obj.rangeLimit};
    else if(obj instanceof core.SnowEmitter)
        return {"type": "snow", "box": recurse(obj.box),
            "lifetime": obj.cacheTime};
    else if(obj instanceof core.RainEmitter)
        return {"type": "rain", "box": recurse(obj.box),
            "lifetime": obj.cacheTime};
    else if(obj instanceof core.PaymentRegion)
        return {"type": "payment region", "box": recurse(obj.box),
            "name": obj.name, "requires": obj.requires};
    else if(obj instanceof core.PortalRegion)
        return {"type": "portal region", "output": obj.output,
            "input": obj.input, "box": recurse(obj.box)};
    else if(obj instanceof core.LighteningEffect)
        return {"type": "lightening", "box": recurse(obj.box),
            "color": obj.color};
    else if(obj instanceof mods.Group)
        throw new Error("A mods.Group must be contained in an array");
    else
        throw new Error("Unknown object to save: "+(obj && obj.name))
}

function applyVersionToSave(save) {
    if(save instanceof Array)
        return {"type": "_buffer", "objs": save,
            "version": 2};
    else if(save["type"]=="level") {
        save["version"] = 2;
        return save;
    }
    else {
        return {"type": "_buffer", "objs": [save],
            "version": 2};
    }
}

//Used to debug any issues with level format generation. Unused.
function debugCmpJSON(j1, j2) {
    if((!j1 || !j2))
        return j1===j2;
    if((typeof j2)=="string" || (typeof j2)=="boolean" || (typeof j2)=="number") {
        return j1===j2;
    }
    else if(j1 instanceof Array) {
        if(j1.length!=j2.length)
            return false;
        for(let i=0; i<j1.length; ++i)
            if(!cmpJSON(j1[i], j2[i])) {
                console.log("diff", j1[i], j2[i]);
                return false;
            }
    }
    else {
        var k1 = Object.keys(j1);
        var k2 = Object.keys(j2);
        if(k1.length!=k2.length) {
            return false;
        }
        else {
            for(let k of k1) {
                var res = (k in j2) && cmpJSON(j1[k], j2[k]);
                if(!res) {
                    console.log(k, j1, j2, j1[k], j2[k]);
                    return false;
                }
            }
        }
    }
    return true;
}
