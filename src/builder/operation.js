//Copyright Matthew Pottage 2019.
//Generic Operation.
//Each member function returns the new operation that results from calling it,
// which may be the same operation.
//To chain operations, so that when one finishes, another begins, pass the next
// operation as a the first parameter to the constructor (next).
// Follow the same pattern in any derived classes for consistency.
export class Operation {
    constructor(next) {
        this.next = next || null;
        this.obj = null; //Object being operated on.
    }
    //touchstart or mousedown.
    press(state, game, gameX, gameY, color) { this.move(state, game, gameX, gameY);  return this; }
    //touchmove or mousemove.
    move(state, game, gameX, gameY) { return this; }
    //touchend or mouseup.
    release(state, game) { return this.next; } //Default to ending operation.
    abort(state, game) { this.release(state, game); }
    draw(state, game, context) { }
    autoscroll() { return true; }
    //Returns root objects that it would be reasonable to act on should a button
    //be pressed AND should any of the objects cease to exist (before press,
    //move or release is next called) the operation should be aborted.
    //Post-conditions: Ordered: Last to first rendered (top-to-bottom).
    getModObjs(game) { return []; }
}
