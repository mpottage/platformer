import * as core from '../core/import-level-objects.js';
import * as mods from './mods/import-level-objects.js';
import {ActionGroup} from './action.js';
import {AddMultipleObjects} from './add-delete-actions.js';
import {getImages, getImageName, getImagesUsing} from './images.js';
import {getFlagsAndControls} from './connections.js';
import {textObjectSave} from './save-objects.js';

export function toClipboardStr(objs) {
    return textObjectSave(objs.slice(0).reverse());
}

//Removes references to flags with no owner in objs by any objects in objs.
//No history action.
export function removeUnownedFlags(objs) {
    var flags = getFlagsAndControls(objs);
    for(var key in flags) {
        var d = flags[key];
        if(!d.owner) {
            for(var obj of d.users) {
                if(obj instanceof core.ApplyFlag)
                    objs[objs.indexOf(obj)] = obj.obj;
                else if(obj instanceof core.MakeAndApplyFlag)
                    objs[objs.indexOf(obj)] = new mods.MakeFlagFrom(obj.name, obj.obj);
                else if(obj instanceof core.RelayFlag || obj instanceof core.NegateFlag
                        || obj instanceof core.FlagBeenTrue || obj instanceof core.Timeout
                        || obj instanceof core.ToggleFlag)
                    obj.tracking = "";
                else if(obj instanceof core.OrFlags || obj instanceof core.AndFlags
                        || obj instanceof core.HoldAnd) {
                    var i = obj.flagNames.indexOf(key);
                    if(i!=-1)
                        obj.flagNames.splice(i, 1);
                }
            }
        }
    }
}

export function mergeTextures(state, game, objs, newTextures) {
    var actions = [new AddMultipleObjects(objs)];
    //Assumes that if the textures were put in the clipboard, it was
    //the right ones
    var textureActions = [];
    var usedNames = [];
    for(var name in newTextures) {
        var res = getImageName(game, newTextures[name], usedNames);
        if(res.action)
            actions.push(res.action);
        usedNames.push(res.name);
        for(var u of getImagesUsing(objs, name))
            u.textureName = res.name;
    }
    //Apply actions and finally add the objects
    var action = new ActionGroup(actions);
    action.apply(game);
    state.actionHistory.push(action);
}
export function clearTextures(state, game, objs) {
    var action = new AddMultipleObjects(objs);
    //If texture loading fails, use an no texture.
    for(var i of getImages(objs))
        i.textureName = "";
    action.apply(game);
    state.actionHistory.push(action);
}
