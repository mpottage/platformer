//Copyright Matthew Pottage 2019.
import {movePlayer} from './readonly-shortcuts.js';
import {insertObjAt} from './insert.js';
import {MoveOperation} from './move.js';
import {round5} from './utility.js';
import {AdjustLinks} from './adjust-links.js';
import {ConnectPortals} from './connect-portals.js';
import {SelectOperation} from './select.js';
import {CutoutOperation} from './cutout.js';
import {applyObjectAt} from './apply.js';
import {getTopObj, getRootParent} from './filtering.js';
import {ActionGroup} from './action.js';
import {isImageObject, getImageName, SwapImage, imageUserCount, DeleteTexture} from './images.js';
import {getDeleteAction} from './delete-utilities.js';
import {ColorObject} from './color-utilities.js';

//Start a new operation based on the current mode.
export function initiateOperation(state, game, operation, queryDialog, gameX, gameY, ctrlKey) {
    if(operation)
        throw new Error("Operation already in progress.")
    else if(ctrlKey || state.mode=="j") //Place the player at an arbitary position.
        movePlayer(game, gameX, gameY);
    else if(state.mode=="d") //Delete object state.mode.
        deleteObjAt(state, game, gameX, gameY);
    else if (state.mode=="i") { //Insert game object state.mode.
        var newX = gameX;
        var newY = gameY;
        if(state.snapToGrid) {
            newX = round5(newX);
            newY = round5(newY);
        }
        return insertObjAt(state, game, queryDialog, newX, newY);
    }
    else if (state.mode=="m") {
        return (new MoveOperation()).press(state, game, gameX, gameY);
    }
    else if (state.mode=="c") //Colour objects state.mode.
        colorObjAt(state, game, gameX, gameY)
    else if (state.mode=="b")
        imageObjAt(state, game, gameX, gameY);
    else if (state.mode=="l") {
        return (new AdjustLinks(null)).press(state, game, gameX, gameY);
    }
    else if (state.mode=="p") {
        return (new ConnectPortals(null)).press(state, game, gameX, gameY);
    }
    else if(state.mode=="s") {
        return (new SelectOperation()).press(state, game, gameX, gameY);
    }
    else if(state.mode=="o") {
        return (new CutoutOperation()).press(state, game, gameX, gameY);
    }
    else if(state.mode=="a")
        applyObjectAt(state, game, gameX, gameY);
}

//Changes the image for the top object at (x, y) in level to image.
function imageObjAt(state, game, x, y) {
    var imageObj = getTopObj(state.snapToGrid, game.manager, game.level, x, y);
    if(isImageObject(imageObj)) {
        var details = getImageName(game, state.currentImage);
        var action = new SwapImage(imageObj, imageObj.textureName, details.name);
        //Does the image need adding to the level?
        if(details.action) {
            var actions = [action];
            //Remove the previous texture from the level if imageObj was the only user
            if(imageObj.textureName!="" && imageUserCount(game, imageObj.textureName)==1) {
                actions.push(new DeleteTexture(game.level, imageObj.textureName));
            }
            actions.push(details.action);
            action = new ActionGroup(actions);
        }
        action.apply(game);
        state.actionHistory.push(action);
    }
}

//Removes the top object at (x, y) in level.
function deleteObjAt(state, game, x, y) {
    var target = getTopObj(state.snapToGrid, game.manager, game.level, x, y);
    if(target) {
        //Check for the visible object being nested in another.
        var delObj = getRootParent(target, game.level.objs) || target;
        var action = getDeleteAction(game, delObj, target);
        action.apply(game);
        state.actionHistory.push(action);
    }
}
//Colors the top object at (x, y) in level to color (if it can).
function colorObjAt(state, game, x, y) {
    var colorObj = getTopObj(state.snapToGrid, game.manager, game.level, x, y);
    if(colorObj && colorObj.color!==undefined) {
        var action = new ColorObject(colorObj, colorObj.color, state.color);
        action.apply(game);
        state.actionHistory.push(action);
    }

}
