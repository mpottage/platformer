//Copyright Matthew Pottage 2019.
export const selectHighlight = "rgba(255,214,0,.7)";
export const hoverHighlight = "rgba(0,199,229,.5)";
export const selectHandleHighlight = "rgba(255,214,0,.8)";
export const previewHandleHighlight = "rgba(0,199,229,.8)";
export const handleRadius = 13;
export const movePathWidth = 5;

//Outline for when a object is selected or hovered by a mouse.
export function highlightBox(context, box, color, scale=1) {
    context.lineWidth = 3/scale;
    context.lineJoin = "round";
    context.strokeStyle = color;
    var diff = 1.5/scale;
    context.strokeRect(box.x-diff, box.y-diff, box.width+2*diff, box.height+2*diff);
}
//Used to draw an rectangle to show where objects will move (and objects outside
//it won't move as a result of the suspending optimisation).
export function highlightMovementLimit(context, center, movementLimitX, movementLimitY, color, scale) {
    context.lineWidth = 3/scale;
    context.strokeStyle = color;
    context.strokeRect(center.x-movementLimitX, center.y-movementLimitY,
        movementLimitX*2, movementLimitY*2);
}

//Visual for just one drag handle (all drag handles look the same)
export function drawOneHandle(context, scale, position, color) {
    context.beginPath();
    var radius = handleRadius/scale;
    context.arc(position.x, position.y, radius, 0, Math.PI*2);
    context.closePath();
    context.fillStyle = color;
    context.fill();
}

//Draw a rectangle with a dashed outline.
//  box: Description of the rectangle
//  fillStyle: Color to fill the rectangle with
//  strokeStyle: Color of the dashed outline
export function drawOutlineBox(context, box, fillStyle, strokeStyle) {
    context.beginPath();
    context.lineWidth = 3;
    context.fillStyle = fillStyle;
    context.strokeStyle = strokeStyle;
    context.setLineDash([10, 5]);
    context.rect(box.x, box.y, box.width, box.height);
    context.fill();
    context.stroke();
    context.setLineDash([]);
}

//Variation on drawOutlineBox for objects that can turned on and off with links.
//Uses a grey outline, with is faded when the object is turned off.
export function activatedRegionBox(context, obj, fillStyle) {
    var outline = "#333";
    if(!obj.activated)
        outline = "#888";
    drawOutlineBox(context, obj.box, fillStyle, outline);
}

//Outline for a platform that is turned off or destroyed.
export function outlineDisabled(context, obj) {
    context.strokeStyle = obj.color || "black";
    context.lineWidth = 1;
    obj.box.stroke(context);
}

function movePathStroke(context, color) {
    let prevGA = context.globalAlpha;
    context.globalAlpha *= .6;
    context.lineWidth = movePathWidth;
    context.lineCap = "butt";
    context.strokeStyle = color || "#555";
    context.stroke();
    context.globalAlpha = prevGA;
}

//Visual indicating the limits of a LR path.
export function drawLRPath(context, environment, obj) {
    if(!obj.disabled && environment.get("builder details")) {
        context.beginPath();
        let barHeight = Math.min(obj.box.height*8/10, 50);
        let startY = obj.box.y+(obj.box.height-barHeight)/2;
        let midY = obj.box.y+obj.box.height/2;
        let endY = startY+barHeight;
        let startX = obj.startPos.x-2.5;
        let endX = obj.endPos.x+obj.box.width+2.5;
        context.moveTo(startX, startY);
        context.lineTo(startX, endY);
        context.moveTo(startX, midY);
        context.lineTo(endX, midY);
        context.moveTo(endX, startY);
        context.lineTo(endX, endY);
        let linePos = obj.alternateStartX;
        let heightDiff = 0;
        if(obj.alternateStartX==obj.startPos.x)
            linePos = startX-7.5;
        else if(obj.alternateStartX==obj.endPos.x)
            linePos = endX+7.5;
        else
            heightDiff = obj.box.height*.2;
        context.moveTo(linePos, startY+heightDiff/2);
        context.lineTo(linePos, endY-heightDiff/2);
        movePathStroke(context, obj.color);
    }
}

//Visual indicating the limits of a UD path.
//  Assumes that the startPos.y and endPos.y parameters on obj are for the top
//  edge (unlike for a BadTriangle)
export function drawUDPath(context, environment, obj) {
    if(!obj.disabled && environment.get("builder details")) {
        context.beginPath();
        let barWidth = Math.min(obj.box.width*8/10, 50);
        let startX = obj.box.x+(obj.box.width-barWidth)/2;
        let midX = obj.box.x+obj.box.width/2;
        let endX = startX+barWidth;
        let startY = obj.startPos.y-2.5;
        let endY = obj.endPos.y+obj.box.height+2.5;
        context.moveTo(startX, startY);
        context.lineTo(endX, startY);
        context.moveTo(midX, startY);
        context.lineTo(midX, obj.box.y);
        context.moveTo(midX, obj.box.y+obj.box.height);
        context.lineTo(midX, endY);
        context.moveTo(startX, endY);
        context.lineTo(endX, endY);
        let linePos = obj.alternateStartY;
        if(obj.alternateStartY==obj.startPos.y)
            linePos = startY-7.5;
        else if(obj.alternateStartY==obj.endPos.y)
            linePos = endY+7.5;
        context.moveTo(startX, linePos);
        context.lineTo(endX, linePos);
        movePathStroke(context, obj.color);
    }
}

//Visual previewing where a ninja starts in the level
export function ninjaStartPos(context, environment, obj) {
    if(environment.get("builder details") && !obj.disabled) {
        var prevGA = context.globalAlpha;
        context.globalAlpha *= .2;
        context.fillStyle = obj.bodyColor;
        context.fillRect(obj.startPos.x, obj.startPos.y, obj.box.width,
                obj.box.height);
        context.fillStyle = obj.color;
        context.fillRect(obj.startPos.x, obj.startPos.y+obj.box.height/10, obj.box.width, obj.box.height/10);
        context.globalAlpha = prevGA;
    }
}

//Visual previewing where a rectangular enemy starts in the level
export function startPosSquare(context, environment, obj) {
    if(environment.get("builder details") && !obj.disabled) {
        var prevGA = context.globalAlpha;
        context.globalAlpha *= .2;
        context.fillStyle = obj.bodyColor;
        context.fillRect(obj.startPos.x, obj.startPos.y, obj.box.width,
                obj.box.height);
        context.globalAlpha = prevGA;
    }
}

//Displays a faded laser beam for when an accurate beam extent hasn't been
//computed yet as a result of the laser being turned off, the level being
//frozen (the previous two making it invisible), or the laser having been moved
//(putting it in the wrong place), or any other situation making it otherwise
//invisible or point to the wrong location.
//
//Returns "true" if a laser direction preview was shown
export function showLaserDir(context, environment, laser) {
    if(environment.get("builder details") &&
            (laser.dirVec.x!=0 || laser.dirVec.y!=0)) {
        if(!laser.activated ||
                (laser.box.x==laser.prevAdjPos.x && laser.box.y==laser.prevAdjPos.y) ||
                //If the beam has gone to the wrong place:
                (laser.dirVec.x*(laser.box.x-laser.follow.box.x)<0) ||
                (laser.dirVec.x*(laser.box.x+laser.box.width-laser.follow.box.x-laser.follow.box.width)<0) ||
                (laser.dirVec.y*(laser.box.y-laser.follow.box.y)<0) ||
                (laser.dirVec.y*(laser.box.y+laser.box.height-laser.follow.box.y-laser.follow.box.height)<0)) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= .3;
            context.strokeStyle = laser.beamColor;
            if(laser.dirVec.x!=0)
                context.lineWidth = laser.box.width;
            else
                context.lineWidth = laser.box.height;
            var c1 = laser.follow.box.center;
            context.beginPath();
            context.moveTo(c1.x, c1.y);
            var limit = Math.min(250, laser.rangeLimit);
            context.lineTo(c1.x+laser.dirVec.x*limit, c1.y+laser.dirVec.y*limit);
            var prevCap = context.lineCap;
            context.lineCap = "round";
            context.stroke();
            context.lineCap = prevCap;
            context.globalAlpha = prevGA;
            return true;
        }
    }
    return false;
}

export const logicInputRadius = 5; //Size of semi-circle for inputs to logic blocks.
//Render logic block with a semicircle connector on the LHS and colored box on
//the RHS.
export function drawLogicItem(context, box, color, radius) {
    context.fillStyle = color;
    context.fillRect(box.x+radius, box.y, box.width-radius, box.height);
    context.beginPath();
    context.arc(box.x+radius, box.y+box.height/2, radius, Math.PI/2, 3/2*Math.PI);
    context.fillStyle = "black";
    context.fill();
}

//Render logic block that has no input, which is a circle with a smaller circle
//of a (usually) different color inside it.
export function rootLogicNode(context, center, radius, color, centerColor="black") {
    context.beginPath();
    context.arc(center.x, center.y, radius, 0, Math.PI*2);
    context.fillStyle = color;
    context.fill();
    context.beginPath();
    context.arc(center.x, center.y, logicInputRadius, 0, Math.PI*2);
    context.fillStyle = "black";
    context.fill();
}

//A bracket ( at the center of an object, used to indicate it has an
//incoming link applied to it when it doesn't usually.
export function drawApplyNode(context, center) {
    context.beginPath();
    context.strokeStyle = "#000";
    context.lineWidth = 3;
    context.arc(center.x-1, center.y, logicInputRadius, Math.PI/2, 3*Math.PI/2);
    context.stroke();
}
//A square bracket ] to indicate an object has an outgoing link when it wouldn't
//normally.
export function drawMakeNode(context, center) {
    context.beginPath();
    context.strokeStyle = "#000";
    context.lineWidth = 3;
    var startY = center.y-logicInputRadius;
    var startX = center.x+1;
    context.moveTo(startX, startY);
    var endX = startX+logicInputRadius;
    context.lineTo(endX, startY);
    var endY = center.y+logicInputRadius;
    context.lineTo(endX, endY);
    context.lineTo(startX, endY);
    context.stroke();
}
