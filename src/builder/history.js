//Copyright Matthew Pottage 2019.
//Used to store a list of Actions that have been applied and undo/redo actions
//in order.
export class ActionHistory {
    //callbacks: Functions called every time the history state is changed.
    constructor() {
        //History manager.
        //Note: This supports infinite undo.
        this.callbacks = [];
            //List of callbacks called every time the history is changed.
        this.clear();
    }
    clear() {
        this.actions = []; //List of actions applied.
        //Assumes that if this.currentPos>=0, then this.currentPos is a valid index
        //into this.actions, the same for this.maxPos..
        this.currentPos = -1; //Current applied action index.
        this.maxPos = -1; //Maximum valid index.
        this._callCallbacks();
    }
    _callCallbacks() {
        this.callbacks.forEach(function(c) {
            c(this);
        }, this);
    }
    undo(game) {
        if(this.canUndo()) {
            this.actions[this.currentPos].revoke(game);
            --this.currentPos; //New current action.
            this._callCallbacks();
        }
    }
    redo(game) {
        if(this.canRedo()) {
            ++this.currentPos;
            this.actions[this.currentPos].apply(game);
            this._callCallbacks();
        }
    }
    push(action) {
        ++this.currentPos;
        this.actions[this.currentPos] = action;
        this.maxPos = this.currentPos;
        this._callCallbacks();
    }
    canUndo() {
        return this.currentPos>=0;
    }
    canRedo() {
        return this.currentPos<this.maxPos;
    }
}
