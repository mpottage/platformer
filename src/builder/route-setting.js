//Copyright Matthew Pottage 2019.
import {Operation} from './operation.js';
import * as core from '../core/import-level-objects.js';
import {Action} from './action.js';
import {clipLevelX, clipLevelY, getScale, round5} from './utility.js';
import {getRootParent} from './filtering.js';
import {highlightBox, selectHighlight, selectHandleHighlight, drawOneHandle, movePathWidth} from './visuals.js';
import {to3dp} from '../core/utility.js';

export function canSetLRRoute(obj) {
    return (obj instanceof core.LRPlatform) || (obj instanceof core.BadTriangle);
}

//Get drag handle to change left/right route (assumes obj has a left/right
//route).
export function getLRRouteHandle(obj) {
    var c = {x: obj.endPos.x+obj.box.width+movePathWidth/2, y: obj.box.y+obj.box.height/2};
    if(obj instanceof core.BadTriangle)
        c.y = obj.startPos.y-obj.box.height/2;
    if(obj.startPos.x!=obj.endPos.x &&
        obj.endPos.x==obj.alternateStartX)
        c.x = obj.startPos.x-movePathWidth/2;
    return c;
}

//Change a left/right route, given a one fixed endpoint (alternateStartX).
// Object to modify will have startPos, endPos and alternateStartX to describe
// route (supports any object where canSetLRRoute(obj) is true).
// No history entry recorded (so that it works when creating a new object).
export class LRMovingOperation extends Operation {
    //obj: Object to modify
    constructor(next, obj) {
        super(next);
        this.obj = obj;
        //Start x to use when setting the route from the other end
        this.defaultStart = obj.startPos.x;
        this.defaultEnd = obj.endPos.x;
    }
    move(state, game, gX, gY) {
        var gameX = clipLevelX(game.level, gX);
        if(state.snapToGrid)
            gameX = round5(gameX);
        if(gameX>this.obj.alternateStartX+this.obj.box.width) {
            //If setting the other end, must have shrunk this side to 0
            this.defaultEnd = this.obj.alternateStartX;
            this.obj.startPos.x = this.defaultStart;
            var newEnd = to3dp(gameX-this.obj.box.width);
            this.obj.endPos.x = newEnd;
        }
        else if(gameX<this.obj.alternateStartX) {
            //If setting the other end, must have shrunk this side to 0
            this.defaultStart = this.obj.alternateStartX;
            this.obj.endPos.x = this.defaultEnd;
            this.obj.startPos.x = gameX;
        }
        else {
            //Set to a 0 length route
            this.defaultStart = this.defaultEnd = this.obj.alternateStartX;
            this.obj.startPos.x = this.obj.endPos.x = this.obj.alternateStartX;
        }
        updateLRMoving(game, this.obj);
        return this;
    }
    draw(state, game, context) {
        highlightBox(context, this.obj.box, selectHighlight, getScale(game));
        drawOneHandle(context, getScale(game), getLRRouteHandle(this.obj),
            selectHandleHighlight);
    }
}

//Handle any special behaviour when adjusting a LR route.
function updateLRMoving(game, obj) {
    if(obj.routeHalf!==undefined)
        obj.routeHalf = Math.abs(obj.endPos.x-obj.startPos.x)/2;
    if(game.forceFrozen) {
        var root = getRootParent(obj, game.level.objs) || obj;
        root.reset();
    }
}

//Like LRMovingOperation, but creates a history entry for setting the new route.
export class HistoryLRMovingOperation extends LRMovingOperation {
    //obj: Object to modify
    constructor(next, obj) {
        super(next, obj);
        this.startBefore = this.obj.startPos.x;
        this.endBefore = this.obj.endPos.x;
    }
    release(state, game) {
        var startAfter = this.obj.startPos.x;
        var endAfter = this.obj.endPos.x;
        state.actionHistory.push(new LRMovingAction(this.obj,
            this.startBefore, this.endBefore, startAfter, endAfter));
        return super.release(state, game);
    }
}

//Change the movement endpoints of an object that has an LR (left/right) route.
class LRMovingAction extends Action {
    //startBefore/endBefore: Original x value for start/end.
    //startAfter/endAfter: New adjusted x value for start/end.
    constructor(obj, startBefore, endBefore, startAfter, endAfter) {
        super();
        this.obj = obj;
        this.startBefore = startBefore;
        this.endBefore = endBefore;
        this.startAfter = startAfter;
        this.endAfter = endAfter;
    }
    apply(game) {
        this.obj.startPos.x = this.startAfter;
        this.obj.endPos.x = this.endAfter;
        updateLRMoving(game, this.obj);
    }
    revoke(game) {
        this.obj.startPos.x = this.startBefore;
        this.obj.endPos.x = this.endBefore;
        updateLRMoving(game, this.obj);
    }
}

//Change the movement endpoints of an object that has an UD (up/down) route.
export function canSetUDRoute(obj) {
    return (obj instanceof core.UDPlatform);
}
//Get drag handle to change up/down route (assumes obj has a up/down
//route).
export function getUDRouteHandle(obj) {
    var c = {x: obj.startPos.x+obj.box.width/2, y: obj.endPos.y+obj.box.height+movePathWidth/2};
    if(obj.startPos.y!=obj.endPos.y &&
        obj.endPos.y==obj.alternateStartY)
        c.y = obj.startPos.y-movePathWidth/2;
    return c;
}

//Change a up/down route, given a one fixed endpoint (alternateStartY).
// Object to modify will have startPos, endPos and alternateStartY to describe
// route (supports any object where canSetUDRoute(obj) is true).
// No history entry recorded (so that it works when creating a new object).
export class UDMovingOperation extends Operation {
    //obj: Object to modify
    constructor(next, obj) {
        super(next);
        this.obj = obj;
    }
    move(state, game, gX, gY) {
        var gameY = clipLevelY(game.level, gY);
        if(state.snapToGrid)
            gameY = round5(gameY);
        if(gameY>=this.obj.alternateStartY) {
            this.obj.startPos.y = this.obj.alternateStartY;
            var newEnd = gameY-this.obj.box.height;
            if(newEnd>this.obj.startPos.y)
                this.obj.endPos.y = newEnd;
            else
                this.obj.endPos.y = this.obj.startPos.y;
        }
        else {
            this.obj.endPos.y = this.obj.alternateStartY;
            this.obj.startPos.y = gameY;
        }
        updateUDMoving(game, this.obj);
        return this;
    }
    draw(state, game, context) {
        highlightBox(context, this.obj.box, selectHighlight, getScale(game));
        drawOneHandle(context, getScale(game), getUDRouteHandle(this.obj),
            selectHandleHighlight);
    }
}

//Handle any special behaviour when adjusting a UD route.
function updateUDMoving(game, obj) {
    if(obj.routeHalf!==undefined)
        obj.routeHalf = Math.abs(obj.endPos.y-obj.startPos.y)/2;
    if(game.forceFrozen) {
        var root = getRootParent(obj, game.level.objs) || obj;
        root.reset();
    }
}

//Like UDMovingOperation, but creates a history entry for setting the new route.
export class HistoryUDMovingOperation extends UDMovingOperation {
    //obj: Object to modify
    constructor(next, obj) {
        super(next, obj);
        this.startBefore = this.obj.startPos.y;
        this.endBefore = this.obj.endPos.y;
    }
    release(state, game) {
        var startAfter = this.obj.startPos.y;
        var endAfter = this.obj.endPos.y;
        state.actionHistory.push(new UDMovingAction(this.obj,
            this.startBefore, this.endBefore, startAfter, endAfter));
        return super.release(state, game);
    }
}

class UDMovingAction extends Action {
    //startBefore/endBefore: Original y value for start/end.
    //startAfter/endAfter: New adjusted y value for start/end.
    constructor(obj, startBefore, endBefore, startAfter, endAfter) {
        super();
        this.obj = obj;
        this.startBefore = startBefore;
        this.endBefore = endBefore;
        this.startAfter = startAfter;
        this.endAfter = endAfter;
    }
    apply(game) {
        this.obj.startPos.y = this.startAfter;
        this.obj.endPos.y = this.endAfter;
        updateUDMoving(game, this.obj);
    }
    revoke(game) {
        this.obj.startPos.y = this.startBefore;
        this.obj.endPos.y = this.endBefore;
        updateUDMoving(game, this.obj);
    }
}
