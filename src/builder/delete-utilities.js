//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js'
import * as mods from './mods/import-level-objects.js'
import {DeleteObject} from './add-delete-actions.js';
import {getFlagsAndControls, getPortals} from './connections.js';
import {hasFlagOutput} from './adjust-links.js';
import {CleanUsers, CleanFlagReplacements} from './adjust-links.js';
import {ActionGroup} from './action.js';
import {getImagesUsing, isImageObject, imageUserCount, getTextureNames, DeleteTexture} from './images.js';
import {ClearPortalInput} from './connect-portals.js';
import {Action} from './action.js';
import {getUniqueId} from './utility.js';
import {getVisibleChildren, getRootParent} from './filtering.js';
import {RemoveStartPosition} from './start-position-actions.js';

//Get appropriate action to delete delObj, where
//game.level.objs.indexOf(delObj)!=-1, with the focussed visible object being
//visibleTarget (which is, or or a descendant of delObj).
//If delObj has multiple visible children it will malfunction unless
//(delObj instanceof mods.Group).
export function getDeleteAction(game, delObj, visibleFocus) {
    var action = new DeleteObject(delObj);
    var flagsDetails = getFlagsAndControls(game.level.objs);
    var pipesDetails = getPortals(game.level.objs);
    //Handle removing flags when deleting logic blocks.
    //(including removing unused ApplyFlag, etc).
    if(hasFlagOutput(game, delObj))
        action = new ActionGroup([action,
                new CleanUsers(delObj.name, flagsDetails[delObj.name].users),
                new CleanFlagReplacements()]);
    else if(delObj instanceof core.ApplyFlag) {
        var actions = [action];
        if(hasFlagOutput(game, delObj.obj)) //Nested flag output.
            actions.push(new CleanUsers(delObj.obj.name, flagsDetails[delObj.obj.name].users))
        actions.push(new CleanFlagReplacements());
        action = new ActionGroup(actions);
    }
    //Deleting a start position deletes an object _inside_ a
    //mods.StartPositions object, not a root object in the level.
    else if(delObj instanceof mods.StartPositions
            && delObj.visualPos.length>1 && visibleFocus)
        action = new RemoveStartPosition(delObj, visibleFocus);
    //Tidy up dependencies of a group.(no broken flag cleanup actions have been
    //created earlier as delObj is looked at for flag cleanup, not visibleFocus).
    if(delObj instanceof mods.Group) {
        var actions = [];
        //Remove textures only used by objects in this group
        var textures = getTextureNames(delObj.objs);
        for(var name of textures) {
            if(getImagesUsing(delObj.objs, name).length==imageUserCount(game, name))
                actions.push(new DeleteTexture(game.level, name));
        }
        //Cleanup external flag object replacements which are needed for a relay
        //(or something) in the group, which are no longer needed.
        var groupFlags = getFlagsAndControls(delObj.objs);
        for(var f in groupFlags) {
            if(!groupFlags[f].owner)
                actions.push(new CleanFlagReplacements());
        }
        //Any extra cleanup needed?
        if(actions.length>0)
            action = new ActionGroup([action].concat(actions));
    }
    //Disconnect any pipes connected to the pipe being deleted
    else if(visibleFocus instanceof core.PortalRegion
        && pipesDetails[visibleFocus.output].user) {
        action = new ActionGroup([action,
            new ClearPortalInput(pipesDetails[visibleFocus.output].user,
                pipesDetails[visibleFocus.output].user.input)]);
    }
    //Remove texture, if visibleFocus is the only user of it
    else if(isImageObject(visibleFocus) && imageUserCount(game, visibleFocus.textureName)==1)
        action = new ActionGroup([action, new DeleteTexture(game.level, visibleFocus.textureName)]);
    return action;
}

export function groupDelete(state, game, roots) {
    //Get the first visible child of each root (needed to call getDeleteAction)
    // (deleting mods.Groups is unaffected by the rest of their children being
    // ignored, as they are self-contained)
    var visible = roots.map(r => getVisibleChildren([r])[0]);
    //Remove the objects one by one (ensuring that any cleanup actions
    //needed for objects involving flags/portals are applied correctly).
    // The precomputed roots are not used as the root may change as a result of
    // previous objects being deleted (say for a object wrapped in ApplyFlag and
    // the switch is deleted), however the visible object will not be removed
    // from the level as a result of previous objects being deleted.
    var actions = visible.map(o => {
        var root = getRootParent(o, game.level.objs) || o
        var a = getDeleteAction(game, root, o);
        a.apply(game);
        return a;
    });
    var action = new ActionGroup(actions);
    state.actionHistory.push(action);
}
