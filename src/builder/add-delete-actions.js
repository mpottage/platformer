//Copyright Matthew Pottage 2019.
import {registerAll} from './utility.js';
import {Action} from './action.js';

export class AddObject extends Action {
    //obj: Object to add to the current level.
    constructor(obj) {
        super();
        this.obj = obj;
    }
    apply(game) {
        if(game.level.objs.indexOf(this.obj)==-1) {
            game.level.objs.push(this.obj);
            this.obj.reset();
            registerAll(game);
        }
    }
    revoke(game) {
        if(game.level.objs[game.level.objs.length-1]===this.obj) {
            game.level.objs.pop();
            registerAll(game);
        }
    }
}
export class AddMultipleObjects extends Action {
    constructor(objs) {
        super();
        this.objs = objs;
    }
    apply(game) {
        game.level.objs = game.level.objs.concat(this.objs);
        this.objs.forEach(o => o.reset());
        registerAll(game);
    }
    revoke(game) {
        game.level.objs.splice(game.level.objs.length-this.objs.length,
            this.objs.length);
        registerAll(game);
    }
}
export class DeleteObject extends Action {
    //obj: Object to remove from the current level.
    constructor(obj) {
        super();
        this.obj = obj;
        this.insertIndex = -1;
    }
    apply(game) {
        var i = game.level.objs.indexOf(this.obj);
        if(i!=-1) {
            this.insertIndex = i;
            game.level.objs.splice(i, 1);
            registerAll(game);
        }
    }
    revoke(game) {
        if(game.level.objs.indexOf(this.obj)==-1
            && this.insertIndex!=-1) {
            game.level.objs.splice(this.insertIndex, 0, this.obj);
            this.obj.reset();
            registerAll(game);
        }
    }
}

//Swap an object, for another, at the same position in the level as the previous
//object.
//  Used when wrapping an object in ApplyFlag, MakeFlagFrom or MakeAndApplyFlag
export class ReplaceObject extends Action {
    constructor(oldObj, newObj) {
        super();
        this.old = oldObj;
        this.add = newObj;
        this.index = -1;
    }
    apply(game) {
        if(this.index==-1) {
            this.index = game.level.objs.indexOf(this.old);
            if(this.index==-1)
                throw new TypeError("ReplaceObject: Unable to replace object.");
        }
        game.level.objs[this.index] = this.add;
        this.add.reset();
        registerAll(game);
    }
    revoke(game) {
        if(this.index==-1)
            throw new Error("ReplaceObject action needs to be run before being revoked.");
        game.level.objs[this.index] = this.old;
        this.old.reset();
        registerAll(game);
    }
}
