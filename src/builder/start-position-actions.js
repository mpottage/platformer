//Copyright Matthew Pottage 2019
import {Action} from './action.js';
import {registerAll} from './utility.js';

export class AddStartPosition extends Action {
    //obj: Start positions container
    //visual: Visual that marks the start position
    constructor(obj, visual) {
        super();
        this.obj = obj;
        this.visual = visual;
    }
    apply(game) {
        this.obj.visualPos.push(this.visual);
        this.obj.syncPositions(game.manager);
        registerAll(game);
    }
    revoke(game) {
        this.obj.visualPos.pop();
        this.obj.syncPositions(game.manager);
        registerAll(game);
    }
}

export class RemoveStartPosition extends Action {
    //obj: Start positions container
    //visual: Visual that marks the start position
    constructor(obj, visual) {
        super();
        this.obj = obj;
        this.visual = visual;
        this.index = -1; //Remember where the start position was before deletion
    }
    apply(game) {
        if(this.index==-1)
            this.index = this.obj.visualPos.indexOf(this.visual);
        if(this.index!=-1)
            this.obj.visualPos.splice(this.index, 1);
        this.obj.syncPositions(game.manager);
        registerAll(game);
    }
    revoke(game) {
        if(this.index==-1)
            this.obj.visualPos.push(this.visual);
        else
            this.obj.visualPos.splice(this.index, 0, this.visual);
        this.obj.syncPositions(game.manager);
        registerAll(game);
    }
}
