//Copyright Matthew Pottage 2019.
import {ActionHistory} from './history.js';

export function registerAll(game) {
    game.manager.clear();
    game.collisions.clear();
    game.environment.clear();
    if(game.level.suspendingOptimization)
        game.collisions.setShutdown(game.shutdownTest.bind(game));
    game.environment.apply(game.level);
    game.level.register(game.collisions, game.manager, game.caches);
    game.player.register(game.collisions, game.manager, game.caches);
    game.collisions.autoProtect();
    game.updateConnections();
    refreshCache(game);
}

export function refreshCache(game) {
    game.caches.refresh(game.scale*(game.camera.scale||1)*game.level.scale,
        game.textures);
}

export function getGridSize(snapToGrid) {
    if(snapToGrid)
        return 5;
    else
        return 1;
}
//Smallest permitted width or height for a box, which is double the size the
//smallest grid square.
export function getMinResize(snapToGrid) {
    return getGridSize(snapToGrid)*2;
}
export function getDefaultState() {
    return {
        snapToGrid: true,
        selectBackground: true,
        lazySelect: false,
        mode: "n",
        insertObject: "Platform",
        actionHistory: new ActionHistory(),
        color: "#5f5f5f",
        currentImage: ""
    };
};

export function clipLevelX(level, gameX) {
    return Math.max(0, Math.min(gameX, level.width));
}
export function clipLevelY(level, gameY) {
    return Math.max(0, Math.min(gameY, level.height));
}

export function getScale(game) {
    return game.camera.scale*game.level.scale;
}

//Resize a box, with one fixed, and one changed corner.
//origin: Fixed corner that exists on box and will exist on the resized box
//(gameX, gameY): Opposite corner to origin for the resized box.
//box: Box to resize (will be modified)
//force: Do resize even if it won't necessarily take minimum and grid size into
//account (optional, default false).
export function resizeBox(origin, gameX, gameY, box, snapToGrid=false, force=false) {
    //Cache smallest size for width/height
    var minSize = getMinResize(snapToGrid);
    if(force)
        minSize = 0;
    if(!force && snapToGrid) {
        gameX = round5(gameX);
        gameY = round5(gameY);
    }
    var xDiff = Math.abs(origin.x-gameX);
    if(gameX>origin.x) {
        box.x = origin.x;
        if(xDiff>=minSize)
            box.width = xDiff;
        else
            box.width = minSize;
    }
    else if(gameX<origin.x) {
        if(xDiff>=minSize) {
            box.x = gameX;
            box.width = xDiff;
        }
        else {
            box.x = origin.x-minSize;
            box.width = minSize;
        }
    }
    var yDiff = Math.abs(origin.y-gameY);
    if(gameY>origin.y) {
        box.y = origin.y;
        if(yDiff>=minSize)
            box.height = yDiff;
        else
            box.height = minSize;
    }
    else if(gameY<origin.y) {
        box.y = origin.y;
        if(yDiff>=minSize) {
            box.y = gameY;
            box.height = yDiff;
        }
        else {
            box.height = minSize;
            box.y = origin.y-minSize;
        }
    }
}

//Round to the nearest multiple of 5
export function round5(x) {
    return Math.round(x/5)*5;
}
//Round up to the nearest multiple of 5
export function ceil5(x) {
    return Math.ceil(x/5)*5;
}
//Round down to the nearest multiple of 5
export function floor5(x) {
    return Math.floor(x/5)*5;
}

//A a string of text that is as short as possible, composed of a prefix and a
//number and not in a supplied list of strings.
//  starter: Prefix for the string (excluding trailing "/")
//  known: Strings which should not be returned.
//For getUniqueId("tmp", ["tmp/0", "k/0", "k/1"]) a valid result is "tmp/1".
export function getUniqueId(starter, known) {
    var alreadySeen = known.filter(o => o.match("^"+starter+"/"));
    var i = 0;
    var name = starter+"/"+i;
    while(alreadySeen.indexOf(name)!==-1) {
        ++i;
        name = starter+"/"+i;
    }
    return name;
}
