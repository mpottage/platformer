//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js';
import * as mods from './mods/import-level-objects.js';

//Get the object containing obj, in knownObjs, if any.
export function getRootParent(obj, knownObjs) {
    if(hasRootRefVisual(obj))
        return obj.builderGroup;
    var res = knownObjs[containedIndex(knownObjs, obj)];
    if(res && res!=obj)
        return res;
    else
        return null;
}
//Object where the group is precomputed and attached as a member, and as it is a
//visual it is not part of any level barriers, and could only be nested in the
//precomputed group (if any), otherwise is a root in the level objects, should
//it be being displayed.
function hasRootRefVisual(obj) {
    return (obj instanceof core.VisualOnlyBackground)
        || (obj instanceof core.VisualOnlyOverlay);
}

//Returns the index where obj is contained in the list, returns -1 if obj is not
//found. Accounts for nesting, but not for being contained in spawners/boxes.
export function containedIndex(list, obj) {
    for(var i=0; i<list.length; ++i) {
        if(list[i]===obj ||
                ((list[i] instanceof core.ApplyFlag ||
                  list[i] instanceof core.MakeFlagFrom ||
                  list[i] instanceof core.MakeAndApplyFlag)
                 && list[i].obj===obj)
                || (list[i] instanceof core.MakeFlagFrom
                    && list[i].obj instanceof core.BouncingPickup
                    && list[i].obj.pickup==obj)
                || (list[i] instanceof mods.StartPositions
                    && list[i].visualPos.indexOf(obj)!=-1)
                || ((list[i] instanceof core.BouncingPickup)
                    && list[i].pickup==obj)
                || ((list[i] instanceof core.Laser)
                    && list[i].follow==obj)
                || ((list[i] instanceof core.ApplyFlag)
                    && (list[i].obj instanceof core.Laser)
                    && (list[i].obj.follow==obj))
                || ((list[i] instanceof mods.Group
                    && containedIndex(list[i].objs, obj)!=-1))
                ) {
            return i;
        }
    }
    return -1;
}
//Repeated check for whether to return the object for getTopObj below.
function _getTopObj_check(obj, selectBox, level, objStart, check) {
    if(((!obj.disabled && obj.select!==false) || obj.select)
            && obj.box && core.Box.isIntersection(obj.box, selectBox)) {
        if(hasRootRefVisual(obj))
            return !check || check(obj, obj.builderGroup);
        var i = containedIndex(level.objs, obj);
        return (i>=objStart &&
                (!check || check(obj, (obj!=level.objs[i] && level.objs[i]))));
    }
    return false;
}
//Gets the topmost object that isn't disabled, is within 1px (or 2px with
//snapToGrid) of (x, y), is contained in level.objs, not a barrier for
//level and the (optional) function call check(obj, container) returns true.
export function getTopObj(snapToGrid, manager, level, x, y, check) {
    var selectBox = null;
    if(snapToGrid) //Use a bigger selection area as everything is snapped to 5px.
        selectBox = new core.Box(x-2, y-2, 5, 5); //2px padded box around (x, y).
    else
        selectBox = new core.Box(x-1, y-1, 3, 3); //1px padded box around (x, y).
    var objStart = getNonBarrierStart(level);
    for(var i=manager.overlayObjs.length-1; i>=0; --i) {
        var obj = manager.overlayObjs[i];
        if(_getTopObj_check(obj, selectBox, level, objStart, check))
            return obj;
    }
    for(var i=manager.drawObjs.length-1; i>=0; --i) {
        var obj = manager.drawObjs[i];
        if(_getTopObj_check(obj, selectBox, level, objStart, check))
            return obj;
    }
    for(var i=manager.backgroundObjs.length-1; i>=0; --i) {
        var obj = manager.backgroundObjs[i];
        if(_getTopObj_check(obj, selectBox, level, objStart, check))
            return obj;
    }
    return null;
}
export function getAllObjs(manager, level, selectBox, check) {
    var res = [];
    var objStart = getNonBarrierStart(level);
    for(var i=manager.overlayObjs.length-1; i>=0; --i) {
        var obj = manager.overlayObjs[i];
        if(_getTopObj_check(obj, selectBox, level, objStart, check))
            res.push(obj);
    }
    for(var i=manager.drawObjs.length-1; i>=0; --i) {
        var obj = manager.drawObjs[i];
        if(_getTopObj_check(obj, selectBox, level, objStart, check)
                && res.indexOf(obj)==-1)
            res.push(obj);
    }
    for(var i=manager.backgroundObjs.length-1; i>=0; --i) {
        var obj = manager.backgroundObjs[i];
        if(_getTopObj_check(obj, selectBox, level, objStart, check)
                && res.indexOf(obj)==-1)
            res.push(obj);
    }
    return res;
}

//Get the index of the first object that isn't a default barrier in
//level.objs.
//Used for compatibility with the few levels created _before_ the level builder
//had protected special barriers. The top barrier is added by updateBoundaries
//if the left/right barriers existed, so one existing implies the other.
export function getNonBarrierStart(level) {
    var i = 0;
    if(level.objs.length>=3) {
        //Check for (approx) default barriers (as in getCleanLevel).
        if(level.objs[0].box &&
                level.objs[0].box.x==-1 && level.objs[1].box &&
                level.objs[1].box.x==level.width && level.objs[2].box.x==0 &&
                level.objs[2].box && level.objs[2].box.y==level.objs[0].box.y)
            i = 3;
        if(level.objs[i].box && level.objs[i].box.y==level.height+80)
            ++i;
    }
    return i;
}

//from: List of objects to find the visual objects from.
//A visible object, has a box, so not ApplyFlag or mods.Group, and will have a
//visual drawn (directly or indirectly, not checked) when not in "Normal" mode.
export function getVisibleChildren(from) {
    var tmp = [];
    for(var obj of from) {
        if(obj instanceof mods.Group)
            tmp = tmp.concat(obj.objs);
        else if(obj instanceof mods.StartPositions)
            tmp = tmp.concat(obj.visualPos);
        else
            tmp.push(obj);
    }
    return tmp.map(function(obj) {
        var res = obj;
        if(!obj.box && obj.obj) //MakeFlagFrom, ApplyFlag, etc.
            res = obj.obj;
        //Selectable object is the nested collidable (e.g. CuttingLaser).
        if(res.select===false && res.follow && res.follow.box)
            res = res.follow;
        return res;
    });
}
