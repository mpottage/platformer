# Level Builder for Platform 2.

Functions and classes here are used to provide level editing capabilities, and
combined in [../builder/game.js](../builder/game.js)x and [../builder/builder.js](../builder/builder.js).

For a usage guide for the resulting level editor see [here](../builder/docs.html)

Copyright Matthew Pottage 2015-2019.

