//Copyright Matthew Pottage 2019.
import {Action} from './action.js';

export function getRGB(hexColor) {
    return [parseInt(hexColor.slice(1, 3), 16),
        parseInt(hexColor.slice(3, 5), 16),
        parseInt(hexColor.slice(5, 7), 16)
    ];
}
//Convert hexColor (form #XXXXXX) to rgba(X, X, X, X), with alpha channel set to
//alpha (string representation, alpha is in (0, 1]).
export function applyAlpha(hexColor, alpha) {
    var alphaVal = parseFloat(alpha);
    if(isNaN(alphaVal) || alphaVal>=1 || alphaVal<0)
        return hexColor;
    else {
        let [red, green, blue] = getRGB(hexColor);
        return "rgba("+red+","+green+","+blue+","+alpha+")";
    }
}

//Factor must be in [0,1), smaller values cause the colour to be lightened
//less
//Accepts colors in the format rgba(r,g,b,a) and #XXXXXX
export function lightenColor(color, factor) {
    let r=0, g=0, b=0, a=1;
    if(color[0]=="#") {
        ([r,g,b] = getRGB(color));
    }
    else if(color.startsWith("rgba")){
        let match = color.match(/rgba\((\d+), ?(\d+), ?(\d+), ?(\d+|(\d?\.\d+))\)/);
        [r,g,b,a] = [parseInt(match[1]), parseInt(match[2]), parseInt(match[3]), parseFloat(match[4])];
    }
    r += Math.floor((255-r)*factor);
    g += Math.floor((255-g)*factor);
    b += Math.floor((255-b)*factor);
    return "rgba("+r+","+g+","+b+","+a+")";
}

//obj: Object to change color on.
//prevColor: previous (or current) color (used to revoke)
//newColor: color to change to
export class ColorObject extends Action {
    constructor(obj, prevColor, newColor) {
        super();
        this.obj = obj;
        this.prevColor = prevColor;
        this.newColor = newColor;
    }
    revoke() {
        this.obj.color = this.prevColor;
    }
    apply() {
        this.obj.color = this.newColor;
    }
}

