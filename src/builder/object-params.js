//Copyright Matthew Pottage 2019.
import {Action} from './action.js';
import {registerAll} from './utility.js';

export class ChangeParam extends Action {
    constructor(obj, param, prevValue, newValue) {
        super();
        this.obj = obj;
        this.param = param;
        this.prev = prevValue;
        this.succ = newValue;
    }
    apply(game) {
        this.obj.setParam(this.param, this.succ);
        registerAll(game);
    }
    revoke(game) {
        this.obj.setParam(this.param, this.prev);
        registerAll(game);
    }
}

//Compare every parameter to check they are all identical.
export function allParamsEquiv(state, objs) {
    var allParams = objs.map(o => ((o.getParams && o.getParams(state)) || []))
    var notFirst = allParams.slice(1);
    for(let ls of notFirst) {
        if(ls.length!=allParams[0].length)
            return false;
        for(let i=0; i<ls.length; ++i) {
            for(let key in ls[i]) {
                //Every type has the same set of keys, so no need to check
                //for extra keys
                if(ls[i][key]!==allParams[0][i][key]) {
                    return false;
                }
            }
        }
    }
    return true;
}
