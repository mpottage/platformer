//Copyright Matthew Pottage 2019.
import * as core from '../core/import-level-objects.js';
import {registerAll, getUniqueId, refreshCache} from './utility.js';
import {getVisibleChildren} from './filtering.js';
import {Action} from './action.js';

export function isImageObject(obj) {
    return obj instanceof core.ImageBackground || obj instanceof core.ImageOverlay;
}
export function getImages(objs) {
    return getVisibleChildren(objs).filter(isImageObject);
}
export function getImagesUsing(objs, name) {
    return getVisibleChildren(objs).filter(isImageObject).filter(o => o.textureName===name);
}
export function imageUserCount(game, name) {
    return getImagesUsing(game.level.objs, name).length;
}
export function getTextureNames(objs) {
    var images = getImages(objs);
    var textures = [];
    for(var i of images)
        if(textures.indexOf(i.textureName)==-1)
            textures.push(i.textureName);
    return textures;
}

export function getImageName(game, dataURL, blacklist=[]) {
    for(var n in game.level.uniqueTextures) {
        if(game.level.uniqueTextures[n]===dataURL)
            return {action: null, name: n};
    }
    var name = getUniqueId("__", Object.keys(game.level.uniqueTextures).concat(blacklist));
    return {action: new AddTexture(game.level, name, dataURL), name: name};
}

export function anyTextureDependencies(objs) {
    return getVisibleChildren(objs).some(isImageObject);;
}
//Only get textures from allTextures that are used by objs
export function restrictTextures(objs, allTextures) {
    var names = getImages(objs).map(o => o.textureName);
    var res = {};
    for(var n of names) {
        res[n] = allTextures[n];
    }
    return res;
}

//Adds an image texture to a level
export class AddTexture extends Action {
    //level: Level to modify
    //name: Name of texture to add
    //dataURL: base64 data URL describing the image to add
    constructor(level, name, dataURL) {
        super();
        this.level = level;
        this.name = name;
        this.dataURL = dataURL;
    }
    apply(game) {
        this.level.uniqueTextures[this.name] = this.dataURL;
        this.level.addTextures(game.textures);
        game.textures.load().then(function() {
            refreshCache(game);
        });
    }
    revoke(game) {
        delete this.level.uniqueTextures[this.name];
        game.textures.remove(this.name);
        game.caches.purgeMissingTextures(game.textures);
        registerAll(game);
    }
}

//Remove an image texture from a level
export class DeleteTexture extends Action {
    //level: Level to modify
    //name: Name of texture to remove
    constructor(level, name) {
        super();
        this.level = level;
        this.name = name;
        this.dataURL = "";
    }
    apply(game) {
        this.dataURL = this.level.uniqueTextures[this.name];
        delete this.level.uniqueTextures[this.name];
        game.textures.remove(this.name);
        game.caches.purgeMissingTextures(game.textures);
        registerAll(game);
    }
    revoke(game) {
        this.level.uniqueTextures[this.name] = this.dataURL;
        this.level.addTextures(game.textures);
        game.textures.load().then(function() {
            refreshCache(game);
        });
    }
}

//Change the image an object displaying an image uses
export class SwapImage extends Action {
    constructor(obj, prevName, newName) {
        super();
        this.obj = obj;
        this.prevName = prevName;
        this.newName = newName;
    }
    apply(game) {
        this.obj.textureName = this.newName;
        game.caches.removePrefixed(this.prevName);
        registerAll(game);
    }
    revoke(game) {
        this.obj.textureName = this.prevName;
        game.caches.removePrefixed(this.newName);
        registerAll(game);
    }
}

//For SVGs without a width/height attribute takes the width/height of the
//viewbox and uses that instead, returns a new dataURL for the corrected image.
//This ensures that Chrome and Firefox behave the same (Chrome does this fix by
//default), and as many SVGs as possible work.
export function fixSVGRender(dataURL) {
    var parser = new DOMParser();
    var split = dataURL.split(",");
    var dom = parser.parseFromString(atob(split[1]), "application/xml");
    var root = dom.documentElement;
    if(!root.hasAttribute("width")
            || !root.hasAttribute("height")) {
        //Provides behaviour of Chrome, using the viewbox width/height for
        //width/height, which is better than not rendering the SVG at all.
        if(root.hasAttribute("viewBox")) {
            var viewBox = root.viewBox.baseVal;
            root.setAttribute("width", viewBox.width);
            root.setAttribute("height", viewBox.height);
        }
        //Missing viewbox, and width/height, and rendering is inconsistent, even
        //when using the same browser at different zooms and sizes, so switch
        //rendering off (with width/height set to 0).
        else {
            root.setAttribute("width", 0);
            root.setAttribute("height", 0);
        }
        var newXML = (new XMLSerializer()).serializeToString(dom);
        return split[0]+","+btoa(newXML);
    }
    return dataURL;
}
