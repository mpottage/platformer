//Copyright Matthew Pottage 2019-2020.

//Represents an action that has been applied. Used by ActionHistory.
// A sequence of actions is always assumed to be applied, and if revoked,
// revoked in the opposite order to being applied.
export class Action {
    constructor() { }
    revoke(game) { }
    apply(game) { }
}

//Group multiple actions together to be applied as a single action
export class ActionGroup extends Action {
    constructor(actions) {
        super();
        this.actions = actions;
    }
    apply(game) {
        for(var i=0; i<this.actions.length; ++i)
            this.actions[i].apply(game);
    }
    revoke(game) {
        for(var i=this.actions.length-1; i>=0; --i)
            this.actions[i].revoke(game);
    }
}
