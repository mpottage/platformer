//Copyright Matthew Pottage 2019.
import {Player} from './player/player.js';
import {Collidable, registerCDU} from './collisions/collidable.js';
import {Box, to3dp, absValue, screenHeight} from './utility.js';

//Platform for jumping onto and walking on.
export class Platform extends Collidable {
    //  color: Colour of the platform, default is black.
    constructor(box, color="#000") {
        super();
        this.box = box;
        this.color = color;
    }
    draw(context) {
        context.fillStyle = this.color;
        context.fillRect(this.box.x, this.box.y, this.box.width, this.box.height);
    }
    reset() { }
}

//Platform that objects can move up through.
export class JumpThroughPlatform extends Platform {
    isCollision(collided) {
        //Returns true if the base of collided.box is below the top of the
        //platform before the collision - uses the fact that collided hasn't
        //been moved into the colliding position yet (see CollisionEngine), ie.
        //object is moving already above the top of the platform.
        return to3dp(collided.box.y+collided.box.height)<=this.box.y;
    }
}

//Platform that objects can move downwards through.
export class DownThroughPlatform extends Platform {
    isCollision(collided) {
        return to3dp(collided.box.y)>=this.box.y+this.box.height;
    }
}

//Platform that objects can move rightwards through.
export class RightThroughPlatform extends Platform {
    isCollision(collided) {
        return to3dp(collided.box.x)>=this.box.x+this.box.width;
    }
}

//Platform that objects can move leftwards through.
export class LeftThroughPlatform extends Platform {
    isCollision(collided) {
        return to3dp(collided.box.x+collided.box.width)<=this.box.x;
    }
}

//A collapsing platform, collapses (falls down) a delay (default 0.3s) after
//impact by a player.
export class CollapsingPlatform extends Platform {
    constructor(box, color, delay) {
        super(box, color);
        this.startY = box.y;
        this.hit = false; //Whether the player has hit the platform.
        if(delay===undefined)
            this.delay = 0.3;
        else
            this.delay = delay;
        this.sinceHit = 0; //How long since the platform was hit.
        this.collapsed = false; //Whether the platform has collapsed.
        this.motion = {x: 0, y: 0}; //y-velocity, used when the platform falls downwards.
    }
    isCollision(obj) {
        return !this.collapsed || obj.box.y+obj.box.height<=this.box.y;
    }
    handleCollision(obj, info) {
        //Checks that it isn't already being stood on, isn't collapsed and has been
        //  hit by a player.
        if(!this.hit && !this.collapsed && obj instanceof Player) {
            this.hit = true;
        }
    }
    //Times this.delay seconds after a player has hit the platform, after which
    //it causes the platform to collapse.
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            if(!this.collapsed) {
                if(this.hit) {
                    this.sinceHit += interval;
                    if(this.sinceHit>this.delay)
                        this.collapsed = true;
                }
            }
            else { //Only affected by gravity when it has collapsed.
                var prevY = this.motion.y;
                this.motion.y += environment.get("gravity")*interval;
                this.box.y += (prevY+this.motion.y)*interval/2; //Move platform
                if(Math.abs(this.box.y-this.startY)>2*screenHeight)
                    this.disabled = true;
            }
        }
    }
    draw(context) {
        if(!this.disabled)
            super.draw(context);
    }
    reset() {
        this.box.y = this.startY;
        this.hit = false;
        this.sinceHit = 0;
        this.collapsed = false;
        this.disabled = false;
        this.motion.y = 0;
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
    }
    getOutputVal() {
        return this.collapsed;
    }
}

//Moving platform going left/right.
export class LRPlatform extends Platform {
    // box: Describes platform and its starting position.
    // startX: Leftmost point the platform reaches.
    // endY: Rightmost point the platform reaches.
    // color: Platform's color.
    // xAccel: Applied x-acceleration.
    constructor(box, startX, endX, color, xAccel=500) {
        super(box, color);
        this.startPos = {x: Math.min(startX, endX), y: box.y};
        this.endPos = {x: Math.max(startX, endX), y: box.y};
        this.alternateStartX = box.x;
        this.routeHalf = Math.abs(startX-endX)/2;
        this.direction = 1; //1=right, -1=left
        if(this.endPos.x==this.box.x)
            this.direction = -1;
        this.motion = {x: 0, y: 0};
        this.failedLast = false;
        this.justTurned = false;
        this.xAccel = xAccel;
        this.activated = true;
    }
    handleMoved(details, amount) {
        if(!this.justTurned && amount==0 && (details.left || details.right)) {
            this.motion.x = 0;
            this.failedLast = true;
            this.direction = -this.direction;
            this.justTurned = true;
        }
    }
    //Moves under simple-harmonic-motion.
    update(interval, collisions) {
        if(!this.activated) {
            this.motion.x = 0;
            return;
        }
        this.justTurned  = false;
        var diff = this.box.x-this.startPos.x;
        var vx = 0;
        if(diff<0)
            this.box.x = this.startPos.x;
        //Calculate x-velocity
        else if(diff<=this.routeHalf)
            vx = Math.sqrt(2*this.xAccel*(diff));
        else if(this.box.x<=this.endPos.x)
            vx = Math.sqrt(2*this.xAccel*(this.endPos.x-this.box.x));
        else
            this.box.x = this.endPos.x;
        if(vx==0)
            vx = this.xAccel*interval;
        vx *= this.direction;
        var moveX = (this.motion.x+vx)*interval/2;
        var newX = this.box.x+moveX;
        if(this.startPos.x>newX) {
            this.direction = 1; //Go right
            moveX = this.startPos.x-this.box.x;
            this.motion.x = 0;
            this.justTurned  = true;
        }
        else if(this.endPos.x<newX) {
            this.direction = -1; //Go left
            moveX = this.endPos.x-this.box.x;
            this.motion.x = 0;
            this.justTurned  = true;
        }
        else if(!this.failedLast)
            this.motion.x = vx;
        this.failedLast = false;
        collisions.queue(this, moveX, 0);
    }
    reset() {
        this.box.x = this.alternateStartX;
        this.direction = 1;
        if(this.endPos.x==this.box.x)
            this.direction = -1;
        this.motion = {x: 0, y: 0};
        this.activated = true;
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
    }
}

//Moving platform going up/down.
export class UDPlatform extends Platform {
    // box: Describes platform and its starting position.
    // startY: Highest point the platform reaches.
    // endY: Lowest point the platform reaches.
    // color: platform's color.
    // yAccel: Applied y-acceleration.
    constructor(box, startY, endY, color, yAccel=250) {
        super(box, color);
        this.startPos = {x: box.x, y: Math.min(startY, endY)};
        this.endPos = {x: box.x, y: Math.max(startY, endY)};
        this.alternateStartY = box.y;
        this.routeHalf = Math.abs(startY-endY)/2;
        this.motion = {x: 0, y: 0};
        this.yAccel = yAccel;
        this.direction = 1; //1==down, -1==up
        if(this.endPos.y==this.box.y)
            this.direction = -1;
        this.activated = true;
    }
    handleMoved(details, amount) {
        if(amount==0 && (details.up || details.down)) {
            this.motion.y = 0;
            this.direction = -this.direction;
        }
    }
    //Moves under simple-harmonic-motion.
    update(interval, collisions) {
        if(!this.activated) {
            this.motion.y = 0;
            return;
        }
        //Calculate speed and move.
        var diff = this.box.y-this.startPos.y;
        var vy = 0;
        if(diff<0)
            this.box.y = this.startPos.y;
        //Calculate the correct velocity (v^2=u^2+2*a*x)
        else if(diff<=this.routeHalf)
            vy = Math.sqrt(2*this.yAccel*(diff));
        else if(this.box.y<=this.endPos.y)
            vy = Math.sqrt(2*this.yAccel*(this.endPos.y-this.box.y));
        else
            this.box.y = this.endPos.y;
        if(vy==0)
            vy = this.yAccel*interval;
        vy *= this.direction; //Go the right way.
        var moveY = (this.motion.y+vy)*interval/2; //Correct distance.
        var newY = moveY+this.box.y;
        //Ensure the platform doesn't go too far.
        if(this.startPos.y>newY) {
            this.direction = 1; //Go down
            moveY = this.startPos.y-this.box.y;
            this.motion.y = 0;
        }
        else if(this.endPos.y<newY) {
            this.direction = -1; //Go up
            moveY = this.endPos.y-this.box.y;
            this.motion.y = 0;
        }
        else
            this.motion.y = vy;
        collisions.queue(this, 0, moveY);
    }
    reset() {
        this.box.y = this.alternateStartY;
        this.direction = 1;
        if(this.endPos.y==this.box.y)
            this.direction = -1;
        this.motion = {x: 0, y: 0};
        this.activated = true;
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
    }
}

export class IcePlatform extends Platform {
    constructor(box, color) {
        super(box, color);
    }
    handleCollision(obj, details) {
        if(details.up && obj.motion && obj.motion.state)
            obj.motion.state.setSurface("ice");
    }
}

export class ThinIcePlatform extends CollapsingPlatform {
    constructor(box, color) {
        super(box, color, 0);
    }
    handleCollision(obj, details) {
        super.handleCollision(obj, details);
        if(details.up && obj.motion && obj.motion.state)
            obj.motion.state.setSurface("ice");
    }
}

//Plain object (color and box) that causes the loss of 1 health when hit.
export class HealthLoss extends Platform {
    constructor(box, color) {
        super(box, color);
    }
    handleCollision(obj) {
        super.handleCollision(obj);
        if(obj.health)
            obj.health.increase(-1);
    }
}

export class Destructible extends Platform {
    constructor(box, color) {
        super(box, color);
        this.reset();
    }
    draw(context, caches) {
        if(!this.disabled)
            super.draw(context, caches);
    }
    reset() {
        this.disabled = false;
    }
    //Disable the platform if an object moves quickly enough into it.
    //For example, the minimum required speeds for:
    //AirborneStone is 600px/s, Player is 300px/s, LRPlatform is 0px/s, .
    handleCollision(obj, details) {
        if(obj.motion
                && absValue(obj.motion.x, obj.motion.y)>=(30*obj.pushRank))
            this.disabled = true;
    }
}

//Barrier that fades when activated.
export class FadingPlatform extends Platform {
    constructor(box, color) {
        super(box, color);
        this.activated = true; //false means open.
        //May be activated at any point in loop, prevents extra collisions
        this.wasActivated = this.activated;
        this.opacity = 1; //Fade the door in and out.
        this._setDelayFrames();
    }
    _setDelayFrames() {
        //Delay before starting fading after registration (reduces flickering).
        this.delayFrames = 2;
    }
    reset() {
        super.reset();
        this.activated = true;
        this.wasActivated = this.activated;
        this._setDelayFrames();
        this.opacity = 1;
    }
    isCollision(obj) {
        return this.activated && this.wasActivated;
    }
    update(interval, collisions) {
        if(this.delayFrames<=0) {
            if(!this.activated && this.opacity>0)
                this.opacity = Math.max(this.opacity-20*interval, 0);
            else if(this.activated && this.opacity<1)
                this.opacity = Math.max(this.opacity+20*interval, 0);
        }
        else {
            --this.delayFrames;
            if(this.delayFrames<1 && !this.activated)
                this.opacity = 0;
        }
        if(this.activated && !this.wasActivated) {
            this.wasActivated = this.activated;
            collisions.protect(this);
        }
        this.wasActivated = this.activated;
    }
    register(collisions, manager) {
        this._setDelayFrames();
        registerCDU(this, collisions, manager);
    }
    draw(ctx) {
        var oldAlpha = ctx.globalAlpha;
        ctx.globalAlpha *= this.opacity;
        super.draw(ctx);
        ctx.globalAlpha = oldAlpha;
    }
}
