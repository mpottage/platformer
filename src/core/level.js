//Copyright Matthew Pottage 2019.
//Stores all the information needed for a level.
export class Level {
    //  width: Width (in game pixels) of the level
    //  height: Height (game pixels) of the level.
    //  objs: Objects in the level.
    //  scale: Change default zoom.
    //  suspendingOptimization: Determines whether to stop objects are are
    //  so far out of view to not make a difference.
    //  uniqueTextures: Extra textures required just for this level. Dictionary, {textureName: textureBase64String}.
    //      It is assumed that the textureNames do not clash with any core game textures.
    //  env: Environment variables as setup for this level. Names as found in
    //  class Environment.
    constructor(width, height, objs, scale=1, suspendingOptimization=true,
            uniqueTextures={}, environmentSetup={}) {
        this.width = width;
        this.height = height;
        this.objs = objs;
        this.scale = scale;
        this.suspendingOptimization = suspendingOptimization;
        this.uniqueTextures = uniqueTextures;
        this.environmentSetup = environmentSetup;
    }
    register(collisions, manager, caches) {
        this.objs.forEach(function(obj) {
            obj.register(collisions, manager, caches);
        });
    }
    addTextures(textures) {
        for(var n in this.uniqueTextures)
            textures.addDataURL(n, this.uniqueTextures[n]);
    }
    //Resets the level to, e.g., replay it.
    reset() {
        this.objs.forEach(function(obj) {
            obj.reset();
        });
    }
}

//Set different start positions for players for a level (rather than (10, 10),
//see single.js).
//There should NOT be more than one instance of StartPositions per level.
export class StartPositions {
    //positions: Array of {x, y} positions that the players start at, the first
    // player starts at the the first position, etc.
    // If there are not enough positions the remaining players are placed at an
    // offset of 10px (x-axis, RHS) from the last player put at that position.
    constructor(positions) {
        if(!positions || positions.length==0)
            throw new TypeError("Some positions required.");
        positions.forEach(function(p) {
            if(p.x===undefined || p.y===undefined)
                throw new TypeError("Bad position");
        });
        this.positions = positions;
        this.reset();
    }
    reset() {
        this.teleported = false; //Players been teleported to start positions?
    }
    register(collisions, manager) {
        manager.addUpdate(this);
    }
    update(interval, collisions, manager) {
        if(!this.teleported) {
            var players = manager.players;
            var positions = this.positions;
            //Determine the position to place each player at (see constructor).
            var toPositions = [];
            //One position per player.
            for(var i=0; i<players.length && i<positions.length; ++i)
                toPositions.push({x: positions[i].x, y: positions[i].y});
            //Too few positions, so reuse the first position (with an offset).
            if(players.length>positions.length) {
                for(var i=positions.length; i<players.length; ++i) {
                    toPositions.push({
                        x: positions[0].x+players[0].box.width*2*i,
                        y: positions[0].y
                    });
                }
            }
            //Teleport each player to their position and set it as their default
            //teleport position.
            players.forEach(function(p, i) {
                p.teleporter.setDefault(toPositions[i].x, toPositions[i].y);
                p.teleporter.toDefault();
            });
            this.teleported = true;
        }
    }
}
