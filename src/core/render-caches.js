//Copyright Matthew Pottage 2019.
//Bug workaround for SVGs: Firefox crops SVG images slightly (or renders them
//larger than the canvas they are cached to) when they are scale them down, with
//more being cropped the more the image is shrunk.  Setting the image's
//"original size" to something smaller that it will be scaled to resolves this.
export class StretchImageCache {
    constructor(imgName, width, height) {
        this.name = imgName;
        this.width = width;
        this.height = height;
        this.scale = 0;
        this.canvas = null;
    }
    gotTextures(textures) {
        return textures.has(this.name);
    }
    refreshCache(scale, textures) {
        if(this.scale==scale
            || !this.gotTextures(textures))
            return;
        if(!this.canvas)
            this.canvas = document.createElement('canvas');
        this.scale = scale;
        this.canvas.width = Math.ceil(this.width*scale);
        this.canvas.height = Math.ceil(this.height*scale);
        var img = textures.get(this.name);
        var ctx = this.canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
    }
    draw(context, box) {
        if(this.canvas)
            context.drawImage(this.canvas, box.x, box.y, box.width, box.height);
    }
}

export class ZoomImageCache {
    constructor(imgName, width, height) {
        this.name = imgName;
        this.width = width;
        this.height = height;
        this.scale = 0;
        this.canvas = null;
    }
    gotTextures(textures) {
        return textures.has(this.name);
    }
    refreshCache(scale, textures) {
        if(this.scale==scale
            || !this.gotTextures(textures))
            return;
        if(!this.canvas)
            this.canvas = document.createElement('canvas');
        this.scale = scale;
        this.canvas.width = Math.ceil(this.width*scale);
        this.canvas.height = Math.ceil(this.height*scale);
        var img = textures.get(this.name);
        var dimScale = Math.min(this.height/img.height, this.width/img.width);
        var effectiveWidth = img.width*dimScale*scale;
        var effectiveHeight = img.height*dimScale*scale;
        var ctx = this.canvas.getContext('2d');
        ctx.drawImage(img, (this.canvas.width-effectiveWidth)/2, (this.canvas.height-effectiveHeight)/2,
            effectiveWidth, effectiveHeight);
    }
    draw(context, box) {
        if(this.canvas)
            context.drawImage(this.canvas, box.x, box.y, box.width, box.height);
    }
}
