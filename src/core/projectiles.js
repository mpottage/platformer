//Copyright Matthew Pottage 2019.
import {Player} from './player/player.js';
import {ForwardHealthComponent,
    ForwardSpecialsComponent} from './player/forward-components.js';
import {FreeFallMotion, ReboundFreeFall} from './motion.js';
import {neededTextures, getClosest, Box, Triangle, getAngle, absValue, fillCircle} from './utility.js';
import {Collidable, registerCDU} from './collisions/collidable.js';
import {isMinion} from './minions.js';
import {ActiveScanner} from './scanner.js';
import {StretchImageCache} from './render-caches.js';
import {Platform} from './platforms.js';

export function playerSelector(obj) {
    return (obj instanceof Player) || isMinion(obj);
}

export function isProjectile(obj) {
    return (obj instanceof AirborneStone) || (obj instanceof Missile)
        || (obj instanceof PinkBullet) || (obj instanceof OrangeBullet)
        || (obj instanceof PurpleBullet) || (obj instanceof PurpleMine)
        || (obj instanceof TurquoiseBullet) || (obj instanceof BlueBullet)
        || (obj instanceof BrownBullet) || (obj instanceof MiniBrownBullet)
        || (obj instanceof YellowBullet) || (obj instanceof NinjaBullet);
}

export function isDamagingProjectile(obj) {
    return (((obj instanceof AirborneStone) && !obj.nonDamaging) ||
        (!(obj instanceof AirborneStone) && isProjectile(obj)))
        && !(obj instanceof BrownBullet);
}

//A missile (visual is pink rotating triangle). Causes 1 health loss on impact,
//passes through all walls and only interacts with objects specified by
//"selector" function (by default just the player) and other projectiles.
export class PinkBullet extends Collidable {
    constructor(x, y, vx, vy, selector=playerSelector) {
        super();
        this.reset();
        this.box = new Box(x, y, 28, 28);
        //Used to draw rotating triangle for visual.
        this.drawTriangle = new Triangle(-14, 12, 0, -12, 14, 12);
        this.pushRank = 22;
        this.motion.x = vx;
        this.motion.y = vy;
        this.selector = selector;
    }
    reset() {
        this.rotation = 0;
        this.opacity = 1;
        this.motion = {x: 0, y: 0};
        this.timeLeft = 20;
        this.disabled = false;
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addOverlay(this);
        manager.addUpdate(this);
    }
    disable() {
        this.disabled = true;
        this.opacity = 0;
    }
    respawn(x, y, vx, vy) {
        this.reset();
        this.box.x = x;
        this.box.y = y;
        this.motion.x = vx;
        this.motion.y = vy;
    }
    overlayDraw(context) {
        if(this.opacity>0) {
            context.save();
            context.globalAlpha *= this.opacity;
            //Rotate this.drawTriangle about the (0, 0) by this.rotation and
            //render it in the center of this.box.
            context.translate(this.box.x+this.box.width/2,
                    this.box.y+this.box.height/2);
            context.rotate(this.rotation);
            this.drawTriangle.drawPath(context);
            context.fillStyle = "#fca9cf";
            context.fill();
            context.restore();
        }
    }
    isCollision(obj) {
        return this.selector(obj) || isProjectile(obj);
    }
    handleCollision(obj) {
        if(!this.disabled) {
            if(obj.health)
                obj.health.increase(-1);
            this.disabled = true;
        }
    }
    update(interval, collisions, manager) {
        if(!this.disabled) {
            collisions.queue(this, this.motion.x*interval,
                    this.motion.y*interval);
            this.rotation += Math.PI*4*interval;
            while(this.rotation>Math.PI*2)
                this.rotation -= Math.PI*2;
            this.timeLeft -= interval;
            if(this.timeLeft<=0)
                this.disabled = true;
        }
        else if(this.opacity>0)
            this.opacity -= interval*20/3; //Fade within 0.15s.
    }
}


//A stone intended for throwing/launching.
// > Causes objects with health to lose 1 if it is moving fast enough.
// > Collects health, coins, and specials for the thrower if it can use them.
export class AirborneStone extends Collidable {
    constructor(x, y, xVelocity, yVelocity, thrownBy, color="grey") {
        super();
        this.motion = new ReboundFreeFall(.1);
        this.motion.x  = xVelocity;
        this.motion.y  = yVelocity;
        this.box = new Box(x, y, 12, 12);
        this.color = color;
        this.setThrownBy(thrownBy);
        this.nonDamaging = false;
        this.disabled = false; //is disabled if it causes an object to lose a life.
        this.pushRank = 20;
    }
    //Change the identity of the thrower, if null, then no thrower.
    setThrownBy(newThrower) {
        //The object that threw the stone (if any).
        this.thrownBy = newThrower || null;
        this.increaseCoins = null;
        this.health = null;
        this.specials = null;
        //Collect coins, health and specials for the thrower if can use them.
        if(this.thrownBy) {
            if(this.thrownBy.increaseCoins)
                this.increaseCoins = (function(value) {
                    if(value>0)
                        this.thrownBy.increaseCoins(value);
                }).bind(this);
            if(this.thrownBy.health)
                this.health = new ForwardHealthComponent(this.thrownBy);
            //Forward specials hit.
            if(this.thrownBy.specials)
                this.specials = new ForwardSpecialsComponent(this.thrownBy);
        }
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.color;
            context.fillRect(this.box.x,this.box.y,this.box.width,this.box.height);
        }
    }
    update(interval, collisions, manager, caches, environment) {
        if(this.disabled)
            return;
        //Stop the stone from causing any more damage if it has slowed and is on the
        //ground.
        if(this.motion.touching.down && Math.abs(this.motion.x)<AirborneStone.safeSpeed
                && Math.abs(this.motion.y)<AirborneStone.safeSpeed) {
            this.nonDamaging = true;
        }
        this.motion.update(this, interval, collisions, environment);
    }
    isCollision(obj) {
        //Ignore a particular object and any other objects with health/stones when
        //moving too slowly.
        return !(this.nonDamaging && (obj.health || isProjectile(obj)));
    }
    handleCollision(obj, details) {
        if(!this.disabled) {
            //Causes an object to lose health.
            if(obj.health && !(obj instanceof AirborneStone)) {
                obj.health.increase(-1);
                this.disabled = true; //Only causes 1 health loss.
            }
            else if(isProjectile(obj) && !(obj instanceof AirborneStone)
                && obj.pushRank<=this.pushRank)
                this.disabled = true;
            //Normal reaction only when it is still enabled.
            else
                this.motion.handleCollision(this, obj, details);
        }
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
    }
    //Do almost nothing (stones being rethrown is undesirable).
    reset() {
        this.nonDamaging = false;
        this.disabled = false;
        this.motion.reset();
    }
}
AirborneStone.safeSpeed = 50;

//Rotating turqoise square projectile. Ignores gravity and air currents.
//Same conditions for hurting objects as OrangeBullet.
export class TurquoiseBullet extends Collidable {
    constructor(x, y, vx, vy, selector=playerSelector) {
        super();
        this.box = new Box(x, y, 18, 18);
        this.selector = selector;
        this.pushRank = 20;
        this.reset();
        this.motion.x = vx;
        this.motion.y = vy;
        this.bodyColor = "#69edbf";
    }
    reset() {
        this.disabled = false;
        this.motion = {x: 0, y: 0};
        this.timeLeft = 20;
        this.opacity = 1;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    disable() {
        this.disabled = true;
    }
    //Position at (x, y), velocity (vx, vy).
    respawn(x, y, vx, vy) {
        this.reset();
        this.box.x = x;
        this.box.y = y;
        this.motion.x = vx;
        this.motion.y = vy;
    }
    isCollision(obj) {
        return (obj instanceof Platform) || isProjectile(obj)
            || this.selector(obj);
    }
    handleCollision(obj) {
        if(!this.disabled) {
            if(obj.health)
                obj.health.increase(-1);
            this.disabled = true;
        }
    }
    update(interval, collisions, manager, caches) {
        if(!this.disabled) {
            collisions.queue(this, this.motion.x*interval, this.motion.y*interval);
            this.timeLeft -= interval;
            if(this.timeLeft<=0)
                this.disabled = true;
        }
        else if(this.opacity>0)
            this.opacity -= 3*interval;
    }
    draw(context) {
        if(this.opacity>0) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.opacity;
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
    }
}

neededTextures.push("NinjaBullet.svg");
//Rotating warped triangle missile.
//Only harms objects it collides with, platforms, other projectiles and objects
//for which the "selector" function returns true (defaults to only players).
// > Has a 50% chance of inflicting of 1 damage on another object.
// > Slows down all objects with a setSpeedMod function in the motion component by 20%
// (so not GreenSquares, GreySquares or any BadTriangle).
// > Due to it's speed it will cause signification knock back.
export class NinjaBullet extends Collidable {
    constructor(x, y, vx, vy, selector=(obj => obj instanceof Player)) {
        super();
        this.box = new Box(x, y, 40, 40);
        this.drawBox = new Box(-this.box.width/2, -this.box.height/2,
            this.box.width, this.box.height);
        this.pushRank = 9;
        this.selector = selector;
        this.motion = new FreeFallMotion();
        this.reset();
        this.motion.x = vx;
        this.motion.y = vy;
        this.multiplier = 0.7;
    }
    reset() {
        this.rotation = 0;
        this.motion.reset();
        this.disabled = false;
        this.timeLeft = 20;
        this.applied = {obj: null, timeLeft: 7, enabled: false};
        //Used to made the bullet fade and carry on moving after it hits
        //something without doing any extra damage.
        this.opacity = 1;
        this.madeContact = false;
    }
    isCollision(obj) {
        return !(this.madeContact) && ((obj instanceof Platform)
            || isProjectile(obj) || this.selector(obj));
    }
    handleCollision(obj, details) {
        if(!this.madeContact) {
            if(obj.health) {
                if(Math.random()>=.5)
                    obj.health.increase(-1);
                if(obj.motion && obj.motion.setSpeedMod) {
                    this.applied.obj = obj;
                    obj.motion.setSpeedMod(this.multiplier, this.multiplier);
                    this.applied.enabled = true;
                }
            }
            this.madeContact = true;
            this.motion.handleCollision(this, obj, details);
        }
    }
    //Switch off early.
    disable() {
        this.disabled = true;
    }
    //Position at (x, y), velocity (vx, vy).
    rethrow(x, y, vx, vy) {
        this.reset();
        this.box.x = x;
        this.box.y = y;
        this.motion.x = vx;
        this.motion.y = vy;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
        if(!caches.get("NinjaBullet"))
            caches.set("NinjaBullet", new StretchImageCache("NinjaBullet.svg", this.box.width,
                        this.box.height));
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            this.motion.accel.y = -4*environment.get("gravity")/5;
            this.motion.update(this, interval, collisions, environment);
            //Rotating square visual.
            this.rotation += interval*Math.PI*2.5;
            while(this.rotation>Math.PI*2)
                this.rotation -= Math.PI*2;
            //Limit existence.
            this.timeLeft -= interval;
            if(this.timeLeft<=0)
                this.madeContact = true;
        }
        else if(this.applied.enabled) {
            this.applied.timeLeft -= interval;
            if(this.applied.timeLeft<=0) {
                this.applied.obj.motion.setSpeedMod(1/this.multiplier, 1/this.multiplier);
                this.applied.enabled = false;
            }
        }
        if(this.madeContact) {
            this.opacity -= 5*interval;
            if(this.opacity<=0)
                this.disabled = true;
        }
    }
    draw(context, caches) {
        if(!this.disabled) {
            //Rotated square (applying rotation updated in update).
            context.save();
            context.globalAlpha *= this.opacity;
            var center = this.box.center;
            context.translate(center.x, center.y);
            context.rotate(this.rotation);
            caches.get("NinjaBullet").draw(context, this.drawBox);
            context.restore();
        }
    }
}

export const orangeBulletColor = "#ffa500";
export const orangeTransitionColor = "#ffd900";
//Rotating orange square missile. Doesn't respond to gravity or air currents.
//Only harms objects it collides with: platforms, other projectiles and objects
//for which the "selector" function returns true (defaults to only players).
export class OrangeBullet extends Collidable {
    constructor(x, y, vx, vy, selector=playerSelector) {
        super();
        this.reset();
        this.box = new Box(x, y, 25, 25);
        var drawWidth = Math.sqrt(this.box.width*this.box.width/2);
        this.drawBox = new Box(-drawWidth/2, -drawWidth/2, drawWidth, drawWidth);
        this.pushRank = 20;
        this.selector = selector;
        this.motion.x = vx;
        this.motion.y = vy;
    }
    reset() {
        this.rotation = 0;
        this.motion = {x: 0, y: 0};
        this.disabled = false;
        this.exploded = false;
        this.explosionRadius = 0;
        this.timeLeft = 20;
    }
    isCollision(obj) {
        return (obj instanceof Platform) || isProjectile(obj)
            || this.selector(obj);
    }
    handleCollision(obj) {
        if(!this.disabled) {
            if(obj.health)
                obj.health.increase(-1);
            this.disabled = true;
            this.exploded = true;
        }
    }
    //Switch off early.
    disable() {
        this.disabled = true;
        this.exploded = false;
    }
    //Position at (x, y), velocity (vx, vy).
    respawn(x, y, vx, vy) {
        this.reset();
        this.box.x = x;
        this.box.y = y;
        this.motion.x = vx;
        this.motion.y = vy;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    update(interval, collisions) {
        if(!this.disabled) {
            collisions.queue(this, this.motion.x*interval, this.motion.y*interval);
            //Rotating square visual.
            this.rotation += interval*Math.PI;
            while(this.rotation>Math.PI*2)
                this.rotation -= Math.PI*2;
            //Limit existence.
            this.timeLeft -= interval;
            if(this.timeLeft<=0)
                this.disabled = true;
        }
        //Expanding circle on impact visual.
        else if(this.exploded)
            this.explosionRadius += interval*150;
    }
    draw(context) {
        if(!this.disabled) {
            //Rotated square (applying rotation updated in update).
            context.save();
            var center = this.box.center;
            context.translate(center.x, center.y);
            context.rotate(this.rotation);
            context.fillStyle = orangeBulletColor;
            this.drawBox.fill(context);
            context.restore();
        }
        //Expanding and fading circle around the last position of the missile.
        else if(this.exploded && this.explosionRadius<60) {
            context.lineWidth = 7;
            context.strokeStyle = orangeBulletColor;
            var prevGA = context.globalAlpha;
            context.globalAlpha *= (60-this.explosionRadius)/60;
            context.beginPath();
            var center = this.box.center;
            context.arc(center.x, center.y, this.explosionRadius, 0, Math.PI*2);
            context.stroke();
            context.globalAlpha = prevGA;
        }
    }
}

neededTextures.push("Missile.svg");
//A missile targetting objects, satisfying a specific condition. When it hits an
//object with health the object loses 2. It lasts for 20s before disintegrating.
//An appropriate object is selected for it to target (normally the closest).
export class Missile extends Collidable {
    //(x, y): Initial position. (vx, vy): Initial velocity vector.
    //selector: Function (returns true/false) to test for targetting an object.
    //ignore: Optional object to ignore and not collide with.
    constructor(x, y, vx, vy, selector, ignore) {
        super();
        this.box = new Box(x, y, 20, 20);
        var range = 2000;
        this.scanner = new ActiveScanner(new Box(x-range, y-range, 2*range, 2*range),
                selector);
        this.motion = {x: vx, y: vy};
        this.selector = selector;
        this.ignore = ignore;
        this.pushRank = 9;
        this.reset();
    }
    reset() {
        this.disabled = false;
        this.target = null;
        this.timeout = 20;
        this.retargetTime = 0;
        //Delay a frame before interpreting Scanner (avoids giving objects other
        //than BadTriangles (i.e. move using collisions engine) preference when
        //a missile registered during play).
        this.retargetIgnore = true;
        this.scanner.reset();
    }
    isCollision(obj) {
        return obj!==this.ignore;
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addDraw(this);
        manager.addUpdate(this);
        this.scanner.register(collisions, manager);
        if(!caches.get("Missile.svg"))
            caches.set("Missile.svg", new StretchImageCache("Missile.svg", 40, 20));
    }
    draw(context, caches) {
        if(!this.disabled) {
            //Rotate graphic so that the tip points in the direction that the
            //missile is moving.
            context.save();
            var center = this.box.center;
            context.translate(center.x, center.y);
            context.rotate(getAngle(this.motion.x, this.motion.y) || 5*Math.PI/2);
            caches.get("Missile.svg").draw(context, new Box(-10, -10, 40, 20));
            context.restore();
        }
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            this.scanner.recenter(this.box.x, this.box.y);
            this.timeout -= interval;
            this.updateTarget(interval, collisions);
            this.updateMotion(interval, collisions, environment);
            if(this.timeout<=0)
                this.disable();
        }
    }
    updateTarget(interval, collisions) {
        this.retargetTime -= interval;
        //Rescans for a new target every 1s.
        if(!this.retargetIgnore && this.retargetTime<=0) {
            var seen = this.scanner.seen;
            var firstClosest = getClosest(this.box, seen);
            var closest = firstClosest;
            this.target = null;
            //Find the closest object in range for which there is an
            //unobstructed route to.
            while(!this.target && closest) {
                //Scan route and filter out non-collisions.
                var trace = collisions.rayTrace(this.box.x+this.box.width/2,
                        this.box.y+this.box.height/2,
                        closest.box.x+closest.box.width/2,
                        closest.box.y+closest.box.height/2).filter(function(o) {
                    return !isProjectile(o) && o.isCollision(this) && this.isCollision(o)
                        && !(this.selector(o));
                }, this);
                //Some collisions, so get the next closest.
                if(trace.length>0) {
                    seen = seen.filter(function(o) { return o!=closest });
                    var newClosest = getClosest(this.box, seen);
                    //No object found to be in a clear line of sight, so use the
                    //closest object scanned.
                    if(!newClosest)
                        this.target = firstClosest;
                    //Continue scanning (another one to test).
                    else
                        closest = newClosest;
                }
                //Found a clear route, set target and retarget delay.
                else {
                    this.target = closest;
                    this.retargetTime = 1;
                }
            }
        }
        this.retargetIgnore = false;
    }
    updateMotion(interval, collisions, environment) {
        var prevMotion = {x: this.motion.x, y: this.motion.y};
        if(this.target) {
            //Accelerates to ideal velocity to hit the target.
            var speed = 450;
            //To hit target in 1s.
            var ideal = {x: this.target.box.x+this.target.box.width/2-this.box.x,
                y: this.target.box.y+this.target.box.height/2-this.box.y};
            //Scale to have desired speed.
            var scale = speed/absValue(ideal.x, ideal.y);
            ideal.x *= scale;
            ideal.y *= scale;
            var accel = speed; //Accelerates to normal speed in 1s from 0.
            if(this.motion.x<ideal.x) {
                this.motion.x += accel*interval;
                if(this.motion.x>ideal.x)
                    this.motion.x = ideal.x;
            }
            else if(this.motion.x>ideal.x) {
                this.motion.x -= accel*interval;
                if(this.motion.x<ideal.x)
                    this.motion.x = ideal.x;
            }
            if(this.motion.y<ideal.y) {
                this.motion.y += accel*interval;
                if(this.motion.y>ideal.y)
                    this.motion.y = ideal.y;
            }
            else if(this.motion.y>ideal.y) {
                this.motion.y -= accel*interval;
                if(this.motion.y<ideal.y)
                    this.motion.y = ideal.y;
            }
        }
        else {
            //Fall under gravity.
            this.motion.y += interval*environment.get("gravity");
        }
        collisions.queue(this, (this.motion.x+prevMotion.x)*interval/2,
                (this.motion.y+prevMotion.y)*interval/2);
    }
    handleCollision(obj, details) {
        if(!this.disabled) {
            if(obj.health)
                obj.health.increase(-2);
            //Switch off if this hits something.
            this.disable();
        }
    }
    disable() {
        this.disabled = true;
        this.scanner.disable();
    }
}

//An unusual type of projectile.
//This causes no damage until it is "frozen" (by method call) in position. Then
//it causes health loss to the first object with health to hit/intersect it.
export class PurpleBullet extends Collidable {
    constructor(x, y, vx, vy, ignore, color="purple") {
        super();
        this.motion = new ReboundFreeFall(.1);
        this.motion.x = vx;
        this.motion.y = vy;
        this.box = new Box(x, y, 12, 12);
        this.color = color;
        this.ignore = ignore;
        this.pushRank = 20;

        this.canHarm = true;
        this.frozen = false;
        this.timeLeft = 6;
        this.disabled = false;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    isCollision(obj) {
        return this.canHarm && obj!==this.ignore
            && (this.frozen || (obj instanceof Platform));
    }
    _hardHit(obj) {
        if(!this.disabled) {
            if(obj.health)
                obj.health.increase(-1);
            this.disabled = true; //Used up
        }
    }
    handleCollision(obj, details) {
        if(this.frozen)
            this._hardHit(obj);
        else {
            //A fixed object.
            if(!obj.motion) {
                //Freeze early, but prevent any health loss to objects.
                this.frozen = true;
                this.canHarm = false;
            }
            //Bounce off normally otherwise.
            else
                this.motion.handleCollision(this, obj, details);
        }
    }
    handleIntersection(obj, details) {
        //Handle BadTriangles (ie. Objects that don't move normally).
        if(this.frozen && obj.isCollision(this) && this.isCollision(obj)
            && !(obj instanceof PurpleBullet))
            this._hardHit(obj);
    }
    handleMoved(details, amount) {
        if(!this.frozen)
            this.motion.handleMoved(details, amount);
    }
    //Lock this object in position so it can cause damage (if it hasn't already
    //stopped).
    freeze() {
        this.frozen = true;
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            if(this.frozen) {
                //Limited amount of time when frozen.
                this.timeLeft -= interval;
                if(this.timeLeft<=0)
                    this.disabled = true;
                //Stop harming in last second.
                else if(this.timeLeft<1)
                    this.canHarm = false;
                else if(this.canHarm) //Detect hitting a BadTriangle.
                    collisions.queue(this, 0, 0);
            }
            else
                this.motion.update(this, interval, collisions, environment);
        }
    }
    draw(context, caches) {
        if(!this.disabled) {
            var prevGA = context.globalAlpha;
            //The bullet is faded when it cannot harm any objects.
            if(this.timeLeft<1) //Fade away
                context.globalAlpha *= this.timeLeft;
            if(!this.frozen || !this.canHarm) //Slightly faded
                context.globalAlpha *= .5;
            context.fillStyle = this.color;
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
    }
}

//Very bouncy projectile.
//  > Only harms objects satifying selector.
//  > Disables after 4 collisions.
//  > Times out after 20s
export class YellowBullet extends Collidable {
    constructor(x, y, vx, vy, selector=playerSelector) {
        super();
        this.box = new Box(x, y, 15, 15);
        this.color = "#E7BF6F";
        this.motion = new ReboundFreeFall(.9, .8, 1);
        this.reset();
        this.selector = selector;
        this.motion.x  = vx;
        this.motion.y  = vy;
        this.pushRank = 20;
    }
    reset() {
        this.opacity = 1;
        this.timeLeft = 20; //How long until this explodes
        this.hitTimes = 0;
        this.disabled = false;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    isCollision(obj) {
        return (obj instanceof Platform) || isProjectile(obj)
            || this.selector(obj);
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            this.motion.accel.y = -environment.get("gravity")/4;
            this.timeLeft -= interval;
            if(this.timeLeft<=0 || this.hitTimes>4)
                this.disabled = true;
            else
                this.motion.update(this, interval, collisions, environment);
        }
        else if(this.opacity>0) {
            this.opacity = Math.max(0, this.opacity-4*interval);
        }
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
        ++this.hitTimes;
        if(!this.disabled && obj.health) {
            this.disabled = true;
            obj.health.increase(-1);
        }
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    draw(context) {
        if(this.opacity>0) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.opacity;
            context.fillStyle = this.color;
            fillCircle(context, this.box);
            context.globalAlpha = prevGA;
        }
    }
}

//A very bouncy large projectile.
//  > 1.5s after impacting anything it explodes (or immediately for
//  impacting damaging projectiles).
//  > The explosion releases 9 small projectiles, that are projected radially
//  and fade shortly afterwards (but cause damage to anything they hit).
//  > Effect of gravity is limited.
export class BrownBullet extends Collidable {
    constructor(x, y, vx, vy) {
        super();
        this.box = new Box(x, y, 36, 36);
        this.color = "#784421";
        this.motion = new ReboundFreeFall(.9);
        this.reset();
        this.motion.x  = vx;
        this.motion.y  = vy;
        this.pushRank = 20;
        this.explosionDelay = 1.4;
    }
    reset() {
        this.timeLeft = 20; //How long until this explodes
        this.disabled = false;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            this.motion.accel.y = -environment.get("gravity")/4;
            this.timeLeft -= interval;
            if(this.timeLeft<=0) {
                this.disabled = true;
                //Release little projectiles sweeping a circle.
                var speed = 800; //Relative speed of each projectile
                var center = this.box.center;
                var bullets = [];
                for(var step=0; step<2*Math.PI; step+=Math.PI*2/9) {
                    var bullet = new MiniBrownBullet(center.x-6, center.y-6,
                        this.motion.x+speed*Math.cos(step),
                        this.motion.y+speed*Math.sin(step));
                    bullets.push(bullet);
                }
                //Ensure that any intersecting enemies get hurt.
                bullets.forEach(b => collisions.unprotectedImpact(b, [this]));
                bullets.forEach(b => b.register(collisions, manager, caches));
            }
            else {
                this.motion.update(this, interval, collisions, environment);
            }
        }
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
        if(this.timeLeft>this.explosionDelay) //Bring forward explosion time
            this.timeLeft = this.explosionDelay;
        if(isDamagingProjectile(obj)) //Explode now (damaging projectile)
            this.timeLeft = 0;
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.color;
            this.box.fill(context);
        }
    }
}
//Used by BrownBullet.
//  > Unaffected by gravity.
//  > Fades after 0.6s, causing damage to any objects hit in that time.
class MiniBrownBullet extends Collidable {
    constructor(x, y, vx, vy) {
        super();
        this.box = new Box(x, y, 12, 12);
        this.motion = {x: vx, y: vy};
        this.timeLeft = .6;
        this.opacity = 1;
        this.initial = this.timeLeft;
        this.color = "#784421";
        this.pushRank = 20;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    handleCollision(obj) {
        if(!this.disabled) {
            if(obj.health)
                obj.health.increase(-1);
            this.disabled = true;
        }
    }
    update(interval, collisions, manager, caches) {
        if(!this.disabled) {
            collisions.queue(this, this.motion.x*interval,
                this.motion.y*interval);
            this.timeLeft -= interval;
            if(this.timeLeft<=0)
                this.disabled = true;
        }
        if(this.opacity>0)
            this.opacity -= 1/this.initial*interval;
    }
    draw(context) {
        if(this.opacity>0) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.opacity;
            context.fillStyle = this.color;
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
    }
}

//Delayed impact bullet, like a timed sticky grenade.
// > "Explodes" 5s after impact (or disintegrates if nothing to hurt).
// > Ignores gravity.
export class BlueBullet extends Collidable {
    constructor(x, y, vx, vy, selector) {
        super();
        this.box = new Box(x, y, 18, 18);
        this.selector = selector;
        this.pushRank = 20;
        this.reset();
        this.motion.x = vx;
        this.motion.y = vy;
        this.bodyColor = "#40accc";
    }
    reset() {
        this.disabled = false;
        this.motion = {x: 0, y: 0};
        this.timeLeft = 20;
        this.opacity = 1; //Fades after damage done.
        this.untilDamage = 5;
        this.toDamage = null;
        //Offset from object it will damage.
        this.dx = this.dy = 0;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    isCollision(obj) {
        return (obj instanceof Platform) || isProjectile(obj)
            || this.selector(obj);
    }
    handleCollision(obj) {
        if(!this.disabled) {
            if(!isProjectile(obj) && obj.health) {
                //Ensure that the difference makes this bullet touch obj.
                this.dx = Math.max(Math.min(obj.box.width, this.box.x-obj.box.x), -this.box.width);
                this.dy = Math.max(Math.min(obj.box.height, this.box.y-obj.box.y), -this.box.height);
                this.toDamage = obj;
                this.opacity = .5;
            }
            else {
                //Fade immediately, a "hard" target.
                this.untilDamage = 0;
            }
            this.disabled = true;
        }
    }
    update(interval, collisions, manager, caches) {
        if(!this.disabled) {
            collisions.queue(this, this.motion.x*interval, this.motion.y*interval);
            this.timeLeft -= interval;
            if(this.timeLeft<=0)
                this.disabled = true;
        }
        //Stuck to an object
        else if(this.untilDamage>0 && this.toDamage) {
            this.untilDamage -= interval;
            if(this.untilDamage<=0)
                this.toDamage.health.increase(-1);
            if(this.toDamage.disabled)
                this.untilDamage = 0;
        }
        //Used up.
        else if(this.opacity>0)
            this.opacity -= 3*interval;
    }
    draw(context) {
        if(this.opacity>0) {
            //Have this follow the object it's about to damage.
            if(this.toDamage) {
                this.box.x = this.toDamage.box.x+this.dx;
                this.box.y = this.toDamage.box.y+this.dy;
            }
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.opacity;
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
    }
}

//A fixed purple square that causes damage to some objects and times out after
//5s (fading for a further 1s).
export class PurpleMine extends Collidable {
    constructor(x, y, selector=playerSelector) {
        super();
        this.box = new Box(x, y, 12, 12);
        this.color = "purple";
        this.selector = selector;
        this.pushRank = 20;
        this.reset();
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    reset() {
        this.timeLeft = 6;
        this.disabled = false;
    }
    disable() {
        this.disabled = true;
    }
    respawn(x, y) {
        this.reset();
        this.box.x = x;
        this.box.y = y;
    }
    isCollision(obj) {
        return this.timeLeft>1 &&
            ((obj instanceof Platform) || isProjectile(obj)
                || this.selector(obj));
    }
    handleCollision(obj) {
        if(!this.disabled) {
            if(this.timeLeft>1) {
                if(obj.health)
                    obj.health.increase(-1);
            }
            this.disabled = true;
        }
    }
    update(interval, collisions, manager, caches) {
        this.timeLeft -= interval;
        if(this.timeLeft<0)
            this.disabled = true;
    }
    draw(context, caches) {
        if(!this.disabled) {
            var prevGA = context.globalAlpha;
            if(this.timeLeft<1) //Fade away
                context.globalAlpha *= this.timeLeft;
            context.fillStyle = this.color;
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
    }
}
