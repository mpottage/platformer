## Button system
This lets the Player (or similar) press buttons, hit regions of the level and
then see a reaction based on what they did, e.g. platform starts moving or stone
launchers switch off.

If a button is toggled on (e.g. Player stands on it) it sets a flag in
Manager with a specified name to true (false if the button is toggled off).

ApplyFlag takes the value of a named flag (in Manager) and an object. If the
flag is true it activates the object, otherwise it deactivates it
(via obj.activated).

Toggles don't need to come from visible objects, psuedo game objects, such as
OrFlags or NegateFlag can create a new named flag that depends on the
value of other flags. This permits, e.g. a platform moves only if no
buttons are pressed.

The psuedo objects generally don't have an update function and only change
their flag value in response to the callback watchedChanged.

Typically this.name is the name of the associated flag the object sets.
