//Copyright Matthew Pottage 2019.
//Applies "or" operation to a collection of flags and sets the result in a new
//flag.
export class OrFlags {
    constructor(resultName, flagNames) {
        this.flagNames = flagNames;
        this.name = resultName;
    }
    register(collisions, manager) {
        this.flagNames.forEach(function(n) {
            manager.addWatcher(n, this);
        }, this);
    }
    watchedChanged(manager) {
        //Apply "or" operation.
        var res = false;
        this.flagNames.forEach(function(n) {
            res = res || !!manager.getFlag(n);
        });
        manager.setFlag(this.name, res);
    }
    reset() {};
}

export class AndFlags {
    constructor(resultName, flagNames) {
        this.flagNames = flagNames;
        this.name = resultName;
    }
    register(collisions, manager) {
        this.flagNames.forEach(function(n) {
            manager.addWatcher(n, this);
        }, this);
    }
    watchedChanged(manager) {
        //Apply "and" operation.
        var res = true;
        this.flagNames.forEach(function(n) {
            res = res && !!manager.getFlag(n);
        });
        manager.setFlag(this.name, res);
    }
    reset() {};
}

//Is true if all its inputs are true or an input is still true since all inputs
//were true. Otherwise false.
export class HoldAnd {
    constructor(name, flagNames) {
        this.flagNames = flagNames;
        this.name = name;
        this.reset();
    }
    register(collisions, manager) {
        this.flagNames.forEach(function(n) {
            manager.addWatcher(n, this);
        }, this);
    }
    watchedChanged(manager) {
        //Apply "and" operation.
        var and = true;
        var or = false;
        this.flagNames.forEach(function(n) {
            var value = !!manager.getFlag(n);
            and = and && value;
            or = or || value;
        });
        if(and) //All true
            this.wasOn = true;
        else if(!or) //None true
            this.wasOn = false;
        manager.setFlag(this.name, this.wasOn);
    }
    reset() {
        //On last time? (then all inputs must have been true before).
        this.wasOn = false;
    }
}

//New flag with the same value as another flag, useful for isolating components.
export class RelayFlag {
    //relayName: Name of new flag
    //fromName: Original flag to relay
    constructor(relayName, fromName) {
        this.name = relayName;
        this.tracking = fromName;
    }
    register(collisions, manager) {
        manager.addWatcher(this.tracking, this);
    }
    watchedChanged(manager) {
        manager.setFlag(this.name, manager.getFlag(this.tracking));
    }
    reset() {}
}

//Creates a new flag with the negated value of another flag.
export class NegateFlag {
    //negatedName: Name of new flag.
    //toNegateName: Name of flag to negate.
    constructor(negatedName, toNegateName) {
        this.name = negatedName;
        this.tracking = toNegateName;
    }
    register(collisions, manager) {
        manager.addWatcher(this.tracking, this);
    }
    watchedChanged(manager) {
        manager.setFlag(this.name, !(manager.getFlag(this.tracking)));
    }
    reset() {}
}

//Sets its associated flag to true if the flag it monitors is set to true.
//Doesn't set its flag to false.
export class FlagBeenTrue {
    constructor(name, flagName) {
        this.name = name;
        this.tracking = flagName;
        this.prevValue = false;
    }
    register(collisions, manager) {
        manager.addWatcher(this.tracking, this);
    }
    watchedChanged(manager) {
        var val = manager.getFlag(this.tracking);
        if(val || val==this.prevValue) {
            manager.setFlag(this.name, val);
            this.prevValue = val;
        }
    }
    reset() {
        this.prevValue = false;
    }
}

//Outputs true if for the last duration seconds its input has been true
//uninterrupted (the true value "times out").
export class Timeout {
    //duration: Number of seconds which inputName must be true for true output.
    constructor(name, inputName, duration) {
        this.name = name;
        this.tracking = inputName;
        this.duration = duration;
        this.reset();
    }
    reset() {
        this.prevVal = false;
        this.timeLeft = this.duration;
    }
    register(collisions, manager) {
        manager.addUpdate(this);
        manager.addWatcher(this.tracking, this);
    }
    watchedChanged(manager) {
        var newVal = manager.getFlag(this.tracking);
        if(newVal && !this.prevVal)
            this.timeLeft = this.duration;
        this.prevVal = newVal;
    }
    update(interval, collisions, manager) {
        this.timeLeft -= interval;
        manager.setFlag(this.name, this.prevVal && this.timeLeft<=0);
    }
}

//Sets its associated flag to "true".
export class AlwaysTrue {
    constructor(name) {
        this.name = name;
    }
    reset() { }
    register(collisions, manager) {
        manager.addUpdate(this);
    }
    update(interval, collisions, manager) {
        manager.setFlag(this.name, true);
    }
}

//Toggles on/off every time it's input transitions from "on" to "off" and "on"
//again.
export class ToggleFlag {
    //toggleName: Name of new flag.
    //toggledBy: Name of flag to negate.
    constructor(toggleName, toggledBy) {
        this.name = toggleName;
        this.tracking = toggledBy;
        this.reset();
    }
    register(collisions, manager) {
        manager.addWatcher(this.tracking, this);
    }
    watchedChanged(manager) {
        if(manager.getFlag(this.tracking)) {
            manager.setFlag(this.name, !(manager.getFlag(this.name)));
        }
        else if(!this.set) {
            manager.setFlag(this.name, false);
            this.set = true;
        }
    }
    reset() {
        this.set = false;
    }
}

//Output is "true" with probability denom/numer, updated after every reset.
export class RandomlyTrue {
    constructor(name, numer=1, denom=2) {
        if(numer>denom)
            throw new Error("Invalid probability");
        this.name  = name;
        this.value = false;
        this.denom = denom;
        this.numer = numer;
        this.reset();
    }
    reset() {
        this.value = (Math.random())<(this.numer/this.denom);
    }
    register(collisions, manager) {
        manager.addUpdate(this);
    }
    update(interval, collisions, manager) {
        manager.setFlag(this.name, this.value);
    }
}
