//Copyright Matthew Pottage 2019.
//Activates an object based on a flag's value.
export class ApplyFlag {
    //flagName: Flag to activate based on.
    //toObj: Object to activate.
    constructor(flagName, toObj) {
        if(toObj.activated===undefined)
            new TypeError("Activatable game object required.");
        this.flag = flagName;
        this.obj = toObj;
        //Defaults to off (avoids unintended activation for a frame).
        this.obj.activated = false;
    }
    register(collisions, manager, caches) {
        manager.addWatcher(this.flag, this);
        this.obj.register(collisions, manager, caches);
    }
    watchedChanged(manager) {
        this.obj.activated = manager.getFlag(this.flag);
    }
    reset() {
        this.obj.reset();
        this.obj.activated = false;
    }
}

//For obj MakeFlagFrom sets the flag flagName to true if either obj.collapsed
//or obj.disabled.
//Used to make CollapsingPlatforms (true if collapsed), BadTriangles (true if
//killed) and Pickups (true if collected) work as controls.
export class MakeFlagFrom {
    constructor(flagName, obj) {
        this.name = flagName;
        this.obj = obj;
    }
    register(collisions, manager, caches) {
        manager.addUpdate(this);
        this.obj.register(collisions, manager, caches);
    }
    reset() {
        this.obj.reset();
    }
    //Either the object is disabled, or it has a function getOutputVal that
    //returns true.
    static isOn(obj) {
        return obj.disabled || (obj.getOutputVal && obj.getOutputVal());
    }
    update(interval, collisions, manager) {
        manager.setFlag(this.name, MakeFlagFrom.isOn(this.obj));
    }
}

//Combination of MakeFlagFrom and ApplyFlag.
export class MakeAndApplyFlag {
    constructor(makeName, applyName, obj) {
        this.flag = applyName;
        this.name = makeName;
        this.obj = obj;
        this.obj.activated = false;
    }
    reset() {
        this.obj.reset();
        this.obj.activated = false;
    }
    register(collisions, manager, caches) {
        this.obj.register(collisions, manager, caches);
        manager.addWatcher(this.flag, this);
        manager.addUpdate(this);
    }
    watchedChanged(manager) {
        this.obj.activated = manager.getFlag(this.flag);
    }
    update(interval, collisions, manager) {
        manager.setFlag(this.name, MakeFlagFrom.isOn(this.obj));
    }
}
