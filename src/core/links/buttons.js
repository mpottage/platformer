//Copyright Matthew Pottage 2019.
import {Platform} from '../platforms.js';
import {Box, to3dp} from '../utility.js';
import {Frame} from '../frame.js';
import {registerCDU} from '../collisions/collidable.js';

//Button which is toggled on when the player stands on it.
export class Button extends Platform {
    constructor(flagName, x, y, color) {
        super(new Box(x, y, 50, 15), color);
        this._setConstraints();
        this.pushRank = 15;
        this.bounceBackAccel = 1000;
        this.motion = {x: 0, y: 0};
        this.startY = y;
        this.toggled = false;
        this.name = flagName;
    }
    reset() {
        super.reset();
        this.box.y = this.startY;
        this.toggled = false;
        this.motion.y = 0;
    }
    _setConstraints() {
        //Prevent the button being pushed out of position.
        var moveHeight = 3*this.box.height/2;
        this.frames = [
            new Frame(new Box(this.box.x, this.box.y+moveHeight,
                        this.box.width, 1), this),
            new Frame(new Box(this.box.x-1, this.box.y, 1, moveHeight),
                    this),
            new Frame(new Box(this.box.x+this.box.width, this.box.y, 1,
                        moveHeight), this),
            new Frame(new Box(this.box.x, this.box.y-1, this.box.width, 1),
                    this)
        ];
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
        this.frames.forEach(function(f) {
            collisions.add(f);
        });
    }
    draw(ctx) {
        //Draws button, cutting off the part that is absorbed into the surface
        //when pushed down.
        ctx.fillStyle = this.color;
        ctx.fillRect(this.box.x, this.box.y, this.box.width,
                this.box.height-this.box.y+this.startY);
    }
    isCollision(collided) {
        //Acts like a jump-through platform, but collides with frames.
        return this.frames.indexOf(collided)!=-1 ||
                to3dp(collided.box.y+collided.box.height)<=to3dp(this.box.y);
    }
    handleMoved(details, amount) {
        if(amount==0 && (details.up || details.down))
            this.motion.y = 0;
    }
    bounceUp(interval, collisions) {
        //Accelerate back up to starting position.
        if(this.box.y!=this.startY) {
            this.motion.y = Math.sqrt(Math.abs(2*this.bounceBackAccel*(this.box.y-this.startY)));
            if(this.box.y>this.startY)
                this.motion.y = -this.motion.y;
            collisions.queue(this, 0, this.motion.y*interval);
        }
        else
            this.motion.y = 0;
    }
    update(interval, collisions, manager) {
        this.bounceUp(interval, collisions);
        this.toggled = Math.abs(this.box.y-this.startY)>this.box.height/3;
        manager.setFlag(this.name, this.toggled);
    }
}

//Like a button except pushed upwards instead of downwards.
export class CeilingButton extends Button {
    constructor(flagName, x, y, color) {
        super(flagName, x, y, color);
        this.bounceBackAccel = -this.bounceBackAccel; //Move opposite direction to Button.
    }
    _setConstraints() {
        //Prevent the button being pushed out of position.
        var moveHeight = 3*this.box.height/2;
        var minY = this.box.y-this.box.height/2;
        this.frames = [
            new Frame(new Box(this.box.x, this.box.y+this.box.height,
                        this.box.width, 1), this),
            new Frame(new Box(this.box.x-1, minY, 1, moveHeight), this),
            new Frame(new Box(this.box.x+this.box.width, minY, 1, moveHeight),
                    this),
            new Frame(new Box(this.box.x, minY, this.box.width, 1), this)
        ];
    }
    isCollision(collided) {
        //Acts like a reverse jump-through platform, but collides with frames.
        return this.frames.indexOf(collided)!=-1 ||
                to3dp(collided.box.y)>=to3dp(this.box.y+this.box.height);
    }
    draw(ctx) {
        //Draws button, cutting off the part that is absorbed into the surface
        //when pushed up.
        ctx.fillStyle = this.color;
        ctx.fillRect(this.box.x, this.startY, this.box.width,
                this.box.height-Math.abs(this.box.y-this.startY));
    }
}

//Like a button, except it toggles "on/off" on each press.
export class ToggleButton extends Button {
    constructor(name, x, y, color) {
        super(name, x, y, color);
        this.wasDown = false; //Detect changing state.
    }
    reset() {
        super.reset();
        this.wasDown = false;
    }
    update(interval, collisions, manager) {
        var nowDown = this.box.y>(this.startY+this.box.height/3);
        if(nowDown && !this.wasDown)
            this.toggled = !this.toggled;
        this.wasDown = nowDown;
        manager.setFlag(this.name, this.toggled);
        this.bounceUp(interval, collisions);
    }
    draw(ctx) {
        super.draw(ctx);
        //Draw a light/line at the base of the button to indicate its state.
        if(this.toggled)
            ctx.fillStyle = "#0adc34";
        else
            ctx.fillStyle = "#ee0b1f";
        ctx.fillRect(this.box.x, this.startY+3*this.box.height/4, this.box.width,
                this.box.height/4);
    }
}

export class WallButton extends Platform {
    constructor(flagName, x, y, color) {
        super(new Box(x, y, 15, 50), color);
        this._setConstraints();
        this.pushRank = 15;
        this.bounceBackAccel = 1000;
        this.motion = {x: 0, y: 0};
        this.startX = x;
        this.toggled = false;
        this.name = flagName;
    }
    reset() {
        super.reset();
        this.box.x = this.startX;
        this.toggled = false;
        this.motion.x = 0;
    }
    _setConstraints() {
        //Prevent the button being pushed out of position.
        var moveWidth = 3*this.box.width/2;
        this.frames = [
            new Frame(new Box(this.box.x+moveWidth, this.box.y, 1,
                this.box.height), this),
            new Frame(new Box(this.box.x, this.box.y-1, moveWidth, 1), this),
            new Frame(new Box(this.box.x, this.box.y+this.box.height, moveWidth,
                1), this),
            new Frame(new Box(this.box.x-1, this.box.y, 1, this.box.height),
                this)
        ];
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
        this.frames.forEach(function(f) {
            collisions.add(f);
        });
    }
    draw(ctx) {
        //Draws button, cutting off the part that is absorbed into the surface
        //when pushed down.
        ctx.fillStyle = this.color;
        ctx.fillRect(this.box.x, this.box.y,
            this.box.width-this.box.x+this.startX, this.box.height);
    }
    isCollision(collided) {
        //Acts like a jump-through platform rotated so the hard surface points
        //left, but collides with frames.
        return this.frames.indexOf(collided)!=-1 ||
                to3dp(collided.box.x+collided.box.width)<=to3dp(this.box.x);
    }
    handleMoved(details, amount) {
        if(amount==0 && (details.left || details.right))
            this.motion.x = 0;
    }
    bounceBack(interval, collisions) {
        //Accelerate back up to starting position.
        if(this.box.x!=this.startX) {
            this.motion.x = Math.sqrt(Math.abs(2*this.bounceBackAccel*(this.box.x-this.startX)));
            if(this.box.x>this.startX)
                this.motion.x = -this.motion.x;
            collisions.queue(this, this.motion.x*interval, 0);
        }
        else
            this.motion.x = 0;
    }
    update(interval, collisions, manager) {
        this.bounceBack(interval, collisions);
        this.toggled = Math.abs(this.box.x-this.startX)>this.box.width/3;
        manager.setFlag(this.name, this.toggled);
    }
}

//Like a wall button except pushed left instead of right.
export class LeftWallButton extends WallButton {
    constructor(flagName, x, y, color) {
        super(flagName, x, y, color);
        this.bounceBackAccel = -this.bounceBackAccel; //Move opposite direction to Button.
    }
    _setConstraints() {
        //Prevent the button being pushed out of position.
        var moveWidth = 3*this.box.width/2;
        var minX = this.box.x-this.box.width/2;
        this.frames = [
            new Frame(new Box(this.box.x+this.box.width, this.box.y,
                        1, this.box.width), this),
            new Frame(new Box(minX, this.box.y-1, moveWidth, 1), this),
            new Frame(new Box(minX, this.box.y+this.box.height, moveWidth, 1),
                    this),
            new Frame(new Box(minX, this.box.y, 1, this.box.height), this)
        ];
    }
    isCollision(collided) {
        //Hard surface on opposite side to WallButton
        return this.frames.indexOf(collided)!=-1 ||
                to3dp(collided.box.x)>=to3dp(this.box.x+this.box.width);
    }
    draw(ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.startX, this.box.y,
            this.box.width-Math.abs(this.box.x-this.startX), this.box.height);
    }
}
