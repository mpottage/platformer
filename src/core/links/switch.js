//Copyright Matthew Pottage 2019.
import {Platform} from '../platforms.js';
import {Frame} from '../frame.js';
import {registerCDU} from '../collisions/collidable.js';
import {Box} from '../utility.js';

export class Switch extends Platform {
    constructor(name, x, y, color, defaultState=false) {
        super(new Box(x, y, 15, 30), color);
        this.name = name;
        this.startX = x;
        this.slideWidth = 30;
        this.pushRank = 30;
        //Prevents objects riding on the top of the switch (like a moving
        //platform) by not using this.motion.
        this.xVelocity = 0;
        this.bounceBackAccel = 500;
        this.defaultOn = defaultState;
        this._setConstraints();
        this._setDefaultState();
    }
    reset() {
        this.box.x = this.startX;
        this._setDefaultState();
        this.xVelocity = 0;
    }
    _setDefaultState() {
        this.toggled = this.defaultOn;
        if(this.toggled)
            this.box.x = this.startX+this.slideWidth-this.box.width;
        else
            this.box.x = this.startX;
    }
    _setConstraints() {
        this.frames = [
            new Frame(new Box(this.startX, this.box.y+this.box.height,
                        this.slideWidth, 1), this),
            new Frame(new Box(this.startX-1, this.box.y, 1, this.box.height),
                    this),
            new Frame(new Box(this.startX+this.slideWidth, this.box.y, 1,
                        this.box.height), this),
            new Frame(new Box(this.startX, this.box.y-1, this.slideWidth, 1),
                    this)
        ];
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
        this.frames.forEach(function(f) {
            collisions.add(f);
        });
    }
    update(interval, collisions, manager) {
        var slideOverPoint = this.startX+this.slideWidth/2-this.box.width/2;
        this.toggled = this.box.x>slideOverPoint;
        manager.setFlag(this.name, this.toggled);
        //Move the switch over to the rightmost (on)/leftmost (off) position.
        if(this.toggled) {
            var diff = this.box.x-this.startX-this.slideWidth;
            this.xVelocity = Math.sqrt(Math.abs(2*this.bounceBackAccel*diff));
            collisions.queue(this, this.xVelocity*interval, 0);
        }
        else {
            var diff = this.box.x-this.startX;
            this.xVelocity = -Math.sqrt(Math.abs(2*this.bounceBackAccel*diff));
            collisions.queue(this, this.xVelocity*interval, 0);
        }
    }
    handleMoved(details, amount) {
        if(amount==0)
            this.xVelocity = 0;
    }
    draw(ctx) {
        super.draw(ctx);
        var prevAlpha = ctx.globalAlpha;
        ctx.globalAlpha *= .5;
        ctx.fillRect(this.startX-5, this.box.y+10, 10+this.slideWidth, 10);
        ctx.globalAlpha = prevAlpha;
    }
}
