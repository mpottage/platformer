//Copyright Matthew Pottage 2019.
import {Collidable, registerCDU} from './collisions/collidable.js';
import {Platform} from './platforms.js';
import {SpikeRow, SpikeColumn} from './spikes.js';
import {HealthComponent, TeleportComponent} from './components.js';
import {HoverMotion} from './motion.js';
import {isProjectile} from './projectiles.js';
import {Box, drawFlashRect} from './utility.js';

export function isMinion(obj) {
    return (obj instanceof GreenBuddy);
}

//A minion designed to be targetted (following an object) and optionally turn on
//damage mode to use a as a weapon.
export class GreenBuddy extends Collidable {
    constructor(x, y, selector) {
        super();
        this.box = new Box(x, y, 15, 15);
        this.motion = new HoverMotion(4, 1, .5, 1, 1);
        this.teleporter = new TeleportComponent();
        this.bodyColor = "#00de76";
        this.pushRank = 16;
        this.selector = selector;
        this.health = new HealthComponent(this, 1);
        this.reset();
    }
    reset() {
        this.motion.reset();
        this.teleporter.reset();
        this.health.reset();
        this.disabled = false;
        this.target = null;
        this.damaging = false;
        this.timeLeft = 180;
    }
    respawn(x, y) {
        this.reset();
        this.box.x = x;
        this.box.y = y;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    setFollow(obj) {
        this.target = obj;
    }
    setDamaging(value) {
        this.damaging = value;
    }
    isCollision(obj) {
        return (obj instanceof Platform) || (obj instanceof SpikeRow)
            || (obj instanceof SpikeColumn) || isProjectile(obj)
            || this.selector(obj);
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
        if((this.damaging && obj.health)
            && !(obj instanceof GreenBuddy))
            obj.health.increase(-1);
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
        }
        drawFlashRect(context, this, "#0f0");
    }
    update(interval, collisions, manager, caches, environment) {
        this.health.update(interval);
        if(!this.disabled) {
            this.teleporter.update(this, collisions);
            if(this.damaging)
                this.motion.multiplier.x = 8;
            else
                this.motion.multiplier.x = 4;
            this.motion.clearActions();
            //Accelerate to the target
            if(this.target)
                this.motion.targetActions(this, this.target);
            this.timeLeft -= interval;
            if(this.timeLeft<=0)
                this.disabled = true;
            this.motion.update(this, interval, collisions, environment);
        }
    }
}
