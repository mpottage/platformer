//Copyright Matthew Pottage 2019.
import {Pickup} from './pickup.js';
import {Player} from '../player/player.js';
import {neededTextures, Box} from '../utility.js';
import {StretchImageCache} from '../render-caches.js';
//A Star, with a coloured center..
//Doesn't do anything and can only be picked up (hidden) by direct contact with
//a player. Can be used with flags to set up puzzles/level win conditions.
neededTextures.push("Star.svg");
export class Star extends Pickup {
    constructor(x, y, color) {
        super();
        this.box = new Box(x, y, 65, 60);
        this.color = color;
    }
    handleIntersection(collided) {
        if(!this.disabled && (collided instanceof Player))
            this.disabled = true;
    }
    draw(context, caches) {
        if(!this.disabled) {
            context.fillStyle = this.color;
            //Fill the inside of the star.
            context.beginPath();
            //Rectangles specific to graphic.
            context.rect(this.box.x+20, this.box.y+25, 25, 20);
            context.rect(this.box.x+28, this.box.y+18, 10, 10);
            context.fill();
            //Place star on top of fill.
            caches.get("Star.svg").draw(context, this.box);
        }
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addDraw(this);
        if(!caches.get("Star.svg"))
            caches.set("Star.svg", new StretchImageCache("Star.svg", this.box.width,
                        this.box.height));
    }
}
