//Copyright Matthew Pottage 2019.
import {neededTextures, Box} from '../utility.js';
import {Pickup} from './pickup.js';
import {StretchImageCache} from '../render-caches.js';
//Health pickup, gives the player a health boost of 1.
neededTextures.push("HealthPoint.svg");
export class HealthPoint extends Pickup {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, 20, 20);
    }
    handleIntersection(hit) {
        if(!this.disabled && hit.health) {
            hit.health.increase(1);
            this.disabled = true;
        }
    }
    draw(context, caches) {
        if(!this.disabled)
            caches.get("HealthPoint").draw(context, this.box);
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addDraw(this);
        if(!caches.get("HealthPoint"))
            caches.set("HealthPoint", new StretchImageCache("HealthPoint.svg", this.box.width,
                        this.box.height));
    }
}
HealthPoint.color = "red";
