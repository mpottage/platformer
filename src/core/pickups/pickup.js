//Copyright Matthew Pottage 2019.
import {Collidable} from '../collisions/collidable.js';
//Base class for all pickups.
export class Pickup extends Collidable {
    isCollision(obj) { return false; };
    //Permit collecting the pickup again.
    //  Note: Doesn't remove it from a Player's inventory.
    reset() {
        this.disabled = false; //Whether the pickup has been collected.
    }
}
