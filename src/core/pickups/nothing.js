//Copyright Matthew Pottage 2019.
import {Pickup} from './pickup.js';
import {Box} from '../utility.js';
//Placeholder pickup (does nothing and is invisible), used to make an empty
//WoodenBox.
export class NothingPickup extends Pickup {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, 30, 30);
    }
    register(collisions, manager, caches) {}
    draw(ctx, caches) {}
}
