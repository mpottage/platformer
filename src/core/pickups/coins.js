import {Pickup} from './pickup.js';
import {Box, fillCircle, neededTextures} from '../utility.js';
import {StretchImageCache} from '../render-caches.js';

//Coin pickup (worth 1 coin).
export class Coin extends Pickup {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, Coin.radius*2, Coin.radius*2);
    }
    handleIntersection(collided) {
        if(!this.disabled && collided.increaseCoins) {
            this.disabled = true;
            collided.increaseCoins(1);
        }
    }
    draw(context) {
        if(!this.disabled) {
            //Draw a circle.
            context.fillStyle = Coin.color;
            fillCircle(context, this.box);
        }
    }
}
Coin.color = "gold";
Coin.radius = 11; //Radius of coin circle.

//A coin worth 3 coins.
neededTextures.push("TripleCoin.svg");
export class TripleCoin extends Pickup {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, TripleCoin.radius*2, TripleCoin.radius*2);
    }
    handleIntersection(collided) {
        if(!this.disabled && collided.increaseCoins) {
            this.disabled = true;
            collided.increaseCoins(3);
        }
    }
    //Draws a TripleCoin (large coin with a grey '3' in the centre).
    draw(context, caches) {
        if(!this.disabled)
            caches.get("TripleCoin").draw(context, this.box);
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addDraw(this);
        if(!caches.get("TripleCoin"))
            caches.set("TripleCoin", new StretchImageCache("TripleCoin.svg", this.box.width,
                        this.box.height));
    }
}
TripleCoin.radius = 18;

neededTextures.push("TenCoin.svg");
export class TenCoin extends Pickup {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, 58, 58);
    }
    handleIntersection(collided) {
        if(!this.disabled && collided.increaseCoins) {
            this.disabled = true;
            collided.increaseCoins(10);
        }
    }
    draw(context, caches) {
        if(!this.disabled)
            caches.get("TenCoin.svg").draw(context, this.box);
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addDraw(this);
        if(!caches.get("TenCoin.svg"))
            caches.set("TenCoin.svg", new StretchImageCache("TenCoin.svg", this.box.width,
                        this.box.height));
    }
}
