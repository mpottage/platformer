//Copyright Matthew Pottage 2019.
import {Pickup} from './pickup.js';
import {Collidable} from '../collisions/collidable.js';
import {ReboundFreeFall} from '../motion.js';
import {Platform} from '../platforms.js';

export class BouncingPickup extends Collidable {
    constructor(pickup, initialVX, initialVY) {
        if(!(pickup instanceof Pickup) || pickup.update || pickup.motion)
            throw new TypeError("Non-moving Pickup required.");
        super();
        this.startPos = {x: pickup.box.x, y: pickup.box.y};
        this.initialV = {x: initialVX||0, y: initialVY||0};
        this.motion = new ReboundFreeFall(0.7, 0.5);
        this.motion.x = this.initialV.x;
        this.motion.y = this.initialV.y;
        this.pushRank = 21;
        this.box = pickup.box;
        this.pickup = pickup;
    }
    reset() {
        this.motion.reset();
        this.pickup.box.x = this.startPos.x;
        this.pickup.box.y = this.startPos.y;
        this.pickup.reset();
        this.disabled = false;
        this.motion.x = this.initialV.x;
        this.motion.y = this.initialV.y;
    }
    register(collisions, manager, caches) {
        this.pickup.register(collisions, manager, caches);
        collisions.add(this);
        manager.addUpdate(this);
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
    }
    handleIntersection(obj) { //Prevent pickup skipping objects.
        this.pickup.handleIntersection(obj);
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    isCollision(obj) {
        return !(obj instanceof BouncingPickup)
            && ((obj instanceof Platform)
                    || (!obj.increaseCoins && !obj.health));
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            if(this.pickup.disabled)
                this.disabled = true;
            else
                this.motion.update(this, interval, collisions, environment);
        }
    }
}
