//Copyright Matthew Pottage 2019.
import {Pickup} from './pickup.js';
import {neededTextures, Box} from '../utility.js';
import {ExtraJump, FixedAltitude, XSpeedBoost, RapidReload, TripleThrow,
    PinkBulletSpecial, OrangeBulletSpecial, NinjaBulletSpecial, MissileSpecial,
    PurpleBulletSpecial, GreenSwarmSpecial, BlueBulletSpecial,
    BrownBulletSpecial, YellowBulletSpecial} from '../specials.js';
import {StretchImageCache} from '../render-caches.js';

class SpecialPickup extends Pickup {
    constructor(x, y, imageName) {
        super();
        this.box = new Box(x, y, 30, 30);
        this.image = imageName;
    }
    handleIntersection(obj) {
        if(!this.disabled && obj.specials) {
            obj.specials.setNext(this.getSpecialInstance());
            this.disabled = true;
        }
    }
    draw(context, caches) {
        if(!this.disabled)
            caches.get(this.image).draw(context, this.box);
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addDraw(this);
        if(!caches.get(this.image))
            caches.set(this.image, new StretchImageCache(this.image, this.box.width,
                        this.box.height));
    }
    //Defined in derived classes, used to get correct special to pick up.
    getSpecialInstance() { return null; }
}

neededTextures.push("ExtraJump.svg");
export class ExtraJumpPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "ExtraJump.svg");
    }
    getSpecialInstance() { return new ExtraJump(); }
}
neededTextures.push("FixedAltitude.svg");
export class FixedAltitudePickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "FixedAltitude.svg");
    }
    getSpecialInstance() { return new FixedAltitude(); }
}

neededTextures.push("XSpeedBoost.svg");
export class XSpeedBoostPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "XSpeedBoost.svg");
    }
    getSpecialInstance() { return new XSpeedBoost(); }
}

neededTextures.push("RapidReload.svg");
export class RapidReloadPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "RapidReload.svg");
    }
    getSpecialInstance() { return new RapidReload(); }
}

neededTextures.push("TripleThrow.svg");
export class TripleThrowPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "TripleThrow.svg");
    }
    getSpecialInstance() { return new TripleThrow(); }
}

neededTextures.push("specials-variants/triple-throw-unlimited.svg");
export class TripleThrowUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/triple-throw-unlimited.svg");
    }
    getSpecialInstance() { return new TripleThrow(Infinity); }
}

neededTextures.push("PinkBulletSpecial.svg");
export class PinkBulletPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "PinkBulletSpecial.svg");
    }
    getSpecialInstance() { return new PinkBulletSpecial(); }
}

neededTextures.push("specials-variants/pink-bullet-unlimited.svg");
export class PinkBulletUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/pink-bullet-unlimited.svg");
    }
    getSpecialInstance() { return new PinkBulletSpecial(Infinity); }
}

neededTextures.push("OrangeBulletSpecial.svg");
export class OrangeBulletPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "OrangeBulletSpecial.svg");
    }
    getSpecialInstance() { return new OrangeBulletSpecial(); }
}

neededTextures.push("specials-variants/orange-bullet-unlimited.svg");
export class OrangeBulletUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/orange-bullet-unlimited.svg");
    }
    getSpecialInstance() { return new OrangeBulletSpecial(Infinity); }
}

neededTextures.push("NinjaBulletSpecial.svg");
export class NinjaBulletPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "NinjaBulletSpecial.svg");
    }
    getSpecialInstance() { return new NinjaBulletSpecial(); }
}

neededTextures.push("specials-variants/ninja-bullet-unlimited.svg");
export class NinjaBulletUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/ninja-bullet-unlimited.svg");
    }
    getSpecialInstance() { return new NinjaBulletSpecial(Infinity); }
}

neededTextures.push("MissileSpecial.svg");
export class MissileSpecialPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "MissileSpecial.svg");
    }
    getSpecialInstance() {
        return new MissileSpecial();
    }
}

neededTextures.push("specials-variants/missile-unlimited.svg");
export class MissileSpecialUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/missile-unlimited.svg");
    }
    getSpecialInstance() {
        return new MissileSpecial(Infinity);
    }
}

neededTextures.push("PurpleBulletSpecial.svg");
export class PurpleBulletPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "PurpleBulletSpecial.svg");
    }
    getSpecialInstance() {
        return new PurpleBulletSpecial();
    }
}

neededTextures.push("specials-variants/purple-bullet-unlimited.svg");
export class PurpleBulletUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/purple-bullet-unlimited.svg");
    }
    getSpecialInstance() {
        return new PurpleBulletSpecial(Infinity);
    }
}

neededTextures.push("GreenSwarmSpecial.svg");
export class GreenSwarmPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "GreenSwarmSpecial.svg");
    }
    getSpecialInstance() { return new GreenSwarmSpecial(); }
}

neededTextures.push("specials-variants/green-swarm-unlimited.svg");
export class GreenSwarmUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/green-swarm-unlimited.svg");
    }
    getSpecialInstance() { return new GreenSwarmSpecial(Infinity); }
}

neededTextures.push("BlueBulletSpecial.svg");
export class BlueBulletPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "BlueBulletSpecial.svg");
    }
    getSpecialInstance() { return new BlueBulletSpecial(); }
}

neededTextures.push("specials-variants/blue-bullet-unlimited.svg");
export class BlueBulletUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/blue-bullet-unlimited.svg");
    }
    getSpecialInstance() { return new BlueBulletSpecial(Infinity); }
}

neededTextures.push("BrownBulletSpecial.svg");
export class BrownBulletPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "BrownBulletSpecial.svg");
    }
    getSpecialInstance() {
        return new BrownBulletSpecial();
    }
}

neededTextures.push("specials-variants/brown-bullet-unlimited.svg");
export class BrownBulletUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/brown-bullet-unlimited.svg");
    }
    getSpecialInstance() {
        return new BrownBulletSpecial(Infinity);
    }
}

neededTextures.push("YellowBulletSpecial.svg");
export class YellowBulletPickup extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "YellowBulletSpecial.svg");
    }
    getSpecialInstance() {
        return new YellowBulletSpecial();
    }
}

neededTextures.push("specials-variants/yellow-bullet-unlimited.svg");
export class YellowBulletUnlimited extends SpecialPickup {
    constructor(x, y) {
        super(x, y, "specials-variants/yellow-bullet-unlimited.svg");
    }
    getSpecialInstance() {
        return new YellowBulletSpecial(Infinity);
    }
}
