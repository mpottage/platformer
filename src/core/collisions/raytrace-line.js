//Copyright Matthew Pottage 2019-2020.
import {Box} from '../utility.js'

//Used when conducting a raytrace.
// Used to trace from (startX, startY) to (endX, endY).
export class RayTraceLine {
    constructor(startX, startY, endX, endY) {
        this.startX = startX;
        this.endX = endX;
        this.startY = startY;
        this.endY = endY;
        this.box = new Box(Math.min(startX, endX), Math.min(startY, endY),
                Math.abs(startX-endX), Math.abs(startY-endY));
        this.axis = {x: -this.startY+this.endY, y:this.startX-this.endX };
        this.proj = this.axis.x*this.startX+this.axis.y*this.startY;
    }
    //Uses Separating Axis Theorem to check for intersection.
    isIntersectionBox(box) {
        if(Box.isIntersection(this.box, box)) {
            //If the line is vertical/horizontal
            if(this.axis.x==0 || this.axis.y==0)
                return true; //Already tested via Box.isIntersection
            else {
                var projs = [
                    this.axis.x*box.x+this.axis.y*box.y,
                    this.axis.x*(box.x+box.width)+this.axis.y*box.y,
                    this.axis.x*(box.x+box.width)+this.axis.y*(box.y+box.height),
                    this.axis.x*box.x+this.axis.y*(box.y+box.height)
                    ];
                projs = projs.sort(function(a, b) { return a-b;});
                return projs[0]<this.proj && projs[projs.length-1]>this.proj;
            }
        }
        else
            return false;
    }
}

