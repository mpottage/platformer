//Copyright Matthew Pottage 2019-2020.
import {Collidable} from './collidable.js';
import {Box, to3dp} from '../utility.js';
import {pushableObjs, combinePushMotions} from './pushing.js';
import {RayTraceLine} from './raytrace-line.js';

//Detects and resolves all collisions
export class CollisionsEngine {
    constructor() {
        this.entities = [];
        this.ignoring = [];
        this._startIgnoring = false;
        this.queuedXMotions = [];
        this.queuedYMotions = [];
        this.queuedIntersections = [];
        this.shutdownTest = function() { return false; };

        this.clear()
    }
    clear() {
        this.entities = []; //all collidables.
        //To avoid collisions errors (e.g. inserted in collision) some objects may
        //ignore others temporarily (is an array of arrays, each entry corresponds
        //to an entity).
        this.ignoring = [];
        //Automatically set ignore objects for newly added entities.
        this._startIgnoring = false;
        this.queuedXMotions = [/*{obj, move}*/]; //Moving objects only.
        this.queuedYMotions = [/*{obj, move}*/]; //Moving objects only.
        //Objects to test for intersections.
        this.queuedIntersections = [/*{obj, oldX, oldY*/];
        this.shutdownTest = function() { return false; };
    }
    add(c) {
        if(!(c instanceof Collidable))
            throw new TypeError("Collidable required.");
        if(this._startIgnoring)
            this.ignoring.push(this._unprotectedAll(c));
        else
            this.ignoring.push([]);
        this.entities.push(c);
    }
    //Make the collidable c ignore any collisions with objects it currently collides
    //with, at it's current position (useful for teleporting).
    protect(c) {
        var i = this.entities.indexOf(c);
        if(i!=-1)
            this.ignoring[i] = this._unprotectedAll(c);
    }
    //Automatically protect any newly added objects.
    autoProtect() {
        this._startIgnoring = true;
    }
    remove(c) {
        var i = this.entities.indexOf(c);
        if(i!=-1) {
            this.entities.splice(i, 1);
            this.ignoring.splice(i, 1);
        }
    }
    //Adds a motion to be checked (and adjusted) when update() is called.  Motion
    //is applied by the engine via setting boxedObj.box.x and boxedObj.box.y to the
    //new values, so that the object doesn't intersect other "hard" objects.
    // Zero motions are ignored (so the handleMoved callback isn't called).
    queue(boxedObj, moveX, moveY) {
        if(!(boxedObj instanceof Collidable) || !(boxedObj.box instanceof Box))
            throw new TypeError("Collidable with valid box required.");
        if(!boxedObj.disabled) {
            if(moveX!=0)
                this.queuedXMotions.push({obj: boxedObj, move: moveX});
            if(moveY!=0)
                this.queuedYMotions.push({obj: boxedObj, move: moveY});
            this.queuedIntersections.push({obj: boxedObj,
                oldX: boxedObj.box.x, oldY: boxedObj.box.y});
        }
    }
    setShutdown(shutdownCriterion) {
        if(!(shutdownCriterion instanceof Function))
            throw new TypeError("Function needed.");
        this.shutdownTest = shutdownCriterion;
    }
    //Checks for collisions between all collidables and queuedMotions.
    // If a collision is detected it calls isCollision on both objects. If
    //  neither is false, it is treated as a "hard" collision; the objects are moved
    //  to avoid the collision, and handleCollision(otherObj, {left, right, up,
    //  down}) is called on both objects (but only if another "hard" collision isn't
    //  obstructing this collision from happening) after all objects have moved and
    //  all handleMoved handlers have been dispatched.
    // The handler handleIntersection is called similarly for all collisions
    //  between collidables and queuedMotions, but after individual motions.
    // If a motion has been requested on an object, after moving its handleMoved
    // callback is called, the second argument being the absolute amount the object
    // was moved because it requested a motion.
    // If an object that is moving has a higher "pushRank" (ie. closer to 0) than
    //  the object it collides with, and the collided object's pushRank is non-zero,
    //  then the object will be pushed/moved as much as is necessary for the
    //  originally moving object to move its full distance. The happens recursively,
    //  as an objects the collided objects hits may also be pushed (if their
    //  pushRank is lower than the original object's pushRank).
    update() {
        var collisions = []; //All collisions information (used to dispatch handlers).
        //Sort the x and y motions so that (of the objects moving up) the topmost
        //moves first, (of going left) the leftmost and the same for right and down.
        this.queuedXMotions.sort(function(m1, m2) {
            return m1.obj.box.x-m2.obj.box.x;
        })
        this.queuedYMotions.sort(function(m1, m2) {
            return m1.obj.box.y-m2.obj.box.y;
        })
        for(var i=0; i<this.queuedXMotions.length; ++i) {
            var itm = this.queuedXMotions[i];
            if(itm.move<0) //only left
                collisions.push(
                        this._adjustXMotion(itm.obj,itm.obj.box.x+itm.move));
        }
        for(var i=this.queuedXMotions.length-1; i>=0; --i) {
            var itm = this.queuedXMotions[i];
            if(itm.move>0) //only right
                collisions.push(
                        this._adjustXMotion(itm.obj,itm.obj.box.x+itm.move));
        }
        for(var i=0; i<this.queuedYMotions.length; ++i) {
            var itm = this.queuedYMotions[i];
            if(itm.move<0) //only up
                collisions.push(
                    this._adjustYMotion(itm.obj,itm.obj.box.y+itm.move));
        }
        for(var i=this.queuedYMotions.length-1; i>=0; --i) {
            var itm = this.queuedYMotions[i];
            if(itm.move>0) //only down
                collisions.push(
                    this._adjustYMotion(itm.obj,itm.obj.box.y+itm.move));
        }
        //Run intersection checks.
        this.queuedIntersections.forEach(function(detail) {
            if(!this.shutdownTest(detail.obj)) {
                var box = detail.obj.box;
                //Create a box sweeping out the object's motion.
                var sweptBox = new Box(Math.min(box.x, detail.oldX),
                        Math.min(box.y, detail.oldY),
                        Math.abs(box.x-detail.oldX)+box.width,
                        Math.abs(box.y-detail.oldY)+box.height);
                this.handleIntersections(detail.obj, sweptBox);
            }
        }, this);
        this.dispatchHandlers(collisions);
        this.refreshIgnoring();
        //reset queues
        this.queuedXMotions = [];
        this.queuedYMotions = [];
        this.queuedIntersections = [];
    }
    //Used on all queued objects. See CollisionsEngine.update.
    //Invokes intersection callbacks (handleIntersection) for all collisions
    //detected with the area (sweptBox) the object swept over when moving and now
    //occupies. This, e.g., causes coin collection and the player to slow in water.
    handleIntersections(obj, sweptBox) {
        this.entities.forEach(function(collidable) {
            if(obj!==collidable && !collidable.disabled
                    && Box.isIntersection(sweptBox, collidable.box)) {
                //Inform both objects of the intersection collision.
                collidable.handleIntersection(obj)
                obj.handleIntersection(collidable);
            }
        });
    }
    dispatchHandlers(collisions) {
        collisions.forEach(function(clist) {
            clist.motions[0].obj.handleMoved(clist.r_details, clist.motions[0].offset);
        });
        collisions.forEach(function(clist) {
            clist.motions.forEach(function(m) {
                m.collides.forEach(function(collides) {
                    collides.handleCollision(m.obj, clist.details);
                    m.obj.handleCollision(collides, clist.r_details);
                });
            });
        });
    }
    //Update ignoring information, so that if two objects that did ignore each other
    //have now moved out of collision, they stop ignoring each other.
    refreshIgnoring() {
        for(var i=0; i<this.ignoring.length; ++i) {
            if(this.ignoring[i].length>0 && !this.shutdownTest(this.entities[i]))
                this.ignoring[i] = this._unprotectedAll(this.entities[i]);
        }
    }
    //See CollisionsEngine.prototype.update
    //This attempts to move obj to newX so that is isn't colliding with other
    //objects. It called _determineXMotions to limit the movement (if it cannot move
    //to moveX) and push other objects as necessary.
    _adjustXMotion(obj, newX) {
        var details = {left: newX>obj.box.x, right: newX<obj.box.x};
        var r_details = {left: details.right, right: details.left};
        var motions = [];
        if(this.shutdownTest(obj))
            motions.push({obj: obj, collides: [], move: 0, offset: 0});
        else {
            motions = this._determineXMotions(obj, newX);
            motions.forEach(function(m, i) {
                m.obj.box.x += m.move;
            }, this);
        }
        return {details: details, r_details: r_details, motions: motions};
    }
    //See CollisionsEngine.prototype.update
    //This attempts to move obj to newY so that is isn't colliding with other
    //objects. It called _determineYMotions to limit the movement (if it cannot move
    //to moveY) and push other objects as necessary.
    _adjustYMotion(obj, newY) {
        var details = {down: newY<obj.box.y, up: newY>obj.box.y};
        var r_details = {down: details.up, up: details.down};
        var motions = [];
        if(this.shutdownTest(obj))
            motions.push({obj: obj, collides: [], move: 0, offset: 0});
        else {
            motions = this._determineYMotions(obj, newY);
            motions.forEach(function(m, i) {
                m.obj.box.y += m.move;
            }, this);
        }
        return {details: details, r_details: r_details, motions: motions};
    }
    //Determine the motions necessary to move obj as close as possible to
    //(obj.box.x, newY). Returns a sorted list of the motions in increasing order, by
    //the distance of the object from obj when it is moved.
    _determineYMotions(obj, newY, ignore, pushRank, offsetShift) {
        ignore = ignore || []; //Objects to ignore collisions for, helps prevent infinite recusion.
        offsetShift = offsetShift || 0;
        if(pushRank===undefined)
            pushRank = obj.pushRank;
        var allHits = this._allYCollisions(obj, newY, ignore); //Sorted collisions.
        if(allHits.length==0) //Nothing to do.
            return [{obj: obj, collides: [], move: newY-obj.box.y,
                offset: Math.abs(obj.box.y-newY)+offsetShift}];
        var pushObjs = pushableObjs(pushRank, allHits);
        var stopObj = allHits[pushObjs.length]; //First object not in push objs.
        if(stopObj) { //Adjust newY as necessary, to avoid hitting unpushable objects.
            if(newY<obj.box.y)
                newY = stopObj.box.y+stopObj.box.height;
            else
                newY = stopObj.box.y-obj.box.height;
        }
        //If objects need to be pushed in order to move, push them, recursively.
        if(pushObjs.length>0 && obj.box.y!=newY) {
            ignore.push(obj);
            //Attempt to move all pushable objects.
            var extraMotions = pushObjs.map(function(o) {
                if(newY<obj.box.y)
                    return this._determineYMotions(o, newY-o.box.height, ignore,
                            pushRank, obj.box.y-o.box.y-o.box.height);
                else if(newY>obj.box.y)
                    return this._determineYMotions(o, newY+obj.box.height, ignore,
                            pushRank, o.box.y-obj.box.y-obj.box.height);
            }, this);
            var dir = newY>obj.box.y? 1 : -1;
            var merged = combinePushMotions(extraMotions,dir,obj);
            //Shift offsets.
            merged.forEach(function(m) { m.offset += offsetShift+obj.box.height; });
            var objMotion = merged[0];
            objMotion.offset -= obj.box.height; //obj isn't offset by its height.
            if(stopObj && objMotion.move+obj.box.y==newY)
                //Add collision for unpushable objects stopping motion.
                objMotion.collides.push(stopObj);
            return merged;
        }
        //Normal hit, just limit movement (already done).
        else if(stopObj) {
            return [{obj: obj, collides: [stopObj], move: newY-obj.box.y,
                offset: Math.abs(obj.box.y-newY)+offsetShift}];
        }
    }
    //Determine the motions necessary to move obj as close as possible to
    //(newX, obj.box.y). Returns a sorted list of the motions in increasing order, by
    //the distance of the object from obj when it is moved.
    _determineXMotions(obj, newX, ignore, pushRank, offsetShift) {
        ignore = ignore || []; //Objects to ignore collisions for, helps prevent infinite recusion.
        offsetShift = offsetShift || 0;
        if(pushRank===undefined)
            pushRank = obj.pushRank;
        var allHits = this._allXCollisions(obj, newX, ignore); //Sorted collisions.
        if(allHits.length==0) //Nothing to do.
            return [{obj: obj, collides: [], move: newX-obj.box.x,
                offset: Math.abs(obj.box.x-newX)+offsetShift}];
        var pushObjs = pushableObjs(pushRank, allHits);
        var stopObj = allHits[pushObjs.length]; //First object not in push objs.
        if(stopObj) { //Adjust newX as necessary, to avoid hitting unpushable objects.
            if(newX<obj.box.x)
                newX = stopObj.box.x+stopObj.box.width;
            else
                newX = stopObj.box.x-obj.box.width;
        }
        //If objects need to be pushed in order to move, push them, recursively.
        if(pushObjs.length>0 && obj.box.x!=newX) {
            ignore.push(obj);
            //Attempt to move all pushable objects.
            var extraMotions = pushObjs.map(function(o) {
                if(newX<obj.box.x)
                    return this._determineXMotions(o, newX-o.box.width, ignore,
                            pushRank, obj.box.x-o.box.x-o.box.width);
                else if(newX>obj.box.x)
                    return this._determineXMotions(o, newX+obj.box.width, ignore,
                            pushRank, o.box.x-obj.box.x-obj.box.width);
            }, this);
            var dir = newX>obj.box.x? 1 : -1;
            var merged = combinePushMotions(extraMotions,dir,obj);
            //Shift offsets.
            merged.forEach(function(m) { m.offset += offsetShift+obj.box.width; });
            var objMotion = merged[0];
            objMotion.offset -= obj.box.width; //obj isn't offset by its width.
            if(stopObj && objMotion.move+obj.box.x==newX)
                //Add collision for unpushable objects stopping motion.
                objMotion.collides.push(stopObj);
            return merged;
        }
        //Normal hit, just limit movement (already done).
        else if(stopObj) {
            return [{obj: obj, collides: [stopObj], move: newX-obj.box.x,
                offset: Math.abs(obj.box.x-newX)+offsetShift}];
        }
    }
    //All intersections that would be collisions, for obj given that it has box box
    //and is now at obj.box, ignoring all items in the array ignore.
    all(obj, box, ignore) {
        var protectIgnore = this.ignoring[this.entities.indexOf(obj)] || [];
        var res = this.entities.filter(function(collidable, i) {
            return (collidable!==obj && !collidable.disabled
                   && Box.isIntersection(box, collidable.box)
                   && protectIgnore.indexOf(collidable)===-1
                   && this.ignoring[i].indexOf(obj)===-1
                   && collidable.isCollision(obj)
                   && obj.isCollision(collidable));
        }, this);
        if(!ignore || ignore.length==0) //Anything to ignore?
            return res;
        else //Something to ignore.
            return res.filter(function(obj) { return ignore.indexOf(obj)==-1; });
    }
    //Ignores any protection applied directly to obj.
    //Used to determine collisions that should be ignored.
    _unprotectedAll(obj) {
        return this.entities.filter(function(collidable, i) {
            return (collidable!==obj && !collidable.disabled
                   && Box.isIntersection(obj.box, collidable.box)
                   && this.ignoring[i].indexOf(obj)===-1
                   && collidable.isCollision(obj)
                   && obj.isCollision(collidable));
        }, this);
    }
    //Would any objects collide with obj, given that its box is replacementBox.
    any(obj, replacementBox) {
        return this.all(obj, replacementBox, []).length>0;
    }
    //Simulate collisions with all objects currently colliding with obj (ie. the
    //collisions suppressed by protect() and autoProtect()).
    //e.g. To ensure for a new projectile that intersecting opponents get hurt.
    //XXX: Usage is confusing.
    unprotectedImpact(obj, ignore) {
        this._unprotectedAll(obj)
            .filter(o => ignore.indexOf(o)===-1)
            .forEach(function(o) {
                obj.handleCollision(o, {});
                o.handleCollision(obj, {});
            });
    }
    _allYCollisions(obj, newY, ignore) {
    //Returns collisions encountered by obj moving to (obj.box.x, newY), sorted from
    //the collision closest to obj, to the furthest away.
        var box = obj.box;
        if(box.y==newY)
            return [];
        else {
            //Box encompassing all the area swept by obj if it is moved to newY.
            var extrapolated = new Box(box.x, Math.min(box.y, newY),
                    box.width, box.height+Math.abs(newY-box.y));
            var res = this.all(obj, extrapolated, ignore);
            //Ensure res is sorted, correctly.
            if(newY<obj.box.y)
                res.sort(function(a, b) {
                    return Math.abs(a.box.y+a.box.height-obj.box.y)
                        -Math.abs(b.box.y+b.box.height-obj.box.y);
                });
            else
                res.sort(function(a, b) {
                    return Math.abs(a.box.y-obj.box.y-obj.box.height)
                        -Math.abs(b.box.y-obj.box.y-obj.box.height);
                });
            return res;
        }
    }
    _allXCollisions(obj, newX, ignore) {
    //Returns collisions encountered by obj moving to (newX, obj.box.x), sorted from
    //the collision closest to obj, to the furthest away.
        var box = obj.box;
        if(box.x==newX)
            return [];
        else {
            //Box encompassing all the area swept by obj if it is moved to newX.
            var extrapolated = new Box(Math.min(box.x, newX), box.y,
                    box.width+Math.abs(newX-box.x), box.height);
            var res = this.all(obj, extrapolated, ignore);
            //Ensure res is sorted, correctly.
            if(newX<obj.box.x)
                res.sort(function(a, b) {
                    return Math.abs(a.box.x+a.box.width-obj.box.x)
                        -Math.abs(b.box.x+b.box.width-obj.box.x);
                });
            else
                res.sort(function(a, b) {
                    return Math.abs(a.box.x-obj.box.x-obj.box.width)
                        -Math.abs(b.box.x-obj.box.x-obj.box.width);
                });
            return res;
        }
    }
    //Returns a list of all the objects hit by a line.
    //Line starts at point (startX, startY), ends at (endX, endY).
    rayTrace(startX, startY, endX, endY) {
        var trace = new RayTraceLine(startX, startY, endX, endY);
        var hit = [];
        this.entities.forEach(function(c) {
            if(!c.disabled && trace.isIntersectionBox(c.box))
                hit.push(c);
        });
        return hit;
    }
}
