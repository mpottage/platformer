//Copyright Matthew Pottage 2019-2020.
//Inherited by all collidables. Supplies default event callbacks and settings.
export class Collidable {
    constructor() {
        this.box = null; //AABB representing the collidable's entity.
        this.disabled = false; //No events, movement or collisions?
        //See CollisionsEngine.prototype.update
        this.pushRank = 0
    }
    //See CollisionsEngine.prototype.update for the below.
    handleCollision() {}
    handleIntersection() {}
    isCollision() { return true; };
    handleMoved(details) { };
    register(collisions, manager) {
        manager.addDraw(this);
        collisions.add(this);
    }
}

export function registerCDU(obj, collisions, manager) {
    //Register collidable, visible object that needs updating.
    manager.addDraw(obj);
    manager.addUpdate(obj);
    collisions.add(obj);
}
