//Copyright Matthew Pottage 2019.
import {Platform} from './platforms.js';
import {Pool, Box, to3dp, getClosest} from './utility.js';
import {AirborneStone} from './projectiles.js';
import {Scanner} from './scanner.js';
import {Player} from './player/player.js';

//Used with StoneLaunchers.
class StoneFromLauncher extends AirborneStone {
    constructor() {
        super(0, 0, 0, 0, null);
        //Stones are preregistered by StoneLaunchers and enabled when needed.
        this.disabled = true;
    }
    isCollision(obj) {
        return super.isCollision(obj) && !(obj instanceof StoneFromLauncher);
    }
    respawn(x, y, xV, yV, tb) {
        super.reset();
        this.box.x = x;
        this.box.y = y;
        this.motion.x = xV;
        this.motion.y = yV;
        this.thrownBy = tb;
    }
    reset() {
        super.reset();
        this.disabled = true;
    }
}

export class StoneLauncher extends Platform {
    //launchVec: {x, y} direction to launch and it is assumed abs(x)==abs(y)==1.
    //delay: Interval between stone launches.
    constructor(box, launchVec, color, delay) {
        super(box, color || "darkgrey");
        this.launchVec = launchVec;
        this.launchDelay = delay || 1;
        this.tilLaunch = 0;
        //See launchStone for details on this.pool.
        var poolElems = []; //Pool of stones.
        var neededStones = 10/this.launchDelay; //Want 10s worth of stones.
        for(var i=0; i<neededStones; ++i) //Fill pool.
            poolElems.push(new StoneFromLauncher());
        this.pool = new Pool(poolElems);
        this.activated = true;
    }
    reset() {
        super.reset();
        this.tilLaunch = 0;
        this.pool.apply(stone => stone.reset()); //Reset pooled stones
        this.activated = true;
    }
    register(collisions, manager) {
        manager.addUpdate(this);
        manager.addOverlay(this);
        collisions.add(this);
        //Preregister all stones (as they will all eventually be used).
        this.pool.register(collisions, manager);
    }
    update(interval, collisions, manager) {
        if(this.activated) {
            this.tilLaunch -= interval;
            if(this.tilLaunch<=0) {
                this.tilLaunch = this.launchDelay;
                this.launchStone(collisions, manager);
            }
        }
    }
    //Overlayed to cover the stones it is launching.
    overlayDraw(context) {
        this.draw(context);
    }
    launchStone(collisions, manager) {
        //Launches the stone from a pool of stones available to the launcher.
        //A pool is necessary to avoid excessive heap allocation (ie.  1 stone
        //every launchDelay for the running of the level), which damages
        //performance.
        var s = this.pool.get();
        //The stone was registered in register, so just need to throw the stone.
        s.respawn(this.box.x+this.box.width/2-6,
                    this.box.y+this.box.height/2-6,
                    this.launchVec["x"]*500,
                    this.launchVec["y"]*500-200,
                    this
                );
        collisions.protect(s);
    }
}

//Targets the nearest player, within a 1000px by 1000px box about the stone
//launcher.
export class TargettingStoneLauncher extends StoneLauncher {
    constructor(bx, lv, c, d) {
        super(bx, lv, c, d);
        var range = 1000;
        this.scanner = new Scanner(new Box(this.box.x+this.box.width/2-range,
                    this.box.y+this.box.height/2-range, 2*range, 2*range),
                function(o) { return o instanceof Player});
        this.originalLV = this.launchVec;
        this.effectivePower = to3dp(Math.sqrt(lv.x*lv.x+lv.y*lv.y));
    }
    reset() {
        super.reset();
        this.scanner.reset();
        this.launchVec = this.originalLV;
    }
    update(interval, collisions, manager, caches, environment) {
        if(this.scanner.seen.length>0) {
            var obj = getClosest(this.box, this.scanner.seen);
            //Direct the StoneLauncher at the player.
            var newVec = {x: obj.box.x-this.box.x, y: obj.box.y-this.box.y};
            var scaleVec = Math.sqrt(2)/Math.sqrt(newVec.x*newVec.x+newVec.y*newVec.y);
            scaleVec *= this.effectivePower;
            newVec.x *= scaleVec;
            newVec.y *= scaleVec;
            this.launchVec = newVec;
        }
        else //Reset targetting to none.
            this.launchVec = this.originalLV;
        super.update(interval, collisions, manager, caches, environment);
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.scanner.register(collisions, manager, caches);
    }
}
