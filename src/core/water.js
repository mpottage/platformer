//Copyright Matthew Pottage 2019.
import {Collidable} from './collisions/collidable.js';

//Water, causes an object's jump to be reduced in it and limits its speed.
// (Done via motion states)
export class Water extends Collidable {
    constructor(box) {
        super();
        this.box = box;
    }
    isCollision(obj) { return false; }
    //Sets the player's state appropriately. The parameters for the state are
    //set in FreeFallMotion.
    handleIntersection(obj) {
        if(obj.motion && obj.motion.state)
            obj.motion.state.setFluid("water");
    }
    backgroundDraw(context) {
        context.fillStyle = Water.color;
        this.box.fill(context);
    }
    overlayDraw(context) {
        context.globalAlpha *= 0.5;
        this.backgroundDraw(context);
        context.globalAlpha /= 0.5;
    }
    register(collisions, manager) {
        manager.addBackground(this);
        manager.addOverlay(this);
        collisions.add(this);
    }
    reset() { }
}
Water.color = "#00ccff";

//MarshWater is like water, except that when a player is 80% submerged in it,
//  the player loses a health point and is reset to the start of the level.
export class MarshWater extends Water {
    handleIntersection(obj) {
        //If obj can be teleported, check if at most 20% of it is above the
        //water, otherwise it loses a life and is reset to (10, 480) (start).
        if(obj.teleporter && obj.health
                && obj.box.y+obj.box.height-this.box.y>=0.8*obj.box.height) {
            //Place object at ground level.
            obj.teleporter.toAuto(10, 480);
            obj.health.increase(-1);
        }
        else //Act like Water.
            super.handleIntersection(obj);
    }
    backgroundDraw(context) {
        context.fillStyle = MarshWater.color;
        this.box.fill(context);
    }
}
MarshWater.color = "#0d9674";
