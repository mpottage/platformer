//Copyright Matthew Pottage 2019.
import {Player} from './player/player.js';
import {Collidable} from './collisions/collidable.js';

export class Region extends Collidable {
    constructor(box) {
        super();
        this.reset();
        this.box = box;
    }
    isCollision() { return false; }
    register(collisions, manager) {
        collisions.add(this);
    }
    reset() {}
}

//Region supporting activation to control its special behaviour.
class ActivatedRegion extends Region {
    constructor(box) {
        super(box);
        this.activated = true;
    }
    reset() {
        super.reset();
        this.activated = true;
    }
}

export class DeathRegion extends ActivatedRegion {
    handleIntersection(obj) {
        if(this.activated && obj.health)
            obj.health.kill();
    }
}

export class WinRegion extends ActivatedRegion {
    handleIntersection(obj) {
        if(this.activated && obj instanceof Player)
            obj.passedLevel = true;
    }
}

//Teleports any objects with a teleporter to their default position (ie. start
//position).
export class ToStartRegion extends ActivatedRegion {
    constructor(box) {
        super(box);
    }
    handleIntersection(obj) {
        if(this.activated && obj.teleporter)
            obj.teleporter.toDefault();
    }
}

//Prevent stones being thrown by a StonesComponent attached to objects (e.g.
//Players) in the region. Uses StonesComponent.prototype.suppress().
//Transferring between SuppressWeapons regions may result in an instant where
//stone throwing is permitted.
export class SuppressWeapons extends ActivatedRegion {
    constructor(box) {
        super(box);
        this.appliedTo = []; //Objects suppressed
        this.intersecting = []; //Objects that should be suppressed
    }
    reset() {
        this.appliedTo = [];
        this.intersecting = [];
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        manager.addUpdate(this);
    }
    handleIntersection(obj) {
        if(obj.stones && this.intersecting.indexOf(obj)===-1)
            this.intersecting.push(obj);
    }
    update(interval, collisions, manager) {
        if(this.activated) {
            //Permit weapons for objects that have left the region.
            this.appliedTo.forEach(function(obj) {
                if(this.intersecting.indexOf(obj)===-1)
                    obj.stones.suppress(false);
            }, this);
            //Suppress (or resuppress) for objects in the region.
            this.intersecting.forEach(obj => obj.stones.suppress(true));
            this.appliedTo = this.intersecting;
        }
        else {
            this.appliedTo.forEach(obj => obj.stones.suppress(false));
            this.appliedTo = [];
        }
        this.intersecting = [];
    }
}

//Remove the next special (if any), setting it to null.
export class ClearSpecials extends ActivatedRegion {
    handleIntersection(obj) {
        if(this.activated && obj.specials)
            obj.specials.setNext(null);
    }
}

//Sets the message parameter on any players that intersect it.
//Used to give messages to a player.
//TODO: Don't set obj.message directly.
export class MessageRegion extends ActivatedRegion {
    constructor(box, message) {
        super(box);
        this.message = message;
    }
    handleIntersection(obj) {
        if(this.activated && (obj instanceof Player)) {
            obj.message = this.message;
        }
    }
}

//Hidden region that toggles true when the player intersects it.
export class HitRegion extends Region {
    //name: Flag to set to true when the player intersects the region.
    constructor(name, box) {
        super(box);
        this.toggled = false;
        this.name = name;
    }
    handleIntersection(obj) {
        if(obj instanceof Player)
            this.toggled = true;
    }
    update(interval, collisions, manager) {
        manager.setFlag(this.name, this.toggled);
        this.toggled = false;
    }
    register(collisions, manager) {
        collisions.add(this);
        manager.addUpdate(this);
    }
    reset() {
        this.toggled = false;
    }
}

//Triggers simulation of a directed air current in a region.
export class AirCurrent extends ActivatedRegion {
    //box: Area to affect,
    //vec: Movement vector {x, y} (absolute value should be 1).
    //power: Current strength multiplier.
    constructor(box, vec={x: 1, y: 0}, power=1) {
        super(box);
        this.vec = vec;
        this.power = power;
        this.accel = 300;
        this.reset();
    }
    reset() {
        super.reset();
        this.applyTo = [];
    }
    register(collisions, manager) {
        collisions.add(this);
        manager.addUpdate(this);
    }
    handleIntersection(obj) {
        if(obj.pushRank>this.pushRank && obj.motion && obj.motion.accel
                && this.applyTo.indexOf(obj)==-1)
            this.applyTo.push(obj);
    }
    update(interval, collisions, manager, caches, environment) {
        if(this.activated) {
            this.applyTo.forEach(function(o) {
                o.motion.current.y += this.vec.y*this.power*this.accel;
                if(this.vec.y<0) {
                    //Prevent falling down.
                    o.motion.current.y -= environment.get("gravity");
                    //Prevent bobbing bottom-to-top-to-bottom repeatedly.
                    if(o.motion.y>0)
                        o.motion.current.y += this.vec.y*this.power*this.accel;
                }
                o.motion.current.x += this.vec.x*this.accel*this.power;
            }, this);
        }
        this.applyTo = [];
    }
}

//Charges the first object to intersect it with a sufficient coins that number
//of coins (coins), then outputs true. e.g. For for shops, gate tolls...
export class PaymentRegion extends ActivatedRegion {
    //name: Flag output
    //coins: Coins charged
    constructor(name, box, coins) {
        super(box);
        this.name = name;
        this.requires = coins;
        this.paid = false;
    }
    reset() {
        super.reset();
        this.paid = false;
    }
    register(collisions, manager) {
        collisions.add(this);
        manager.addUpdate(this);
    }
    handleIntersection(obj) {
        if(this.activated && !this.paid && obj.increaseCoins
            && obj.coins>=this.requires) {
            this.paid = true;
            obj.increaseCoins(-this.requires);
        }
    }
    update(interval, collisions, manager) {
        manager.setFlag(this.name, this.paid);
    }
}

//Used to connect two distant regions in a level.
//  > When an object with a teleporter enters the region, it is pushed to its
//  output pipe.
//  > When a new object is recieved from its input pipe, the object is
//  teleported to this region.
//(connections are setup by the input/output pipes going between regions)
export class PortalRegion extends Region {
    constructor(output, input, box) {
        super(box);
        this.output = output;
        this.input = input;
        this.reset();
    }
    register(collisions, manager) {
        super.register(collisions);
        manager.addUpdate(this);
        manager.addPipe(this.output);
    }
    reset() {
        this._newApplyTo = [];
        this.ignore = [];
        this.activated = true;
    }
    handleIntersection(obj) {
        if(obj.teleporter && this._newApplyTo.indexOf(obj)==-1)
            this._newApplyTo.push(obj);
    }
    update(interval, collisions, manager) {
        if(this.activated) {
            //Inform complementing portal of objects to move
            this._newApplyTo
                .filter(o => this.ignore.indexOf(o)==-1)
                .forEach(o => manager.pushPipe(this.output, o));
            this.ignore = this._newApplyTo;
            this._newApplyTo = [];
        }
        else
            this._newApplyTo = this.ignore = [];
        //Move objects here, as necessary
        var center = this.box.center;
        var toHere = manager.popPipe(this.input);
        while(toHere) {
            toHere.teleporter.to(center.x-toHere.box.width/2,
                center.y-toHere.box.height/2);
            this.ignore.push(toHere);
            toHere = manager.popPipe(this.input);
        }
    }
}
