//Copyright Matthew Pottage 2019.
//Maps onto another specials component in another object (used in AirborneStone).
export class ForwardSpecialsComponent {
    constructor(forwardTo) {
        if(!forwardTo.specials)
            throw new TypeError("Object with SpecialsComponent required.");
        this.forwardTo = forwardTo;
    }
    setNext(sp) {
        if(sp)
            this.forwardTo.specials.setNext(sp);
    }
}
export class ForwardHealthComponent {
    constructor(forwardTo) {
        this.forwardTo = forwardTo;
    }
    increase(change) {
        if(change>0)
            this.forwardTo.health.increase(change);
    }
    kill() {}
}
