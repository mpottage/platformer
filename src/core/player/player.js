//Copyright Matthew Pottage 2019.
import {TeleportComponent, HealthComponent} from '../components.js';
import {ControlledMotion} from '../motion.js';
import {Box, Transition, drawFlashRect, drawTransparentRect} from '../utility.js';
import {StonesComponent, SpecialsComponent} from './components.js';
import {Collidable, registerCDU} from '../collisions/collidable.js';
import {Coin} from '../pickups/coins.js';

//Player optionally controlled by user.
export class Player extends Collidable {
    constructor(width, height, color) {
        super();
        this.box = new Box(0, 0, width, height);
        this.motion = new ControlledMotion();
        //Used to move the player around without confusing collisions.
        this.teleporter = new TeleportComponent();
        //Actions for the player to do. Usually requested by a human player.
        this.action = {
            left: false,
            right: false,
            jump: false,
            throwStone: false,
            useSpecial: false
        };
        this.stones = new StonesComponent(); //The player can throw a stone every 3s.
        this.color = color;
        this.pushRank = 10;
        //See Player.prototype.handleCollision() for details.
        this.touching = {down: null}; //For any Camera.
        this.coins = 0; //Number of coins the player has.
        this.health = new HealthComponent(this, 3);
        this.transitionCoins = null;
        this.transitionColor = "rgb(230,0,0)";
        this.passedLevel = false; //Has the Player passed the current level.
        this.message = ""; //Message to show, if any.
        this.specials = new SpecialsComponent();
    }
    //change: Change to player's number of coins, may be negative.
    increaseCoins(change) {
        this.coins += change;
        if(change<0)
            this.transitionCoins = new Transition(1, 0, .5);
    }
    handleCollision(collided, details) {
        if(details.down)
            this.touching.down = collided;
        this.motion.handleCollision(this, collided, details);
    }
    isCollision(collided) {
        return !(collided instanceof Player);
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    //Forwards commands to this.motion (to move) and this.stones (to throw a stone)
    //as appropriate. Uses this.motion.stateParams for health loss jump.
    updateControls(collisions, manager) {
        this.motion.action.left = this.action.left;
        this.motion.action.right = this.action.right;
        if(!this.teleporter.positionReset
             && this.health.justLost) {
            this.motion.y = -7*this.motion.stateParams["ground/air"].jumpVelocity/8;
            this.motion.action.jump = false;
        }
        else
            this.motion.action.jump = this.action.jump;
        //Throw a stone (available every 3s).
        if(this.action.throwStone)
            this.stones.launch(this, collisions, manager);
        this.action.throwStone = false;
    }
    //Run every frame to update the player's state.
    update(interval, collisions, manager, caches, environment) {
        if(this.disabled) //Nothing to do.
            return;
        if(this.transitionCoins) {
            this.transitionCoins.update(interval);
            if(this.transitionCoins.value<=0)
                this.transitionCoins = null;
        }
        this.stones.update(this, interval);
        //Teleport
        this.teleporter.update(this, collisions);
        //Health flash and delay
        this.health.update(interval);
        //Handle controls.
        this.updateControls(collisions, manager);
        //Update special behaviour (if any).
        this.specials.update(this, this.action.useSpecial, interval,
                collisions, manager, caches, environment);
        this.action.useSpecial = false;
        //Move
        this.motion.update(this, interval, collisions, environment);
        //Reset touching detection.
        this.touching.down = null;
    }
    //Draws the player onto a canvas context.
    draw(context, caches) {
        context.fillStyle = this.color;
        this.box.fill(context);
        drawFlashRect(context, this, this.transitionColor);
        if(this.transitionCoins)
            drawTransparentRect(context, this.box, Coin.color,
                this.transitionCoins.value)
        this.specials.inlineDraw(this, context, caches);
    }
    hintDraw(context, caches) {
        this.specials.hintDraw(this, context, caches);
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
        manager.addHintDraw(this);
        manager.addPlayer(this);
    }
}
