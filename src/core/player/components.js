//Copyright Matthew Pottage 2019.
import {AirborneStone} from '../projectiles.js';

//Permits throwing a stone every 3s from an object with a motion component.
//  Launching can be temporarily stopped by setting suppresed to true.
export class StonesComponent {
    constructor() {
        this.reloadDelay = 3;
        this.sinceLastStone = 0;
        this._suppressed = false;
    }
    update(from, interval) {
        this.sinceLastStone += interval;
    }
    suppress(val) {
        this._suppressed = val;
    }
    launch(from, collisions, manager) {
        if(!from.motion)
            throw new Error("Cannot launch stone from fixed object.");
        //Stones go in the direction from is moving, and its x and y
        //velocities are separately proportional to from's x and y
        //velocities.
        if(this.sinceLastStone>=this.reloadDelay && !this._suppressed) {
            this.sinceLastStone = 0;
            var stone = new AirborneStone(from.box.x, from.box.y,
                from.motion.x*5, from.motion.y*2, from);
            //Ensure that any intersecting enemies get hurt.
            collisions.unprotectedImpact(stone, [from]);
            stone.register(collisions, manager);
        }
    }
    canLaunch() {
        return this.sinceLastStone>=this.reloadDelay;
    }
    reloadNow() {
        this.sinceLastStone = this.reloadDelay;
    }
}

//Manages using specials for, e.g. a Player.
//It permits at most one special to be in use and one to be queued to use.
export class SpecialsComponent {
    constructor() {
        this._next = null;
        this._inUse = null;
    }
    //Set the next special to be used (queue is of length one).
    setNext(special) {
        if(special)
            special.merge(this._next);
        this._next = special;
    }
    //startUsingNext: Use the special queued to be used next.
    update(obj, startUsingNext, interval, collisions, manager, caches, environment) {
        //Replace this._inUse with this._next.
        if(startUsingNext) {
            if(this._inUse) //Clean up.
                this._inUse.stopUsing(obj);
            this._inUse = this._next;
            this._next = null;
            if(this._inUse)
                this._inUse.use(obj, collisions, manager, caches);
        }
        else if(this._inUse)
            this._inUse = this._inUse.updateUse(obj, interval, collisions, manager, caches, environment);
    }
    //Called by objects with this component.
    hintDraw(obj, context) {
        if(this._inUse)
            this._inUse.drawUseHint(obj, context);
    }
    //Draw in the same layer as the object with this component.
    inlineDraw(obj, context) {
        if(this._inUse)
            this._inUse.drawUseInline(obj, context);
    }
}
