//Copyright Matthew Pottage 2019.
import {Collidable} from './collisions/collidable.js';
import {absValue} from './utility.js';

//Finds all objects in a specified region satisfying a criterion.
export class Scanner extends Collidable {
    //check: Function, returns true if the object should be noticed.
    constructor(box, check) {
        super();
        this.box = box;
        this.seen = [];
        this._nextSeen = [];
        this._checkFunc = check;
    }
    reset() {
        this.seen = [];
        this._nextSeen = [];
        this.disabled = false;
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addUpdate(this);
    }
    update(interval, collisions, manager, caches) {
        if(!this.disabled) {
            this.seen = this._nextSeen;
            this._nextSeen = [];
        }
    }
    isCollision(obj) {
        return false;
    }
    handleIntersection(obj) {
        if(this._checkFunc(obj) && this._nextSeen.indexOf(obj)==-1)
            this._nextSeen.push(obj);
    }
    recenter(x, y) {
        this.box.x = x-this.box.width/2
        this.box.y = y-this.box.height/2
    }
    disable() {
        this.disabled = true;
    }
}

//Like Scanner, but requests intersection checks for all objects, including
//those that don't move.
export class ActiveScanner extends Scanner {
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            super.update(interval, collisions, manager, caches, environment);
            collisions.queue(this);
        }
    }
}
