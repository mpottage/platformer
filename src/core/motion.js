//Copyright Matthew Pottage 2019.
import {Platform} from './platforms.js';
//Provides "Physics" engine for an object. Requires that handleCollision and
//handleMoved events are forwarded.
//The object has an initial velocity vector, and then falls under gravity.
//Typically a member variable, name "motion".
export class FreeFallMotion {
    //[xy]FrictionMult: Non-zero multiplier to apply to all friction values.
    constructor(xFrictionMult, yFrictionMult) {
        this.state = new MotionState();
        //Separates the parameters used for motion, for cleaner code.
        //State names are of the form "[surface]/[fluid]".
        //Known surfaces: ground, spring, none.
        //Known fluids: air, water.
        this.stateParams = {
            "ground/air": {
                xFriction: 4000, yFriction: 0
            },
            "ground/water": {
                xFriction: 5000, yFriction: 400
            },
            "none/air": {
                xFriction: 0, yFriction: 0
            },
            "none/water": {
                xFriction: 400, yFriction: 400
            },
            "ice/air": {
                xFriction: 100, yFriction: 0
            },
        };
        this.stateParams["spring/air"] = this.stateParams["ground/air"];
        this.stateParams["spring/water"] = this.stateParams["ground/water"];
        this.stateParams["ice/water"] = this.stateParams["none/water"];
        this.x = 0;
        this.y = 0;
        this.accel = {x: 0, y: 0}; //(internal) forced acceleration to apply.
        this.current = {x: 0, y: 0}; //(external) acceleration due to fluids.
        this.touching = {down: null};
        this.movingFloor = null;
        this.prevFloorMotion = {x: 0};
        this.frictionMultiplier = {x: xFrictionMult||1, y: yFrictionMult||1};
    }
    reset() {
        this.x = this.y = this.accel.x = this.accel.y = 0;
        this.current.x = this.current.y = 0;
        this.touching.down = false;
        this.movingFloor = null;
        this.prevFloorMotion.x = 0;
        this.state.reset();
    }
    //Changes velocity based on hitting an object.
    //If it hits an object that pushes (or can push) it, it takes the x or y velocity
    //of the pushing object or if it can't push a static object it sets the
    //relevant velocity to 0.
    handleCollision(toObj, collided, details) {
        if(details.right) {
            if(collided.pushRank<=toObj.pushRank) {
                if(collided.pushRank<toObj.pushRank && collided.motion) {
                    if(this.x>0)
                        this.x = collided.motion.x;
                    else
                        this.x = Math.min(collided.motion.x, this.x);
                }
                else if(this.x>0)
                    this.x = 0;
            }
        }
        else if(details.left) {
            if(collided.pushRank<=toObj.pushRank) {
                if(collided.pushRank<toObj.pushRank && collided.motion) {
                    if(this.x<0)
                        this.x = collided.motion.x;
                    else
                        this.x = Math.max(collided.motion.x, this.x);
                }
                else if(this.x<0)
                    this.x = 0;
            }
        }
        //Aquires y-velocity from the object it landed on.
        else if(details.down) {
            this.touching.down = true;
            if(collided.pushRank<=toObj.pushRank) {
                if(collided.pushRank<toObj.pushRank && collided.motion) {
                    if(collided instanceof Platform)
                        this.movingFloor = collided;
                    if(this.y>0)
                        this.y = collided.motion.y;
                    else
                        this.y = Math.min(collided.motion.y, this.y);
                }
                else if(this.y>0)
                    this.y = 0;
            }
        }
        else if(details.up) {
            if(collided.pushRank<=toObj.pushRank) {
                if(collided.pushRank<toObj.pushRank && collided.motion) {
                    if(this.y<0)
                        this.y = collided.motion.y;
                    else
                        this.y = Math.max(collided.motion.y, this.y);
                }
                else if(this.y<0)
                    this.y = 0;
            }
        }
    }
    //Set velocity to 0 in the direction not moved.
    handleMoved(obj, details, amount) {
        if(amount==0) {
            if(details.right || details.left)
                this.x = 0;
            else
                this.y = 0;
        }
    }
    //Updates velocity of obj and queues the correct motion with collisions.
    update(obj, interval, collisions, environment) {
        this.setState();
        var prevMotion = {x: this.x, y: this.y};
        this.updateX(interval, environment);
        this.updateY(interval, environment);
        collisions.queue(obj, (this.x+prevMotion.x)/2*interval,
                (this.y+prevMotion.y)/2*interval);
        this.movingFloor = null;
        this.touching.down = false;
        this.state.reset();
        this.current.x = this.current.y = 0;
    }
    //Detects the correct state.
    //   If the surface/fluid has already been changed this does nothing (e.g.
    //   so player moves slower in water).
    setState() {
        if(this.touching.down)
            this.state.setSurface("ground");
    }
    updateX(interval, environment) {
        var floorRMotion = this.x-this.prevFloorMotion.x;
        var fx = (this.movingFloor && this.movingFloor.motion.x) || 0;
        var params = this.stateParams[this.state.get()];
        var xFriction = params.xFriction*this.frictionMultiplier.x;
        var xAccel = this.accel.x;
        //Current doesn't ignore friction so we need to consider its impact.
        if(this.current.x!=0) {
            //Current assists friction.
            if(xAccel==0 && (
                       (this.current.x<0 && floorRMotion>0)
                    || (this.current.x>0 && floorRMotion<0)
                    ))
                xFriction += Math.abs(this.current.x);
            //Acceleration (which always overcomes it) in same direction.
            if((this.current.x>0 && this.accel.x>0)
                    || (this.current.x<0 && this.accel.x<0))
                xAccel += this.current.x;
            //Current overwheming friction.
            else if(Math.abs(this.current.x)>xFriction) {
                if(this.current.x>0)
                    xAccel += (this.current.x-xFriction);
                else
                    xAccel += (this.current.x+xFriction);
            }
        }
        //Accelerate to floor (if any) velocity and retaining previous velocity
        //relative to the floor where reasonable.
        //This gives deceleration when stopped also.
        if(xFriction!=0) {
            var recoverRFloor = 0; //Velocity relative to floor to retain.
            if((xAccel<0 && floorRMotion<0)
                    || (xAccel>0 && floorRMotion>0))
                recoverRFloor = floorRMotion;
            else
                xFriction = xFriction+Math.abs(xAccel);
            //Calculate "time" used up to accelerate to the floor's velocity.
            var tLeft = interval-Math.abs((this.x-fx-recoverRFloor)/xFriction);
            if(tLeft<0) { //Can't match floor speed.
                if(this.x<fx)
                    this.x += xFriction*interval;
                else if(this.x>fx)
                    this.x -= xFriction*interval;
            }
            else //Matched floor speed
                this.x = fx+recoverRFloor;
            //Accelerate normally (value for xAccel includes friction)
            this.x += xAccel*interval;
        }
        //Accelerates if already moving in the same direction or forced to by
        //current, or air control is permitted.
        else if(environment.get("air control")
                || (this.x>=fx && (xAccel>fx || this.current.x<0))
                || (this.x<=fx && (xAccel<fx || this.current.x>0)))
            this.x += xAccel*interval;
        this.prevFloorMotion.x = fx;
    }
    updateY(interval, environment) {
        this.y += (environment.get("gravity")+this.accel.y+this.current.y)*interval;
        var frictionChange = interval*this.stateParams[this.state.get()].yFriction;
        frictionChange *= this.frictionMultiplier.y;
        if(this.y>0) {
            if(frictionChange<this.y)
                this.y -= frictionChange;
            else
                this.y = 0;
        }
        if(this.y<0) {
            if(frictionChange<-this.y)
                this.y += frictionChange;
            else
                this.y = 0;
        }
    }
}

//Manages combining motion states.
// Typically used as a contained object of a FreeFallMotion instance, so to
// access use obj.motion.state.
export class MotionState {
    constructor() {
        this.reset();
    }
    //Defaults to none/air.
    reset() {
        this._surface = "none";
        this._fluid = "air";
    }
    //Change the surface (e.g. ground/none) if it hasn't already been changed.
    setSurface(val) {
        if(this._surface=="none")
            this._surface = val;
    }
    //Change the fluid (air/water) if it hasn't already been changed.
    setFluid(val) {
        if(this._fluid=="air")
            this._fluid = val;
    }
    //Get the name of the current state, format "[surface]/[fluid]".
    get() {
        return this._surface+"/"+this._fluid;
    }
}

//See FreeFallMotion.
//Object is controlled, goes left, right and jumps (e.g. Player).
export class ControlledMotion extends FreeFallMotion {
    constructor(xMultiplier, yMultiplier, xFM, yFM) {
        super(xFM, yFM);
        //Multiple of x/y state params to use
        this.multiplier = {x: xMultiplier || 1, y: yMultiplier || 1};
        this.speedMod = {x: 1, y: 1};
        this.action = {
            left: false,
            right: false,
            jump: false
        };
        //fluid runAir is for when the object was moving quickly when it jumped.
        this.stateParams = {"ground/air": {
                xAccel: 200, xFriction: 4000, xMaxSpeed: 600, jumpVelocity: 555,
                yFriction: 0
            },
            "ground/water": {
                xAccel: 100, xFriction: 5000, xMaxSpeed: 200, jumpVelocity: 440,
                yFriction: 400
            },
            "spring/air": {
                xAccel: 200, xFriction: 4000, xMaxSpeed: 600, jumpVelocity: 0,
                yFriction: 0
            },
            "spring/water": {
                xAccel: 100, xFriction: 5000, xMaxSpeed: 200, jumpVelocity: 0,
                yFriction: 400
            },
            "none/air": {
                xAccel: 150, xFriction: 0, xMaxSpeed: 200, jumpVelocity: 0,
                yFriction: 0
            },
            "none/runAir": {
                xAccel: 150, xFriction: 0, xMaxSpeed: 350, jumpVelocity: 0,
                yFriction: 0
            },
            "none/specialAir": {
                xAccel: 1200, xFriction: 1, xMaxSpeed: 700, jumpVelocity: 0,
                yFriction: 0
            },
            "none/iceAir": {
                xAccel: 150, xFriction: 0, xMaxSpeed: 600, jumpVelocity: 0,
                yFriction: 0
            },
            "none/water": {
                xAccel: 100, xFriction: 400, xMaxSpeed: 200, jumpVelocity: 440,
                yFriction: 400
            },
            "ice/air": {
                xAccel: 300, xFriction: 100, xMaxSpeed: 800, jumpVelocity: 420,
                yFriction: 0
            },
        };
        this.stateParams["ice/water"] = this.stateParams["none/water"];
        //Shouldn't be needed:
        this.stateParams["spring/runAir"] = this.stateParams["spring/air"];
        this.stateParams["ground/runAir"] = this.stateParams["ground/air"];
        this.stateParams["ice/runAir"] = this.stateParams["ice/air"];
        this.stateParams["spring/iceAir"] = this.stateParams["spring/air"];
        this.stateParams["ground/iceAir"] = this.stateParams["ground/air"];
        this.stateParams["ice/iceAir"] = this.stateParams["ice/air"];
        this.stateParams["spring/specialAir"] = this.stateParams["spring/air"];
        this.stateParams["ground/specialAir"] = this.stateParams["ground/air"];
        this.stateParams["ice/specialAir"] = this.stateParams["ice/air"];
    }
    reset() {
        super.reset();
        this.speedMod.x = 1;
        this.speedMod.y = 1;
    }
    update(obj, interval, collisions, environment) {
        this.setState();
        //Use values of this.action to set this.accel.x and this.y.
        var params = this.stateParams[this.state.get()];
        if(this.action.left)
            this.accel.x = -params.xAccel*this.multiplier.x*this.speedMod.x;
        else if(this.action.right)
            this.accel.x = +params.xAccel*this.multiplier.x*this.speedMod.x;
        else
            this.accel.x = 0;
        if(this.action.jump && this.touching.down)
            this.y -= params.jumpVelocity*this.multiplier.y*this.speedMod.y;
        super.update(obj, interval, collisions, environment);
    }
    updateX(interval, environment) {
        super.updateX(interval, environment);
        //From FreeFallMotion.prototype.updateX().
        var fx = (this.movingFloor && this.movingFloor.motion.x) || 0;
        //Limit speed, uses stateParams to enforce the appropriate limit. This
        //results in different limits when, e.g., on the ground or airborne.
        var xMaxSpeed = this.stateParams[this.state.get()].xMaxSpeed*this.multiplier.x*this.speedMod.x;
        if(this.x-fx>xMaxSpeed)
            this.x = fx+xMaxSpeed;
        else if(this.x-fx<-xMaxSpeed)
            this.x = fx-xMaxSpeed;
    }
    setState() {
        super.setState();
        if(!this.touching.down) {
            var absX = Math.abs(this.x);
            if(absX>this.stateParams["ground/air"].xMaxSpeed)
                this.state.setFluid("iceAir");
            else if(absX>this.stateParams["none/air"].xMaxSpeed)
                this.state.setFluid("runAir");
        }
    }
    setSpeedMod(xMult, yMult) {
        this.speedMod.x *= xMult;
        this.speedMod.y *= yMult;
    }
}

//Provides motion of an object that rebounds with a fixed coefficient of
//restitution off every object it hits.
//Requires "handleMoved" events are forwarded. See FreeFallMotion.
export class ReboundFreeFall extends FreeFallMotion {
    constructor(rebound, xFM, yFM) {
        super(xFM, yFM);
        this.rebound = rebound || .2; //Coefficient of restitution.
        //Last known valid velocity. Used to calculate rebounds.
        this.prevY = 0;
        this.prevCurrentY = 0;
        this.prevGravity = 0;
    }
    update(obj, interval, collisions, environment) {
        this.prevY = this.y;
        this.prevCurrentY = this.current.y;
        this.prevGravity = environment.get("gravity");
        super.update(obj, interval, collisions, environment);
    }
    reset() {
        super.reset();
        this.prevY = 0;
        this.prevCurrentY = 0;
    }
    handleMoved(toObj, details, amount) {
        //Set velocity to the velocity that this would have been at to move
        //amount while accelerating due to gravity.
        if(amount!=0 && details.down && this.y>0) {
            var yAccel = this.prevGravity+this.prevCurrentY+this.accel.y;
            this.y = Math.sqrt(Math.abs(this.prevY*this.prevY+2*yAccel*amount));
        }
        else
            super.handleMoved(toObj, details, amount);
    }
    handleCollision(toObj, collided, details) {
        var prevMotion = {x: this.x, y: this.y};
        super.handleCollision(toObj, collided, details);
        if(collided.pushRank<=toObj.pushRank) {
            //Bounce relative to objects hit.
            var baseX = 0;
            var baseY = 0;
            if(collided.motion && collided.pushRank<toObj.pushRank) {
                baseX = collided.motion.x;
                baseY = collided.motion.y;
            }
            if((details.left && prevMotion.x<baseX) ||
                    (details.right && prevMotion.x>baseX))
                this.x -= (prevMotion.x-baseX)*this.rebound;
            else if((details.up && prevMotion.y<baseY)
                    || (details.down && prevMotion.y>baseY))
                this.y -= (prevMotion.y-baseY)*this.rebound;
        }
    }
}

//Motion for object hovering. See ReboundFreeFall.
//  > Gravity is ignored (technically counteracted).
//  > Can set the object to accelerate in a given direction (this.action).
export class HoverMotion extends ReboundFreeFall {
    //xMult, yMult: Multiplies to apply to x/y acceleration.
    //rebound: See ReboundFreeFall,
    //xFM, yFM: See FreeFallMotion [xy]FrictionMultiplers.
    constructor(xMult, yMult, rebound, xFM, yFM) {
        super(rebound, xFM, yFM);
        this.multiplier = {x: xMult||1, y: yMult||1};
        this.speedMod = {x: 1, y: 1};
        //Used to specify directions in which to accelerate.
        this.action = {left: false, right: false, up: false, down: false};
        this.defaultAccel = 60;
        //Set friction to slow down object faster that it accelerated.
        this.stateParams = {
            "ground/air": {
                xFriction: 4000+this.defaultAccel*3, yFriction: this.defaultAccel*2,
            },
            "ground/water": {
                xFriction: 5000+this.defaultAccel*3, yFriction: 400+this.defaultAccel*2,
            },
            "none/air": {
                xFriction: this.defaultAccel*3, yFriction: this.defaultAccel*2,
            },
            "none/water": {
                xFriction: 400+this.defaultAccel*3, yFriction: 400+this.defaultAccel*2,
            },
            "ice/air": {
                xFriction: 100+this.defaultAccel*3, yFriction: this.defaultAccel*2,
            },
        };
        this.stateParams["spring/air"] = this.stateParams["ground/air"];
        this.stateParams["spring/water"] = this.stateParams["ground/water"];
        this.stateParams["ice/water"] = this.stateParams["none/water"];
    }
    reset() {
        super.reset();
        this.speedMod.x = 1;
        this.speedMod.y = 1;
    }
    update(obj, interval, collisions, environment) {
        this.setState();
        var params = this.stateParams[this.state.get()];
        this.accel.x = 0;
        this.accel.y = -environment.get("gravity"); //Cancel gravity.
        //Set acceleration to appropriate direction (cancelling as necessary).
        if(this.action.left)
            this.accel.x -= this.defaultAccel*this.multiplier.x*this.speedMod.x;
        if(this.action.right)
            this.accel.x += this.defaultAccel*this.multiplier.x*this.speedMod.x;
        if(this.action.up)
            this.accel.y -= params.yFriction+this.defaultAccel*this.multiplier.y*this.speedMod.y;
        if(this.action.down)
            this.accel.y += params.yFriction+this.defaultAccel*this.multiplier.y*this.speedMod.y;
        super.update(obj, interval, collisions, environment);
    }
    clearActions() {
        this.action.left = this.action.right = false;
        this.action.down = this.action.up = false;
    }
    //Set actions so that from would acclerate to target with this motion.
    targetActions(from, target) {
        this.action.left = target.box.x+target.box.width<from.box.x;
        this.action.right = target.box.x>from.box.x+from.box.width;
        this.action.up = target.box.y+target.box.height*.7<from.box.y;
        this.action.down = target.box.y+target.box.height*.3>from.box.y+from.box.height;
    }
    setSpeedMod(xMult, yMult) {
        this.speedMod.x *= xMult;
        this.speedMod.y *= yMult;
    }
}
