//Copyright Matthew Pottage 2019.
import {screenHeight} from './utility.js';
//Responsive canvas (resize to fit).
// This ensures that 1 CSS px is the same as 1px on the canvas, to avoid
// pixellation, and that HiDPI screens are supported.
// The main game uses "game pixels", where 600 game pixels are equivalent to the
// height of the game canvas - the "game pixels" are translated to canvas pixels
// by applying a scale factor to the canvas before drawing (see main).
export function resizeCanvas(canvas) {
    //Scale to support HiDPI screens.
    var ratio = window.devicePixelRatio || 1;
    var style = window.getComputedStyle(canvas);
    canvas.width = parseInt(style.width.replace("px$",""), 10)*ratio;
    canvas.height = parseInt(style.height.replace("px$",""), 10)*ratio;
    return canvas.height/screenHeight;
}
