//Copyright Matthew Pottage 2019.
import {Collidable, registerCDU} from './collisions/collidable.js';
import {Box} from './utility.js';
import {Platform} from './platforms.js';
import {Player} from './player/player.js';

//A laser (common base class).
//  > Beam color and width is set by constructor.
//  > The beam tracks (moves with) a collidable (1-2 frame lag).
//  > Beam goes either left, right, up or down and is blocked by solid objects.
//  > Derived lasers should change "handleIntersection" and "update" to set
//    behaviour.
export class Laser extends Collidable {
    //obj: Object the laser moves around with (Collidable required).
    //vec: {x:-1,y:0}, {x:0,y:-1}, {x:1,y:0}, {x:0,y:1}, {x:0,y:0}.
    //targetDim: Ideal beam width/height.
    //rangeLimit: Limit of how far the laser can reach (normally about 
    constructor(obj, vec, targetDim, beamColor, rangeLimit=25000) {
        super();
        if(!(obj instanceof Collidable))
            return new TypeError("Collidable required.");
        this.follow = obj; //Collidable to follow.
        //Check that vec is an acceptable direction.
        var accepted = [-1, 0, 1];
        if((vec.x!=0 && vec.y!=0) || accepted.indexOf(vec.x)==-1
                || accepted.indexOf(vec.y)==-1)
            throw new Error("Bad laser vector.");
        this.dirVec = vec;
        this.targetDim = targetDim;
        this.beamColor = beamColor;
        this.rangeLimit = rangeLimit;
        this.pushRank = 4000; //Beam can be "pushed" by anything.
        this.box = new Box(0, 0, this.targetDim, this.targetDim);
        //Prevent objects passing through the beam undetected.
        this.bar = new IntersectionCallback(new Box(0,0,0,0),
                this.handleIntersection.bind(this));
        this._setDimensions();
        //Position reached by beam last frame.
        this.prevLimPos = {x: this.box.x, y: this.box.y};
        this._centerObj();
        //Last adjusted center position.
        this.prevAdjPos = {x: this.box.x, y: this.box.y};
        this.activated = true;
    }
    reset() {
        this.follow.reset();
        this._centerObj();
        this.prevLimPos = {x: this.box.x, y: this.box.y};
        this.prevAdjPos = {x: this.box.x, y: this.box.y};
        this.bar.box.x = this.box.x;
        this.bar.box.y = this.box.y;
        this.bar.box.width = this.box.width;
        this.bar.box.height = this.box.height;
        this.activated = true;
        this.reachedLimit = true;
    }
    //Set size of laser beam to be contained in this.follow.
    _setDimensions() {
        this.box.width = Math.min(this.follow.box.width*.9, this.targetDim);
        this.box.height = Math.min(this.follow.box.height*.9, this.targetDim);
        this.bar.box.width = this.box.width;
        this.bar.box.height = this.box.height;
    }
    //Center the beam's start on this.follow.
    _centerObj() {
        this.box.x = this.follow.box.x+this.follow.box.width/2-this.box.width/2;
        this.box.y = this.follow.box.y+this.follow.box.height/2-this.box.height/2;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
        this.follow.register(collisions, manager, caches);
        this.bar.register(collisions, manager, caches);
    }
    handleCollision(obj, details) {
        this.reachedLimit = false;
    }
    isCollision(obj) {
        return obj!==this.follow && (obj instanceof Platform)
            && this.follow.isCollision(obj);
    }
    updateBar() {
        //Shifts and resizes this.bar using the shortest extent of the beam
        //(from last frame and the frame before) as appropriate.
        if(this.dirVec.x!=0) {
            if(this.dirVec.x<0) {
                this.bar.box.x = Math.max(this.prevLimPos.x, this.box.x);
                this.bar.box.width = this.prevAdjPos.x+this.box.width
                    -this.bar.box.x;
            }
            else {
                this.bar.box.x = this.prevAdjPos.x;
                this.bar.box.width = Math.min(this.prevLimPos.x, this.box.x)
                    +this.box.width-this.prevAdjPos.x;
            }
            this.bar.box.y = Math.min(this.prevAdjPos.y, this.prevLimPos.y);
            this.bar.box.height = Math.max(this.prevAdjPos.y, this.prevLimPos.y)
                +this.box.height-this.bar.box.y;
        }
        else if(this.dirVec.y!=0) {
            if(this.dirVec.y<0) {
                this.bar.box.y = Math.max(this.prevLimPos.y, this.box.y);
                this.bar.box.height = this.prevAdjPos.y+this.box.height
                    -this.bar.box.y;
            }
            else {
                this.bar.box.y = this.prevAdjPos.y;
                this.bar.box.height = Math.min(this.prevLimPos.y,
                        this.box.y)+this.box.height-this.prevAdjPos.y;
            }
            this.bar.box.x = Math.min(this.prevAdjPos.x, this.prevLimPos.x);
            this.bar.box.width = Math.max(this.prevAdjPos.x, this.prevLimPos.x)
                +this.box.width-this.bar.box.x;
        }
    }
    update(interval, collisions, manager, caches) {
        this.updateBar();
        this.prevLimPos = {x: this.box.x, y: this.box.y};
        this._centerObj();
        this.prevAdjPos = {x: this.box.x, y: this.box.y};
        this.reachedLimit = true;
        //Switch off if followed object is disabled.
        this.disabled = this.bar.disabled = this.follow.disabled;
        if(this.activated && !this.disabled)
            collisions.queue(this, this.dirVec.x*this.rangeLimit,
                    this.dirVec.y*this.rangeLimit);
        if(this.follow.activated!==undefined) //Forward activated state.
            this.follow.activated = this.activated;
    }
    draw(context) {
        if(this.activated && !this.disabled) {
            //Draw the beam from the laser, which extends up to this.box.
            var prevGA = context.globalAlpha;
            context.globalAlpha *= .7;
            context.strokeStyle = this.beamColor;
            var origin = this.follow.box.center;
            context.beginPath();
            context.moveTo(origin.x, origin.y);
            var prevCap = context.lineCap;
            //Round end when limit is reached without hitting a barrier
            if(this.reachedLimit)
                context.lineCap = "round";
            else
                context.lineCap = "square";
            if(this.dirVec.x!=0) {
                context.lineWidth = this.box.height;
                if(this.dirVec.x<0)
                    context.lineTo(this.box.x+this.box.height/2, origin.y);
                else if(this.dirVec.x>0)
                    context.lineTo(this.box.x+this.box.width-this.box.height/2,
                        origin.y);
            }
            else if(this.dirVec.y!=0) {
                context.lineWidth = this.box.width;
                if(this.dirVec.y<0)
                    context.lineTo(origin.x, this.box.y+this.box.width/2);
                else if(this.dirVec.y>0)
                    context.lineTo(origin.x,
                        this.box.y+this.box.height-this.box.width/2);
            }
            context.stroke();
            context.lineCap = prevCap;
            context.globalAlpha = prevGA;
        }
    }
}

//Laser used to "cut" through objects.
//  > Has a thick orange beam and objects will lose health on intersecting it.
export class CuttingLaser extends Laser {
    constructor(obj, vec, rangeLimit) {
        super(obj, vec, 10, "#ff8b00", rangeLimit);
    }
    handleIntersection(obj) {
        if(this.activated && obj.health && obj!==this.follow)
            obj.health.increase(-1);
    }
}

//A trip-wire laser.
//  > A thin red beam
//  > Sets a flag to true when a player intersects the beam.
export class TriggerLaser extends Laser {
    //name: Flag to set.
    constructor(name, obj, vec, rangeLimit) {
        super(obj, vec, 6, "#f00", rangeLimit);
        this.name = name;
    }
    reset() {
        super.reset();
        this.hit = false; //Intersected the player?
    }
    handleIntersection(obj) {
        if(this.activated && (obj instanceof Player))
            this.hit = true;
    }
    update(interval, collisions, manager, caches, environment) {
        manager.setFlag(this.name, this.hit);
        this.hit = false;
        super.update(interval, collisions, manager, caches, environment);
    }
}

//Used to prevent phasing through laser without impact.
class IntersectionCallback extends Collidable {
    //func: Function to call when an object intersects this.
    constructor(box, func) {
        super();
        this.box = box;
        this.func = func;
        this.disabled = false;
    }
    reset() {}
    register(collisions, manager, caches) {
        collisions.add(this);
    }
    isCollision(obj) { return false; }
    handleIntersection(obj) {
        this.func(obj);
    }
}
