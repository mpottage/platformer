//Level camera. Provides both side-scrolling and up/down scrolling.  To apply
//the effects of the camera to the viewport use apply(context, ...).
export class Camera {
    constructor(level) {
        //Offsets of the start of the level from the camera.
        this.offsets = {x: 0, y: 0};
        this.scale = 1;
        this.level = level;
        this.targetYOffset = 0;
         //Set if the obj almost fell offscreen (up==-1 or down==1).
        this.objFalling = 0;
         //obj's offset from the top of the viewport.
        this.prevViewportOffsetY = 0;
        this.prevObjY = 0; //Last known y-position of obj (from update).
        this.wasGrounded = false;
    }
    //Positions the camera (by adjusting this.offsets) such that obj (eg.
    //Player) is the focus.
    //Should be called AFTER the collisions detection and resolution has taken
    //place.
    // viewportWidth: Screen width (in game pixels)
    // viewportHeight: Screen height (in game pixels)
    update(obj, viewportWidth, viewportHeight, interval,
            collisions) {
        this.updateX(obj, viewportWidth/this.level.scale,
            viewportHeight/this.level.scale);
        this.updateY(obj, viewportWidth/this.level.scale,
            viewportHeight/this.level.scale, interval);
    }
    //Move the camera along the x-axis.
    updateX(obj, viewportWidth, viewportHeight) {
        if(viewportWidth>=this.level.width)
            this.offsets.x = 0;
        else {
            var partWidth = 4*viewportWidth/10;
            if(obj.box.x>this.level.width-partWidth)
                this.offsets.x = viewportWidth-this.level.width;
            else if(obj.box.x<=partWidth)
                this.offsets.x = 0;
            else if(this.offsets.x+obj.box.x>=viewportWidth-partWidth)
                this.offsets.x = viewportWidth - partWidth - obj.box.x;
            else if(this.offsets.x+obj.box.x<=partWidth)
                this.offsets.x = partWidth - obj.box.x;
        }
    }
    //Move the camera along the y-axis.
    updateY(obj, viewportWidth, viewportHeight, interval) {
        var level = this.level;
        //Almost nothing to do.
        if(level.height<=viewportHeight) {
            this.targetYOffset = this.offsets.y = 0;
            return;
        }
        //Landing on a platform.
        else if(obj.touching && obj.touching.down) {
            this.objFalling = 0; //Not falling (now grounded).
            var platform = obj.touching.down;
            var halfViewport = viewportHeight*0.5;
            //Place the platform landed on a 50% down the screen.
            this.targetYOffset = -platform.box.y+halfViewport;
            //Ensure moving platforms are also are placed 50% down the screen.
            if(this.wasGrounded && this.prevObjY+obj.box.height>=halfViewport
                    && this.prevObjY+obj.box.height<level.height-halfViewport)
                this.offsets.y = this.prevViewportOffsetY-obj.box.y;
        }
        else if(obj.teleporter && obj.teleporter.justUsed) {
            this.targetYOffset = this.offsets.y = -obj.box.y-obj.box.height+viewportHeight*0.5;
        }
        //About to start falling offscreen (80-90% down), going up to high (top
        //5%) or already attempted to fall offscreen/go too high.
        //  This ensures that obj remains/moves to the top 20-30% (going down)
        //  or bottom 20%-30% (going up) of the screen
        else if(this.prevObjY<obj.box.y && (this.objFalling==1
                || (obj.box.y+obj.box.height+this.offsets.y>viewportHeight*0.9))) {
            this.objFalling = 1;
            var baseY = obj.box.y+obj.box.height;
            //The camera offset is adjusted to maintain obj's last offset from
            //the top of the screen (moving it to around 20% down the screen).
            this.targetYOffset = viewportHeight*0.3-baseY;
            this.offsets.y = this.prevViewportOffsetY-obj.box.y;
        }
        else if(this.prevObjY>obj.box.y && (this.objFalling==-1
                || (obj.box.y+this.offsets.y<viewportHeight*0.05))) {
            this.objFalling = -1;
            //The obj is moved to around 30% up the screen.
            this.targetYOffset = viewportHeight*0.6-obj.box.y;
            this.offsets.y = this.prevViewportOffsetY-obj.box.y;
        }
        else
            this.objFalling = 0;
        this.clipYOffsets(viewportHeight);
        this.animateYOffset(obj, interval); //Glide to the new offset.
        this.prevViewportOffsetY =
            Math.min(Math.max(0, this.offsets.y+obj.box.y), viewportHeight);
        this.prevObjY = obj.box.y;
        this.wasGrounded = !!obj.touching.down;
    }
    clipYOffsets(viewportHeight) {
        //Ensure the view doesn't leave the level (either by looking too high up
        //or low down).
        var minOffset = -this.level.height+viewportHeight;
        var maxOffset = 0;
        //Ensure view stays on the level.
        if(this.targetYOffset>maxOffset)
            this.targetYOffset = maxOffset;
        else if(this.targetYOffset<minOffset)
            this.targetYOffset = minOffset;
        if(this.offsets.y>maxOffset)
            this.offsets.y = maxOffset;
        else if(this.offsets.y<minOffset)
            this.offsets.y = minOffset;
    }
    //Provides a smooth transition to the target y offset.
    animateYOffset(obj, interval) {
        if(this.targetYOffset!=this.offsets.y) {
            var offsetDiff = this.targetYOffset-this.offsets.y;
            if(Math.abs(offsetDiff)<=1) //At the offset.
                this.offsets.y = this.targetYOffset;
            else
                //Change proportional to the difference between the current
                //offset to the target offset.
                this.offsets.y += offsetDiff*Math.min(1, interval*4);
        }
    }
    //Assumes that the transformations caused by the Camera when apply was last
    //called have been reversed;
    apply(context) {
        if(this.level.scale!=1)
            context.scale(this.level.scale, this.level.scale);
        context.translate(this.offsets.x, this.offsets.y);
    }
    reset() {
        this.offsets.x = 0;
        this.offsets.y = 0;
        this.targetYOffset = 0;
        this.objFalling = 0;
        this.prevViewportOffsetY = 0;
        this.prevObjY = 0;
    }
    //Changes the level that the Camera targets and resets the camera.
    changeLevel(toLevel) {
        this.reset();
        this.level = toLevel;
    }
}
