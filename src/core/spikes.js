//Copyright Matthew Pottage 2019-2020.
import {Collidable} from './collisions/collidable.js';
import {to3dp} from './utility.js';

export const defaultSpikeColor = "#868686";
//A row of spikes, pointing up (default) or down.
//The spikes are generated to fit the box provided, and have their width
//approximate targetWidth as well as possible.
export class SpikeRow extends Collidable {
    //dir: -1/<=0 is point up, 1/>0 is point down.
    constructor(box, targetWidth, color, dir) {
        super()
        this.targetWidth = targetWidth || 10;
        this.color = color || defaultSpikeColor;
        this.dir = dir || -1;
        this.box = box;
        this._setSpikeWidth(this.targetWidth);
    }
    handleCollision(obj) {
        if(obj.health)
            obj.health.increase(-1);
    }
    draw(context) {
        var baseY = this.box.y;
        var spikeY = this.box.y;
        if(this.dir<=0)
            baseY += this.box.height;
        else
            spikeY += this.box.height;
        context.beginPath();
        context.moveTo(this.box.x, baseY);
        for(var i=this.eachWidth; to3dp(i)<=this.box.width; i+=this.eachWidth) {
            var endX = this.box.x+i;
            context.lineTo(endX-this.eachWidth/2, spikeY);
            context.lineTo(endX, baseY);
        }
        context.fillStyle = this.color;
        context.fill();
    }
    _setSpikeWidth(targetWidth) {
        //Determine the width for each spike so that the box is filled.
        var numNeeded = Math.floor(this.box.width/targetWidth) || 1;
        this.eachWidth = this.box.width/numNeeded;
    }
    reset() { }
}

//A column of spikes, pointing either left (default) or right.
//The spikes are generated to fit the box provided, and have their width
//approximate targetHeight as well as possible.
export class SpikeColumn extends Collidable {
    //dir: -1/<=0 is point left, 1/>0 is point right.
    constructor(box, targetHeight, color, dir) {
        super()
        this.targetHeight = targetHeight || 10;
        this.color = color || defaultSpikeColor;
        this.box = box;
        this.dir = dir || -1;
        this._setSpikeHeight(this.targetHeight);
    }
    handleCollision(obj) {
        if(obj.health)
            obj.health.increase(-1);
    };
    draw(context) {
        var baseX = this.box.x;
        var spikeX = this.box.x;
        if(this.dir<=0)
            baseX += this.box.width;
        else
            spikeX += this.box.width;
        context.beginPath();
        context.moveTo(baseX, this.box.y);
        for(var i=this.eachHeight; to3dp(i)<=this.box.height; i+=this.eachHeight) {
            var endY = this.box.y+i;
            context.lineTo(spikeX, endY-this.eachHeight/2);
            context.lineTo(baseX, endY);
        }
        context.fillStyle = this.color;
        context.fill();
    };
    _setSpikeHeight(targetHeight) {
        //Determine the height for each spike so that the box is filled.
        var numNeeded = Math.floor(this.box.height/targetHeight) || 1;
        this.eachHeight = this.box.height/numNeeded;
    }
    reset() { }
}

//For GreySquare: Specialised upwards SpikeRow. When enabled it becomes disabled
//after 10s. Must be explicitly positioned AFTER construction to be enabled.
export class DeployedSpike extends SpikeRow {
    constructor(box, color=defaultSpikeColor) {
        super(box, box.width, color, -1);
        this.reset();
    }
    reset() {
        super.reset();
        this.disabled = true;
        this.untilDisabled = 0;
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        manager.addUpdate(this);
    }
    //Enables and positions the spike.
    position(x, y) {
        this.reset();
        this.untilDisabled = 10;
        this.disabled = false;
        //Assumes just changing the box position moves a SpikeRow.
        this.box.x = x;
        this.box.y = y;
    }
    update(interval) {
        if(!this.disabled) { //Times out spike.
            this.untilDisabled -= interval;
            if(this.untilDisabled<=0)
                this.disabled = true;
        }
    }
    draw(context, caches) {
        if(!this.disabled)
            super.draw(context, caches);
    }
}
