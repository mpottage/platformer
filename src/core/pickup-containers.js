//Copyright Matthew Pottage 2019.
import {Pickup} from './pickups/pickup.js';
import {BouncingPickup} from './pickups/bouncing.js';
import {Box, neededTextures} from './utility.js';
import {HealthComponent} from './components.js';
import {Platform} from './platforms.js';
import {StretchImageCache} from './render-caches.js';

//Centers obj's box within box and registers it.
function spawnCenter(box, obj, collisions, manager, caches) {
    obj.box.x = box.x+box.width/2-obj.box.width/2;
    obj.box.y = box.y+box.height/2-obj.box.height/2;
    obj.register(collisions, manager, caches);
}
export class PickupBox extends Platform {
    constructor(imageName, x, y, obj) {
        if(!(obj instanceof Pickup) && !(obj instanceof BouncingPickup))
            throw new TypeError("Require a Pickup or a BouncingPickup.");
        super(new Box(x, y, 90, 65), "brown");
        this.imageName = imageName;
        this.obj = obj;
        this.health = new HealthComponent(this, 1, 0);
        this.health.setCallback(function(o) { o.deployPickup = true; });
        this.reset();
    }
    reset() {
        this.health.reset();
        this.disabled = false;
        this.deployPickup = false;
        this.obj.reset();
    }
    draw(context, caches) {
        if(!this.disabled)
            caches.get(this.imageName).draw(context, this.box);
    }
    update(interval, collisions, manager, caches) {
        this.health.update(interval);
        if(!this.disabled) {
            if(this.deployPickup) {
                spawnCenter(this.box, this.obj, collisions, manager, caches);
                this.disabled = true;
            }
        }
    }
    register(collisions, manager, caches) {
        collisions.add(this);
        manager.addDraw(this);
        manager.addUpdate(this);
        if(!caches.get(this.imageName))
            caches.set(this.imageName, new StretchImageCache(this.imageName,
                        this.box.width, this.box.height));
        if(this.disabled) //Deployed contained pickup.
            this.obj.register(collisions, manager, caches);
    }
}

neededTextures.push("WoodenBox.svg");
export class WoodenBox extends PickupBox {
    constructor(x, y, obj) {
        super("WoodenBox.svg", x, y, obj);
    }
}
neededTextures.push("Present.svg");
export class Present extends PickupBox {
    constructor(x, y, obj) {
        super("Present.svg", x, y, obj);
    }
}

//Spawns a random pickup when first activated (activated by default unless
//it has an incoming link).
export class PickupSpawner {
    //objs: Pickups to randomly choose from when spawning an object.
    //box: Box such that its center is where the spawned object is placed.
    constructor(objs, box) {
        this.objs = objs;
        if(this.objs.length<1)
            throw new Error("Require at least one object.");
        this.objs.forEach(function(o) {
            if(!(o instanceof Pickup) && !(o instanceof BouncingPickup))
                throw new TypeError("Only Pickups and BouncingPickups permitted.");
        });
        this.box = box;
        this.reset();
    }
    reset() {
        this.activated = true;
        this.spawned = null; //Used to check if already spawned a pickup.
    }
    register(collisions, manager, caches) {
        manager.addUpdate(this);
        if(this.spawned) //Reregister spawned item (if any).
            this.spawned.register(collisions, manager, caches);
    }
    update(interval, collisions, manager, caches) {
        if(this.activated && !this.spawned) {
            this.activated = false;
            //Pick randomly.
            this.spawned = this.objs[Math.floor(this.objs.length*Math.random())];
            //Center the spawned object within this.box and register it.
            this.spawned.reset();
            spawnCenter(this.box, this.spawned, collisions, manager, caches);
        }
    }
    getOutputVal() {
        return this.spawned && this.spawned.disabled;
    }
}
