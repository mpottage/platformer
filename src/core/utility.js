//Copyright Matthew Pottage 2019.
export const screenHeight = 600;;
export let neededTextures = [];

//start: start value, end: end value, duration: time duration.
//Provides a cubic ease in transition. TODO, when needed, add more transitions.
//Call update to animate the value, current value accessible via property value.
export class Transition {
    constructor(start, end, duration) {
        this.start = start;
        this.change = end-start;
        this.value = start;
        this.duration = duration;
        this.elapsed = 0;
    }
    //interval: Time elapsed since last calling update.
    update(interval) {
        if(this.elapsed>=this.duration)
            return;
        this.elapsed+=interval;
        var proportion = this.elapsed/this.duration;
        this.value = this.start + this.change*proportion*proportion*proportion;
        if(this.elapsed>=this.duration)
            this.value = this.start+this.change;
    }
}
//Round val to 3 d.p.
//  Use: Game assumes all objects are at least 0.001px in dimension and hence
//  it is safe to, when checking for collisions, round coordinates to 3 d.p.,
//  which greatly reduces the impact of floating point rounding errors.
export function to3dp(val) {
    return Math.round(val*1000)/1000;
}
//Box: AABB, to be used in collision detection.
export class Box {
    constructor(x, y, width, height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    fill(context) {
        context.fillRect(this.x, this.y, this.width, this.height);
    }
    stroke(context) {
        context.strokeRect(this.x, this.y, this.width, this.height);
    }
    //Checks for an intersection between two boxes (accurate to 3 d.p.).
    static isIntersection(box1, box2) {
        return to3dp(box1.x+box1.width)>to3dp(box2.x) && to3dp(box1.x)<to3dp(box2.x+box2.width)
            && to3dp(box1.y+box1.height)>to3dp(box2.y) && to3dp(box1.y)<to3dp(box2.y+box2.height);
    }
    get center() {
        return {x: this.x+this.width/2, y: this.y+this.height/2};
    }
    get corners() {
        return [
            {x: this.x, y: this.y},
            {x: this.x, y: this.y+this.height},
            {x: this.x+this.width, y: this.y},
            {x: this.x+this.width, y: this.y+this.height}
        ];
    }
    //Assumes corner is a corner of this box.
    oppositeCorner(corner) {
        return this.corners.filter(c => c.x!=corner.x && c.y!=corner.y)[0];
    }
}

export function fillCircle(context, box) {
    var c = box.center;
    context.beginPath();
    context.arc(c.x, c.y, box.width/2, 0, Math.PI*2, true);
    context.fill();
}

//Triangle shape, points have coordinates (x1, y1), (x2, y2) and (x3, y3).
export class Triangle {
    constructor(x1, y1, x2, y2, x3, y3) {
        this.points = [{x: x1, y: y1}, {x: x2, y: y2}, {x: x3, y: y3}];
        this.box = new Box(0, 0, 0, 0);
        this.updateBox();
    }
    drawPath(context) {
        context.beginPath();
        context.moveTo(this.points[0].x, this.points[0].y);
        context.lineTo(this.points[1].x, this.points[1].y);
        context.lineTo(this.points[2].x, this.points[2].y);
        context.closePath();
    };
    //Translates the triangle by the vector (dx, dy).
    translate(dx, dy) {
        this.points.forEach(function(point) {
            point.x += dx;
            point.y += dy;
        });
        this.updateBox();
    };
    //Used internally by Triangle,
    // It updates the AABB to surround the triangle in its current new position.
    updateBox() {
        var minX = Math.min(this.points[0].x, this.points[1].x, this.points[2].x);
        var maxX = Math.max(this.points[0].x, this.points[1].x, this.points[2].x);
        var minY = Math.min(this.points[0].y, this.points[1].y, this.points[2].y);
        var maxY = Math.max(this.points[0].y, this.points[1].y, this.points[2].y);
        this.box.x = minX;
        this.box.y = minY;
        this.box.width = maxX-minX;
        this.box.height = maxY-minY;
    };
    //Checks whether the triangle intersects the Box box.
    //  Uses Separating Axis Theorem to check for intersection.
    isIntersectionBox(box) {
        if(Box.isIntersection(this.box, box)) {
            var axises = [];
            var points = this.points;
            for(var i=0; i<points.length-1; ++i) {
                axises.push({x: points[i].y-points[i+1].y, y: points[i+1].x-points[i].x});
            }
            axises.push({x: points[0].y-points[2].y, y: points[2].x-points[0].x});
            for(var i=0; i<axises.length; ++i) {
                var axis = axises[i];
                var boxProjs = [
                    axis.x*box.x+axis.y*box.y,
                    axis.x*(box.x+box.width)+axis.y*box.y,
                    axis.x*(box.x+box.width)+axis.y*(box.y+box.height),
                    axis.x*box.x+axis.y*(box.y+box.height)
                    ];
                var thisProjs = [];
                points.forEach(function(point) {
                    thisProjs.push(axis.x*point.x+axis.y*point.y);
                });
                thisProjs = thisProjs.sort(function(a, b) { return a-b;});
                boxProjs = boxProjs.sort(function(a, b) { return a-b;});
                if(!(thisProjs[0]<boxProjs[boxProjs.length-1] && thisProjs[thisProjs.length-1]>boxProjs[0])
                        && !(boxProjs[0]<thisProjs[thisProjs.length-1] && boxProjs[boxProjs.length-1]>thisProjs[0]))
                    return false;
            }
            return true;
        }
        return false;
    }
}

//Used to reduce object allocation by reusing objects.
export class Pool {
    constructor(contained) {
        if(contained.length==0)
            throw new Error("Non-empty list of elements required.");
        this._objs = contained;
        this._index = 0;
    }
    //Get the oldest object (or greatest time since last returned by get()).
    get() {
        var res = this._objs[this._index];
        this._index = (this._index+1)%this._objs.length;
        return res;
    }
    apply(func) {
        this._objs.forEach(func);
    }
    every(func) {
        return this._objs.every(func);
    }
    //Assumes that all contained objects have a "register" function.
    register(collisions, manager, caches) {
        this._objs.forEach(o => o.register(collisions, manager, caches));
    }
}

//Returns the angle of (x, y) from the x-axis, going anti-clockwise.
export function getAngle(x, y) {
    if(x<0)
        return Math.PI+Math.atan(y/x);
    else if(y<0)
        return Math.PI*2+Math.atan(y/x);
    else
        return Math.atan(y/x);
}

export function drawTransparentRect(context, box, color, transparency) {
    var prevGA = context.globalAlpha;
    context.globalAlpha *= transparency;
    context.fillStyle = color;
    box.fill(context);
    context.globalAlpha = prevGA;
}
export function drawFlashRect(context, obj, color) {
    var flash = obj.health.getFlash();
    //Update flashing a different color on health loss.
    if(flash>0)
        drawTransparentRect(context, obj.box, color, flash);
}

//Gets the absolute value of the vector (x, y).
export function absValue(x, y) {
    return Math.sqrt(x*x+y*y);
}

//Get the object closest to the center of toBox in objs.
export function getClosest(toBox, objs) {
    var res = objs[0];
    var dist = Infinity;
    objs.forEach(function(o) {
        var d = absValue((o.box.x+o.box.height/2-toBox.x-toBox.width/2),
                            (o.box.y+o.box.width/2-toBox.y-toBox.height/2));
        if(d<dist) {
            res = o;
            dist = d;
        }
    });
    return res;
}
