//Copyright Matthew Pottage 2019.
import {Collidable, registerCDU} from './collisions/collidable.js';
import {AirborneStone, OrangeBullet, PinkBullet, NinjaBullet, Missile,
    PurpleBullet, BlueBullet, BrownBullet,  YellowBullet, isProjectile} from './projectiles.js';
import {Box, absValue, getClosest} from './utility.js';
import {GreenBuddy} from './minions.js';
import {Platform} from './platforms.js';
import {FreeFallMotion, ControlledMotion, ReboundFreeFall} from './motion.js';

//Special effect/action available. Usually acquired via a SpecialPickup.
export class Special {
    use(obj, collisions, manager, caches) { return null; }
    //Combine this with another special instance, where possible.
    merge(other) { }
    //The below assume that use(obj) has been called (and not returned null) and
    //stopUsing(obj) has not been called yet.
    updateUse(obj, interval, collisions, manager, caches, environment)
    { return null; }
    stopUsing(obj) { return null; }
    //Draw in the same layer as obj.
    drawUseInline(obj, context) {}
    //Draw in the hints layer.
    drawUseHint(obj, context) {}
}
//Causes an object to jump, regardless of circumstance.
export class ExtraJump extends Special {
    use(obj) {
        //Force motion upwards.
        if(obj.motion)
            obj.motion.y = -700;
        //Prevent jumping simulataneously (which will give a very large jump).
        if(obj.motion instanceof ControlledMotion)
            obj.motion.action.jump = false;
        return null;
    }
}
//Makes the altitude (y coordinate) of an object constant for 3s.
export class FixedAltitude extends Special {
    constructor() {
        super();
        this.timeLeft = 3;
        this._alpha = 0;
        this._barCycle = 1.5;
    }
    use(obj) {
        if(obj.motion && obj.motion instanceof FreeFallMotion)
            return this;
        else
            return null;
    }
    updateUse(obj, interval, collisions, manager, caches, environment) {
        this.timeLeft -= interval;
        if(this.timeLeft>=0) {
            //Calculate required previous velocity such that when gravity and
            //and y-friction is applied the mean of the new and previous is 0.
            //XXX: This depends on formulas in FreeFallMotion.prototype.updateY.
            var yFric = obj.motion.stateParams[obj.motion.state.get()].yFriction;
            if(yFric<environment.get("gravity"))
                obj.motion.y = (yFric-environment.get("gravity"))/2*interval;
            else
                obj.motion.y = 0;
            if(obj.motion instanceof ControlledMotion) {
                obj.motion.action.jump = false;
                obj.motion.state.setFluid("specialAir");
            }
            return this;
        }
        else
            return null;
    }
    //Draw bars mirrored above and below box at a specified proportion of
    //box.height/2 up/down from the top/bottom.
    drawBars(proportion, box, context) {
        var scanHeight = box.height/2;
        var yOffset = proportion*scanHeight;
        var y1 = yOffset+box.y+box.height;
        var y2 = box.y-yOffset;
        var width = box.width*(1.5+4*proportion);
        var x = box.x+box.width/2-width/2;
        context.fillStyle = "rgba(128,203,219,.6)";
        context.fillRect(x, y1, width, 10);
        context.fillRect(x, y2-10, width, 10);
    }
    drawUseHint(obj, context) {
        //Draw bars moving up/down above and below obj, with their width
        //inversely related to their distance from obj (see drawBars).
        if(obj.box) {
            //Set alpha based on the position through the usage cycle (ie.
            //ending/starting).
            var alphaProp = this.timeLeft%this._barCycle/this._barCycle;
            if(this.timeLeft>this._barCycle && this._alpha<1)
                this._alpha = Math.max(this._alpha, 1-alphaProp);
            else if(this.timeLeft<this._barCycle/3) {
                this._alpha = alphaProp*3;
            }
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this._alpha;
            //Cycle from 1->0->1 in this._barCycle time.
            var barProp = 2*Math.abs((this.timeLeft%this._barCycle)/this._barCycle-.5);
            this.drawBars(1-barProp, obj.box, context);
            this.drawBars(barProp, obj.box, context);
            context.globalAlpha = prevGA;
        }
    }
}

//Provides a 2x speed boost along x-axis for 2s when used.
export class XSpeedBoost extends Special {
    constructor() {
        super();
        this.mult = 2;
        this.timeLeft = 2;
        this._alpha = 0;
    }
    use(obj) {
        if(obj.motion instanceof ControlledMotion) {
            obj.motion.multiplier.x *= this.mult;
            return this;
        }
        else
            return null;
    }
    updateUse(obj, interval) {
        if(this.timeLeft>=0) {
            this.timeLeft -= interval;
            //Fade in/out trails from the speed boost within 0.2s.
            if(this._alpha<1 && this.timeLeft>.2)
                this._alpha = Math.min(1, this._alpha+interval/.2);
            else if(this._alpha>0 && this.timeLeft<.2)
                this._alpha = Math.max(0, this._alpha-interval/.2);
            return this;
        }
        else
            this.stopUsing(obj);
        return null;
    }
    stopUsing(obj) {
        obj.motion.multiplier.x /= this.mult;
    }
    drawUseInline(obj, context) {
        //Draw up to twelve green (colour of pickup) shadow boxes due to moving
        //fast. First at 200px/s, second at 400px/s, etc.
        if(obj.box) {
            var box = obj.box;
            var prevGA = context.globalAlpha;
            context.fillStyle = "#4a0";
            context.globalAlpha *= this._alpha*.5; //Fade in with max alpha 0.5.
            var width = box.width*.7;
            for(var i=200; i<=1200; i+=200) {
                if(obj.motion.x>=i) //Going right
                    context.fillRect(box.x-i/200*width, box.y, width, box.height);
                else if(obj.motion.x<=-i) //Going left
                    context.fillRect(box.x+box.width+(i/200-1)*width, box.y, width,
                            box.height);
                else
                    break;
                context.globalAlpha *= .8;
            }
            context.globalAlpha = prevGA;
        }
    }
}

//Permits throwing stones every 0.5s instead of every 3s (default for
//StoneComponent).
export class RapidReload extends Special {
    constructor() {
        super();
        this.sinceReload = Infinity; //Immediately loaded.
        this.timeLeft = 8;
    }
    use(obj) {
        if(obj.stones)
            return this;
        else
            return null;
    }
    merge(other) {
        if(other instanceof RapidReload)
            this.timeLeft += Math.max(0, this.timeLeft);
    }
    updateUse(obj, interval, collisions, manager, caches) {
        this.timeLeft -= interval;
        if(this.timeLeft>=0) {
            if(obj.stones.canLaunch()) //Nothing to do.
                this.sinceReload = 0;
            else { //Time 0.5s to reload.
                this.sinceReload += interval;
                if(this.sinceReload>=.5) {
                    obj.stones.reloadNow();
                    this.sinceReload = 0;
                }
            }
            return this;
        }
        else //Timed out.
            return null;
    }
    drawUseHint(obj, context) {
        if(obj.box && !obj.stones.canLaunch()) {
            //Show yellow stone bouncing left/right if not yet reloaded.
            var x = obj.box.x;
            var prop = 4*this.sinceReload/.5;
            //Start at obj.box.x, then go to obj.box.x+obj.box.width, back to
            //obj.box.x, next obj.box.x-obj.box.width then finally obj.box.x.
            if(prop<1)
                x += prop*obj.box.width;
            else if(prop<3)
                x += (2-prop)*obj.box.width;
            else
                x += (prop-4)*obj.box.width;
            context.fillStyle = "#d4aa00"; //Colour from icon.
            context.fillRect(x, obj.box.y, 12, 12); //Size of AirborneStone.
        }
    }
}

export class TripleThrow extends Special {
    constructor(count=6) {
        super();
        this.left = count;
        this.launchDelay = 4;
        this.untilNext = 0;
    }
    use(obj, collisions, manager, caches) {
        if(obj.motion && this.left>0) {
            if(this.untilNext<=0) {
                var vx = obj.motion.x*5;
                var vy = obj.motion.y*2;
                for(var i=0; i<3; ++i) {
                    var stone = new AirborneStone(obj.box.x, obj.box.y+15*i,
                        vx, vy+(i-1)*100, obj, "red");
                    collisions.unprotectedImpact(stone, [obj]);
                    stone.register(collisions, manager, caches);
                }
                this.untilNext = this.launchDelay;
                --this.left;
            }
            obj.specials.setNext(this);
            return this;
        }
    }
    merge(other) {
        if(other instanceof TripleThrow)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, manager, collisions, caches) {
        this.untilNext -= interval;
        return this;
    }
}

export class OrangeBulletSpecial extends Special {
    constructor(count=7) {
        super();
        this.left = count;
        this.launchDelay = 3;
        this.untilNext = 0;
    }
    use(obj, collisions, manager, caches) {
        if(obj.motion && this.left>0) {
            if(this.untilNext<=0) {
                var bullet = new OrangeBullet(obj.box.x, obj.box.y,
                    obj.motion.x*6, obj.motion.y*2,
                    o=>(o.health && o!==obj));
                collisions.unprotectedImpact(bullet, [obj]);
                bullet.register(collisions, manager, caches);
                this.untilNext = this.launchDelay;
                --this.left;
            }
            obj.specials.setNext(this);
            return this;
        }
    }
    merge(other) {
        if(other instanceof OrangeBulletSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, manager, collisions, caches) {
        this.untilNext -= interval;
        return this;
    }
}

export class PinkBulletSpecial extends Special {
    constructor(count=8) {
        super();
        this.left = count;
        this.launchDelay = 2;
        this.untilNext = 0;
    }
    use(obj, collisions, manager, caches) {
        if(obj.motion && this.left>0) {
            if(this.untilNext<=0) {
                var bullet = new PinkBullet(obj.box.x, obj.box.y,
                    obj.motion.x*2.5, obj.motion.y*1.5, o=>(o.health && o!==obj));
                collisions.unprotectedImpact(bullet, [obj]);
                bullet.register(collisions, manager, caches);
                this.untilNext = this.launchDelay;
                --this.left;
            }
            obj.specials.setNext(this);
            return this;
        }
    }
    merge(other) {
        if(other instanceof PinkBulletSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, manager, collisions, caches) {
        this.untilNext -= interval;
        return this;
    }
}

export class NinjaBulletSpecial extends Special {
    constructor(count=5) {
        super();
        this.left = count;
        this.launchDelay = 2;
        this.untilNext = 0;
    }
    use(obj, collisions, manager, caches) {
        if(obj.motion && this.left>0) {
            if(this.untilNext<=0) {
                (new NinjaBullet(obj.box.x, obj.box.y,
                    obj.motion.x*3, obj.motion.y*1, o=>(o.health && o!==obj))
                ).register(collisions, manager, caches);
                this.untilNext = this.launchDelay;
                --this.left;
            }
            obj.specials.setNext(this);
            return this;
        }
    }
    merge(other) {
        if(other instanceof NinjaBulletSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, manager, collisions, caches) {
        this.untilNext -= interval;
        return this;
    }
}
export class MissileSpecial extends Special {
    constructor(count=3) {
        super();
        this.left = count;
        this.launchDelay = 1;
        this.untilLaunch = 0;
    }
    use(from, collisions, manager, caches) {
        if(this.untilLaunch<=0) {
            var missile = new Missile(from.box.x, from.box.y, from.motion.x*3,
                         from.motion.y*1.2, function(obj) {
                              return obj!==from && obj.health
                                  && !(obj instanceof AirborneStone)
                                  && !(obj instanceof Platform);
                         }, from);
            collisions.unprotectedImpact(missile, [from]);
            missile.register(collisions, manager, caches);
            this.untilLaunch = this.launchDelay;
            --this.left;
        }
        if(this.left>0) {
            from.specials.setNext(this);
            return this;
        }
        else
            return null;
    }
    merge(other) {
        if(other instanceof MissileSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, collisions, manager, caches) {
        this.untilLaunch -= interval;
        return this;
    }
}

//Launches two PurpleBullets. The next use freezes the bullets. Then it resets
//to firing (if any left, starts with 7).
export class PurpleBulletSpecial extends Special {
    constructor(count=7) {
        super();
        this.left = count;
        this.launchDelay = 2;
        this.untilLaunch = 0;
        this.current = []; //Bullets launched and not frozen.
    }
    use(from, collisions, manager, caches) {
        //Freeze.
        if(this.current.length>0) {
            this.current.forEach(b => b.freeze());
            this.current = [];
        }
        //Ready to launch.
        else if(this.untilLaunch<=0) {
            var vx = from.motion.x*5;
            var vy = from.motion.y*2;
            //One goes slightly up, the other goes slightly down.
            this.current = [
                new PurpleBullet(from.box.x, from.box.y-7, vx, vy-75, from),
                new PurpleBullet(from.box.x, from.box.y+7, vx, vy+75, from),
            ];
            this.current.forEach(b => b.register(collisions, manager, caches));
            this.untilLaunch = this.launchDelay;
            --this.left;
        }
        //Another use left.
        if(this.left>0 || this.current.length>0) {
            from.specials.setNext(this);
            return this;
        }
        else
            return null;
    }
    merge(other) {
        if(other instanceof PurpleBulletSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, collisions, manager, caches) {
        this.untilLaunch -= interval;
        return this;
    }
}

//Used to control a swarm of GreenBuddies.
//  Inital use adds a swarm of 4 to the game.
//  Later uses fire a BuddyGuidingBullet that they follow (while it is active)
//  with damaged enabled. When there is no bullet they don't cause damage and
//  follow the controller.
//  Teleports swarm members who are too far away back to the controller.
export class GreenSwarmSpecial extends Special {
    constructor(count=6) {
        super();
        this.deployed = []; //Swarm deployed
        this.current = null; //Current bullet fired
        this.left = count;
        this.launchDelay = 4.5;
        this.untilLaunch = 0;
    }
    use(obj, collisions, manager, caches) {
        if(!obj.motion)
            return null;
        if(this.deployed.length===0) {
            //Group of 4 GreenBuddies, arranged in a rectangle.
            var selector = (o => o!==obj);
            this.deployed = [
                new GreenBuddy(obj.box.x-25, obj.box.y-10, selector),
                new GreenBuddy(obj.box.x+obj.box.width+10, obj.box.y-10, selector),
                new GreenBuddy(obj.box.x-25, obj.box.y+20, selector),
                new GreenBuddy(obj.box.x+obj.box.width+10, obj.box.y+20, selector),
            ];
            this.deployed.forEach(function(o) {
                o.register(collisions, manager, caches)
                o.setFollow(obj);
            });
            obj.specials.setNext(this);
            return this;
        }
        else if(this.left>0 || this.current) {
            if(this.untilLaunch<=0) {
                //Launch new guidance bullet.
                this.current = new BuddyGuidingBullet(obj.box.x, obj.box.y,
                    obj.motion.x*5, obj.motion.y*2);
                this.current.register(collisions, manager, caches);
                //Get swarm to follow and cause damage based on its motion.
                this.deployed.forEach(function(o) {
                    o.setDamaging(true);
                    o.setFollow(this.current)
                }, this);
                this.untilLaunch = this.launchDelay;
                --this.left;
            }
            obj.specials.setNext(this);
            return this;
        }
        return null;
    }
    merge(other) {
        if(other instanceof GreenSwarmSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, collisions, manager, caches) {
        this.untilLaunch -= interval;
        if(this.current && this.current.disabled) {
            //Reset targetting when the guidance bullet fades.
            this.current = null;
            this.deployed.forEach(function(o) {
                o.setDamaging(false);
                o.setFollow(obj);
            });
        }
        //Teleport swarm members who are a long distance away back.
        this.deployed.forEach(function(o) {
            var d = absValue((o.box.x+o.box.height/2-obj.box.x-obj.box.width/2),
                                (o.box.y+o.box.width/2-obj.box.y-obj.box.height/2));
            if(d>2000)
                o.teleporter.to(obj.box.x, obj.box.y-20);
        });
        return this;
    }
}

//Simple bullet, ignores everything, but platforms and projectiles and falls
//relatively slowly.
//Used to guide a GreenBuddy with the GreenSwarmSpecial.
//Times out after 20s.
class BuddyGuidingBullet extends Collidable {
    constructor(x, y, xVelocity, yVelocity) {
        super();
        this.motion = new ReboundFreeFall(.1, .8);
        this.box = new Box(x, y, 24, 24);
        this.color = "#009e54";
        this.pushRank = 20;
        this.reset();
        this.motion.x = xVelocity;
        this.motion.y = yVelocity;
    }
    reset() {
        this.motion.reset();
        this.timeLeft = 20; //At 4s left it starts to fade away.
        this.opacity = 1;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            this.motion.accel.y = -environment.get("gravity")/2; //Limit effect of gravity.
            this.motion.update(this, interval, collisions, environment);
            this.timeLeft -= interval;
            if(this.timeLeft<=0)
                this.disabled = true;
            else if(this.timeLeft<=4)
                this.opacity = this.timeLeft/4;
        }
    }
    isCollision(obj) {
        return (obj instanceof Platform) || isProjectile(obj);
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
        if(this.timeLeft>4) //Fade away, now.
            this.timeLeft = 4;
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    draw(context) {
        if(!this.disabled) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.opacity*.6; //Fade in last 4s.
            context.fillStyle = this.color;
            context.fillRect(this.box.x,this.box.y,this.box.width,this.box.height);
            context.globalAlpha = prevGA;
        }
    }
}

//Launches "BlueBullets", targetted at the nearest object with health (apart
//from the launcher). Provides 5. Same launch velocities as the TurquoiseNinja.
export class BlueBulletSpecial extends Special {
    constructor(count=5) {
        super();
        this.left = count;
        this.launchDelay = 1;
        this.untilNext = 0;
    }
    use(obj, collisions, manager, caches) {
        if(obj.motion && this.left>0) {
            if(this.untilNext<=0) {
                var bullet = new BlueBullet(obj.box.x, obj.box.y,
                    obj.motion.x*.25, obj.motion.y*.25, o=>(o.health && o!==obj));
                //Find the closest object within 750px to target.
                var center = obj.box.center;
                var scan = new Box(center.x-1000, center.y-1000, 2000, 2000);
                var target = getClosest(scan,
                    collisions.all(bullet, scan)
                    .filter(o => (o!==obj && o.health && !(o instanceof Platform))));
                if(target) {
                    //Launch bullet with speed 350px/s (or 0 if the object is at
                    //launch position).
                    var v = {x: target.box.x-obj.box.x, y: target.box.y-obj.box.y};
                    var scale = 400/Math.sqrt(v.x*v.x+v.y*v.y);
                    if(v.x==0 && v.y==0)
                        scale = 0;
                    bullet.motion.x += v.x*scale;
                    bullet.motion.y += v.y*scale;
                }
                collisions.unprotectedImpact(bullet, [obj]);
                bullet.register(collisions, manager, caches);
                this.untilNext = this.launchDelay;
                --this.left;
            }
            obj.specials.setNext(this);
            return this;
        }
    }
    merge(other) {
        if(other instanceof BlueBulletSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, collisions, manager, caches) {
        this.untilNext -= interval;
        return this;
    }
}

export class BrownBulletSpecial extends Special {
    constructor(count=5) {
        super();
        this.left = count;
        this.launchDelay = 2;
        this.untilLaunch = 0;
    }
    use(from, collisions, manager, caches) {
        if(this.untilLaunch<=0) {
            var bullet = new BrownBullet(from.box.x, from.box.y,
                from.motion.x*3, from.motion.y*1.5);
            collisions.unprotectedImpact(bullet, [from]);
            bullet.register(collisions, manager, caches);
            this.untilLaunch = this.launchDelay;
            --this.left;
        }
        if(this.left>0) {
            from.specials.setNext(this);
            return this;
        }
        else
            return null;
    }
    merge(other) {
        if(other instanceof BrownBulletSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, collisions, manager, caches) {
        this.untilLaunch -= interval;
        return this;
    }
}

export class YellowBulletSpecial extends Special {
    constructor(count=5) {
        super();
        this.left = count;
        this.launchDelay = 4;
        this.untilNext = 0;
        this._hiddenNext = 0;
        this._upcoming = false;
        this._launchV = {x: 0, y: 0};
    }
    _spawn(obj, collisions, manager, caches) {
        var bullet = new YellowBullet(obj.box.x, obj.box.y, this._launchV.x,
            this._launchV.y, (obj=>!!obj.health));
        collisions.unprotectedImpact(bullet, [obj]);
        bullet.register(collisions, manager, caches);
    }
    use(obj, collisions, manager, caches) {
        if(obj.motion && (this.left>0 || this._upcoming)) {
            obj.specials.setNext(this);
            if(this.untilNext<=0 && this.left>0) {
                this._launchV = {x: obj.motion.x*2, y: obj.motion.y*1.2};
                this._spawn(obj, collisions, manager, caches);
                this.untilNext = this.launchDelay;
                --this.left;
                this._hiddenNext = .5;
                this._upcoming = true;
            }
            return this;
        }
    }
    merge(other) {
        if(other instanceof YellowBulletSpecial)
            this.left += Math.max(0, other.left);
    }
    updateUse(obj, interval, collisions, manager, caches) {
        this.untilNext -= interval;
        this._hiddenNext -= interval;
        if(this._upcoming && this._hiddenNext<=0) {
            this._upcoming = false;
            this._spawn(obj, collisions, manager, caches);
        }
        return this;
    }
}
