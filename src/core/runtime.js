//Copyright Matthew Pottage 2019.
//Updates and draws the game, given all the objects in it.
// Stores a collection of flags (used as a rudimentary event system).
// Has a data transfer system, called pipes (used to connect PortalRegions),
// which is Last In First Out.
export class ObjectManager {
    //Each object is registered for drawing and updates by calling the
    //appropriate add? function.
    //Normally each object will have a register function that is called on an
    //instance of ObjectManager (and CollisionsEngine) that registers it:
    //  .register(CollisionsEngine, ObjectManager)
    constructor() {
        this.clear();
    }
    clear() {
        this.drawObjs = [];
        this.backgroundObjs = [];
        this.overlayObjs = [];
        this.hintObjs = [];
        this.updateObjs = [];
        this.dirtyUpdates = false; //Does updateObjs need sorting?
        //Prevent 1 frame lag for update objects added during a call of update.
        this._extraUpdates = [];
        this._inUpdate = false;
        //Collection of values which can be set to true and false by setFlag.
        this._flags = {};
        //Objects to be notified when certain flags change.
        this.watchers = {};
        //All known players.
        this.players = [];
        //Data streams
        this._pipes = {"": []};
    }
    remove(obj) {
        function _remove(array) {
            var i = array.indexOf(obj);
            if(i!=-1)
                array.splice(i, 1);
        }
        _remove(this.drawObjs);
        _remove(this.backgroundObjs);
        _remove(this.overlayObjs);
        _remove(this.hintObjs);
        _remove(this.updateObjs);
        _remove(this.players);
        for(var f in this.watchers)
            _remove(this.watchers[f]);
    }
    addBackground(d) { //Draw in behind everything not a background.
        if(!(d.backgroundDraw))
            throw new TypeError("Object with a backgroundDraw function required.");
        this.backgroundObjs.push(d);
    }
    addDraw(d) { //Draw normally.
        if(!(d.draw))
            throw new TypeError("Object with a draw function required.");
        this.drawObjs.push(d);
    }
    addOverlay(d) { //Draw in front of everything not an overlay.
        if(!(d.overlayDraw))
            throw new TypeError("Object with an overlayDraw function required.");
        this.overlayObjs.push(d);
    }
    addHintDraw(d) { //Visuals hinting at the current action/state (e.g. specials).
        if(!(d.hintDraw))
            throw new TypeError("Object with a hintDraw function required.");
        this.hintObjs.push(d);
    }
    addUpdate(u) { //Add object to be updated each frame.
        if(!(u.update))
            throw new TypeError("Object with an update function required.");
        this.updateObjs.push(u);
        if(this._inLoop)
            this._extraUpdates.push(u);
        this.dirtyUpdates = true;
    }
    addWatcher(flagName, obj) { //Add object monitoring a flag value.
        if(!(obj.watchedChanged))
            throw new TypeError(
                    "Object with a watchedChanged function required.");
        if(!this.watchers[flagName])
            this.watchers[flagName] = [obj];
        else
            this.watchers[flagName].push(obj);
    }
    addPlayer(p) {
        this.players.push(p);
    }
    addPipe(name) {
        this._pipes[name] = this._pipes[name] || [];
    }
    //Set the value of a flag (and dispatch change events if it has changed).
    setFlag(flagName, newVal) {
        var prevVal = this._flags[flagName];
        this._flags[flagName] = newVal;
        if((prevVal===undefined || prevVal!=newVal)
            && this.watchers[flagName])
            this.watchers[flagName].forEach(function(w) {
                w.watchedChanged(this);
            }, this);
    }
    getFlag(name) {
        return this._flags[name];
    }
    pushPipe(name, data) {
        this._pipes[name].push(data);
    }
    popPipe(name) {
        return this._pipes[name].pop();
    }
    //Draws all registered objects.
    draw(context, caches, environment) {
        this.backgroundObjs.forEach(function(obj) {
            obj.backgroundDraw(context, caches, environment);
        });
        this.drawObjs.forEach(function(obj) {
            obj.draw(context, caches, environment);
        });
        this.overlayObjs.forEach(function(obj) {
            obj.overlayDraw(context, caches, environment);
        });
        this.hintObjs.forEach(function(obj) {
            obj.hintDraw(context, caches, environment);
        });
    }
    //Updates all registered objects' state.
    //  interval: Time elapsed since last update (or 0 if no update done yet).
    //Objects are updated by pushRank (assumed to be 0 if none), in ascending
    //order. This lets, e.g. a stone be on a moving platform and have the
    //correct velocity.
    //EXCEPTION: If the object was added during a call of update this may not
    //  hold for its first update.
    update(interval, collisions, caches, environment) {
        if(this.dirtyUpdates) { //New objects to be updated added.
            this.updateObjs.sort(function(o1, o2) {
                //Sort by pushRank (defaults to 0 if none).
                return (o1.pushRank || 0) - (o2.pushRank || 0);
            });
            this.dirtyUpdates = false;
        }
        this._inLoop = true;
        this.updateObjs.forEach(function(obj) {
            obj.update(interval, collisions, this, caches, environment);
        }, this);
        while(this._extraUpdates.length>0)
            this._extraUpdates.pop().update(interval, collisions, this, caches, environment);
        this._inLoop = false;
    }
}

//Load textures, then trigger a callback.
export class TextureManager {
    constructor(dir) {
        if(dir!==undefined)
            this.dir = dir+"/";
        else
            this.dir = ""
        this.texturesToLoad = [];
        this.dataURLToLoad = [];
        this._get = {};
        this._unprotected = [];
        //Returned when loading an image failed (a 1x1px black square).
        this.errorTexture = new Image();
        this.errorTexture.src = "data:image/gif;base64,R0lGODdhAQABAIABAAICAv///ywAAAAAAQABAAACAkQBADs=";
    }
    //Add new image textures to load.
    add(s) {
        if(s instanceof Array)
            this.texturesToLoad = this.texturesToLoad.concat(s);
        else
            this.texturesToLoad.push(s);
    }
    addDataURL(name, data) {
        this.dataURLToLoad.push({name: name, data: data});
    }
    anyPending() {
        return (this.texturesToLoad.length+this.dataURLToLoad.length)>0;
    }
    has(str) {
        return !!this._get[str];
    }
    get(str) {
        return this._get[str];
    }
    //Load all textures that need loading.
    load(statusCallback) {
        return new Promise((function(resolve, reject) {
            var left = this.texturesToLoad.length+this.dataURLToLoad.length;
            var failed = 0;
            var allLength = this.texturesToLoad.length+this.dataURLToLoad.length;
            if(left==0)
                resolve(0);
            var loadFunc = function(name, source) {
                if(!(name in this._get)) {
                    this._unprotected.push(name);
                    var img = new Image();
                    var self = this;
                    img.addEventListener("load", function() {
                        self._get[name] = img;
                        --left;
                        if(statusCallback)
                            statusCallback(allLength-left, allLength);
                        if(left<=0)
                            resolve(allLength);
                    });
                    img.addEventListener("error", function() {
                        ++failed;
                        --left;
                        console.log("Loading texture \""+name+"\" failed");
                        //Make error noticable.
                        self._get[name] = self.errorTexture;
                        if(statusCallback)
                            statusCallback(allLength-left, allLength);
                        if(left<=0)
                            resolve(allLength);
                    });
                    img.src = source;
                    this._get[name] = null; //Filled in the image event handlers
                }
                else
                    --left;
            };
            this.texturesToLoad.forEach(function(str) {
                loadFunc.call(this, str, this.dir+str);
            }, this);
            this.texturesToLoad = [];
            this.dataURLToLoad.forEach(function(d) {
                loadFunc.call(this, d.name, d.data);
            }, this);
            this.dataURLToLoad = [];
            if(left==0) {
                statusCallback(allLength, allLength);
                resolve(allLength);
            }
        }).bind(this));
    }
    //Protect all existing textures from removal
    protect() {
        if(this.anyPending())
            throw new Error("Must load all textures previously requested before protecting.");
        this._unprotected = [];
    }
    //Remove an unprotected texture
    remove(name) {
        var i = this._unprotected.indexOf(name);
        if(i==-1 && name in this._get) {
            throw new Error("Can't delete protected texture.")
        }
        else {
            this._unprotected.splice(i, 1);
            delete this._get[name];
        }
    }
    //Remove all unprotected textures
    removeAll() {
        for(var n of this._unprotected)
            delete this._get[n];
        this._unprotected = [];
    }
}

//Manages named image rendering caches.
//Useful as a cache is often shared between several objects (e.g. TripleCoins
//have the same image) and it is desirable to avoid having global cache objects.
export class RenderCacheManager {
    constructor() {
        this.clear();
    }
    clear() {
        this._all = {};
        this._dirty = false;
    }
    get(str) { //Get a cache of a specific name.
        return this._all[str];
    }
    set(str, val) { //Create a new named cache.
        if(!val || !(val.refreshCache))
            throw new TypeError("Object with refreshCache function required.");
        this._all[str] = val;
        this._dirty = true;
    }
    //Call refreshCache(scale, textures) on all registered object with caches.
    // It is expected that caches are scaled appropriately and drawn in the
    // appropriate draw function.
    refresh(scale, textures) {
        for(var str in this._all)
            this._all[str].refreshCache(scale, textures);
        this._dirty = false;
    }
    //Only refreshes if new caches have been added since the last refresh.
    interimRefresh(scale, textures) {
        if(this._dirty)
            this.refresh(scale, textures);
    }
    purgeMissingTextures(textures) {
        for(var n in this._all) {
            if(!this._all[n].gotTextures(textures))
                delete this._all[n];
        }
    }
    removePrefixed(prefix) {
        for(var n in this._all) {
            if(n.startsWith(prefix))
                delete this._all[n];
        }
    }
}

export function getDefaultEnvSetup() {
    return {"gravity": 1000, "air control": false};
}
//Used to pass around the current values of any environment variables, like
//gravity, which permits them to be different values when eg. a different level
//is being played..
export class Environment {
    constructor() {
        this.clear();
    }
    clear() {
        this._vars = getDefaultEnvSetup();
    }
    get(key) {
        return this._vars[key];
    }
    apply(level) {
        for(var k in level.environmentSetup)
            if(k in this._vars)
                this._vars[k] = level.environmentSetup[k];
    }
}
