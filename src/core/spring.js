//Copyright Matthew Pottage 2019.
import {Collidable, registerCDU} from './collisions/collidable.js';
import {Platform} from './platforms.js';
import {Frame} from './frame.js';
import {Box} from './utility.js';

export class Spring extends Platform {
    //power: Scale factor for spring constant.
    //pushDistance: Distance the spring can be pushed/pushes back through.
    constructor(box, color, power, pushDistance) {
        super(box, color);
        this.pushRank = 25;
        this.motion = {x: 0, y: 0};
        this.startY = this.box.y;
        this.springVal = 100*Math.max(1, power);
        this.springDiff = Math.max(0, pushDistance||40);
        this._setConstraints();
    }
    _setConstraints() {
        this.frames = [
            new Frame( new Box(this.box.x,
                        this.box.y+this.springDiff+this.box.height,
                        this.box.width, 2), this),
            new Frame(new Box(this.box.x-1, this.box.y-this.springDiff,
                        1, 2*this.springDiff+this.box.height), this),
            new Frame(new Box(this.box.x+this.box.width,
                        this.box.y-this.springDiff, 1,
                        2*this.springDiff+this.box.height), this),
            new Frame(new Box(this.box.x, this.box.y-this.springDiff-1,
                        this.box.width, 1), this)
        ];
    }
    update(interval, collisions) {
        if(this.bounceUp) {
            this.pushRank = 2;
            this.motion.y += (this.startY-this.box.y)*this.springVal*interval;
            if(this.motion.y>=0)
                this.bounceUp = false;
            collisions.queue(this, 0, this.motion.y*interval);
        }
        else if(this.box.y!=this.startY && !this.beingPushed) {
            this.pushRank = 2;
            this.motion.y += (this.startY-this.box.y)*this.springVal*interval;
            var newY = this.motion.y*interval+this.box.y;
            if((newY>this.startY && this.box.y<this.startY)
                    || (newY<this.startY && this.box.y>this.startY)) {
                newY = this.startY;
                this.motion.y = 0;
            }
            collisions.queue(this, 0, newY-this.box.y);
        }
        else
            this.pushRank = 25;
        this.beingPushed = false;
    }
    draw(context) {
        context.fillStyle = this.color;
        context.fillRect(this.box.x,this.box.y,this.box.width,this.box.height);
    }
    handleCollision(obj, details) {
        if(details.down && obj===this.frames[0])
            this.bounceUp = true;
        else if(details.up) {
            if(obj==this.frames[2]) {
                this.bounceUp = false;
                this.motion.y = 0;
            }
            if(obj.motion && obj.motion.state)
                obj.motion.state.setSurface("spring");
        }
        if(obj.pushRank<this.pushRank)
            this.beingPushed = true;
    }
    handleMoved(details, amount) {
        if(amount==0 && (details.up || details.down))
            this.motion.y = 0;
    }
    register(collisions, manager) {
        manager.addUpdate(this);
        manager.addDraw(this);
        collisions.add(this);
        this.frames.forEach(function(f) {
            collisions.add(f);
        });
    }
    reset() {
        this.box.y = this.startY;
        this.motion = {x: 0, y: 0};
        this.pushRank = 25;
        this.bounceUp = false;
        this.beingPushed = false;
    }
}
