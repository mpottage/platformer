//Copyright Matthew Pottage 2019.
import {Collidable} from './collisions/collidable.js';

export class Frame extends Collidable {
    constructor(box, notice) {
        super();
        this.box = box;
        this.notice = notice; //Collidable to have hard collisions with.
    }
    isCollision(obj) {
        return obj==this.notice;
    }
}
