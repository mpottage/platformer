//Copyright Matthew Pottage 2019.
//Hides a region of a level, revealing the region when first activated.
export class Cover {
    constructor(box, color) {
        this.box = box;
        this.color = color;
        this.reset();
    }
    reset() {
        this.opacity = 1;
        this.disabled = false;
        this.fading = false;
        this.activated = false;
    }
    register(collisions, manager) {
        manager.addUpdate(this);
        manager.addOverlay(this);
    }
    overlayDraw(context) {
        if(!this.disabled) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.opacity;
            context.fillStyle = this.color;
            context.fillRect(this.box.x, this.box.y, this.box.width,
                    this.box.height);
            context.globalAlpha = prevGA;
        }
    }
    update(interval) {
        if(!this.disabled) {
            if(this.fading) {
                this.opacity -= 10*interval; //Hide cover within 0.1s.
                if(this.opacity<=0)
                    this.disabled = true;
            }
            else if(this.activated)
                this.fading = true;
        }
    }
}
