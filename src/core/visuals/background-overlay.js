//Copyright Matthew Pottage 2019.
//For visual effect only.
import {ZoomImageCache} from '../render-caches.js';
export class VisualOnlyOverlay {
    constructor(box, color) {
        this.box = box;
        this.color = color;
    }
    overlayDraw(context) {
        context.fillStyle = this.color;
        this.box.fill(context);
    }
    register(collisions, manager) {
        manager.addOverlay(this);
    }
    reset() {}
}
export class VisualOnlyBackground {
    constructor(box, color) {
        this.box = box;
        this.color = color;
    }
    backgroundDraw(context) {
        context.fillStyle = this.color;
        this.box.fill(context);
    }
    register(collisions, manager) {
        manager.addBackground(this);
    }
    reset() {}
}

function makeImageCacheName(image) {
    return image.textureName+image.box.width+"*"+image.box.height;
}

export class ImageBackground {
    constructor(box, textureName) {
        this.box = box;
        this.textureName = textureName;
    }
    backgroundDraw(context, caches) {
        caches.get(makeImageCacheName(this)).draw(context, this.box);
    }
    register(collisions, manager, caches) {
        manager.addBackground(this);
        var cacheName = makeImageCacheName(this);
        if(!caches.get(cacheName))
            caches.set(cacheName, new ZoomImageCache(this.textureName, this.box.width, this.box.height));
    }
    reset() {}
}
export class ImageOverlay {
    constructor(box, textureName) {
        this.box = box;
        this.textureName = textureName;
    }
    overlayDraw(context, caches) {
        caches.get(makeImageCacheName(this)).draw(context, this.box);
    }
    register(collisions, manager, caches) {
        manager.addOverlay(this);
        var cacheName = makeImageCacheName(this);
        if(!caches.get(cacheName))
            caches.set(cacheName, new ZoomImageCache(this.textureName, this.box.width, this.box.height));
    }
    reset() {}
}
