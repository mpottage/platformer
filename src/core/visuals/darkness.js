//Copyright Matthew Pottage 2019.
//Darkness, obscures a region, except for a circle around each player.
export class Darkness {
    constructor(box, color) {
        this.box = box;
        this.color = color;
        this.reset();
    }
    reset() {
        this.players = [];
        this.activated = true;
    }
    register(collisions, manager) {
        manager.addUpdate(this);
        manager.addOverlay(this);
    }
    update(interval, collisions, manager) {
        if(this.activated)
            this.players = manager.players;
        else
            this.players = [];
    }
    overlayDraw(context) {
        context.save();
        if(this.players.length!=1) {
            //Clip (excluding) circles around each player.
            this.players.forEach(function(p) {
                context.beginPath();
                context.rect(this.box.x, this.box.y, this.box.width, this.box.height);
                var c = p.box.center;
                context.arc(c.x, c.y, 150, 0, Math.PI*2, true);
                context.clip();
            }, this);
            //Now draw darkness.
            context.fillStyle = this.color;
            context.fillRect(this.box.x, this.box.y, this.box.width, this.box.height);
        }
        else { //Smoother circle outline for single player.
            context.beginPath();
            context.rect(this.box.x, this.box.y, this.box.width, this.box.height);
            context.clip();
            context.beginPath();
            var c = this.players[0].box.center;
            context.arc(c.x, c.y, 150, 0, Math.PI*2, true);
            //Prevent outline of Darkness.
            context.rect(this.box.x-1, this.box.y-1, this.box.width+2, this.box.height+2);
            context.fillStyle = this.color;
            context.fill();
        }
        context.restore();
    }
}
