//Copyright Matthew Pottage 2019.
import {Box, neededTextures} from '../utility.js';
import {StretchImageCache} from '../render-caches.js';

export class BackgroundImageDisplay {
    //imgName: Name of image (as loaded by an TextureManager).
    //box: Box giving the position and size to render the image.
    constructor(imgName, box) {
        this.box = box;
        this.imgName = imgName;
    }
    backgroundDraw(context, caches) {
        caches.get(this.imgName).draw(context, this.box);
    }
    register(collisions, manager, caches) {
        manager.addBackground(this);
        if(!caches.get(this.imgName))
            caches.set(this.imgName, new StretchImageCache(this.imgName, this.box.width,
                        this.box.height));
    }
    reset() {}
}
neededTextures.push("SignRight.svg");
export class SignRight extends BackgroundImageDisplay {
    constructor(x, y) {
        super("SignRight.svg", new Box(x, y, 75, 65));
    }
}
neededTextures.push("SignLeft.svg");
export class SignLeft extends BackgroundImageDisplay {
    constructor(x, y) {
        super("SignLeft.svg", new Box(x, y, 75, 65));
    }
}
neededTextures.push("SignUp.svg");
export class SignUp extends BackgroundImageDisplay {
    constructor(x, y) {
        super("SignUp.svg", new Box(x, y, 55, 75));
    }
}
neededTextures.push("SignDown.svg");
export class SignDown extends BackgroundImageDisplay {
    constructor(x, y) {
        super("SignDown.svg", new Box(x, y, 55, 75));
    }
}
neededTextures.push("SignTarget.svg");
export class SignTarget extends BackgroundImageDisplay {
    constructor(x, y) {
        super("SignTarget.svg", new Box(x, y, 75, 65));
    }
}
neededTextures.push("SignWarning.svg");
export class SignWarning extends BackgroundImageDisplay {
    constructor(x, y) {
        super("SignWarning.svg", new Box(x, y, 75, 65));
    }
}
neededTextures.push("SignWriting.svg");
export class SignWriting extends BackgroundImageDisplay {
    constructor(x, y) {
        super("SignWriting.svg", new Box(x, y, 75, 65));
    }
}
