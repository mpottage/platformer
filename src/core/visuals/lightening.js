//Copyright Matthew Pottage 2019.
//Lightening effect, a area is covered with color and flashes/strikes white at
//random intervals to simulate a lightening flash.
//Setting it "off" by ApplyFlag stops it flashing. MakeFlag output is "on" when a
//strike is ongoing.
export class LighteningEffect  {
    constructor(box, color="rgba(0,0,0,.3)") {
        this.box = box;
        this.color = color;
        this.reset();
    }
    reset() {
        this.activated = true;
        this._resetStrike();
    }
    _resetStrike() {
        this.timeUntilStrike = 10*Math.random()+1;
        this.strikeOngoing = false;
        this.strikeHangTime = .2;
        this.strikeOpacity = 0;
    }
    register(collisions, manager, caches) {
        manager.addUpdate(this);
        manager.addOverlay(this);
    }
    getOutputVal() {
        return this.strikeOngoing;
    }
    update(interval, collisions, manager, caches) {
        if(!this.strikeOngoing && this.activated) {
            this.timeUntilStrike -= interval;
            if(this.timeUntilStrike<=0) {
                this.strikeOngoing = true;
                this.strikeOpacity = 1;
            }
        }
        if(this.strikeOngoing) {
            if(this.strikeHangTime<=0) {
                if(this.strikeOpacity<=0)
                    this._resetStrike();
                else
                    this.strikeOpacity = Math.max(0,
                        this.strikeOpacity-interval*4);
            }
            else {
                this.strikeHangTime -= interval
            }
        }
    }
    overlayDraw(context, caches) {
        var prevGA = context.globalAlpha;
        context.globalAlpha *= 1-this.strikeOpacity;
        context.fillStyle = this.color;
        this.box.fill(context);
        context.globalAlpha = prevGA*this.strikeOpacity;
        context.fillStyle = "rgba(255,255,255,.8)";
        this.box.fill(context);
        context.globalAlpha = prevGA;
    }
}
