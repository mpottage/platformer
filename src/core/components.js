//Copyright Matthew Pottage 2019.
import {Transition} from './utility.js';

export class TeleportComponent {
    constructor() {
        this.reset();
    }
    reset() {
        this.justUsed = false;
        this.queued = false;
        this._canReset = false;
        //Didn't move to the position requested by teleport but instead used the
        //default position.
        this.positionReset = false;
        this._default = {x: 10, y: 10};
        this._to = {x: 0, y: 0};
    }
    cancel() {
        this.queued = false;
    }
    toAuto(x, y) {
        this._to.x = x;
        this._to.y = y;
        this.queued = true;
        this._canReset = true;
    }
    to(x, y) {
        this._to.x = x;
        this._to.y = y;
        this.queued = true;
        this._canReset = false;
    }
    //Set position that the teleport defaults to in the event that the requested
    //position to teleport to is blocked (should be the position that the object
    //starts at by default).
    setDefault(x, y) {
        this._default.x = x;
        this._default.y = y;
    }
    toDefault() {
        this.to(this._default.x, this._default.y);
    }
    update(obj, collisions) {
        this.justUsed = false;
        this.positionReset = false;
        if(this.queued) {
            obj.box.x = this._to.x;
            obj.box.y = this._to.y;
            if(this._canReset && collisions.any(obj, obj.box)) {
                obj.box.x = this._default.x;
                obj.box.y = this._default.y;
                this.positionReset = true;
            }
            collisions.protect(obj);
            this.queued = false;
            this.justUsed = true;
        }
    }
}

//All necessary items for an object (obj) to have health.
// Typically a member variable, name "health".
// Only kill() and increase(diff) should be called by objects other than the
// owner.
export class HealthComponent {
    //obj: Object to which this applies.
    //initial: Initial value
    //timeout: Minimum time between successive losses of health
    //flashTime: How long the health loss flash should last
    constructor(obj, initial, timeout=0.5, flashTime=0.75) {
        this._obj = obj;
        this._callback = (o => o.disabled = true);
        this._initial = initial;
        this._timeout = timeout;
        this._flashTime = flashTime;
        this.reset();
    }
    reset() {
        this.value = this._initial;
        //Used to give the object time to move out of the hit zone of an item
        //that caused the loss of health to avoid double/triple/... health loss
        //for hitting an item once.
        this._sinceLost = Infinity;
        //Public parameter, used to trigger health loss jumps
        this.justLost = false;
        this._transition = null;
    }
    //Set function called when health reaches 0, after any post-processing.
    //By default (if not set) func=(o => o.disabled=true), where o is the object
    //that died.
    setCallback(func) {
        this._callback = func;
    }
    update(interval) {
        if(this._sinceLost>0)
            this.justLost = false;
        this._sinceLost += interval;
        if(this._transition) {
            this._transition.update(interval);
            if(this._transition.value<=0)
                this._transition = null;
        }
    }
    //Determine percentage of health loss flash to draw.
    getFlash() {
        return (this._transition && this._transition.value) || 0;
    }
    //Functions to be used by other objects
    increase(change) {
        if(this.value>0 &&
            (change>0 || this._sinceLost>=this._timeout)) {
            this._sinceLost = 0;
            this.value = Math.max(0, this.value+change);
            if(change<0) {
                this.justLost = true;
                this._transition = new Transition(1, 0, this._flashTime);
            }
            if(this.value<=0)
                this._callback(this._obj);
        }
    }
    kill() {
        this._sinceLost = this._timeout; //Ensure health will be lost
        this.increase(-this.value);
    }
}
