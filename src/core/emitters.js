//Copyright Matthew Pottage 2019.
import {Collidable, registerCDU} from './collisions/collidable.js';
import {Box, Pool} from './utility.js';
import {Platform, UDPlatform, LRPlatform} from './platforms.js';
import {Spring} from './spring.js';
import {FreeFallMotion} from './motion.js';

//Emits particles from a rectangular region.
// > New particles every pulseDelay
// > Number released is proportional to the size of its region (scaled by
// 1/ratio).
// > Each particle is given a random position within the region.
// > Particles are created by the createParticle function (and respawned at a
// later time)
class Emitter {
    constructor(box, pulseDelay, ratio, cacheTime) {
        this.box = box;
        this.pulseDelay = pulseDelay;
        this.ratio = ratio;
        this.cacheTime = cacheTime;
        this._setupPulses(cacheTime);
        this.reset();
    }
    createParticle() {
        throw new TypeError("Particle generator missing.");
    }
    _setupPulses() {
        var particles = [this.createParticle()];
        //Used to get random position for a particle to be within this.box.
        this.particleWidth = Math.ceil(this.box.width/particles[0].box.width);
        this.particleHeight = Math.ceil(this.box.height/particles[0].box.height);
        var coverNo = this.particleWidth*this.particleHeight;
        //No. particles released in each pulse.
        this.perPulse = Math.ceil(coverNo/this.ratio); //Chosen by trial and error.
        //Store this.cacheTime worth of particles.
        for(var i=0; i<this.perPulse*this.cacheTime/this.pulseDelay; ++i)
            particles.push(this.createParticle());
        this.pool = new Pool(particles);
    }
    reset() {
        this.activated = true;
        this.pool.apply(o => o.disable());
        this.untilPulse = 0;
    }
    register(collisions, manager, caches) {
        manager.addUpdate(this);
        this.pool.apply(o => o.register(collisions, manager, caches));
    }
    update(interval) {
        this.untilPulse -= interval;
        //Release rain particles every this.pulseDelay seconds when activated.
        if(this.untilPulse<=0 && this.activated) {
            this.untilPulse = this.pulseDelay;
            for(var i=0; i<this.perPulse; ++i) {
                var particle = this.pool.get();
                //Get the desired particle position.
                var x = this.box.x+Math.floor(Math.random()*this.particleWidth)*particle.box.width;
                var y = this.box.y+Math.floor(Math.random()*this.particleHeight)*particle.box.height;
                particle.respawn(x, y, (0.5-Math.random())*30);
            }
        }
    }
}

class ParticleForEmitter extends Collidable {
    respawn(x, y, vx, vy) {
        throw new Error("Write functions");
    }
    isCollision(obj) {
        //Excludes moving platforms and springs, where colliding would cause
        //glitches from the particle not being registered in collisions.
        return obj instanceof Platform
            && !(obj instanceof UDPlatform) && !(obj instanceof LRPlatform) && !(obj instanceof Spring);
    }
    register(collisions, manager, caches) {
        manager.addUpdate(this);
        manager.addDraw(this);
    }
    disable() {
        throw new Error("Write functions");
    }
}

//A small white square that falls slowly.
// > Only interacts with platforms.
// > Fades in after 0s and fades out after 9.3s;
class Snowflake extends ParticleForEmitter {
    constructor(cacheTime, x, y, vx=0, vy=0) {
        super();
        this.box = new Box(x, y, 15, 15);
        this.startPos = {x: x, y: y};
        this.motion = new SnowFreeFall(3);
        this.bodyColor = "#eee"; //Pure white is too bright.
        this.pushRank = 40;
        this.cacheTime = cacheTime;
        this._reset();
        this.motion.x = vx;
        this.motion.y = vy;
    }
    _reset() {
        this.disabled = false;
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.motion.reset();
        this.opacity = 0;
        //Ensures that this has faded when the SnowEmitter cycle repeats.
        this.timeLeft = this.cacheTime-0.5;
    }
    respawn(x, y, vx=0, vy=0) {
        this.startPos.x = x;
        this.startPos.y = y;
        this._reset();
        this.motion.x = vx;
        this.motion.y = vy;
    }
    disable() {
        this.disabled = true;
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            this.motion.accel.y = -3*environment.get("gravity")/4; //Limit effect of gravity.
            this.motion.update(this, interval, collisions, environment);
            this.timeLeft -= interval;
            //Fade out.
            if(this.timeLeft<=0) {
                if(this.opacity>0) {
                    this.opacity = Math.max(0, this.opacity-interval*2);
                }
                else
                    this.disabled = true;
            }
            //Fade in.
            else if(this.opacity<1)
                this.opacity = Math.min(1, this.opacity+interval*2);
        }
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    draw(context) {
        if(!this.disabled) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.opacity;
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
    }
}

//Used to restrict y-velocity of Snowflakes to a reasonable value.
var maxSnowVY = 350;
class SnowFreeFall extends FreeFallMotion {
    updateY(interval, environment) {
        super.updateY(interval, environment);
        if(this.y>maxSnowVY)
            this.y = maxSnowVY;
    }
}

//An emitter, for Snowflakes
// > New snowflakes released every 0.2s
export class SnowEmitter extends Emitter {
    constructor(box, cacheTime=10) {
        super(box, 0.1, 32, cacheTime);
    }
    createParticle() {
        return new Snowflake(this.cacheTime, 0, 0);
    }
}

class RainParticle extends ParticleForEmitter {
    constructor(x, y, vx=0, vy=0) {
        super();
        this.box = new Box(x, y, 15, 15);
        this.startPos = {x: x, y: y};
        this.motion = new FreeFallMotion(1);
        this.bodyColor = "#999"; //Grey rain
        this.pushRank = 40;
        this._reset();
        this.motion.x = vx;
        this.motion.y = vy;
    }
    _reset() {
        this.disabled = false;
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.motion.reset();
        this.opacity = 0;
    }
    respawn(x, y, vx=0, vy=0) {
        this.startPos.x = x;
        this.startPos.y = y;
        this._reset();
        this.motion.x = vx;
        this.motion.x = vy;
    }
    disable() {
        this.disabled = true;
    }
    update(interval, collisions, manager, caches, environment) {
        if(!this.disabled) {
            if(this.opacity<1)
                this.opacity = Math.min(1, this.opacity+interval*4);
            this.motion.update(this, interval, collisions, environment);
        }
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
        this.disabled = true;
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    draw(context) {
        if(!this.disabled) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.opacity;
            context.strokeStyle = this.bodyColor;
            var prevCap = context.lineCap;
            context.lineCap = "round";
            context.lineWidth = 2.5;
            context.fillStyle = this.bodyColor;
            context.beginPath();
            var dx = this.box.width/300*this.motion.x;
            context.moveTo(this.box.x+this.box.width/2-dx/2, this.box.y);
            context.lineTo(this.box.x+this.box.width/2+dx/2, this.box.y+this.box.height);
            context.stroke();
            context.lineCap = prevCap;
            context.globalAlpha = prevGA;
        }
    }
}

//An emitter, for RainParticles.
export class RainEmitter extends Emitter {
    constructor(box, cacheTime=1) {
        super(box, 0.2, 5, cacheTime);
    }
    createParticle() {
        return new RainParticle(0, 0);
    }
}
