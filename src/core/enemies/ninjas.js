//Copyright Matthew Pottage 2019.
import {ControlledMotion} from '../motion.js';
import {Collidable, registerCDU} from '../collisions/collidable.js';
import {HealthComponent, TeleportComponent} from '../components.js';
import {Player} from '../player/player.js';
import {TurquoiseBullet} from '../projectiles.js';
import {Scanner} from '../scanner.js';
import {Box, Pool, getClosest, drawFlashRect, to3dp} from '../utility.js';
import {GreenBuddy} from '../minions.js';
import {Platform} from '../platforms.js';

//Enemy that jumps around the level, attempting to hit the player.
export class Ninja extends Collidable {
    constructor(startX, startY, headbandColor) {
        super();
        this.disabled = false;
        this.box = new Box(startX, startY, 10, 60);
        var xScale = .8;
        this.motion = new NinjaMotion(xScale, 0.9);
        this.teleporter = new TeleportComponent();
        this.scanner = new Scanner(new Box(0, 0, 6000, 6000),
                o => (o instanceof Player));
        this.controller = new NinjaAI(xScale);
        this.bodyColor = "#555";
        this.color = headbandColor || this.bodyColor;
        this.startPos = {x: startX, y: startY};
        this.pushRank = 12;
        this.touching = {left: false, right: false, up: false};
        this.health = new HealthComponent(this, 2, 0);
        this.health.setCallback(
            function(o) { o.disabled = true; o.scanner.disable()});
        this.action = {left: false, right: false, jump: false, ledgeGrab: true};
        this.reset();
    }
    reset() {
        this.disabled = false;
        this.activated = true;
        this.motion.reset();
        this.teleporter.reset();
        this.controller.reset();
        this.scanner.reset();
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.health.reset();
        this.teleporter.setDefault(this.startPos.x, this.startPos.y);
        this.action.left = this.action.right = this.action.jump = false;
        this.action.ledgeGrab = true;
    }
    register(ce, m, rcm) {
        registerCDU(this, ce, m);
        this.scanner.register(ce, m, rcm);
    }
    handleCollision(obj, details) {
        if(!this.disabled) {
            this.motion.handleCollision(this, obj, details);
            this.controller.handleCollision(this, obj, details);
            //Hurt the player.
            if(obj instanceof Player)
                obj.health.increase(-1);
        }
    }
    handleIntersection(obj) {
        this.motion.handleIntersection(this, obj);
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
            context.fillStyle = this.color;
            context.fillRect(this.box.x, this.box.y+this.box.height/10,
                    this.box.width, this.box.height/10);
        }
        //Flash red when lost health (or fade red when killed).
        drawFlashRect(context, this, "rgb(230,0,0)");
    }
    update(interval, collisions, manager, caches, environment) {
        this.health.update(interval);
        if(!this.disabled) {
            this.scanner.recenter(this.box.x+this.box.width/2,
                    this.box.y+this.box.height/2);
            this.healthLossJump();
            this.teleporter.update(this, collisions);
            this.controller.update(this, interval, collisions, manager);
            this.motion.action.left = this.action.left;
            this.motion.action.right = this.action.right;
            this.motion.action.jump = this.action.jump;
            this.motion.action.ledgeGrab = this.action.ledgeGrab;
            this.motion.update(this, interval, collisions, environment);
        }
    }
    healthLossJump() {
        //Limited jump if health has just been lost.
        if(this.health.justLost) {
            if(!this.motion.touching.up)
                this.motion.y = -7*this.motion.stateParams["ground/air"].jumpVelocity/8*this.motion.multiplier.y;
        }
    }
}

class NinjaMotion extends ControlledMotion {
    constructor(xM, yM, xFM, yFM) {
        super(xM, yM, xFM, yFM);
        this.climbOnto = null; //Platform to climb onto.
        this.action.ledgeGrab = true;
        this.grounded = false;
        this.applyVelocityAir = false;
    }
    reset() {
        super.reset();
        this.climbOnto = null;
        this.action.ledgeGrab = true;
        this.grounded = true;
    }
    handleCollision(toObj, collided, details) {
        super.handleCollision(toObj, collided, details);
        //Ledge grab.
        if(collided instanceof Platform && (details.left || details.right))
            this.climbOnto = collided;
        if(details.down && !(collided instanceof Ninja))
            this.grounded = true;
        //Lost some x-velocity (esp. if airborne).
        if(collided.pushRank<=toObj.pushRank
                && ((details.left && this.action.left)
                    || (details.right && this.action.right)))
            this.applyVelocityAir = true;
    }
    handleIntersection(toObj, obj) {
        //Climb onto "Jump through Platforms".
        if(!toObj.disabled && (obj instanceof Platform) && obj.jumpThrough
            && !this.climbOnto && (obj.box.x<=toObj.box.x+toObj.box.width
                    || obj.box.x+obj.box.width>=toObj.box.x))
            this.climbOnto = obj;
    }
    handleMoved(obj, details, amount) {
        if((details.left || details.right)
                && to3dp(amount)==0) //Lost all x-velocity.
            this.applyVelocityAir = true;
    }
    update(obj, interval, collisions, environment) {
        if(this.grounded)
            this.applyVelocityAir = true;
        var prevA = {left: this.action.left, right: this.action.right};
        this._limitGroundMovement(obj, collisions);
        super.update(obj, interval, collisions, environment);
        this._ledgeGrab(obj, environment);
        this._applyVelocity();
        this.action.left, this.action.right = prevA.left, prevA.right;
        this.grounded = false;
    }
    mayLedgeGrab(obj) {
        return this.climbOnto && this.climbOnto.box.y>obj.box.y;
    }
    _ledgeGrab(obj, environment) {
        //When just above the top edge of a platform and touching/intersecting
        //it, climb onto the platform.
        if(this.action.ledgeGrab && this.mayLedgeGrab(obj)) {
            //Set y-velocity such that the Ninja jumps onto climbOnto.
            var moveAmount = this.climbOnto.box.y-obj.box.y-obj.box.height;
            this.y = Math.min(this.y,
                    -Math.sqrt(Math.abs(4*environment.get("gravity")*moveAmount))
                    );
        }
        this.climbOnto = null;
    }
    _applyVelocity() {
        //Apply velocity if it has been lost.
        if(this.applyVelocityAir && !this.grounded
            && (this.action.right || this.action.left)) {
            var speed = 300;
            if(this.action.right)
                this.x += speed*this.multiplier.x*this.speedMod.x;
            else if(this.action.left)
                this.x -= speed*this.multiplier.x*this.speedMod.x;
            this.applyVelocityAir = false;
        }
    }
    _limitGroundMovement(obj, collisions) {
        if(!this.action.jump && (this.action.left || this.action.right)
            && this.grounded
            && !collisions.any(obj, new Box(obj.box.x,
                        obj.box.y-10, obj.box.width, 10))) {
            this.action.left = false;
            this.action.right = false;
        }
    }
}

class NinjaAI {
    constructor(initialXScale) {
        //Used to negate initial multiplier on motion
        this.initialScale = initialXScale;
        this.touching = {left: false, right: false, up: false};
        this.reset();
    }
    reset() {
        this.prevScale = this.initialScale;
        this.touching.left = this.touching.right = this.touching.up = false;
        this._jumpTime = 0;
        this.grounded = false;
        this.preventTouchingUp = false;
        this.target = null;
        this.retargetTime = 0;
        this.hitNinja = null;
    }
    handleCollision(toObj, collided, details) {
        //Bouncing off Ninjas while airborne.
        if(collided instanceof Ninja
                && (details.left || details.right))
            this.hitNinja = collided;
        //Used to maneuver so that it can jump.
        if(details.up)
            this.touching.up = true;
        else if(details.left)
            this.touching.left = true;
        else if(details.right)
            this.touching.right = true;
        //Note special behaviour when Ninjas stack.
        else if(details.down && !(collided instanceof Ninja))
            this.grounded = true;
    }
    update(obj, interval, collisions) {
        this.retargetTime -= interval;
        if(!obj.activated)
            this.target = null;
        else if(this.retargetTime<=0 || !this.target) {
            this.updateTarget(obj);
            this.retargetTime = 2;
        }
        if(this.grounded)
            this.updateGrounded(obj, interval, collisions);
        else
            this.updateAirborne(obj, interval);
        this.updateLedgeGrab(obj, interval);
        this.touching.left = this.touching.right = this.touching.up = false;
        this.grounded = false;
    }
    //Jumps at random intervals and attempts move to make this possible.
    updateGrounded(obj, interval, collisions) {
        if(this.preventTouchingUp) { //Scan above for objects that would block jumping.
            if(!collisions.any(obj, new Box(obj.box.x-obj.box.width/2,
                            obj.box.y-10, obj.box.width*3/2, 10)))
                this.preventTouchingUp = false;
        }
        //Blocked going up (probably by a another Ninja), so move right/left as
        //appropriate to stop being jammed and unable to jump.
        if(this.touching.up || this.preventTouchingUp) {
            if(this.touching.right) {
                obj.action.left = true;
                obj.action.right = false;
            }
            else if(this.touching.left
                    || (!obj.action.left &&
                        !obj.action.right)) {
                obj.action.left = false;
                obj.action.right = true;
            }
            this.preventTouchingUp = true;
        }
        else {
            this._jumpTime -= interval; //Time to jump
            obj.action.left = false;
            obj.action.right = false;
        }
        //When not blocked from jumping, jump after a random interval.
        if(this._jumpTime<=0 && !this.touching.up && !this.preventTouchingUp
                && this.target) {
            obj.action.jump = true;
            var scale = 0.8+0.6*Math.random();
            obj.motion.setSpeedMod(scale/this.prevScale, 1);
            //Target player.
            if(this.target.box.x>=obj.box.x+obj.box.width)
                obj.action.right = true;
            else if(this.target.box.x<=obj.box.x)
                obj.action.left = true;
            //Jumped, random time [0.5, 1.5) till next jump.
            this._jumpTime = Math.random()+0.5;
            this.preventTouchingUp = false;
            this.prevScale = scale;
        }
    }
    updateAirborne(obj, interval) {
        obj.action.jump = false;
        //Bounce off any ninjas hit while airborne.
        if(this.hitNinja) {
            if(this.hitNinja.box.x>obj.box.x) {
                obj.action.left = true;
                obj.action.right = false;
            }
            else {
                obj.action.left = false;
                obj.action.right = true;
            }
        }
        this.hitNinja = null;
        this.preventTouchingUp = this.touching.up && obj.motion.y==0;
    }
    updateTarget(obj) {
        this.target = getClosest(obj.box, obj.scanner.seen);
    }
    //Disable ledge grab if the player is below the platform and the platform
    //isn't tall enough to block moving left/right to get the player.
    updateLedgeGrab(obj, interval) {
        obj.action.ledgeGrab = true;
        if(!this.target || (obj.motion.climbOnto &&
                obj.motion.climbOnto.box.height<=Math.abs(obj.motion.climbOnto.box.y-this.target.box.y)
                    && this.target.box.y+this.target.box.height>obj.motion.climbOnto.box.y))
            obj.action.ledgeGrab = false;
        //Ledge grab & jammed, so go the other direction to unjam.
        else if(obj.motion.mayLedgeGrab(obj) && this.touching.up) {
            obj.action.left = !obj.action.left;
            obj.action.right = !obj.action.right;
        }
    }
}

//Just like a Ninja, except it launches TurquoiseBullets at the player being
//targetted every 2s.
export class TurquoiseNinja extends Ninja {
    constructor(x, y, headbandColor) {
        super(x, y, headbandColor);
        this.bodyColor = "#4edeac";
        var missiles = [];
        for(var i=0; i<10; ++i)
            missiles.push(new TurquoiseBullet());
        this.pool = new Pool(missiles);
        this.delay = 2;
        this.untilLaunch = this.delay;
        this.pool.apply(o => o.disable());
    }
    reset() {
        super.reset();
        if(this.pool) {
            this.untilLaunch = this.delay;
            this.pool.apply(o => o.disable());
        }
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.pool.apply(o => o.register(collisions, manager, caches));
    }
    update(interval, collisions, manager, caches, environment) {
        var prevY = this.motion.y;
        super.update(interval, collisions, manager, caches, environment);
        if(!this.disabled && this.activated) {
            this.untilLaunch -= interval;
            if(this.untilLaunch<=0 && this.controller.target) {
                //Launch missile with speed 350px/s (or 0 if the player is
                //at launch position).
                var v = {x: this.controller.target.box.x-this.box.x,
                    y: this.controller.target.box.y-this.box.y};
                var scale = 350/Math.sqrt(v.x*v.x+v.y*v.y);
                if(v.x==0 && v.y==0)
                    scale = 0;
                var missile = this.pool.get();
                missile.respawn(this.box.x, this.box.y,
                    v.x*scale+this.motion.x*.25, v.y*scale+prevY*.25);
                collisions.protect(missile);
                this.untilLaunch = this.delay;
            }
        }
    }
}

export class GreenNinja extends Ninja {
    constructor(startX, startY, headbandColor) {
        super(startX, startY, headbandColor);
        this.bodyColor = "#01a700";
        this.buddy = new GreenBuddy(startX, startY, obj => obj instanceof Player);
    }
    reset() {
        super.reset();
        if(this.buddy) {
            this.buddy.respawn(this.startPos.x, this.startPos.y);
            this.buddy.setFollow(this);
            this.buddy.setDamaging(true);
        }
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.buddy.register(collisions, manager, caches);
    }
}
