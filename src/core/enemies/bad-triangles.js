//Copyright Matthew Pottage 2019.
import {HealthComponent} from '../components.js';
import {Collidable, registerCDU} from '../collisions/collidable.js'
import {Transition, Triangle, Box, drawFlashRect, Pool, getClosest, absValue} from '../utility.js';
import {Player} from '../player/player.js';
import {PinkBullet, AirborneStone, TurquoiseBullet, isDamagingProjectile} from '../projectiles.js';
import {Scanner} from '../scanner.js';
import {isMinion} from '../minions.js';
import {BouncingPickup} from '../pickups/bouncing.js';
import {Coin} from '../pickups/coins.js';

//Enemy, a moving triangle coloured brown. The player loses 1 health on
//colliding with it.
export class BadTriangle extends Collidable {
    //startX: min x-coordinate (for triangle box),
    //startY: y-coordinate of the triangle's base.
    //endX: max x-coordinate (for triangle box).
    //alternateStartX: start the triangle at a different point in the range
    //  [startX, endX], default is startX.
    constructor(startX, startY, endX, alternateStartX) {
        super();
        this.health = new HealthComponent(this, 1, 0);
        this.startPos = {x: Math.min(startX, endX), y: startY};
        this.endPos = {x: Math.max(startX, endX), y: startY};
        this.triangle = new Triangle(startX, startY, startX+32.5, startY-55,
                startX+65, startY);
        if(alternateStartX!==undefined) {
            this.triangle.translate(alternateStartX-this.startPos.x, 0);
            this.alternateStartX = alternateStartX;
        }
        else
            this.alternateStartX = this.startPos.x;
        this.box = this.triangle.box;
        this.xVelocity = 200;
        if(this.endPos.x==this.box.x) //Set velocity to left.
            this.xVelocity = -this.xVelocity;
        this.bodyColor = "#a67b5e";
        this.transitionColor = "#FF6700";
        this.activated = true;
    }
    isCollision(obj) {
        //Only collide with objects that are destroyed on impact
        //(due to how the triangle is moved this prevents collisions errors).
        return isDamagingProjectile(obj);
    }
    handleIntersection(obj) {
        if(!this.disabled) {
            //Checks that obj can lose health and obj intersects the triangle
            //and not just its AABB.
            if(obj instanceof Player && this.triangle.isIntersectionBox(obj.box))
                obj.health.increase(-1);
            //Let minions hurt the triangle
            else if(isMinion(obj) && this.triangle.isIntersectionBox(obj.box))
                obj.handleCollision(this, {});
        }
    }
    update(interval) {
        this.health.update(interval);
        if(this.disabled || !this.activated)
            return;
        //Move the triangle.
        this.triangle.translate(interval*this.xVelocity, 0);
        //It is before its leftmost point, set speed to be positive and move to
        //the leftmost position.
        if(this.triangle.points[0].x<this.startPos.x) {
            this.xVelocity = Math.abs(this.xVelocity);
            this.triangle.translate(this.startPos.x-this.box.x, 0);
        }
        //If it is after its rightmost point, set speed to be negative and move
        //the rightmost position.
        else if(this.triangle.points[0].x>this.endPos.x) {
            this.xVelocity = -Math.abs(this.xVelocity);
            this.triangle.translate(this.endPos.x-this.box.x, 0);
        }
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.triangle.drawPath(context);
            context.fill();
            var flash = this.health.getFlash();
            if(flash>0) {
                var prevGA = context.globalAlpha;
                context.globalAlpha *= flash;
                context.fillStyle = this.transitionColor;
                context.fill();
                context.globalAlpha = prevGA;
            }
        }
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
    }
    reset() {
        //Reset to start position.
        this.triangle.translate(this.alternateStartX-this.box.x, 0);
        this.health.reset();
        this.disabled = false;
        this.activated = true;
        this.xVelocity = Math.abs(this.xVelocity);
        if(this.endPos.x==this.box.x)
            this.xVelocity = -this.xVelocity;
    }
}

//Similar to BadTriangle, but bigger and with more health.
export class OrangeTriangle extends BadTriangle {
    constructor(startX, startY, endX, alternativeStartX) {
        super(startX, startY, endX, alternativeStartX);
        this.health = new HealthComponent(this, 2, 0);
        var initX = this.triangle.points[0].x;
        this.triangle = new Triangle(initX, startY, initX+37.5, startY-65,
                initX+75, startY);
        this.box = this.triangle.box;
        this.bodyColor = "#ff6803";
        this.transitionColor = "rgb(255,189,0)"; //Color flashed on losing health.
    }
}

//Like a BadTriangle, but is yellow and leaves a coin behind when killed.
export class YellowTriangle extends BadTriangle {
    constructor(sx, sy, ex, asx) {
        super(sx, sy, ex, asx);
        this.bodyColor = "gold";
        this.transitionColor = "#FF9300";
        this.gotCoin = true; //Got a coin to leave when killed.
    }
    update(interval, collisions, manager) {
        super.update(interval, collisions, manager);
        //If killed/disabled and not left its coin behind, leave a coin.
        if(this.disabled && this.gotCoin) {
            var c = new BouncingPickup(
                    new Coin(this.box.x+this.box.width/2-Coin.radius/2,
                                this.box.y+this.box.height/4), this.xVelocity, 0);
            c.register(collisions, manager);
            this.gotCoin = false;
        }
    }
    reset() {
        super.reset();
        this.gotCoin = true;
    }
}

//Like an OrangeTriangle, but has 4 health, is faster and any AirborneStones
//thrown at it will reflect/bounce from it.
export class PurpleTriangle extends OrangeTriangle {
    constructor(startX, startY, endX, altStartX) {
        super(startX, startY, endX, altStartX);
        this.bodyColor = "#8E3FBF";
        this.transitionColor = "rgb(255,30,116)";
        this.xVelocity *= 1.1;
        this.impactedStone = null;
    }
    update(interval, collisions, manager) {
        super.update(interval, collisions, manager);
        if(this.impactedStone) {
            //Stone only disabled if it caused something to lose health.
            if(this.impactedStone.disabled) {
                //Relaunch the stone, in the opposite direction.
                var stoneMotion = {x: this.impactedStone.motion.x,
                    y: this.impactedStone.motion.y};
                this.impactedStone.reset();
                this.impactedStone.motion.x = stoneMotion.x*-1+this.xVelocity;
                this.impactedStone.motion.y = stoneMotion.y*-.9;
                this.impactedStone.setThrownBy(this);
                collisions.protect(this.impactedStone);
            }
            this.impactedStone = null; //await next stone.
        }
    }
    handleCollision(obj) {
        super.handleCollision(obj);
        if(!this.disabled && obj instanceof AirborneStone)
            this.impactedStone = obj;
    }
    reset() {
        super.reset();
        this.impactedStone = null;
    }
}

//Like OrangeTriangle, but invincible, moves slower and temporarily stops moving
//when hit by a stone.
export class GreyTriangle extends BadTriangle {
    constructor(startX, startY, endX, altStartX) {
        super(startX, startY, endX, altStartX);
        this.health.setCallback(function(o) {
            //Prevent the triangle from dying (just freezes)
            o.health.reset();
            //Wait 5s before moving again if would have lost health.
            o.tilContinue = 5;
        });
        this.bodyColor = "#888";
        //See OrangeTriangle for modified size.
        var initX = this.box.x;
        this.triangle = new Triangle(initX, startY, initX+37.5, startY-65,
                initX+75, startY);
        this.box = this.triangle.box;
        this.xVelocity *= .7; //Moves slower than an BrownTriangle.
        this.tilContinue = 0; //Seconds until the GreyTriangle can move again.
    }
    update(interval, collisions, manager) {
        this.tilContinue -= interval;
        if(this.tilContinue<=0)
            super.update(interval, collisions, manager);
    }
    reset() {
        super.reset();
        this.tilContinue = 0;
    }
}

//Like an OrangeTriangle, but slightly slower and fires slow moving PinkBullets
//at the player every 4s.
export class PinkTriangle extends OrangeTriangle {
    constructor(sx, sy, ex, asx) {
        super(sx, sy, ex, asx);
        this.bodyColor = "pink";
        this.transitionColor = "rgb(255,92,121)";

        this.launchDelay = 4; //Wait 4s between launches.
        this.untilLaunch = this.launchDelay;
        this.xVelocity *= 0.9;
        //Pool of missiles to use (see StoneLauncher).
        var poolElems = [];
        for(var i=0; i<4; ++i) {
            poolElems.push(new PinkBullet(0, 0, 0, 0));
            poolElems[i].disable();
        }
        this.pool = new Pool(poolElems);
        this.scanner = new Scanner(new Box(0, 0, 2000, 2000),
                o => (o instanceof Player));
    }
    reset() {
        super.reset();
        this.scanner.reset();
        this.pool.apply(o => o.disable());
        this.untilLaunch = this.launchDelay;
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.pool.register(collisions, manager, caches);
        this.scanner.register(collisions, manager, caches);
    }
    update(interval, collisions, manager, caches) {
        super.update(interval, collisions, manager, caches);
        if(!this.disabled) {
            this.scanner.recenter(this.box.x+this.box.width/2,
                    this.box.y+this.box.height/2);
            this.untilLaunch -= interval;
            if(this.activated && this.untilLaunch<=0) {
                var target = getClosest(this.box, this.scanner.seen);
                if(target) {
                    //Launch missile with speed 300px/s (or 0 if the player is
                    //at launch position).
                    var v = {x: target.box.x-this.box.x,
                        y: target.box.y-this.box.y};
                    var scale = 300/Math.sqrt(v.x*v.x+v.y*v.y);
                    if(v.x==0 && v.y==0)
                        scale = 0;
                    //Throw next missile from the pool.
                    var missile = this.pool.get();
                    missile.respawn(this.box.x, this.box.y, v.x*scale,
                            v.y*scale);
                    this.untilLaunch = this.launchDelay;
                }
            }
        }
    }
}

//Like BadTriangle, but smaller, slower, blue, and bobs up and down.
export class BlueTriangle extends BadTriangle {
    constructor(startX, startY, endX, alternateStartX, offset=Math.random()) {
        super(startX, startY, endX, alternateStartX);
        this.bodyColor = "#add8e6";
        this.transitionColor = "#2284A4";
        //Smaller triangle.
        var initX = this.triangle.points[0].x;
        this.triangle = new Triangle(initX, startY, initX+25, startY-43.5,
                initX+50, startY);
        this.box = this.triangle.box;
        //Used for bobbing up and down along a cosine curve.
        this.startOffset = offset;
        this.angle = offset*Math.PI*2; //Offset along curve.
        this.angleRate = Math.PI*2; //radians/s to move along curve.
        this.adjustFactor = 15; //Max amount to move up/down.
        this._applyAdjust();
        this.xVelocity *= .7; //Slower.
    }
    _applyAdjust() {
        var offset = Math.cos(this.angle)*this.adjustFactor;
        this.triangle.translate(0,
                this.startPos.y+offset-this.triangle.points[0].y);
    }
    update(interval, collisions, manager, caches) {
        super.update(interval, collisions, manager, caches);
        if(!this.disabled) {
            this.angle += interval*this.angleRate;
            while(this.angle>Math.PI*2)
                this.angle -= Math.PI*2;
            this._applyAdjust();
        }
    }
    reset() {
        super.reset();
        this.angle = this.startOffset*Math.PI*2;
        this._applyAdjust();
    }
}

//Like a BadTriangle, but fires a TurquoiseBullet at the closest player when
//killed.
export class TurquoiseTriangle extends BadTriangle {
    constructor(startX, startY, endX, altStartX) {
        super(startX, startY, endX, altStartX);
        this.scanner = new Scanner(new Box(0, 0, 2000, 2000),
                o => (o instanceof Player));
        this.bodyColor = "#4edeac";
        this.transitionColor = "#008B5B";
        this.launched = false;
    }
    reset() {
        super.reset();
        this.scanner.reset();
        this.launched = false;
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.scanner.register(collisions, manager, caches);
    }
    update(interval, collisions, manager, caches) {
        super.update(interval, collisions, manager, caches);
        if(!this.disabled) {
            this.scanner.recenter(this.box.x+this.box.width/2,
                    this.box.y+this.box.height/2);
        }
        else if(!this.launched) {
            this.scanner.disable();
            var target = getClosest(this.box, this.scanner.seen);
            if(target) {
                //Launch missile with speed 300px/s.
                var v = {x: target.box.x-this.box.x,
                    y: target.box.y-this.box.y};
                var scale = 0;
                if(v.x==0 && v.y==0)
                    scale = 0;
                else
                    scale = 500/absValue(v.x, v.y);
                //Throw next missile from the pool.
                (new TurquoiseBullet(this.box.x, this.box.y, v.x*scale, v.y*scale))
                    .register(collisions, manager, caches);
            }
            this.launched = true;
        }
    }
}
