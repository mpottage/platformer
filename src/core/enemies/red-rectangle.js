//Copyright Matthew Pottage 2019.
import {ControlledMotion} from '../motion.js';
import {Collidable, registerCDU} from '../collisions/collidable.js';
import {Scanner} from '../scanner.js';
import {Box, getClosest, drawFlashRect, to3dp} from '../utility.js';
import {Player} from '../player/player.js';
import {TeleportComponent, HealthComponent} from '../components.js';
//A red rectangle, uses teleporting to attack.
//  > Teleports to above the player if it hits an obstacle or a sufficient
//  amount of time has elapsed since it last teleported.
//  > Fires a horizontal beam when it is on a platform at the same level as the
//  closest player that (if it hits the player) makes it lose 1 health.
//  > Moves towards the player when on a surface.
//  > Player loses a health if it comes into contact with it.
export class RedRectangle extends Collidable {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, 50, 110);
        this.startPos = {x: x, y: y};
        this.bodyColor = "#920010";
        this.teleporter = new TeleportComponent();
        this.motion = new ControlledMotion(0.6, 1.2);
        var scanWidth = 2*1200;
        this.scanner = new Scanner(new Box(0, 0, scanWidth, scanWidth),
                o => o instanceof Player);
        this.pushRank = 10;
        this.health = new HealthComponent(this, 2);
        this.health.setCallback(
            function(o) { o.disabled = true; o.scanner.disable(); });
        this.reset();
    }
    reset() {
        this.disabled = false;
        this.motion.reset();
        this.teleporter.reset();
        this.teleporter.setDefault(this.startPos.x, this.startPos.y);
        this.scanner.reset();
        this.activated = true;
        this.health.reset();
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.target = null;
        this.retargetTime = 0; //Time until scanning for a new target.
        this.untilStart = 0; //Used to delay moving when making a beam.
        this.untilBeam = 5; //Initial beam delay of 5s, 15s after that.
        this.beamTo = {x: 0, y: 0}; //Drawing health loss beam.
        this._randomJump();
        this.touchingDown = false; //Used for jumping.
        this._randomTeleport();
        //Used to fade between positions when teleporting.
        this.beforeTeleport = {x: this.startPos.x, y: this.startPos.y};
        this.teleportFade = 0;
    }
    _randomTeleport() {
        this.teleportTime = 10+Math.random()*10;
    }
    _randomJump() {
        this.untilJump = 3+Math.random()*10;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
        this.scanner.register(collisions, manager, caches);
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
        if(obj.pushRank<this.pushRank) {
            if(details.down)
                this.touchingDown = true;
            //Teleport if an obstruction is hit on the left/right.
            else if(this.target
                    && (details.left || details.right)
                    && !this.teleporter.justUsed)
                this.teleportTime = 0;
        }
        if(obj instanceof Player)
            obj.health.increase(-1);
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    update(interval, collisions, manager, caches, environment) {
        this.health.update(interval);
        if(!this.disabled) {
            this.scanner.recenter(this.box.x+this.box.width/2,
                    this.box.y+this.box.height/2);
            if(this.activated)
                this.updateAI(interval, collisions, manager);
            else {
                this.motion.action.left = this.motion.action.right = false;
                this.motion.action.jump = false;
            }
            this.teleporter.update(this, collisions);
            this.motion.update(this, interval, collisions, environment);
            this.touchingDown = false;
        }
    }
    updateAI(interval, collisions, manager) {
        this.setTarget(interval);
        this.useBeams(interval, collisions);
        this.motion.action.right = this.motion.action.left = false;
        if(this.untilStart<=0) {
            this.targetTarget();
            this.triggerTeleports(interval);
            this.triggerJumping(interval);
        }
        else
            this.untilStart -= interval;
    }
    setTarget(interval) {
        this.retargetTime -= interval;
        if(this.retargetTime<=0 || !this.target) {
            this.target = getClosest(this.box, this.scanner.seen);
            this.retargetTime = 3;
        }
    }
    //Move towards the target (if any).
    targetTarget() {
        if(this.target) {
            if(this.target.box.x+this.target.box.width<=this.box.x)
                this.motion.action.left = true;
            else if(this.target.box.x>=this.box.x+this.box.width)
                this.motion.action.right = true;
        }
    }
    //Teleport closer to the target (if any).
    triggerTeleports(interval) {
        this.teleportTime -= interval;
        this.teleportFade = Math.max(0, this.teleportFade-2*interval); //Fade in 0.5s.
        if(this.target && this.teleportTime<=0) {
            this.beforeTeleport.x = this.box.x;
            this.beforeTeleport.y = this.box.y;
            this.teleportFade = 1;
            this.teleporter.toAuto(this.target.box.x+this.target.box.width/2-this.box.width/2,
                    this.target.box.y-150-this.box.height);
            this._randomTeleport();
        }
    }
    triggerJumping(interval) {
        this.motion.action.jump = false;
        //Jump at random intervals, when grounded.
        if(this.touchingDown) {
            this.untilJump -= interval;
            if(this.untilJump<=0) {
                this.motion.action.jump = true;
                this._randomJump();
            }
        }
    }
    useBeams(interval, collisions) {
        this.untilBeam -= interval;
        //Launch a beam if:
        //  > Target to aim at.
        //  > Beam loaded.
        //  > This is on the ground.
        //  > Target is at the same level.
        if(this.target && this.untilBeam<=0 && this.touchingDown
                && to3dp(this.target.box.y+this.target.box.height)==to3dp(
                    this.box.y+this.box.height)) {
            this.untilStart = 1; //Freeze while beam is shown.
            //Beam extends from the center of this for 1000px, horizontally.
            const range = 1000;
            var beamStart = {x: 0, y: this.box.y+this.box.height/2};
            if(this.target.box.x>this.box.x+this.box.width/2)
                beamStart.x = this.box.x+this.box.width/2;
            else
                beamStart.x = this.box.x+this.box.width/2-range;
            //Get all objects this would collide with (any would block the beam).
            var trace = collisions.all(this, new Box(beamStart.x, beamStart.y, range, 1));
            var hit = getClosest(this.box, trace);
            //Determine where the beam stopped.
            this.beamTo = {x: beamStart.x+range, y: beamStart.y};
            if(hit) {
                //Cause damage.
                if(hit.health)
                    hit.health.increase(-1);
                //Determine limit.
                if(beamStart.x<this.box.x)
                    this.beamTo.x = hit.box.x+hit.box.width;
                else
                    this.beamTo.x = hit.box.x;
            }
            this.untilBeam = 15; //Wait 15s for next beam.
        }
    }
    draw(context) {
        if(!this.disabled) {
            this.drawBeam(context);
            this.drawBody(context);
        }
        drawFlashRect(context, this, "#f00");
    }
    drawBeam(context) {
        //Draw beam, if it was just active, fading over time.
        if(this.untilStart>0) {
            context.beginPath();
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.untilStart;
            var center = this.box.center;
            context.moveTo(center.x, center.y);
            context.lineTo(this.beamTo.x, this.beamTo.y);
            context.strokeStyle = "#FF0010";
            context.lineWidth = 20;
            var prevCap = context.lineCap;
            context.lineCap = "butt";
            context.stroke();
            context.lineCap = prevCap;
            context.globalAlpha = prevGA;
        }
    }
    drawBody(context) {
        //Draw, fading between positions when teleporting.
        var prevGA = context.globalAlpha;
        context.globalAlpha *= (1-this.teleportFade);
        context.fillStyle = this.bodyColor;
        this.box.fill(context);
        //If a previous position should still be visible.
        if(this.teleportFade>0) {
            context.globalAlpha = prevGA*this.teleportFade;
            context.fillRect(this.beforeTeleport.x, this.beforeTeleport.y,
                    this.box.width, this.box.height);
        }
        context.globalAlpha = prevGA;
    }
}
