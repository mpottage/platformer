//Copyright Matthew Pottage 2019.
import {TeleportComponent, HealthComponent} from '../components.js';
import {Box, Pool, getClosest, drawFlashRect} from '../utility.js';
import {HoverMotion} from '../motion.js';
import {OrangeBullet, PurpleMine, isProjectile, playerSelector,
    orangeBulletColor, orangeTransitionColor} from '../projectiles.js';
import {Player} from '../player/player.js';
import {Scanner} from '../scanner.js';
import {Collidable, registerCDU} from '../collisions/collidable.js';

//A small moving square (color set in constructor).
//  > Targets the closest player.
//  > Doesn't respond to gravity and has constant acceleration.
//  > Has 1 health.
class HoverSquare extends Collidable {
    constructor(x, y, bodyColor, transitionColor="red") {
        super();
        this.box = new Box(x, y, 30, 30);
        this.motion = new HoverMotion(1, 1, .5, 1, 1);
        this.teleporter = new TeleportComponent();
        this.scanner = new Scanner(new Box(0, 0, 5000, 5000),
                o => (o instanceof Player));
        this.startPos = {x: x, y: y};
        this.bodyColor = bodyColor;
        this.transitionColor = transitionColor;
        this.health = new HealthComponent(this, 1, .5, .4);
        this.health.setCallback(function(o) {
            o.disabled = true; o.scanner.disable();});
        this.pushRank = 16;
        this.activated = true;
    }
    reset() {
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.motion.reset();
        this.teleporter.reset();
        this.teleporter.setDefault(this.startPos.x, this.startPos.y);
        this.health.reset();
        this.scanner.reset();
        this.disabled = false;
        this.activated = true;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
        this.scanner.register(collisions, manager, caches);
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
            drawFlashRect(context, this, this.transitionColor);
        }
        else { //Fade out when killed.
            drawFlashRect(context, this, this.bodyColor);
        }
    }
    update(interval, collisions, manager, caches, environment) {
        this.health.update(interval);
        if(!this.disabled) {
            this.scanner.recenter(this.box.x+this.box.width/2,
                    this.box.y+this.box.height/2);
            this.teleporter.update(this, collisions);
            this.motion.clearActions();
            if(this.activated) {
                var target = getClosest(this.box, this.scanner.seen);
                //Set the motion actions to acclerate towards player.
                if(target)
                    this.motion.targetActions(this, target);
            }
            this.motion.update(this, interval, collisions, environment);
        }
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
}

//An orange HoverSquare.
//  > Launches OrangeBullets every 1s in the direction of motion.
export class OrangeSquare extends HoverSquare {
    constructor(x, y) {
        super(x, y, orangeBulletColor, orangeTransitionColor);
        var missiles = [];
        for(var i=0; i<10; ++i)
            missiles.push(new OrangeBullet());
        this.pool = new Pool(missiles);
        this.delay = 1;
        this.untilLaunch = this.delay;
        this.pool.apply(o => o.disable());
    }
    reset() {
        super.reset();
        this.untilLaunch = this.delay;
        this.pool.apply(o => o.disable());
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.pool.apply(o => o.register(collisions, manager, caches));
    }
    update(interval, collisions, manager, caches, environment) {
        super.update(interval, collisions, manager, caches, environment);
        if(!this.disabled && this.activated) {
            this.untilLaunch -= interval;
            if(this.untilLaunch<=0) {
                var missile = this.pool.get();
                var center = this.box.center;
                missile.respawn(center.x-missile.box.width/2,
                    center.y-missile.box.height/2,
                    this.motion.x*5, this.motion.y*2);
                this.untilLaunch = this.delay;
            }
        }
    }
}

//An almost black HoverSquare.
//  > Causes health loss for a player on impact.
export class BlackSquare extends HoverSquare {
    constructor(x, y) {
        super(x, y, "#555");
    }
    handleCollision(obj, details) {
        super.handleCollision(obj, details);
        if(obj instanceof Player)
            obj.health.increase(-1);
    }
}

export class GhostSquare extends HoverSquare {
    constructor(x, y) {
        super(x, y, "rgba(100,100,100,.5)");
    }
    isCollision(obj) {
        return isProjectile(obj);
    }
    handleIntersection(obj) {
        if(obj instanceof Player)
            obj.health.increase(-1);
    }
}

//Leaves a trail of PurpleMines behind itself and causes health loss on impact
export class PurpleSquare extends HoverSquare {
    constructor(x, y) {
        super(x, y, "purple", "#E01261");
        var mines = [];
        for(var i=0; i<4; ++i)
            mines.push(new PurpleMine());
        this.pool = new Pool(mines);
        this.delay = 1.5;
        this.untilLaunch = this.delay;
        this.pool.apply(o => o.disable());
    }
    reset() {
        super.reset();
        this.untilLaunch = this.delay;
        this.pool.apply(o => o.disable());
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.pool.apply(o => o.register(collisions, manager, caches));
    }
    handleCollision(obj, details) {
        super.handleCollision(obj, details);
        if(obj instanceof Player)
            obj.health.increase(-1);
    }
    update(interval, collisions, manager, caches, environment) {
        super.update(interval, collisions, manager, caches, environment);
        if(!this.disabled && this.activated) {
            this.untilLaunch -= interval;
            if(this.untilLaunch<=0) {
                var mine = this.pool.get();
                var center = this.box.center;
                mine.respawn(center.x-mine.box.width/2,
                    center.y-mine.box.height/2, this);
                this.untilLaunch = this.delay;
            }
        }
    }
}
