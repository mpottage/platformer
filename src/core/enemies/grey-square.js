//Copyright Matthew Pottage 2019.
import {Collidable, registerCDU} from '../collisions/collidable.js';
import {Box, Pool, Transition} from '../utility.js';
import {SpikeRow, DeployedSpike, defaultSpikeColor} from '../spikes.js';
import {ReboundFreeFall} from '../motion.js';
import {AirborneStone} from '../projectiles.js';
import {TeleportComponent} from '../components.js';
import {Player} from '../player/player.js';
//Square that bounces around randomly (see also GreySquareMotion).
// > Leaves a spike (with at most 10 at once) at every fixed point it lands.
// > Killed if the player lands on its head.
export class GreySquare extends Collidable {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, 60, 60);
        this.startPos = {x: x, y: y};
        this.motion = new RandomBouncingMotion(.9);
        this.teleporter = new TeleportComponent();
        this.pushRank = 15;
        this.bodyColor = defaultSpikeColor;
        var spikes = [];
        this.spikeWidth = 15;
        for(var i=0; i<10; ++i)
            spikes.push(new DeployedSpike(
                            new Box(0, 0, this.spikeWidth, this.spikeWidth),
                            this.bodyColor)
                    );
        this.spikes = new Pool(spikes);
        this.reset();
    }
    reset() {
        this.disabled = false;
        this.motion.reset();
        this.teleporter.reset();
        this.teleporter.setDefault(this.startPos.x, this.startPos.y);
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.spikeAdded = false; //Added a spike to this frame.
        this.spikes.apply(o => o.reset());
        this.activated = true;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager, caches);
        this.spikes.register(collisions, manager, caches);
    }
    isCollision(obj) {
        return !(obj instanceof SpikeRow)
            && !((obj instanceof AirborneStone) && obj.nonDamaging);
    }
    handleCollision(obj, details) {
        if(this.disabled)
            return;
        this.motion.handleCollision(this, obj, details);
        if(this.activated && details.down && !obj.motion && !this.spikeAdded) {
            //Get a position on the platform, place the spike there:
            this.placeSpike(Math.round(Math.min(Math.max(this.box.x+this.box.width/2-this.spikeWidth/2, obj.box.x), obj.box.x+obj.box.width-this.spikeWidth)),
                    Math.round(obj.box.y-this.spikeWidth));
        }
        //Handle being killed by a player.
        if((obj instanceof Player) && details.up) {
            this.disabled = true;
            this.transitionOut = new Transition(1, 0, .4);
        }
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    update(interval, collisions, manager, caches, environment) {
        if(this.transitionOut) {
            this.transitionOut.update(interval);
            if(this.transitionOut.elapsed>=this.transitionOut.duration)
                this.transitionOut = null;
        }
        if(!this.disabled) {
            this.teleporter.update(this, collisions);
            this.motion.update(this, interval, collisions, environment);
        }
    }
    placeSpike(x, y) {
        //Determine whether it is safe to put a spike at this position.
        if((x>=this.box.x && x+this.spikeWidth<=this.box.x+this.box.width)
            && (this.spikes.every(s => (s.disabled || s.box.x!=x || s.box.y!=y)))) {
            this.spikes.get().position(x, y);
            this.addedSpike = true;
        }
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
        }
        //Fade out when killed.
        if(this.transitionOut) {
            var prevGA = context.globalAlpha;
            context.globalAlpha *= this.transitionOut.value;
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
    }
}

//For GreySquare: Causes bouncing randomly, jumping only when slowing down.
class RandomBouncingMotion extends ReboundFreeFall {
    constructor(rebound, xFM, yFM) {
        super(rebound, xFM, yFM);
        //x velocity change to apply next frame (prevents friction interfering
        //with x-velocity after leaving the ground).
        this._lateApply = 0;
    }
    reset() {
        super.reset();
        this._lateApply = 0;
    }
    update(obj, interval, collisions, environment) {
        this.x += this._lateApply;
        this._lateApply = 0;
        if(this.touching.down && obj.activated!==false) {
            if(this.prevY>100)
                this._lateApply = 400*(Math.random()-.5);
            else
                this.y -= 400;
        }
        super.update(obj, interval, collisions, environment);
    }
}
