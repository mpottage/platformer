//Copyright Matthew Pottage 2019.
import {FreeFallMotion} from '../motion.js';
import {Collidable, registerCDU} from '../collisions/collidable.js';
import {Scanner} from '../scanner.js';
import {Box, Triangle, drawFlashRect, getClosest, Transition, to3dp} from '../utility.js';
import {isProjectile, isDamagingProjectile, Missile} from '../projectiles.js';
import {HealthComponent} from '../components.js';
import {Player} from '../player/player.js';
import {Platform} from '../platforms.js';
import {isMinion} from '../minions.js';

//A large green square. Functions as a "boss" enemy.
//  > Much more difficult to defeat than a normal opponent.
//  > Spawns (up to 3 at one time) GreenSquareTriangles to defend itself.
//  > Jumps and causes dangerous shockwaves (lose 1 health) when it lands.
//  > Attempts to dodge stones and missiles, and launches Missiles in response
//    to needing to dodge.
//  > Causes the player to lose 1 health on impact.
export class GreenSquare extends Collidable {
    constructor(x, y) {
        super();
        this.startPos = {x: x, y: y};
        this.box = new Box(x, y, 200, 200);
        this.health = new HealthComponent(this, 3);
        this.health.setCallback(function(o) { o.disabled = true; o.scanner.disable(); });
        this.motion = new FreeFallMotion();
        var range = 600;
        //Look for missiles and stones.
        this.scanner = new Scanner(new Box(0, 0, 2*range, 2*range),
            isDamagingProjectile);
        this.pushRank = 9;
        this.bodyColor = "#217867";
        //Triangles to spawn.
        this.triangles = [];
        for(var i=0; i<3; ++i)
            this.triangles.push({t: new GreenSquareTriangle(0, 0), untilLaunch: 0});
        this.reset();
    }
    reset() {
        this.disabled = false;
        this.activated = true;
        this.scanner.reset();
        this.motion.reset();
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.health.reset();
        this.transitionShock = null;
        this.missileTimeout = 0; //Time until a missile can be launched.
        //Launch triangles at 8s intervals, one immediately.
        this.triangles.forEach(function(o, i) {
            o.t.disable();
            o.untilLaunch = i*8;
        });
        this.threatened = false; //Needed to dodge a missile?
        //Used to detect when to cause a shock.
        this.touchingDown = this.wasJumping = false;
        this._randomJumpTime();
    }
    register(collisions, manager, caches) {
        this.triangles.forEach(o => o.t.register(collisions, manager, caches));
        registerCDU(this, collisions, manager, caches);
        this.scanner.register(collisions, manager, caches);
    }
    handleCollision(obj, details) {
        if(!this.disabled) {
            if(obj instanceof Player)
                obj.health.increase(-1);
            if(details.down && obj.pushRank<=this.pushRank)
                this.touchingDown = true;
            this.motion.handleCollision(this, obj, details);
        }
    }
    handleMoved(details, amount) {
        this.motion.handleMoved(this, details, amount);
    }
    update(interval, collisions, manager, caches, environment) {
        this.health.update(interval);
        this.updateTransitions(interval);
        if(!this.disabled) {
            if(this.activated) {
                this.dodgeProjectiles();
                this.shockwaveJumping(interval, collisions);
                this.launchMissiles(interval, collisions, manager, caches);
                this.launchTriangles(interval);
            }
            else
                this.motion.accel.x = 0;
            this.motion.update(this, interval, collisions, environment);
            this.touchingDown = false;
        }
    }
    //Update any active transitions.
    updateTransitions(interval) {
        if(this.transitionShock) {
            this.transitionShock.update(interval);
            if(this.transitionShock.elapsed>=this.transitionShock.duration)
                this.transitionShock = null;
        }
    }
    //Spawn defensive triangles, from pool. Regenerates any disabled triangles
    //after 8s.
    launchTriangles(interval) {
        this.triangles.forEach(function(o) {
            if(o.t.disabled) {
                if(o.untilLaunch<=0) {
                    if(this.touchingDown) {
                        o.untilLaunch = 8;
                        var y = this.box.y+this.box.height;
                        o.t.resetTo(this.box.x+this.box.width/2-o.t.box.width/2,
                                y);
                        this.prevLaunchDir = -this.prevLaunchDir;
                    }
                }
                else
                    o.untilLaunch -= interval;
            }
        }, this);
    }
    //Dodge stones and missiles, sets this.threatened to true if it dodged
    //something.
    //WARNING: Modifies this.motion.accel.x.
    dodgeProjectiles() {
        this.scanner.recenter(this.box.x+this.box.width/2, this.box.y+this.box.height/2);
        var hit = getClosest(this.box, this.scanner.seen);
        if(hit) {
            this.threatened = true;
            if(hit.box.x<(this.box.x+this.box.width/2))
                this.motion.accel.x = 4000;
            else
                this.motion.accel.x = -4000;
        }
        else {
            this.threatened = false;
            if(Math.abs(this.startPos.x-this.box.x)>50) {
                if(this.box.x>this.startPos.x)
                    this.motion.accel.x = -300;
                else//less than
                    this.motion.accel.x = 300;
            }
            else
                this.motion.accel.x = 0;
        }
    }
    //Launch missiles in response to this.threatened being true, 7s minimum interval.
    launchMissiles(interval, collisions, manager, caches) {
        if(this.missileTimeout>0)
            this.missileTimeout -= interval;
        if(this.threatened && this.missileTimeout<=0) {
            //Missile targetting players.
            (new Missile(this.box.x+this.box.width/2, this.box.y+this.box.height/2, 0, 0,
                         o => o instanceof Player, this))
                .register(collisions, manager, caches);
            this.missileTimeout = 7;
            this.threatened = false;
        }
    }
    //Jumping at random intervals (see _randomJumpTime) and causes the effects
    //of shockwaves when GreenSquare lands due to jump.
    shockwaveJumping(interval, collisions) {
        if(this.touchingDown) {
            if(this.wasJumping) {
                this.wasJumping = false;
                this.transitionShock = new Transition(1, 0, 0.5);
                //Cause all players at the same ground level as this to lose one
                //health (2000px range).
                collisions.rayTrace(
                        this.box.x-2000, this.box.y+this.box.height-1,
                        this.box.x+this.box.width+2000,
                        this.box.y+this.box.height-1)
                    .filter(o => (o instanceof Player)
                            && (to3dp(o.box.y+o.box.height)==to3dp(this.box.y+this.box.height)))
                    .forEach(o => o.health.increase(-1));
            }
            //Jump now.
            else if(this.untilJump<=0) {
                this._randomJumpTime();
                this.wasJumping = true;
                this.motion.y -= 400; //Jump velocity;
            }
            else
                this.untilJump -= interval;
        }
    }
    //Jump interval is uniform(2, 6) seconds.
    _randomJumpTime() {
        this.untilJump = 2+6*Math.random();
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
        }
        //Flash green when lost health (or fade from green when killed).
        drawFlashRect(context, this, "#0f0");
        //Flash grey when a shockwave has just been generated.
        if(this.transitionShock) {
            context.fillStyle = "rgba(150,150,150,"+this.transitionShock.value+")";
            this.box.fill(context);
        }
    }
}

//Similar to a BadTriangle, but a different colour, tracks the closest player
//and does't move through walls.
class GreenSquareTriangle extends Collidable {
    constructor(startX, startY, dir) {
        super();
        this.triangle = new Triangle(startX, startY, startX+32.5, startY-55,
                startX+65, startY);
        this.box = this.triangle.box;
        this.startPos = {x: startX, y: startY};
        this.prev = {x: this.box.x, y: this.box.y};
        this.pushRank = 9;
        this._savedSpeed = 100;
        this.bodyColor = "#217867";
        this.health = new HealthComponent(this, 1, 0);
        this.reset();
    }
    reset() {
        this.health.reset();
        this.disabled = false;
        this.xVelocity = this._savedSpeed;
        this.triangle.translate(this.startPos.x-this.box.x, 0);
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
    }
    resetTo(x, y) {
        this.startPos.x = x;
        this.startPos.y = y;
        this.triangle.translate(0, y-this.box.y-this.box.height);
        this.reset();
    }
    disable() {
        this.health.kill();
    }
    isCollision(obj) {
        return (obj instanceof Platform) || isProjectile(obj)
            || isMinion(obj) || (obj instanceof GreenSquareTriangle);
    }
    handleIntersection(obj) {
        //See BadTriangle.
        if(!this.disabled && obj instanceof Player && this.triangle.isIntersectionBox(obj.box))
            obj.health.increase(-1);
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.triangle.drawPath(context);
            context.fill();
        }
    }
    update(interval, collisions, manager, caches) {
        this.health.update(interval);
        if(!this.disabled) {
            this.prev.x = this.box.x;
            this.prev.y = this.box.y;
            var p = getClosest(this.box, manager.players);
            if(p) {
                if(p.box.x+p.box.width<this.box.x)
                    this.xVelocity = -this._savedSpeed;
                else if(p.box.x>this.box.x+this.box.width)
                    this.xVelocity = this._savedSpeed;
                else
                    this.xVelocity = 0;
            }
            else if(this.xVelocity==0)
                this.xVelocity = this._savedSpeed;
            collisions.queue(this, interval*this.xVelocity, 0);
        }
    }
    handleCollision(obj, details) {
        if(obj.pushRank<this.pushRank && (
                   (details.right && this.xVelocity>0)
                || (details.left && this.xVelocity<0)
                    ))
            this.xVelocity *= -1;
    }
    handleMoved(obj, amount) {
        this.triangle.translate(this.box.x-this.prev.x, this.box.y-this.prev.y);
    }
}
