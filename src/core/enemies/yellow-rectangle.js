import {Collidable, registerCDU} from '../collisions/collidable.js';
import {HealthComponent, TeleportComponent} from '../components.js';
import {Scanner} from '../scanner.js';
import {Box, getClosest, drawFlashRect} from '../utility.js';
import {Player} from '../player/player.js';
import {Platform} from '../platforms.js';
import {TripleCoin} from '../pickups/coins.js';
import {BouncingPickup} from '../pickups/bouncing.js';
import {ControlledMotion} from '../motion.js';

//Moves left/right quickly, and can climb walls.
//  > Drops a TripleCoin when killed.
//  > Has 3 health
//  > Hitting any side causes health loss.
export class YellowRectangle extends Collidable {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, 140, 80);
        this.startPos = {x: x, y: y};
        this.bodyColor = "gold";
        this.teleporter = new TeleportComponent();
        this.motion = new ControlledMotion(1.3, 1, .3);
        var scanWidth = 2*4000;
        this.scanner = new Scanner(new Box(0, 0, scanWidth, scanWidth),
                o => o instanceof Player);
        this.pushRank = 10;
        this.health = new HealthComponent(this, 3);
        this.health.setCallback(
            function(o) { o.disabled = true; o.scanner.disable(); });
        this.reset();
    }
    reset() {
        this.disabled = false;
        this.motion.reset();
        this.teleporter.reset();
        this.teleporter.setDefault(this.startPos.x, this.startPos.y);
        this.scanner.reset();
        this.activated = true;
        this.health.reset();
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.target = null;
        this.retargetTime = 0; //Time until scanning for a new target.
        this.leftCoin = false;
    }
    register(collisions, manager, caches) {
        registerCDU(this, collisions, manager);
        this.scanner.register(collisions, manager, caches);
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
        if(obj instanceof Platform && (details.left || details.right))
                this.climbOnto = obj;
        if(obj instanceof Player)
            obj.health.increase(-1);
    }
    setTarget(interval) {
        this.retargetTime -= interval;
        if(this.retargetTime<=0 || !this.target) {
            this.target = getClosest(this.box, this.scanner.seen);
            this.retargetTime = 3;
        }
    }
    //Move towards the target (if any).
    targetTarget() {
        this.motion.action.right = this.motion.action.left = false;
        if(this.target) {
            if(this.target.box.x+this.target.box.width<=this.box.x)
                this.motion.action.left = true;
            else if(this.target.box.x>=this.box.x+this.box.width)
                this.motion.action.right = true;
        }
    }
    ledgeGrab(environment) {
        //When just above the top edge of a platform and touching/intersecting
        //it, climb onto the platform.
        if(this.climbOnto && this.climbOnto.box.y>this.box.y-this.box.height) {
            var moveAmount = this.climbOnto.box.y-this.box.y-this.box.height;
            this.motion.y = Math.min(this.motion.y,
                    -Math.sqrt(Math.abs(2*environment.get("gravity")*moveAmount)),
                    -50
                    );
        }
        this.climbOnto = null;
    }
    update(interval, collisions, manager, caches, environment) {
        this.health.update(interval);
        if(!this.disabled) {
            this.scanner.recenter(this.box.x+this.box.width/2,
                    this.box.y+this.box.height/2);
            if(this.activated) {
                this.setTarget(interval);
                this.targetTarget();
                this.ledgeGrab(environment);
            }
            else {
                this.motion.action.left = this.motion.action.right = false;
                this.motion.action.jump = false;
            }
            this.playerUp = false;
            this.teleporter.update(this, collisions);
            this.motion.update(this, interval, collisions, environment);
        }
        else if(!this.leftCoin) {
            var c = new BouncingPickup(
                    new TripleCoin(this.box.x+this.box.width/2-TripleCoin.radius/2,
                                this.box.y+15), this.xVelocity, 0);
            c.register(collisions, manager, caches);
            this.leftCoin = true;
        }
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
        }
        drawFlashRect(context, this, "#f00");
    }
}
