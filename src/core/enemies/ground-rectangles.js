//Copyright Matthew Pottage 2019.
import {Collidable, registerCDU} from '../collisions/collidable.js';
import {HealthComponent, TeleportComponent} from '../components.js';
import {Scanner} from '../scanner.js';
import {Box, Pool, getClosest, drawFlashRect} from '../utility.js';
import {OrangeBullet, orangeBulletColor, orangeTransitionColor} from '../projectiles.js';
import {Player} from '../player/player.js';
import {ControlledMotion} from '../motion.js';
//A brown rectangle, doesn't jump, and charges at the player, with poor grip on
//the ground.  When it reaches 1 health, it self-inflicts a further 1 health
//damage after 3.5s of flashing orange, effectively killing itself.
export class BrownRectangle extends Collidable {
    constructor(x, y) {
        super();
        this.box = new Box(x, y, 75, 40);
        this.startPos = {x: x, y: y};
        this.bodyColor = "#88412E";
        this.flashColor = "orange";
        this.transitionColor = "#FF3600";
        this.teleporter = new TeleportComponent();
        this.motion = new BrownRectangleMotion();
        var scanWidth = 2*1200;
        this.scanner = new Scanner(new Box(0, 0, scanWidth, scanWidth),
                o => o instanceof Player);
        this.pushRank = 10;
        this.health = new HealthComponent(this, 2);
        this.health.setCallback(
            function(o) { o.disabled = true; o.scanner.disable(); o.flashing = .5; });
        //How long to stay around after dropping to 1 health (+0.5s).
        this.flashDuration = 4;
        this.startFlashing = 1; //Health that flashing should start on.
        this.reset();
    }
    register(collisions, manager) {
        registerCDU(this, collisions, manager);
        this.scanner.register(collisions, manager);
    }
    reset() {
        this.disabled = false;
        this.motion.reset();
        this.teleporter.reset();
        this.teleporter.setDefault(this.startPos.x, this.startPos.y);
        this.scanner.reset();
        this.activated = true;
        this.health.reset();
        this.box.x = this.startPos.x;
        this.box.y = this.startPos.y;
        this.target = null;
        this.retargetTime = 0; //Time until scanning for a new target.
        this.flashing = 0; //Time left until this dies (health.kill())
    }
    handleCollision(obj, details) {
        this.motion.handleCollision(this, obj, details);
        if(obj instanceof Player)
            obj.health.increase(-1);
    }
    update(interval, collisions, manager, caches, environment) {
        if(this.flashing>0)
            this.flashing -= interval;
        this.health.update(interval);
        if(!this.disabled) {
            if(this.health.value<=this.startFlashing && this.flashing==0) {
                this.flashing =
                    this.flashDuration*this.health.value/this.startFlashing;
            }
            this.scanner.recenter(this.box.x+this.box.width/2,
                    this.box.y+this.box.height/2);
            if(this.activated)
                this.updateAI(interval, collisions, manager);
            else
                this.motion.action.left = this.motion.action.right = false;
            this.teleporter.update(this, collisions);
            this.motion.update(this, interval, collisions, environment);
            if(this.health.value<=this.startFlashing && this.flashing<=.5)
                this.health.kill();
        }
    }
    updateAI(interval, collisions, manager) {
        this.retargetTime -= interval;
        if(this.retargetTime<=0 || !this.target) {
            this.target = getClosest(this.box, this.scanner.seen);
            this.retargetTime = 3;
        }
        this.motion.action.right = this.motion.action.left = false;
        if(this.target) {
            if(this.target.box.x+this.target.box.width<=this.box.x)
                this.motion.action.left = true;
            else if(this.target.box.x>=this.box.x+this.box.width)
                this.motion.action.right = true;
        }
    }
    draw(context) {
        if(!this.disabled) {
            context.fillStyle = this.bodyColor;
            this.box.fill(context);
        }
        if(this.flashing>0) {
            //Fade an overlayed orange box in and out every second.
            context.fillStyle = this.flashColor;
            var prevGA = context.globalAlpha;
            context.globalAlpha *= 1-2*Math.abs(this.flashing%1-.5);
            this.box.fill(context);
            context.globalAlpha = prevGA;
        }
        //Accomodate more than usual health
        else if(this.health.value>this.startFlashing) {
            drawFlashRect(context, this, this.transitionColor);
        }
    }
}

//Prevents forced acceleration being used to turn around, and gives a 70%
//acceleration boost without an increased max speed.
class BrownRectangleMotion extends ControlledMotion {
    constructor() {
        super(1, 0, .1, 1);
    }
    updateX(interval, environment) {
        //From FreeFallMotion.prototype.updateX().
        var fx = (this.movingFloor && this.movingFloor.motion.x) || 0;
        if((this.x<fx && this.accel.x>0)
            || (this.x>fx && this.accel.x<0)) {
            this.accel.x = 0;
        }
        else //70% acceleration boost
            this.accel.x *= 1.7;
        super.updateX(interval, environment);
    }
}

//Like BrownRectangle.
//Every 2.5s fires two slow moving OrangeBullets, one in the horizontally
//opposite direction to motion, the other vertically up.
export class OrangeRectangle extends BrownRectangle {
    constructor(x, y) {
        super(x, y);
        this.bodyColor = orangeBulletColor;
        this.flashColor = "red";
        this.transitionColor = orangeTransitionColor;
        var missiles = [];
        for(var i=0; i<10; ++i)
            missiles.push(new OrangeBullet());
        this.pool = new Pool(missiles);
        this.health = new HealthComponent(this, 3);
        this.flashDuration = 6;
        this.startFlashing = 2;
        this.launchDelay = 2.5;
    }
    reset() {
        super.reset();
        this.untilLaunch = this.launchDelay;
        if(this.pool)
            this.pool.apply(o => o.disable());
    }
    register(collisions, manager, caches) {
        super.register(collisions, manager, caches);
        this.pool.apply(o => o.register(collisions, manager, caches));
    }
    update(interval, collisions, manager, caches, environment) {
        super.update(interval, collisions, manager, caches, environment);
        if(!this.disabled && this.activated) {
            this.untilLaunch -= interval;
            if(this.untilLaunch<=0) {
                var vertical = this.pool.get();
                var center = this.box.center;
                vertical.respawn(center.x-vertical.box.width/2,
                    center.y-vertical.box.height/2,
                    0, -50);
                collisions.protect(vertical);
                var horizontal = this.pool.get();
                horizontal.respawn(center.x-horizontal.box.width/2,
                    center.y-horizontal.box.height/2,
                    -this.motion.x/Math.abs(this.motion.x)*50, 0);
                collisions.protect(horizontal);
                this.untilLaunch = this.launchDelay;
            }
        }
    }
}
