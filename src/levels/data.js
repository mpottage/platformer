import * as parse from './parse.js'
import {Level} from '../core/level.js';
import {Platform} from '../core/platforms.js';
import {MessageRegion} from '../core/regions.js';
import {Box} from '../core/utility.js';
//Make AJAX request and parse JSON received from a URL.
//Result is null if the URL fails to load or the text loaded is invalid JSON.
//(returns a Promise)
export function getJSON(url) {
    return new Promise(function(res, fail) {
        var req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.addEventListener("readystatechange", function(e) {
            if(req.readyState==XMLHttpRequest.DONE) {
                if(req.status==200) {
                    try {
                        res(JSON.parse(req.responseText));
                    }
                    catch(e) {
                        res(null);
                    }
                }
                else {
                    res(null);
                }
            }
        });
        req.send();
    });
}
function getFailureLevel(id) {
    return new Level(20000, 600, [
            new Platform(new Box(-1, -5000, 1, 15000)),
            new Platform(new Box(20000, -5000, 1, 15000)),
            new Platform(new Box(0, 600, 20000, 1), "transparent"),
            new MessageRegion(new Box(0, 0, 20000, 600),
                "LOAD FAILED ON \""+id+"\"")
    ]);
}
//Default directory to load levels files from. Uses value in
//localStorage["levels/data-dir"] if set (used to test different level setups)
export var dataDir = localStorage["levels/data-dir"] || "../levels/data";
export var listingFile = dataDir+"/listing.json";
//Load all levels from the default storage directory (normally "../levels/data")
export function loadLevels(statusCallback) {
    return loadLevelsCustom(listingFile, dataDir, statusCallback);
}
//Specific level storage directory and listing file (not normally needed)
export function loadLevelsCustom(listingFile, dataDir, statusCallback) {
    return getJSON(listingFile).then(function(listing) {
        if(listing instanceof Array) {
            if(dataDir.length>0)
                dataDir += "/";
            var res = {};
            var promises = [];
            var left = listing.length;
            for(let entry of listing) {
                if(entry["id"] && entry["source"]) {
                    var levelLoad = getJSON(dataDir+entry["source"]);
                    levelLoad.then((function(id, levelJSON) {
                        var level = null;
                        try {
                            level = parse.loadSavedObject(parse.updateFormat(levelJSON));
                        }
                        catch(e) {
                        }
                        if(!(level instanceof Level)) {
                            console.log("Loading level failed", entry);
                            level = getFailureLevel(id);
                        }
                        res[id] = level;
                        --left;
                        if(statusCallback)
                            statusCallback(listing.length-left, listing.length);
                    }).bind(null, entry["id"]));
                    promises.push(levelLoad);
                }
                else {
                    throw new Error("Bad entry in level listing "+
                        JSON.stringify(entry));
                }
            }
            return Promise.all(promises).then(function() {
                return res;
            });
        }
        else {
            throw new Error("Loading level listing failed \""+listingFile+"\"");
        }
    });
}
