//Copyright Matthew Pottage 2015-2019.
//Much better quality random values than those given by Math.random()
function getRandomValue() {
    return crypto.getRandomValues(new Uint32Array(1))[0]/4294967295;
}
//Get a random level to play next (optionally restrict levels selected to those
//in levelKeys, defaults to all levels).
//prevLevel: Will not return prevLevel as the result _unless_ there is only one
//possible level to select.
export function getRandomLevel(allLevels, prevLevel, levelKeys=Object.keys(allLevels)) {
    //Check there is more than one level available (avoiding an infinite loop).
    if(levelKeys.length>1) {
        //Randomly pick a different level to play next.
        var newLevel = prevLevel;
        while(newLevel==prevLevel) {
            newLevel = allLevels[
                levelKeys[Math.floor(getRandomValue()*levelKeys.length)]];
        }
        return newLevel;
    }
    //Only one choice.
    else if(levelKeys.length==1)
        return allLevels[levelKeys[0]];
    //No choices, use current level.
    else
        return prevLevel;
}

//Given a level, determine what its original name was.
export function recoverLevelName(allLevels, level) {
    for(var key in allLevels) {
        if(allLevels[key]===level)
            return key;
    }
    throw new Error("Unknown level");
}

//Get a random level, restricted to those with their name containing tag
export function getRandomTagged(allLevels, prevLevel, tag) {
    var pat = new RegExp(".*"+tag+".*");
    return getRandomLevel(allLevels, prevLevel, Object.keys(allLevels).filter(s => s.match(pat)));
}
