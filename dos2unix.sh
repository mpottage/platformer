#!/bin/sh

#Converts the line endings of all the files contained in tmp/package.zip to
# Unix line endings. Used for deployment from a cygwin bash shell on Windows

cd tmp
rm -rf scanning
mkdir scanning
cd scanning
unzip ../package.zip
find -type f -exec dos2unix {} \;
rm ../package.zip
zip ../package.zip -r *
