# Platform 2

A platformer game with an extensive level builder.

## Testing
To test the game (as stored locally) run, in the repository root:
```bash
python3 -m http.server 8080
```
And navigate to [localhost:8080](http://localhost:8080) in your web browser.

## Deployment
To minify all code, and create a zip containing all necessary files run:
```bash
./package.sh #Requires java
```
The resulting zip will be placed at [./tmp/package.zip](./tmp/package.zip).

To get the tools required to create the zip run:
```bash
./get_tools.sh #Requires npm, wget and unzip to download tools
```

## Levels
Level data files are found in [./levels](./levels).

## Code organisation

Code which doesn't depend on any specific HTML or CSS is found in
[./src](./src).

Any code that starts the game, and connects UI elements to the code in
[./src](./src) is found in the same directory as the relevant HTML. These
directories include [./single](./single), [./multiplayer](./multiplayer) and
[./builder](./builder).

## Browser support
The game has been tested in Mozilla Firefox and Google Chrome.

---
Copyright Matthew Pottage 2015-2019
